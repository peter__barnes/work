#include <stdio.h>
enum Color
{
    GREEN = 1,
    RED,
    BLUE,
    GREEN_RED = 10,
}ColorVal;
enum Color2
{
    GREEN_BLUE = 0x123456789012345
}ColorVal2;
void main()
{
    int arr[10];
    int a =sizeof(arr);
    printf("%d\t%d\n",a,(int)sizeof(&arr));
    a =sizeof(arr[10]);
    printf("%d\n",a);

    //enum's size can be varied and compiler dependent. It can be a bit,int,unsign int,long,long long or any other size!
    a =sizeof(ColorVal);
    printf("sizeof enum:%d\n",a); 
    a =sizeof(ColorVal2);
    printf("sizeof long enum:%d\n",a);
    return ;
}
