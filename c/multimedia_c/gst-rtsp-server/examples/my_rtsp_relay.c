#include <gst/gst.h>

#include <gst/rtsp-server/rtsp-server.h>

int
main (int argc, char *argv[])
{
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory;

  gst_init (&argc, &argv);

  if (argc < 2) {
	  /*
    g_print ("usage: %s <launch line> \n"
        "example: %s \"( videotestsrc ! x264enc ! rtph264pay name=pay0 pt=96 )\"\n",
        argv[0], argv[0]);
    return -1;
	*/
  }

  loop = g_main_loop_new (NULL, FALSE);

  /* create a server instance */
  server = gst_rtsp_server_new ();
  gst_rtsp_server_set_service (server, "8555");//配置服务器端口
  mounts = gst_rtsp_server_get_mount_points (server);


  factory = gst_rtsp_media_factory_new ();
  //gst_rtsp_media_factory_set_launch (factory, "( rtspsrc  location=rtsp://127.0.0.1:8554/test1 ! queue ! rtph264depay ! queue ! rtph264pay name=pay0 pt=96 )"
  //password auth and MP4 stream for HaiKang Carmer
  gst_rtsp_media_factory_set_launch (factory, "( rtspsrc  location=rtsp://admin:123456@172.16.11.122:554/MPEG-4/ch1/main/av_stream ! queue ! rtph264depay ! queue ! rtph264pay name=pay0 pt=96 )"
);//此处服务器的源来自主服务器的rtsp，ip地址改成相应的地址。
  gst_rtsp_media_factory_set_shared (factory, TRUE);

  gst_rtsp_mount_points_add_factory (mounts, "/test2", factory);

  g_object_unref (mounts);

  gst_rtsp_server_attach (server, NULL);

  /* start serving */
  g_print ("stream ready at rtsp://127.0.0.1:8555/test2\n");
  g_main_loop_run (loop);

  return 0;
}
