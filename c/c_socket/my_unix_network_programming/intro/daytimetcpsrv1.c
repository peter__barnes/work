#include	<time.h>
#define _GNU_SOURCE
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include	"unp.h"
int
main(int argc, char **argv)
{
	int					listenfd, listenfd2, connfd;
	socklen_t			len;
	struct sockaddr_in	servaddr, cliaddr;
	char				buff[MAXLINE];
	time_t				ticks;
    printf("sizeof(struct sockaddr_in):%ld\n",sizeof(struct sockaddr_in));
    printf("sizeof(struct sockaddr):%ld\n",sizeof(struct sockaddr));

	listenfd = Socket(AF_INET, SOCK_STREAM, 0);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); /*this can listen multi interface without knowing local ip*/
    /*inet_aton("127.0.0.1",&servaddr.sin_addr);*//*or : servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); */
	servaddr.sin_port        = htons(2013);	/* daytime server,port <1023 need superprivilege such as root,port like 2013 don't need */

	Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

	Listen(listenfd, LISTENQ);

	for ( ; ; ) {
		len = sizeof(cliaddr);
		connfd = Accept(listenfd, (SA *) &cliaddr, &len);
		printf("connection from %s, port %d\n",
			   Inet_ntop(AF_INET, &cliaddr.sin_addr, buff, sizeof(buff)),
			   ntohs(cliaddr.sin_port)); /*print client side port(not the server or client port,usually nearnest router random port) )*/
        printf("%s\n",buff);
        if(!strcmp(buff,"127.0.0.1"))
        {
            printf("it is localhost\n");
        }

        ticks = time(NULL);
        snprintf(buff, sizeof(buff), "%.24s\r\n", ctime(&ticks));
        Write(connfd, buff, strlen(buff));

		Close(connfd);
	}
}
