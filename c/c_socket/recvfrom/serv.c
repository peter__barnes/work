#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include <arpa/inet.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>

#define SERV_PORT 6000
#define MAX_BUFF_LEN 256
int
main(int argc, char **argv)
{
	int				n=0;
	int				sockfd;
	socklen_t		len = 0;
	char			buff[MAX_BUFF_LEN];
	struct sockaddr_in	servaddr;

	unsigned int recv_port = 6000; 

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(recv_port);
	//servaddr.sin_port        = htons(SERV_PORT);
	//!error handle
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd<0){
		printf("errof!\n");
		exit(1);
	}
	if (bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))<0) {
		perror("connect");
		exit(1);
	}

	printf("receiving!\n");
    n = recvfrom(sockfd, buff, MAX_BUFF_LEN, 0, (struct sockaddr*)&servaddr, &len);
	printf("received!\n");
	if(n<0)//!error handle
	{
		return 0;
	}
    buff[n] = 0;	
	//error handle
    fputs(buff, stdout);
	return 0;
}
