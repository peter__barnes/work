// HlsConvertCmd.cpp : 定义控制台应用程序的入口点。
//

//#include "stdafx.h"

extern "C"
{
#include "libavutil/avstring.h"
#include "libavutil/eval.h"
#include "libavutil/mathematics.h"
#include "libavutil/pixdesc.h"
#include "libavutil/imgutils.h"
#include "libavutil/dict.h"
#include "libavutil/parseutils.h"
#include "libavutil/samplefmt.h"
#include "libavutil/avassert.h"
#include "libavutil/time.h"
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
#include "libavcodec/avfft.h"
#include "libavcodec/avcodec.h"
#include "libswresample/swresample.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/avfiltergraph.h"
}

#include <string>

using namespace std;

#define TRACE printf

struct AudioParam
{
	int channels;
	int sample_rate;
	int audio_rate;
	AVSampleFormat eAudioFmt;
};

struct VideoParam
{
	int width;
	int height;
	int video_rate;
	int ticks_per_frame;
	AVPixelFormat eVideoFmt;
	AVRational videoFps;
	AVRational sample_sapect_ration;
	AVRational time_base;
};

class InFile{
	friend class FileChanger;
public:
	InFile(string strFileUrl);
	~InFile();
	int open();
	int getPacket(AVMediaType &eMediaType, AVPacket *pack);
	VideoParam getVideoParam();
private:
	string m_strFileUrl;
	AVFormatContext *m_pFormatCtx;
	AVStream *m_pAudioStream;
	AVStream *m_pVideoStream;
	VideoParam m_oVideoParam;
	bool m_bValid;
	bool m_bOver;
	int m_nVideoCnts;
};

class OutFile{
	friend class FileChanger;
public:
	OutFile(string strFileUrl);
	~OutFile();
	int open();
	int writePacket(AVPacket *pPacket, AVMediaType eMediaType);
	//int flush(AVMediaType eMedia);
	void setVideoParam(VideoParam videoParam);
private:
	string m_strFileUrl;
	AVFormatContext *m_pFormatCtx;
	AVStream *m_pAudioStream;
	AVStream *m_pVideoStream;
	AVCodecContext *m_pAudioCodecCtx;
	AVCodecContext *m_pVideoCodecCtx;
	VideoParam m_oVideoParam;
	bool m_bValid;
};

class FileChanger{
public:
	FileChanger(string strInFile, string strOutFile);
	~FileChanger();
	int init();
	int change();
private:
	//int writeYUV(AVFrame *pFrame);
	//int writePCM(AVFrame *pFrame, bool in);
private:
	InFile *m_pInFile;
	OutFile *m_pOutFile;
	bool m_bValid;
	FILE *m_pFile;
};

char *get_err(int err)
{
	static char errbuf[AV_ERROR_MAX_STRING_SIZE] = { 0 };
	av_make_error_string(errbuf, AV_ERROR_MAX_STRING_SIZE, err);
	return errbuf;
}

InFile::InFile(string strFileUrl)
: m_bOver(false), m_nVideoCnts(0)
{
	m_strFileUrl = strFileUrl;
	if (m_strFileUrl.empty())
	{
		m_bValid = false;
	}

	m_pFormatCtx = NULL;
	m_pAudioStream = NULL;
	m_pVideoStream = NULL;
	m_bValid = true;
}

InFile::~InFile()
{
	if (m_pFormatCtx)
	{
		avformat_close_input(&m_pFormatCtx);
	}
}

int InFile::open()
{
	if (!m_bValid)
	{
		return -1;
	}

	m_bOver = false;

	int ret = avformat_open_input(&m_pFormatCtx, m_strFileUrl.c_str(), NULL, NULL);
	if (ret < 0)
	{
		TRACE("open input %s failed %s.\n", m_strFileUrl.c_str(), get_err(ret));
		return -1;
	}

	ret = avformat_find_stream_info(m_pFormatCtx, NULL);
	if (ret < 0)
	{
		TRACE("find stream info failed.");
		return -1;
	}

	//av_dump_format(m_pFormatCtx, 0, m_strFileUrl.c_str(), 0);
	int audio_index = av_find_best_stream(m_pFormatCtx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);//duration 60059950 / 1000000 == 60S
	int video_index = av_find_best_stream(m_pFormatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);

	if (-1 < video_index)
	{
		m_pVideoStream = m_pFormatCtx->streams[video_index];//timebase:785647, codec(16384, 785647)
		AVCodecContext *pVideoCodecCtx = m_pVideoStream->codec;
		m_oVideoParam.width = pVideoCodecCtx->width;
		m_oVideoParam.height = pVideoCodecCtx->height;
		m_oVideoParam.eVideoFmt = pVideoCodecCtx->pix_fmt;
		m_oVideoParam.videoFps = pVideoCodecCtx->framerate;
		m_oVideoParam.sample_sapect_ration = pVideoCodecCtx->sample_aspect_ratio;
		m_oVideoParam.ticks_per_frame = pVideoCodecCtx->ticks_per_frame;
		m_oVideoParam.time_base = pVideoCodecCtx->time_base;
	}

	if (-1 < audio_index)
	{
		m_pAudioStream = m_pFormatCtx->streams[audio_index];
	}

	return 0;
}

VideoParam InFile::getVideoParam()
{
	return m_oVideoParam;
}

int InFile::getPacket(AVMediaType &eMediaType, AVPacket *pack)
{
	AVStream *pStream = NULL;

	av_init_packet(pack);
	int ret = av_read_frame(m_pFormatCtx, pack);
	if (ret < 0)
	{
		TRACE("Read frame End.");
		m_bOver = true;
		return -1;
	}

	if (m_pAudioStream)
	{
		if (pack->stream_index == m_pAudioStream->index)
		{
			eMediaType = AVMEDIA_TYPE_AUDIO;
		}
	}

	if (m_pVideoStream)
	{
		if (pack->stream_index == m_pVideoStream->index)
		{
			eMediaType = AVMEDIA_TYPE_VIDEO;
			//rescale pts
			m_nVideoCnts++;
			pack->pts = m_nVideoCnts;
			pack->dts = m_nVideoCnts;

			if (AV_PKT_FLAG_KEY == (pack->flags & AV_PKT_FLAG_KEY))
			{
				//save last key frame?
				
			}
		}
	}

	return 0;
}

OutFile::OutFile(string strFileUrl)
{
	m_strFileUrl = strFileUrl;
	if (m_strFileUrl.empty())
	{
		m_bValid = false;
	}

	m_pFormatCtx = NULL;
	m_pAudioStream = NULL;
	m_pVideoStream = NULL;
	m_bValid = true;
}

OutFile::~OutFile()
{
	if (m_pFormatCtx)
	{
		av_write_trailer(m_pFormatCtx);
	}

	if (m_pFormatCtx)
	{
		if (!(m_pFormatCtx->oformat->flags & AVFMT_NOFILE))
		{
			/* Close the output file. */
			avio_closep(&m_pFormatCtx->pb);
		}

		avformat_free_context(m_pFormatCtx);
	}
}

void OutFile::setVideoParam(VideoParam videoParam)
{
	m_oVideoParam = videoParam;
}

int OutFile::open()
{
	if (!m_bValid)
	{
		return -1;
	}

	int ret = avformat_alloc_output_context2(&m_pFormatCtx, NULL, NULL, m_strFileUrl.c_str());
	if (ret < 0)
	{
		TRACE("Alloc pOutFormatCtx failed.");
		return -1;
	}

	AVOutputFormat *pOFmt = m_pFormatCtx->oformat;

	/* add audio stream */
	if (AV_CODEC_ID_NONE != pOFmt->audio_codec)
	{
		AVCodec *pCodec = NULL;
		pCodec = avcodec_find_encoder(pOFmt->audio_codec);
		if (NULL == pCodec)
		{
			TRACE("Find audio encoder failed.");
			return -1;
		}

		m_pAudioStream = avformat_new_stream(m_pFormatCtx, pCodec);
		if (NULL == m_pAudioStream)
		{
			TRACE("New audio stream failed.");
			return -1;
		}

		m_pAudioStream->id = m_pFormatCtx->nb_streams - 1;

		//must set codec param, else will open file failed
		m_pAudioCodecCtx = avcodec_alloc_context3(pCodec);
		if (!m_pAudioCodecCtx)
		{
			TRACE("Alloc pOutAudioCodecCtx failed.");
			return -1;
		}

		/* need check whether surpport */
		//m_pAudioCodecCtx->sample_fmt = m_audioParam.eAudioFmt;
		m_pAudioCodecCtx->sample_fmt = pCodec->sample_fmts[0];
		m_pAudioCodecCtx->channels = 2;
		m_pAudioCodecCtx->channel_layout = av_get_default_channel_layout(m_pAudioCodecCtx->channels);
		m_pAudioCodecCtx->sample_rate = 44100;
		m_pAudioCodecCtx->bit_rate = 64000;//64kb
		//m_pAudioCodecCtx->time_base = {1, m_pAudioCodecCtx->sample_rate};
		m_pAudioStream->time_base = { 1, m_pAudioCodecCtx->sample_rate };

		if (pOFmt->flags & AVFMT_GLOBALHEADER)
		{
			m_pAudioCodecCtx->flags |= CODEC_FLAG_GLOBAL_HEADER;
		}

		/*
		ret = avcodec_parameters_from_context(m_pAudioStream->codec, m_pAudioCodecCtx);//copy after open
		if (ret < 0)
		{
			TRACE("Could not copy the stream parameters\n");
		}
		*/
	}

	if (AV_CODEC_ID_NONE != pOFmt->video_codec)
	{
		AVCodec *pCodec = NULL;
		pCodec = avcodec_find_encoder(pOFmt->video_codec);
		if (NULL == pCodec)
		{
			TRACE("Find encoder failed.");
			return -1;
		}

		m_pVideoStream = avformat_new_stream(m_pFormatCtx, pCodec);
		if (NULL == m_pVideoStream)
		{
			TRACE("New video stream failed.");
			return -1;
		}

		m_pVideoStream->id = m_pFormatCtx->nb_streams - 1;

		m_pVideoCodecCtx = avcodec_alloc_context3(pCodec);
		if (!m_pVideoCodecCtx)
		{
			TRACE("Alloc pOutVideoCodecCtx failed.");
			return -1;
		}

		m_pVideoCodecCtx->width = m_oVideoParam.width;
		m_pVideoCodecCtx->height = m_oVideoParam.height;
		/* need check whether surpport */
		m_pVideoCodecCtx->pix_fmt = m_oVideoParam.eVideoFmt;
		m_pVideoCodecCtx->framerate = m_oVideoParam.videoFps;
		//m_pVideoCodecCtx->time_base = m_oVideoParam.time_base;//maybe mutipied by ticks_per_frame
		m_pVideoCodecCtx->ticks_per_frame = m_oVideoParam.ticks_per_frame;
		m_pVideoCodecCtx->time_base = { m_oVideoParam.videoFps.den, m_oVideoParam.videoFps.num };
		//m_pVideoStream->time_base = { 1, 25 };
		m_pVideoCodecCtx->sample_aspect_ratio = m_oVideoParam.sample_sapect_ration;
		m_pVideoCodecCtx->gop_size = 1;
		m_pVideoStream->time_base = { m_oVideoParam.videoFps.den, m_oVideoParam.videoFps.num };

		if (pOFmt->flags & AVFMT_GLOBALHEADER)
		{
			m_pVideoCodecCtx->flags |= CODEC_FLAG_GLOBAL_HEADER;
		}

		AVDictionary *param = 0;
		//H.264  
		if (m_pVideoCodecCtx->codec_id == AV_CODEC_ID_H264)
		{
			av_dict_set(&param, "preset", "slow", 0);
			av_dict_set(&param, "tune", "zerolatency", 0);
		}

		//H.265  
		if (m_pVideoCodecCtx->codec_id == AV_CODEC_ID_H265){
			av_dict_set(&param, "preset", "ultrafast", 0);
			av_dict_set(&param, "tune", "zero-latency", 0);
		}

		ret = avcodec_open2(m_pVideoCodecCtx, pCodec, &param);
		if (ret < 0)
		{
			TRACE("Open pOutVideoCodecCtx failed, %s.\n", get_err(ret));
			return -1;
		}

		av_dict_free(&param);

		/*
		ret = avcodec_parameters_from_context(m_pVideoStream->codec, m_pVideoCodecCtx);
		if (ret < 0)
		{
			TRACE("Could not copy the stream parameters\n");
		}
		*/

		//m_pVideoStream->codec = avcodec_alloc_context3(m_pVideoCodecCtx);

	}

	/* open output io, write header */
	if (!(m_pFormatCtx->oformat->flags & AVFMT_NOFILE))//65537 & 1
	{
		int ret = avio_open(&m_pFormatCtx->pb, m_strFileUrl.c_str(), AVIO_FLAG_WRITE);
		if (ret < 0)
		{
			TRACE("Open output file failed.");
			return -1;
		}
	}

	av_dump_format(m_pFormatCtx, 0, m_strFileUrl.c_str(), 1);

	//
	AVDictionary *opts = NULL;
	if (0 == strcmp(m_pFormatCtx->oformat->name, "hls"))
	{
		av_dict_set(&opts, "hls_time", "1", 0);//no used, I frame interval is 3~5s
		//av_dict_set(&opts, "", "", 0);
		//av_dict_set(&opts, "hls_list_size", "1", 0);
	}

	/* Write the stream header, if any. */
	ret = avformat_write_header(m_pFormatCtx, &opts);
	
	av_dict_free(&opts);

	if (ret < 0)
	{
		char *cerr = get_err(ret);
		TRACE("write header failed, %s.", cerr);
		return -1;
	}

	return ret;
}

int OutFile::writePacket(AVPacket *pack, AVMediaType eMediaType)
{
	AVStream *pStream = NULL;
	AVCodecContext *pCodecCtx = NULL;

	if (AVMEDIA_TYPE_AUDIO == eMediaType)
	{
		pStream = m_pAudioStream;
	}

	if (AVMEDIA_TYPE_VIDEO == eMediaType)
	{
		pStream = m_pVideoStream;
	}

	pack->stream_index = pStream->index;
	av_packet_rescale_ts(pack, m_pVideoCodecCtx->time_base, pStream->time_base);

	int ret = av_interleaved_write_frame(m_pFormatCtx, pack);

	return ret;
}

FileChanger::FileChanger(string strInFile, string strOutFile)
{
	m_pInFile = new InFile(strInFile);
	if (NULL == m_pInFile)
	{
		m_bValid = false;
		return;
	}

	m_pOutFile = new OutFile(strOutFile);
	if (NULL == m_pOutFile)
	{
		m_bValid = false;
		delete m_pInFile, m_pInFile = NULL;
		return;
	}

	m_bValid = true;
	m_pFile = NULL;
}

FileChanger::~FileChanger()
{
	if (m_pInFile)
		delete m_pInFile;

	if (m_pOutFile)
		delete m_pOutFile;

	if (m_pFile)
	{
		fclose(m_pFile);
	}
}

int FileChanger::init()
{
	if (!m_bValid)
	{
		TRACE("Not valid.");
		return -1;
	}

	/* open input */
	int ret = 0;
	ret = m_pInFile->open();
	if (0 != ret)
	{
		TRACE("Open input file failed.");
		return -1;
	}

#if 1
	//AudioParam audioParam = m_pInFile->getAudioParam();
	VideoParam videoParam = m_pInFile->getVideoParam();

	//m_pOutFile->setAudioParam(audioParam);
	m_pOutFile->setVideoParam(videoParam);
#endif

	ret = m_pOutFile->open();
	if (0 != ret)
	{
		TRACE("...\n");
		return -1;
	}

	return 0;
}

int FileChanger::change()
{
	AVPacket pack;
	AVMediaType eMediaType = AVMEDIA_TYPE_UNKNOWN;
	int cnt = 0, ret = 0;

	do
	{
#if 0
		if (500 < cnt)
		{
			break;
		}
#endif

		/* get frame */
		ret = m_pInFile->getPacket(eMediaType, &pack);

		/* eof, break */
		if (0 != ret)
		{
			if (m_pInFile->m_bOver)
			{
				break;
			}

			continue;
		}

		if (AVMEDIA_TYPE_VIDEO == eMediaType)
		{
			cnt++;
		}

		m_pOutFile->writePacket(&pack, eMediaType);

	} while (1);

	printf("Cnt %d.\n", cnt);

	return 0;
}


int handle_file(const char * input_url, const char * output_url)
{
	av_register_all();

	FileChanger *pFileChanger = new FileChanger(input_url, output_url);

	if (NULL != pFileChanger)
	{
		if (0 != pFileChanger->init())
		{
			goto error;
		}

		pFileChanger->change();
	}

error:
	if (NULL != pFileChanger)
		delete pFileChanger;

	return 0;
}

int main(int argc, char* argv[])
{
//	handle_file("..\\file\\carmer_101.h264", "..\\file\\ts\\1.m3u8");
	handle_file("./video/test.h264", ".frag/a.m3u8");
	return 0;
}

