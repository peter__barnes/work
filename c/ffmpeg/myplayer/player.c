//#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
//#include <libswscale/swscale.h>

/*
#include "include/libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
*/
int main(int argc, char *argv[]) 
{
    AVFormatContext *pFormatCtx = NULL;
    av_register_all();
    // Open video file
    if(avformat_open_input(&pFormatCtx, argv[1], NULL, NULL)!=0)
    {
        printf("error:can't open file!\n");
        return -1;
    }
    if(avformat_find_stream_info(pFormatCtx, NULL)<0)
    {
        printf("error:can't find stream information!\n");
        return -1; // Couldn't find stream information
    }
    return 0;
}
