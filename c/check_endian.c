#include<stdio.h>
#include<string.h>
int little_endian( )
{
    union check
    {
        int i;
        char ch;
    } c;
    c.i = 1;
    return (c.ch ==1);
}
int main()
{
    printf("is little endian? return :%d\n",little_endian());
    //another way
    int x=0x12345678; /* 305419896 */  
    unsigned char *p=(char *)&x;  
    printf("%0x % 0x %0x %0x",p[0],p[1],p[2],p[3]);  
    return 0;
}
