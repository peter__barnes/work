#include <stdio.h>
int is_multiple_of(int n, int k) {
      // implement this function
      //     return 0;
      //
      //
    while(k > n)
    {
        if(k & 1)
        {
            return 0;
        }
        else
        {
            k = k>>1;
        }
    }
    if(k==0 || k == n)
    {
        return 1;
    }
    return 0;
}

int main()
{
    int i; 
    for(i=0;i<12;i++)
    {
        printf("%d ",is_multiple_of(4,i));
    }
    printf("\n");
    return 0;
}
