#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#define MAX 10000
int main()
{
    FILE *file;
    FILE *file_out;
    int i;
    char *full_name ="test.txt";
    int pid;
    int ret;

    if(0 == (pid = fork()))
    {
        if (NULL == (file = fopen(full_name, "w")))
        {
            return 0;
        }
        else
        {
            for(i=0;i<10;i++)
            {
                ret = fprintf(file,"hello world!%d\n",(i*2+1));
                printf("pid=%d,ret=%d\n",pid,ret);
                sleep(1);
            }
            fclose(file);
        }
    }
    else if(pid > 0 )
    {
        if (NULL == (file = fopen(full_name, "w")))
        {
            return 0;
        }
        else
        {
            for(i=0;i<10;i++)
            {
                ret = fprintf(file,"hello world!%d\n",i*2);
                printf("pid=%d,ret=%d\n",pid,ret);
                sleep(1);
            }
            fclose(file);
        }
    }
    else
    {
        printf("fork error!\n");
    }
    return 0;
}
