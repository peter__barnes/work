#include <stdio.h>
char outbuf[50];
int main()
{
    /* link stdout to outbuf */
    setbuf(stdout,outbuf);
    /* puts to stdout*/
    puts("This is a test of buffered output.");
    puts("This output will go into outbuf");
    puts("and won't appear until the buffer");
    puts("fills up or we flush the stream.\n");
    /* puts outbuf,this will make outbuf to screen and outbuf duplicated to 2 times of before*/
    puts(outbuf);
    fflush(stdout);/*flush stdout won't clean outbuf!*/
    puts(outbuf);
    /* unlink stdout with outbuf */
    setbuf(stdout,NULL);
    return 0;

}
