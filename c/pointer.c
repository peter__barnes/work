#include <stdio.h>

int main()
{
    int a[5]={1,2,3,4,5};
    int *ptr=(int *)(&a+1);//&a+1:move behind 1*sizeof(a) from,then ptr will point to a[5](which is not initialized)
    printf("*(a+1):%d,*(ptr-1):%d\n",*(a+1),*(ptr-1));
    printf("%p,%p\n",a,&a);

    int *ptr1=(int *)(&a+1);//5
    printf("%x\n",ptr1[-1]);
    return 0;
}
