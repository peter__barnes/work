#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//replace 192.168.0.256 to -> 192.168.0.91
int main()
{
	char body[2048]={0};
	char out[2048]={0};
	snprintf(body, 2048,	
		"v=0\n"
		"o=34020000002000000001 0 0 IN IP4 192.168.0.256\n"
		"s=Play\n"
		"c=IN IP4 192.168.0.256\n"
		"t=0 0\n"
		"m=video 6000 RTP/AVP 96 98 97\n"
		"a=recvonly\n"
		"a=rtpmap:96 PS/90000\n"
		"a=rtpmap:98 H264/90000\n"
		"a=rtpmap:97 MPEG4/90000\n"
		"y=0100000001\n"
		"f=\n");

	char *find = NULL;
	char *last = NULL;
	char *begin_mark = "IN IP4 ";
	char *end_mark  = "\n";
	char *replacement = "192.168.0.91";
	for(last = body; (find = strstr(last,begin_mark)) != NULL;)
	{
		strncat(out, last, find - last + strlen(begin_mark));//replace 7 to "IN IP4"
		strcat(out, replacement);
		last = strstr(find, end_mark);
	}
	if(last!=NULL)
	{
		strcat(out, last);
	}
	printf("%s\n",out);
}
