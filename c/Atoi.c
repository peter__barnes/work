 #include <stdio.h>
 #include <stdlib.h>
//strtol is safer than atoi
int Atoi(const char *nptr)
{
	return (int)strtol(nptr, (char **)NULL, 10); 
}

int main()
{
	printf("%d\n",Atoi("a"));
}
