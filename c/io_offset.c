#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define BUFFSIZE 4096
/*test result : 1. input and output to the same fd,share 1 offset, read and write should be r+ not rw */
int main()
{
    int n;
    char buf[BUFFSIZE];
    FILE *file;
    char *full_name ="2.txt";
    if ((file = fopen(full_name, "r+")) == NULL )
    {
            perror("open error");
            exit(1);
    }

    if(NULL==fgets(buf,BUFFSIZE,file)) /*fgets return NULL when meet EOF*/
    {
        perror("read error");
    }
    printf("get:%s\n",buf);
    if(fputs("aaaaa",file)<0)
    {
        perror("write error");
    }
    if(NULL==fgets(buf,BUFFSIZE,file)) /*fgets return NULL when meet EOF*/
    {
        perror("read error");
    }
    printf("get:%s\n",buf);
    return 0;
}
