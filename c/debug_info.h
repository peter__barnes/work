#ifndef DEBUG_INFO_H
#define DEBUG_INFO_H

#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <stdarg.h>
#include  <time.h>
#define DEBUG_FORMAT   "%-20s:%-20s [%05d]:"
#define DEBUG_INFO    __FILE__,__func__,__LINE__

#define Perror(s)		{\
							printf(DEBUG_FORMAT,DEBUG_INFO);\
							char tmBuf[32];  \
							time_t t = time(0);  \
							strftime(tmBuf, sizeof(tmBuf), "%d,%H:%M:%S", localtime(&t));\
							printf("%s.",tmBuf);\
							fflush(stdout);\
							perror(s);\
						}
#define Printf(...)     {\
							printf(DEBUG_FORMAT,DEBUG_INFO);\
							char tmBuf[32];  \
							time_t t = time(0);  \
							strftime(tmBuf, sizeof(tmBuf), "%d,%H:%M:%S", localtime(&t));\
							printf("%s.",tmBuf);\
							fflush(stdout);\
							printf(__VA_ARGS__);\
						}
#define LEV_ERR 4
#define LEV_WAR 3
#define LEV_INF 2
#define LEV_DEB 1

#ifndef PRINT_LEV 
#define PRINT_LEV 1
#endif

#define Printf_lev(lev,...)     {\
									if (PRINT_LEV <= lev) \
									{\
										printf("[lev%d]",lev);\
										Printf(__VA_ARGS__);\
									}\
								}
#define Perror_lev(s)			{\
									if (PRINT_LEV <= LEV_ERR) \
									{\
										printf("[lev%d]",LEV_ERR);\
										Perror(s);\
									}\
								}

#endif
