#include <sys/queue.h>
#include <stdlib.h>
#include <stdio.h>
int main()
{
    /*create and init head*/
    LIST_HEAD(listhead, entry) head;
    LIST_INIT(&head);                       /* Initialize the list. */
    /*define entry*/
    struct entry {
        //...
        LIST_ENTRY(entry) entries;          /* List. */
        //...
    } *n1, *n2, *np;
    /*create and insert entry*/
    n1 = malloc(sizeof(struct entry));      /* Insert at the head. */
    if(n1 ==NULL)
    {
        printf("malloc failed!\n");
        exit(1);
    }
    LIST_INSERT_HEAD(&head, n1, entries);
    n2 = malloc(sizeof(struct entry));      /* Insert after. */
    if(n2 ==NULL)
    {
        printf("malloc failed!\n");
        exit(1);
    }
    LIST_INSERT_AFTER(n1, n2, entries);
    /* Forward traversal. lh_first is first , le_next is pointer to next*/
    for (np = head.lh_first; np != NULL; np = np->entries.le_next)
    {
        //np-> ...
    }
    while (head.lh_first != NULL)           /* Delete. */
    {
        LIST_REMOVE(head.lh_first, entries);
    }
    return 0;
}
