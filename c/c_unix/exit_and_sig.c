#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
void *pexit()
{
    printf("here is pexit\n");
}
void *pexit2()
{
    printf("here is pexit2\n");
    system("date");
}
void sig_alarm(int signo)
{
    printf("here is alarm\n");
}
int main()
{
    int i;
    void *fun=NULL;
    sigset_t *set = malloc(sizeof(sigset_t));
    if(set==NULL)
    {
        printf("malloc failed!\n");
        exit(1);
    }
    sigfillset(set);
    /*
    sigemptyset(set);
    sigaddset(set,SIGALRM);
    */
    sigprocmask(SIG_BLOCK,set,NULL);
    signal(SIGALRM,sig_alarm);
    alarm(3);
    for(i=0;i<5;i++)
    {
        sleep(1);
        printf("time:%d\n",i);
    }
    fun = pexit();
    atexit(fun);
    fun = pexit2();
    atexit(fun);
    exit(0);
}
