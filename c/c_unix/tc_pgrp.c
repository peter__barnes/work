#include <stdio.h>  
#include <unistd.h>  
#include <signal.h>  
#include <termios.h>  
#include <fcntl.h>  
//run in shell:    ./a.out &
//at beginning, running in background, tcsetpgrp change it to foreground
static void judge(void){  
    pid_t pid;  
    pid = tcgetpgrp(STDIN_FILENO);  //get shell foreground process-group id
    if(pid == -1){  
        perror("tcgetpgrp");  
        return;  
    }else if(pid == getpgrp()){  //current process-group id == shell foreground id
        printf("foreground\n");  
    }else{  
        printf("background\n");  
    }  
}  

int main(void){  
    printf("tcgetsid:%d,pgrp=%d,sid=%d\n",tcgetsid(STDIN_FILENO),getpgrp(),getsid(getpid()));  
    signal(SIGTTOU,SIG_IGN);  
    judge();  
    int result;  
    result = tcsetpgrp(STDIN_FILENO,getpgrp());  //set foreground
    if(result == -1){  
        perror("tcsetpgrp");  
        return -1;  
    }  
    judge();  
    return 0;  
}  
