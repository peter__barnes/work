#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <wordexp.h>

//Return 1 for match, 0 for no match.
int match(const char *string, char *pattern)
{
    int    status;
    regex_t    re;


    if (regcomp(&re, pattern, REG_EXTENDED|REG_NOSUB) != 0) {
        return(0);      /* Report error. */
    }
    status = regexec(&re, string, (size_t) 0, NULL, 0);
    regfree(&re); /*must free resource or may cause memleak*/
    if (status != 0) {
        return(0);      /* Report error. */
    }
    return(1);
}
int main()
{
    printf("%d\n",match("hello.c","*.c"));

    regex_t    re;
    char s[100]="hello.c hello2.cpp";
    if (regcomp(&re, "*.c", 0) != 0) {
        return(0);      /* Report error. */
    }
    regexec(&re, s, (size_t) 0, NULL, 0);
    regfree(&re); /*must free resource or may cause memleak*/
    printf("%s\n",s);

    wordexp_t *p2;
    wordexp("ls -l *.c", p2, 0);

}
