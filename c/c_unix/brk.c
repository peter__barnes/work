//使用sbrk(0)来返回程式当前使用了多少内存。
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{  
	int start,end;  
	start = sbrk(0);  
	malloc(1024);
	end = sbrk(0);  
	printf("I used %d vmemory\n",end - start);  
	return 0;
}  
