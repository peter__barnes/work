/*
it is 明德扫码枪usb版，将/dev/input/event8 替换成正确的设备号，可按照仿usb键盘模式读取扫码枪数据
另外，请使用root权限，否则打开设备文件描述符很可能会失败
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include<string.h>
#include<stdbool.h>
#include<unistd.h>
#include<errno.h>
#include <fcntl.h>
//#include </workdir/kern/linux-2.6.20/include/linux/input.h>

#define LOOP_PER 300000
#define MAX_BUFFER_SIZE 4096

/*
return value:
	-1 error (include timeout)
	-2 EOF 
	0+ the number of bytes read
*/	
/* Read "n" bytes from a descriptor. */
ssize_t	readn(int fd, void *vptr, size_t n,bool less)
{
	size_t	nleft;
	ssize_t	nread;
	char	*ptr;
	int timeout_cnt = 0;
	errno =0;

	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ( (nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR)
			{
				nread = 0;		/* and call read() again */
			}
			else if(errno == EAGAIN && timeout_cnt <5) 
			{
				timeout_cnt++;
				usleep(30000);
				errno=0;
				continue;/*data not enough,not timeout yet*/
			}
			else if(errno == EAGAIN && true == less && timeout_cnt >=3)
			{
				break;/*return the number of data readed*/
			}
			else
			{
				return(-1);/*error , or timeout can't read enough data*/
			}
		} 
		else if (nread == 0)
		{
			return (-2);				/* EOF */
		}
		nleft -= nread;
		ptr   += nread;
	}
	return(n - nleft);		/* return >= 0 */
}

ssize_t Readn(int fd, void *ptr, size_t nbytes)
{
	ssize_t		n;

	if ( (n = readn(fd, ptr, nbytes,false)) < 0)
	{
		printf("readn error:%s\n",strerror(errno)); 
	}
	return(n);
}

ssize_t Readn_or_less(int fd, void *ptr, size_t nbytes)
{
	ssize_t		n;
	if ( (n = readn(fd, ptr, nbytes, true)) < 0)
	{
		printf("readn error:%s\n",strerror(errno)); 
	}
	return(n);
}
void comm_scan(int scan_fd)
{
	char recv_buff[MAX_BUFFER_SIZE-10];
	//char send_buff[MAX_BUFFER_SIZE];
	while(1)
	{
		usleep(LOOP_PER);
		int n=Readn_or_less(scan_fd, recv_buff, sizeof(recv_buff));
		if(0 < n && n< sizeof(recv_buff))
		{
			usleep(LOOP_PER);
			n+=Readn_or_less(scan_fd, &recv_buff[n], sizeof(recv_buff)-n); //read fd delay data

			printf("recving scanner data:\n");
			int i;
			for(i=0;i<n;i++)
			{
				printf("%c ",recv_buff[i]);
			}
			/*
			log_ch(L_INF,recv_buff,n);
			log_hex(L_INF,recv_buff,n);
			log_dec(L_INF,recv_buff,n);
			*/

			/*
			if(n+1>(int)sizeof(cfg->qr_code))
			{
				log_msg(L_ERR,"qr_data too large!");
			}
			else
			{
				pthread_mutex_lock(&cfg_mutex);
				cfg->qr_scan_time = time(NULL);
				strncpy(cfg->qr_code,recv_buff,n);

				if(n>2 && 0x0d==cfg->qr_code[n-2] && 0x0a==cfg->qr_code[n-1])
				{
					cfg->qr_code[n-2]='\0';
				}
				cfg->qr_code[n]='\0';
				pthread_mutex_unlock(&cfg_mutex);
			}
			*/
		}
	}
}

int main(int argc, char **argv)
{
	int fd;
	int a;
	int i;
	int flag11;
	unsigned char strOutput[8];

	fd=open("/dev/input/event8",O_RDWR);
	if(fd<0)
	{
		printf ("open keyboard err:%d\n",fd);
	}
	else
	{
		printf ("open keyboard success\n");
	}
	comm_scan(fd);

	close(fd);
	return 0;
}
