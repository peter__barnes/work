#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

/* add NONBLOCK flag*/
int main() 
{
	int	val;
    int fd;
    int flags=O_NONBLOCK;

    if(fd=open("./tmp.txt",O_CREAT)<0)
    {
		perror("open error");
        exit(1);
    }
	if ((val = fcntl(fd, F_GETFL, 0)) < 0)
    {
		perror("fcntl F_GETFL error");
        exit(1);
    }

	val |= flags;		/* turn on flags */

	if (fcntl(fd, F_SETFL, val) < 0)
    {
		perror("fcntl F_SETFL error");
        exit(1);
    }
    printf("set nonblock success\n");
    /*..then nonblock read/write.....*/
    return 0;
}
