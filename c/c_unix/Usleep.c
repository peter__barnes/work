#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<time.h>

/*replace usleep system call, it is a safter version not interrupted by alarm*/
static int Usleep(useconds_t usec);
static inline int Usleep(useconds_t usec)
{
	struct timespec tm;
	tm.tv_sec  = usec / 1000000;
	tm.tv_nsec = (long)(usec % 1000000) * 1000;
	return nanosleep(&tm,NULL);
}

int main()
{
	while(1)
	{
		Usleep(1500000);

		/*print current time*/
		char tmBuf[32];  
		time_t t = time(0);  
		strftime(tmBuf, sizeof(tmBuf), "%d,%H:%M:%S", localtime(&t));
		printf("%s.\n",tmBuf);
	}
	return 0;
}
