#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <errno.h>

//read will return 0 or -1 when no response packet found
int anetKeepAlive(int fd, int interval)  
{  
    int val = 1;  
    //开启keepalive机制  
    if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &val, sizeof(val)) == -1)  
    {  
        printf("setsockopt SO_KEEPALIVE: %s", strerror(errno));  
        return -1;  
    }  
  
    /* Default settings are more or less garbage, with the keepalive time 
     * set to 7200 by default on Linux. Modify settings to make the feature 
     * actually useful. */  
  
    /* Send first probe after interval. */  
    val = interval;  
    if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE, &val, sizeof(val)) < 0) {  
        printf( "setsockopt TCP_KEEPIDLE: %s\n", strerror(errno));  
        return -1;  
    }  
  
    /* Send next probes after the specified interval. Note that we set the 
     * delay as interval / 3, as we send three probes before detecting 
     * an error (see the next setsockopt call). */  
    val = interval/3;  
    if (val == 0) val = 1;  
    if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL, &val, sizeof(val)) < 0) {  
        printf("setsockopt TCP_KEEPINTVL: %s\n", strerror(errno));  
        return -1;  
    }  
  
    /* Consider the socket in error state after three we send three ACK 
     * probes without getting a reply. */  
    val = 3;  
    if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPCNT, &val, sizeof(val)) < 0) {  
        printf("setsockopt TCP_KEEPCNT: %s\n", strerror(errno));  
        return -1;  
    }  
    return 0;  
}  
