#include<stdio.h>
#include<stdlib.h>
#include<sched.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/time.h>
#include<sys/resource.h>
int main()
{
//use nice() and setpriority() change priority, do not use sched_setscheduler() change priority which is often failed.
//use sched_setscheduler() only if want to change scheduler(such as realtime FIFO), this often need to run by root/superprivilige.
    struct sched_param sp;
    int prior ;
    int sche;
    sp.sched_priority= sched_get_priority_max(SCHED_FIFO);
    printf("%d\n",sched_get_priority_max(SCHED_FIFO));
    printf("nice(4)\n");
    nice(4); //priority value +4
    sche = sched_getscheduler(getpid());
    printf("scheduler:%d\n",sche );
    prior = getpriority(PRIO_PROCESS, getpid());
    printf("prior:%d\n",prior);
    setpriority(PRIO_PROCESS, getpid(),6); //priority value =6
    prior = getpriority(PRIO_PROCESS, getpid());
    printf("prior:%d\n\n",prior);

    printf("set sched_priority=5\n");
    printf("%d\n",sched_setscheduler(getpid(),SCHED_FIFO,&sp)); //failed:print -1 ; success:print 0
    sche = sched_getscheduler(getpid());
    printf("scheduler:%d\n",sche );
    prior = getpriority(PRIO_PROCESS, getpid());
    printf("prior:%d\n",prior);
//    sched_setscheduler(pid_t pid, int policy, const struct sched_param *param);
    return 0;
}
