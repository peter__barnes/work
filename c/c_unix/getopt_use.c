#include<unistd.h>
#include<stdio.h>
/*
test command :
    ./a.out -a12345
    ./a.out -bb
    ./a.out -c
    ./a.out -c1   //wrong, "-c" should not followed with optarg "1"
    ./a.out -d
    ./a.out -f    //wrong,"a:b:cde" do not have a valied argv "-f"
*/
int main(int argc,char*argv[])
{
    int ch;
    while((ch=getopt(argc,argv,"a:b:cde"))!= -1)  //"a:" "b:" means "-a" "-b" must followed with optarg
    {
        switch(ch)
        {
            case 'a':
                printf("option a:'%s'\n",optarg); 
                break;
            case 'b':
                printf("option b:'%s'\n",optarg); 
                break;
            case 'c':
                printf("option c:\n"); 
                break;
            default:
                printf("other option\n");
                break;
        }
    }
    return 0;
}
