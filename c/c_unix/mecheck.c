/* run following command ,use mtrace check mem error in this code(Memory not freed):
$ gcc  mtrace() -o test
$ ./test
$ export MALLOC_TRACE=trace.log
$ mtrace test trace.log
*/
#include <mcheck.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char** argv)
{
	mtrace();
	char * buf = (char*) malloc(1024);
	// free(buf);
	return 0;
}
