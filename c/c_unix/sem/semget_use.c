#include<stdio.h>
#include<unistd.h>
#include<sys/sem.h>

int main()
{
    int semid;
    struct sembuf sem_b;  
    semid=semget(ftok("1.sem",3998),1,IPC_CREAT|0666);/*create semaphore ,which can be check by shell command "ipcs -s" */
    if (semctl(semid, 0, SETVAL, 0) == -1)  /*init semaphore*/
    {  
        perror("Initialize semaphore");       
        return -1;  
    }  
    sleep(10);/*semaphore will be reset when all process using semaphore quit*/
    sem_b.sem_num = 0; 
    sem_b.sem_op = 1; /* operation add 1*/   
    sem_b.sem_flg = SEM_UNDO;   
    if (semop(semid, &sem_b, 1) == -1)    /*semaphore value add 1 by sem_b.sem_op */
    {  
        perror("P operation");  
        return -1;  
    }  
    printf("V operation finished\n");
    return 0;
}
