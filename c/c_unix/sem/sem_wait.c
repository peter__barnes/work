#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define SEM_A_NAME "a_tmp"
int main()
{
    int val;
    sem_t *sem_a;
    /*If O_CREAT is specified, and a semaphore with the given name already exists, then mode and value are ignored.*/
    sem_a = sem_open(SEM_A_NAME , O_CREAT, 0644, 1);
    if (sem_a == SEM_FAILED )
    {
        perror("unable to create semaphore");
        sem_unlink(SEM_A_NAME);
        exit(-1);
    }

    sem_getvalue(sem_a, &val);
    printf("1.The sem value is:%d\n", val);
    sem_wait(sem_a);
    sem_getvalue(sem_a, &val);
    printf("2.The sem value is:%d\n", val);
    sleep(20);
    sem_getvalue(sem_a, &val);
    printf("3.The sem value is:%d\n", val);

    sem_close(sem_a);
    sem_unlink(SEM_A_NAME);
    return 0;
}
