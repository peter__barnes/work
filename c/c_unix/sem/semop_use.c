#include<stdio.h>
#include<unistd.h>
#include<sys/sem.h>

int main()
{
    int val;
    struct sembuf sem_b;  
    int semid;
    semid=semget(ftok("1.sem",3998),1,IPC_CREAT|0666);/*create semaphore ,which can be check by shell command "ipcs -s" */
      
    sem_b.sem_num = 0; 
    sem_b.sem_op = -1; /* operation reduce 1*/   
    sem_b.sem_flg = SEM_UNDO;  /*here can set IPC_NOWAIT for nonblocking operation*/ 
  
    val=semctl(semid, 0, GETVAL) ;
    printf("current value:%d\n",val);       
    if (semop(semid, &sem_b, 1) == -1)    /*semaphore value reduce 1 by sem_b.sem_op */
    {  
        perror("C operation");  
        return -1;  
    }  
    val=semctl(semid, 0, GETVAL) ;
    printf("current value:%d\n",val);       
    semctl(semid, 0, IPC_RMID) ;
    return 0; 
}
