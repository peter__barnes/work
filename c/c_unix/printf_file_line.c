#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <stdarg.h>
#define DEBUG_FORMAT   "%-20s:%-20s [%05d]:"
#define DEBUG_INFO    __FILE__,__func__,__LINE__


#define Perror(s)		{\
							printf(DEBUG_FORMAT,DEBUG_INFO);\
							fflush(stdout);\
							perror(s);\
						}
#define Printf(...)     {\
							printf(DEBUG_FORMAT,DEBUG_INFO);\
							fflush(stdout);\
							printf(__VA_ARGS__);\
						}
int main()
{
	printf(DEBUG_FORMAT"111111111111.\n",DEBUG_INFO);
	Perror("222222222222");
	Printf("333333333%d.\n",4444);
	Printf("4444444444444\n");
	return 0;
}
