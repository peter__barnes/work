#include <stdio.h>  
#include <string.h>  
#include <stdlib.h>  
#include <errno.h>  
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/ipc.h>  
#include <sys/msg.h>  
#include <sys/stat.h>  
#define BUF_SIZE 512
struct msg{  
    long msg_types;  
    char msg_buf[BUF_SIZE];  
};  
int main(void)  
{  
    int qid,pid,len;
    struct msg pmsg;  
    key_t key; 
    sprintf(pmsg.msg_buf,"hello! this is pid:%d\n",getpid());
    if((key=ftok("1.msg",6832))==-1)  
    {  
        fprintf(stderr,"Creat Key Error：%s\a\n",strerror(errno));  
        exit(1);  
    }  

    if( (qid=msgget(key,IPC_CREAT|0666)) <0 )
    {
        perror("msgget");
        exit(1);
    }
    if(msgsnd(qid,&pmsg,BUF_SIZE,0)<0)  
    {
        perror("msgsnd");
        exit(1);
    }
    printf("send to queue id :%d\n",qid);
    system("ipcs -q");
    exit(0);  
}
