#include <stdio.h>  
#include <string.h>  
#include <stdlib.h>  
#include <errno.h>  
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/ipc.h>  
#include <sys/msg.h>  
#include <sys/stat.h>  
#define BUFSZ 4096
struct msg{  
    long msg_types;  
    char msg_buf[512];  
};  
int main(void)  
{  
    int qid,pid,len;
    struct msg pmsg;  
    key_t key; 
    if((key=ftok("1.msg",6832))==-1)  
    {  
        fprintf(stderr,"Creat Key Error：%s\a\n",strerror(errno));  
        exit(1);  
    }  
    if((qid = msgget(key,IPC_CREAT|0666))<0)
    {
        fprintf(stderr,"msgget Error：%s\a\n",strerror(errno));  
        exit(1);  
    }
    len =msgrcv(qid,&pmsg,BUFSZ,0,0);/*rcv can read msg,but can't delete message_queue object*/
    if(len>0)
    {
        pmsg.msg_buf[len]='\0';
        printf("reading queue id :%d\n",qid);
        printf("text:%s\n",pmsg.msg_buf);
    }
    else if(len = 0)
    {
        printf("no message\n");
    }
    else
    {
        perror("msgrcv");
        exit(1);
    }
    /*delete*/
    if(msgctl(qid,IPC_RMID,NULL)!=0)
    {
        perror("msgctl");
        exit(1);
    }
    system("ipcs -q");
    exit(0);  
}
