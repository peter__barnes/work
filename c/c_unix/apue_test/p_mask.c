#include <errno.h>
#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <signal.h>
void v()
{
    return;
}

void
main(void)
{
    sigset_t    sigset;
    int         errno_save;

    errno_save = errno;     /* we can be called by signal handlers */
    signal(SIGHUP, v);    /* establish signal handler */
    signal(SIGINT, v);    /* establish signal handler */
    if (sigprocmask(0, NULL, &sigset) < 0)
        printf("sigprocmask error");

    if (sigismember(&sigset, SIGINT))   printf("SIGINT ");
    if (sigismember(&sigset, SIGQUIT))  printf("SIGQUIT ");
    if (sigismember(&sigset, SIGUSR1))  printf("SIGUSR1 ");
    if (sigismember(&sigset, SIGALRM))  printf("SIGALRM ");

    /* remaining signals can go here */

    printf("\n");
    errno = errno_save;
}
