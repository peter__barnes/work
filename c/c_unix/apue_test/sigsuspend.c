#include <errno.h>
#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <signal.h>
static void sig_int(int);

int
main(void)
{
    sigset_t    newmask, oldmask, waitmask;

    printf("program start: ");
    fflush(stdout);

    if (signal(SIGINT, sig_int) == SIG_ERR)
    {
        printf("signal(SIGINT) error");
        fflush(stdout);
    }
    sigemptyset(&waitmask);
    sigaddset(&waitmask, SIGUSR1);
    sigemptyset(&newmask);
    sigaddset(&newmask, SIGINT);

    /*
     * Block SIGINT and save current signal mask.
     */
    if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0)
    {
        printf("SIG_BLOCK error");
        fflush(stdout);
    }

    /*
     * Critical region of code.
     */
    printf("in critical region: ");
    fflush(stdout);

    /*
     * Pause, allowing all signals except SIGUSR1.
     */
    if (sigsuspend(&waitmask) != -1)
    {
        printf("sigsuspend error");
        fflush(stdout);
    }

    printf("after return from sigsuspend: ");

    /*
     * Reset signal mask which unblocks SIGINT.
     */
    if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0)
        printf("SIG_SETMASK error");

    /*
     * And continue processing ...
     */
    printf("program exit: ");

    exit(0);
}

static void
sig_int(int signo)
{
    printf("\nin sig_int: ");
}

