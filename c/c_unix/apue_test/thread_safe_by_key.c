#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#define ARG_MAX 131072

static pthread_key_t key;
static pthread_once_t init_done = PTHREAD_ONCE_INIT;
pthread_mutex_t env_mutex = PTHREAD_MUTEX_INITIALIZER;

extern char **environ;

static void
thread_init(void)
{
    pthread_key_create(&key, free);
}

char *
getenv(const char *name)
{
    int     i, len;
    char    *envbuf;

    pthread_once(&init_done, thread_init);
    pthread_mutex_lock(&env_mutex);
    envbuf = (char *)pthread_getspecific(key); 
    if (envbuf == NULL) {                       //if key is not bind with data yet
printf("1111111111111111111111\n");
        envbuf = malloc(ARG_MAX);               //crate memory space
        if (envbuf == NULL) {
printf("2222222222222222222222\n");
            pthread_mutex_unlock(&env_mutex);
            return(NULL);
        }
        /*Different threads may bind different values to the same key. 
         *These values  are  typically  pointers  to blocks  of  dynamically  
         *allocated  memory that have been reserved for use by the calling thread.
         *values bind with key are not share between threads,it is thread-safe.
         */
        pthread_setspecific(key, envbuf); //bind key with data(envbuf)
    }
    len = strlen(name);
    for (i = 0; environ[i] != NULL; i++) {
        if ((strncmp(name, environ[i], len) == 0) &&
          (environ[i][len] == '=')) {
            strcpy(envbuf, &environ[i][len+1]);
            pthread_mutex_unlock(&env_mutex);
            return(envbuf);
        }
    }
    pthread_mutex_unlock(&env_mutex);
    return(NULL);
}
int main()
{
    printf("%s\n",getenv("PATH"));
    return 0;
}
