#include <dirent.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<stdio.h>

int
main(int argc, char *argv[])
{
    DIR             *dp;
    struct dirent   *dirp;

    if (argc != 2)
        printf("usage: ls directory_name");

    if ((dp = opendir(argv[1])) == 0)
        printf("can't open %s", argv[1]);
    while ((dirp = readdir(dp)) != 0)
        printf("%s,%d\n", dirp->d_name,dirp->d_type);

    closedir(dp);
    exit(0);
}
