#include <stdio.h>
#include <stdlib.h>
#include <wordexp.h>
/*
wordexp_t:
    we_wordc The number of elements in the vector.
    we_wordv The address of the vector. This field has type char **.
*/
int main()
{
    wordexp_t p;
    char **w;
    int i;

    wordexp("#[a-c]*.c", &p, 0); 
    w = p.we_wordv;
    for (i = 0; i < p.we_wordc; i++)
        printf("%s\n", w[i]);

    printf("\n\n");

    wordexp("[a-c]*.c", &p, 0); //file name expansion
    w = p.we_wordv;
    for (i = 0; i < p.we_wordc; i++)
        printf("%s\n", w[i]);
    printf("\n\n");

    wordexp("~/D*", &p, 0); //file name expansion
    w = p.we_wordv;
    for (i = 0; i < p.we_wordc; i++)
        printf("%s\n", w[i]);
    printf("\n\n");

    wordfree(&p);
    exit(EXIT_SUCCESS);
}
