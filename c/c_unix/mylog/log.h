#ifndef __LOG_H__
#define __LOG_H__
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>

void NLogMsg(int level,const char *fmt, ...);

//#define LOG_FILE
extern bool log_to_file;
extern int CurDebugLevel;
extern int CurLogfileNum;
extern int LogInitFlag;
extern char log_file_name[256] ;

//#ifdef LOG_FILE
#define log_msg(level,fmt, ...) \
	if(true == log_to_file) {NLogMsg(level,"file=%s,line=%d  "fmt"\n", __FILE__, __LINE__,  ##__VA_ARGS__);} \
else{if(level <= CurDebugLevel) {time_t t =time(0); printf("%s file=%s,line=%d  "fmt"\n", ctime(&t),__FILE__, __LINE__,  ##__VA_ARGS__);}} 

/*
#else
#define log_msg(level,fmt, ...) \
{if(level <= CurDebugLevel) {printf("file=%s,line=%d  "fmt"\n", __FILE__, __LINE__,  ##__VA_ARGS__);}}
#endif
 */

int	CheckProcessExist(char *procname,int pid);

void ascgettime(char *);
void getdateself(char *);
void gettime(char *);

void log_ch(int lev,const char *buff, int data_len);
void log_hex(int lev,const char *buff, int data_len);
void log_dec(int lev,const char *buff, int data_len);

//日志级别定义 1,2,3
#define L_ERR   1    //错误
#define L_WAR  2    //警告
#define L_INF  3    //信息提示

//#define LOG_FILE_NM  "etc_log"
//#define ITC_LOG_FILE  "itclog"

#define MAX_LOGFILE_LEN  10      //日志文件最大长度，单位 MB  

#define LOG_CIRCLENUM  3

#endif
