#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <memory.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "log.h"

int LogInitFlag = 0;      //日志初始化标识 
int CurLogfileNum = 1;    //当前日志文件号码
int CurDebugLevel = 4;    //当前日志级别
bool log_to_file = false;
char log_file_name[256] = "etc_log";

char *pItcHome=".";  //20170830

void InitLog(void)
{
	char	tmpDate[15];
	struct stat stbuf;
	char	tmpbuf[160];
	int i; 

	LogInitFlag = 1;
	CurLogfileNum = 1;

	getdateself(tmpDate);  //取日期 YYYYMMDD 		

	for(i=1;i<= LOG_CIRCLENUM;i++)
	{
		//sprintf(tmpbuf,"%s/%s_%02d.%8.8s",pItcHome,log_file_name,i,tmpDate);	  
		sprintf(tmpbuf,"%s/%s_%02d.%8.8s",pItcHome,log_file_name,i,tmpDate);	  
		if ( stat(tmpbuf,&stbuf) != -1)
		{
			//if ( stbuf.st_size <= 1024 )	  	
			if ( stbuf.st_size <= MAX_LOGFILE_LEN * 1024 * 1024 )        
			{
				CurLogfileNum = i;
				return;       	
			}

			// 文件大小超过最大限制
			if (i == LOG_CIRCLENUM)
			{
				CurLogfileNum = 1;
				return;
			}
			else
				continue;
		}
		else
		{
			if (errno != ENOENT)  //错误码不是文件不存在
				CurLogfileNum = i;
			else
				CurLogfileNum = 1;
			return;  
		}	
	}//for
	return;
}//InitLog()


//----------------------------------
// 功能： 错误日志记录 
//----------------------------------
//void LogMsg(int level,char *fmt, ...)
void NLogMsg(int level,const char *fmt, ...)
{
	va_list	ap_list;
	FILE	*fp;
	char	tmpDate[15];
	struct stat stbuf;
	char	tmpbuf[160];
	char  levelChar;


	if (LogInitFlag != 1)
		InitLog();

	if ((level < 1) || (level > 3))
		return;
	if (level > CurDebugLevel)
		return;

	if (level == 1)
		levelChar = 'E';
	else if (level == 2)
		levelChar = 'W';  	
	else 
		levelChar = 'I'; 


	//printf("LogMsg: LogInitFlag=%d,level=%d,CurDebugLevel=%d,CurLogfileNum=%d\n",
	//                LogInitFlag,level,CurDebugLevel,CurLogfileNum);

	getdateself(tmpDate);  //取日期 YYYYMMDD 		
	//sprintf(tmpbuf,"%s/%s_%02d.%8.8s",getenv("ITC_HOME"),log_file_name,CurLogfileNum,tmpDate);	
	//20170830
	sprintf(tmpbuf,"%s/%s_%02d.%8.8s",pItcHome,log_file_name,CurLogfileNum,tmpDate);	
	while(1)
	{
		if ( stat(tmpbuf,&stbuf) != -1)
		{
			//if ( stbuf.st_size > 1024 )	  	
			if ( stbuf.st_size > MAX_LOGFILE_LEN * 1024 * 1024 )  
			{// 文件大小超过最大限制
				CurLogfileNum +=1;
				if (CurLogfileNum >  LOG_CIRCLENUM)
					CurLogfileNum =1;

				//sprintf(tmpbuf,"%s/%s_%02d.%8.8s",getenv("ITC_HOME"),log_file_name,CurLogfileNum,tmpDate);	
				//20170830
				sprintf(tmpbuf,"%s/%s_%02d.%8.8s",pItcHome,log_file_name,CurLogfileNum,tmpDate);      	 
				fp = fopen(tmpbuf, "w+");
				if (fp == NULL) 
					return;
				break;               	  
			}
			else    
			{// 文件大小没有超过最大限制
				fp = fopen(tmpbuf, "a+");
				if (fp == NULL) 
					return; 
				break;        
			}
		}
		else
		{
			if (errno != ENOENT)  //错误码不是文件不存在
				return;  

			//错误码为文件不存在 
			fp = fopen(tmpbuf, "w+");
			if (fp == NULL) 
				return;  	
			break;
		}
	}//while(1)

	va_start(ap_list,fmt);

	memset(tmpbuf,'\0',160);
	ascgettime(tmpDate);     //取日期时间 YYYYMMDDhhmmss 
	//fprintf(fp,"%c %s,file=%s,line=%d ",levelChar,tmpDate,__FILE__, __LINE__);
	fprintf(fp,"%c %s,",levelChar,tmpDate);	

	vfprintf(fp, fmt, ap_list);
	fclose(fp);

	va_end(ap_list);

	return;
}//LogMsg()

//----------------------------------
// 功能： 检查某程序是否在运行
// 参数： procname--程序名
//        pid -- 进程号
// 返回： 0 -- 没有运行
//        1 -- 正在运行 
//----------------------------------
int	CheckProcessExist(char *procname,int pid)
{
	FILE	*pfp;
	int	proc_exist;
	int	tpid;
	char	cmd[80];
	char	tmpbuf[160];

	sprintf(tmpbuf,
			"ps -ef|grep %s | grep %d | awk '{print $2,$8}'",
			procname,pid); 

	if ((pfp=popen(tmpbuf,"r"))==NULL) 
	{
		printf("Popen failed: errno=%d\n",errno);
		return(0);
	}
	proc_exist = 0;
	while(fgets(tmpbuf,120,pfp))
	{
		sscanf(tmpbuf,"%d %80s",&tpid,cmd);
		if ((strlen(cmd) == 2) && (strcmp(cmd,"sh") == 0))
			continue;
		if ((strlen(cmd) == 4) && (strcmp(cmd,"grep") == 0))
			continue;
		if ((strlen(cmd) == 2) && (strcmp(cmd,"vi") == 0))
			continue;
		if (tpid != pid)
			continue;
		proc_exist = 1;

		break;
	}
	pclose(pfp);

	return(proc_exist);
}

//--------------------------------------
// 功能： 取时间函数，并转化为字符串
//         YYYYMMDDhhmmss
//------------------------------------ 
void ascgettime(char *asc_date)
{
	struct tm *ptr;
	time_t lt;

	lt = time(0x00);
	ptr = localtime(&lt);
	sprintf(asc_date,"%04d%02d%02d%02d%02d%02d",
			(1900+ptr->tm_year),
			(ptr->tm_mon + 1),
			ptr->tm_mday,
			ptr->tm_hour,
			ptr->tm_min,
			ptr->tm_sec);
}

//--------------------------------------
// 功能： 取时间函数，并转化为字符串
//        YYYYMMDD 
//------------------------------------ 
void getdateself(char *asc_date)
{
	struct tm *ptr;
	time_t lt;

	lt = time(0x00);
	ptr = localtime(&lt);
	sprintf(asc_date,"%04d%02d%02d",(1900+ptr->tm_year),
			(ptr->tm_mon + 1),
			ptr->tm_mday);
}
//--------------------------------------
// 功能： 取时间函数，并转化为字符串
//        YYYY/MM/DD hh:mm:ss
//------------------------------------  
void gettime(char *asc_date)
{
	struct tm *ptr;
	time_t lt;

	lt = time(0x00);
	ptr = localtime(&lt);
	sprintf(asc_date,"%04d/%02d/%02d %02d:%02d:%02d",
			(1900+ptr->tm_year),
			(ptr->tm_mon + 1),
			ptr->tm_mday,
			ptr->tm_hour,
			ptr->tm_min,
			ptr->tm_sec);
}

void log_ch(int lev,const char *buff, int data_len)
{
	int i;
	char log_buff[data_len*4+32];
	int n = sprintf(log_buff,"by ch:\n");
	for(i=0;i<data_len;++i)
	{
		//printf("%c ",(unsigned char)buff[i]);
		if(isprint(buff[i]))
		{
			n+= sprintf(&log_buff[n],"%c ",(unsigned char)buff[i]);
		}
		else
		{
			n+= sprintf(&log_buff[n],"  ");
		}
		if(39 == i%40)
		{
			//change line each time after print 10 number;
			n+= sprintf(&log_buff[n],"\n");
		}
		else if(9 == i%10)
		{
			n+= sprintf(&log_buff[n],"\t");
		}
	}
	n+= sprintf(&log_buff[n],"\n");
	log_msg(lev,"%s",log_buff);
	return;
}

void log_hex(int lev,const char *buff, int data_len)
{
	int i;
	char log_buff[data_len*4+32];
	int n = sprintf(log_buff,"by hex:\n");
	for(i=0;i<data_len;++i)
	{
		//printf("%c ",(unsigned char)buff[i]);
		n+= sprintf(&log_buff[n],"%x ",(unsigned char)buff[i]);
		if(39 == i%40)
		{
			//change line each time after print 10 number;
			n+= sprintf(&log_buff[n],"\n");
		}
		else if(9 == i%10)
		{
			n+= sprintf(&log_buff[n],"\t");
		}
	}
	n+= sprintf(&log_buff[n],"\n");
	log_msg(lev,"%s",log_buff);
	return;
}

void log_dec(int lev,const char *buff, int data_len)
{
	int i;
	char log_buff[data_len*4+32];
	int n = sprintf(log_buff,"by dec:\n");
	for(i=0;i<data_len;++i)
	{
		//printf("%c ",(unsigned char)buff[i]);
		n+= sprintf(&log_buff[n],"%u ",(unsigned char)buff[i]);
		if(39 == i%40)
		{
			//change line each time after print 10 number;
			n+= sprintf(&log_buff[n],"\n");
		}
		else if(9 == i%10)
		{
			n+= sprintf(&log_buff[n],"\t");
		}
	}
	n+= sprintf(&log_buff[n],"\n");
	log_msg(lev,"%s",log_buff);
	return;
}

