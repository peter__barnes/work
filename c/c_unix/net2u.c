#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iconv.h>
#include <string.h>
#include <arpa/inet.h>

uint32_t net2u(const char *buf,size_t len)
{
	if(len > sizeof(uint32_t))
	{
		printf("string is too long to be formatted, use other type larger than uint32_t!\n");
		return 0;
	}
	uint32_t net=0;
	char *p = (char*)&net;
	memcpy((unsigned char *)&p[sizeof(uint32_t)-len], buf,len);
	return ntohl(net);
}

int u2net(uint32_t in,char *buf,size_t buf_len)
{
	memset(buf,0,buf_len);
	uint32_t net = htonl(in);
	char *p = (char*)&net;
	printf("%x %x %x %x\n",p[0],p[1],p[2],p[3]);
	int i;
	for(i=0;i<sizeof(uint32_t) && p[i]== 0;i++)
	{
	}
	if(sizeof(uint32_t)-i > buf_len)
	{
		printf("data is too long to be formatted, use a larger buffer!\n");
		return -1;
	}
	memcpy(buf,&p[i],sizeof(uint32_t)-i);
	return 0;
}

int main()
{
	char buf[2] ={0x44,0x12};
	uint32_t NetRoadID = net2u(buf,2);
	printf("%x,%x\n",buf[0],buf[1]);
	printf("%d\n",NetRoadID );

	int t =17426;
	char buf2[2];
	u2net(t,buf2,2);
	printf("%x,%x\n",buf2[0],buf2[1]);

	return 0;

}






