#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#define MMAP_SIZE 1000
#define SHM_NAME "001.shm"
#define MAX_DATA_LEN 100
void err_sys(char *str)
{
    perror(str);
    exit(1);
}
void Ftruncate(int fd, off_t length)
{
	if (ftruncate(fd, length) == -1)
		err_sys("ftruncate error");
}
void Close(int fd)
{
	if (close(fd) == -1)
		err_sys("close error");
}
int Shm_open(const char *pathname, int oflag, mode_t mode)
{
	int		fd;

	if ( (fd = shm_open(pathname, oflag, mode)) == -1)
		err_sys("shm_open error ");
	return(fd);
}
void * Mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset)
{
	void	*ptr;

	if ( (ptr = mmap(addr, len, prot, flags, fd, offset)) == MAP_FAILED)
		err_sys("mmap error");
	return(ptr);
}

int main()
{
	int		fd;
	char	*ptr;
    char    data[MAX_DATA_LEN] = "Hello World\n";

	fd = Shm_open(SHM_NAME, O_RDWR | O_CREAT, 0644);
	Ftruncate(fd, MMAP_SIZE );/*resize to MMAP_SIZE */

	ptr = Mmap(NULL, MMAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    Close(fd);

    strcpy(ptr,data);/*copy data to share memory */
    sleep(15);

	exit(0);
}
