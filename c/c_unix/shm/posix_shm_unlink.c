#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#define MMAP_SIZE 1000
#define SHM_NAME "001.shm"
#define MAX_DATA_LEN 100
void err_sys(char *str)
{
    perror(str);
    exit(1);
}
void Ftruncate(int fd, off_t length)
{
	if (ftruncate(fd, length) == -1)
		err_sys("ftruncate error");
}
int Shm_open(const char *pathname, int oflag, mode_t mode)
{
	int		fd;

	if ( (fd = shm_open(pathname, oflag, mode)) == -1)
		err_sys("shm_open error ");
	return(fd);
}
void Close(int fd)
{
	if (close(fd) == -1)
		err_sys("close error");
}
void * Mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset)
{
	void	*ptr;

	if ( (ptr = mmap(addr, len, prot, flags, fd, offset)) == MAP_FAILED)
		err_sys("mmap error");
	return(ptr);
}
void Shm_unlink(const char *pathname)
{
	if (shm_unlink(pathname) == -1)
		err_sys("shm_unlink error");
}

int main()
{
	int		fd;
	char	*ptr;
    char    data[MAX_DATA_LEN] ;

	fd = Shm_open(SHM_NAME, O_RDWR | O_CREAT, 0644);
	Ftruncate(fd, MMAP_SIZE );/*resize to MMAP_SIZE */

	ptr = Mmap(NULL, MMAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    Close(fd);

    strcpy(data,ptr); /*read data from share memory*/
    printf("get shm data:%s\n",data);

    Shm_unlink(SHM_NAME);
    printf("shm deleted\n");

	exit(0);
}
