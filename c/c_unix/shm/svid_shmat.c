#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <sys/shm.h>

#define KEY_FILE_NAME "tmp.shm"
#define	SHM_KEY 8864 
#define	SHM_SIZE	100
#define	SHM_MODE	0640	/* user read/write */
#define MAX_DATA_LEN 100

/*sometimes this program show Permission denied, run with sudo*/
int main(void)
{
	int		shmid;
	char	*shmptr;
    char    data[MAX_DATA_LEN] ;
	if ((shmid = shmget(ftok(KEY_FILE_NAME ,SHM_KEY ), SHM_SIZE, IPC_CREAT|0644)) < 0) /*create*/
    {
		perror("shmget error");
        exit(1);
    }
	if ((shmptr = shmat(shmid, 0, 0)) <0 ) /*attach*/
    {
		perror("shmat error");
        exit(1);
    }
    strcpy(data,shmptr); /*read data from share memory*/
    printf("get shm data:%s\n",data);

	if (shmdt(shmptr) <0)  /*detach */
    {
		perror("shmdt error");
        exit(1);
    }
	exit(0);
}
