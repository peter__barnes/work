#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include <sys/shm.h>

#define KEY_FILE_NAME "tmp.shm"
#define	SHM_KEY 8864 
#define	SHM_SIZE	100
#define	SHM_MODE	0640	/* user read/write */
#define MAX_DATA_LEN 100

/*sometimes this program show Permission denied, run with sudo*/
int main(void)
{
	int		shmid;
	char	*shmptr;
    char    data[MAX_DATA_LEN] = "Hello World\n";
	if ((shmid = shmget(ftok(KEY_FILE_NAME,SHM_KEY ), SHM_SIZE, IPC_CREAT|0644)) < 0) /*create*/
    {
		perror("shmget error");
        exit(1);
    }
	if ((shmptr = shmat(shmid, 0, 0)) <0 ) /*attach*/
    {
		perror("shmat error");
        exit(1);
    }
    strcpy(shmptr,data);/*copy data to share memory */

    sleep(15);
	if (shmdt(shmptr) <0)  /*detach */
    {
		perror("shmdt error");
        exit(1);
    }
	if (shmctl(shmid, IPC_RMID, NULL) < 0) /*remove*/
    {
		perror("shmctl error");
        exit(1);
    }
	exit(0);
}
