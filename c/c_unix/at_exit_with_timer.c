/*compile with :   gcc  at_exit_use.c -lrt*/
#include <stdio.h>   
#include <stdlib.h>   
#include <string.h>
#include <time.h>
#include <unistd.h>   
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#undef _POSIX_C_SOURCE 
#define _POSIX_C_SOURCE >= 199309L
typedef void (*sighandler_t)(int);
sighandler_t signal_mask(int signo, sighandler_t func)
{
	struct sigaction	act, oact;
	act.sa_handler = func;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	if (signo == SIGALRM) {
#ifdef	SA_INTERRUPT
		act.sa_flags |= SA_INTERRUPT;	/* SunOS 4.x */
#endif
	} else {
#ifdef	SA_RESTART
		act.sa_flags |= SA_RESTART;		/* SVR4, 44BSD */
#endif
	}
	if (sigaction(signo, &act, &oact) < 0)
		return(SIG_ERR);
	return(oact.sa_handler);
}
/* end signal */

sighandler_t Signal_mask(int signo, sighandler_t func)	/* for our signal() function */
{
	sighandler_t sigfunc;

	if ( (sigfunc = signal_mask(signo, func)) == SIG_ERR)
		perror("signal error");
	return(sigfunc);
}
void sig_chld(int signo)
{
    pid_t	pid;
    int		stat;

    while ( (pid = waitpid(-1, &stat, WNOHANG)) > 0)
        printf("child %d terminated\n", pid);
    return;
}
void exit_fn1(void)   
{   
    printf("Exit function #1 called\n");   
}   
void exit_fn2(void)   
{   
    printf("Exit function #2 called\n");   
}   
void timer_handle()  
{  
    time_t t;  
    char p[32];  

    time(&t);  
    strftime(p, sizeof(p), "%T", localtime(&t));  
    printf("signal captured.\n");  
    exit(0);  
}  
int main(void)   
{   
    clockid_t clock_sleep;
    struct sigevent evp ; 
    struct itimerspec ts;  
    timer_t timer;  
    memset(&ts,0,sizeof(struct itimerspec ));
    ts.it_value.tv_sec= 10;
    ts.it_interval.tv_sec = ts.it_value.tv_sec;
    evp.sigev_value.sival_ptr = &timer;  
    evp.sigev_notify = SIGEV_SIGNAL;  
    evp.sigev_signo = SIGUSR1;
    signal(SIGUSR1, timer_handle);  
    if( timer_create(CLOCK_REALTIME, &evp, &timer)) 
    {
        perror("timer_create");  
    }
    /* post exit function #1 */   
    atexit(exit_fn1);   
    /* post exit function #2 */   
    atexit(exit_fn2);   
    Signal_mask(SIGCHLD,sig_chld);
    if(fork()>0) //parent
    {
        //sleep() may be implemented using SIGALRM; mixing calls to alarm() and sleep() is a bad idea.
        //and sleep() is only thread safe, when using muti-processes, this can run with error!
        //sleep(10); //child will be zoobie at this time! check it by "ps aux|grep a.out"
//        timer_create(clock_sleep);
        if(timer_settime(timer, 0, &ts, NULL))  
        {
            perror("timer_settime");  
        }
        while(1);  
    }
    exit(0);   
}   
