#include <stdio.h>  
#include <string.h>  
#include <stdlib.h>  
#include <errno.h>  
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/ipc.h>  
#include <sys/msg.h>  
#include <sys/stat.h>  
#define BUFSZ 4096
struct msg{  
    long msg_types;  
    char msg_buf[512];  
};  
int main(void)  
{  
    int qid,pid,len;
    struct msg pmsg;  
    if( (qid=msgget((key_t)1234,IPC_CREAT|0666)) <0 )
    {
        perror("msgget");
        exit(1);
    }
	pmsg.msg_types=1;//important!positive number (>0), or 0 will failed and crash in some environment
    len =msgrcv(qid,&pmsg,sizeof(pmsg.msg_buf),0,0);
    if(len>0)
    {
        pmsg.msg_buf[len]='\0'; 
        printf("reading queue id :%d\n",qid);
        printf("text:%s\n",pmsg.msg_buf);
    }
    else if(len = 0)
    {
        printf("no message\n");
    }
    else
    {
        perror("msgrcv");
        exit(1);
    }
    system("ipcs -q");
    exit(0);  
}
