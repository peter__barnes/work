#include <stdio.h>  
#include <string.h>  
#include <stdlib.h>  
#include <errno.h>  
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/ipc.h>  
#include <sys/msg.h>  
#include <sys/stat.h>  
#define BUF_SIZE 512
struct msg{  
    long msg_types;  
    char msg_buf[BUF_SIZE];  
};  
int main(void)  
{  
    int qid,pid,len;
    struct msg pmsg;  
    /*  msg.msg_types correspond to msgrcv()'s 4th arugment msgtyp,
        if we set this value,msgrcv can choose which msg to read by type(omit others) 
    */
    /*pmsg.msg_types = getpid();*/
    sprintf(pmsg.msg_buf,"hello! this is pid:%d\n",getpid());

    if( (qid=msgget((key_t)1234,IPC_CREAT|0666)) <0 )
    {
        perror("msgget");
        exit(1);
    }
	pmsg.msg_types=1;
    if(msgsnd(qid,&pmsg,strlen(pmsg.msg_buf)+1,0)<0)  
    {
        perror("msgsnd");
        exit(1);
    }
    printf("send to queue id :%d\n",qid);
    system("ipcs -q");
    exit(0);  
}
