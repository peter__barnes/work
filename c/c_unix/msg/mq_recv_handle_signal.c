#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <signal.h>
#define FILE_MODE S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH
#define MAX_MSG 30
#define MQ_FILE "/mq_tmp1"
/* gcc need -lrt*/
mqd_t mqd;
static void sig_usr1(int signo)
{
    int prio;
    char msg[MAX_MSG]="";
    struct mq_attr attr;
    mq_notify(mqd,NULL);/*remove mq notification*/
    mq_getattr(mqd,&attr);

    if(mq_receive(mqd,msg,attr.mq_msgsize,&prio) < 0)
    {  
        perror("mq_receive");  
        exit(2);
    }  
    printf("mq_recive msg:%s\n",msg);

}
int main()
{
    struct sigevent sigev;
    mqd = mq_open(MQ_FILE,O_RDWR|O_CREAT,FILE_MODE,NULL); /*create or open mq file:/tmp/mq_tmp1*/
    if (mqd<0) 
    {  
        perror("mq_open");  
        return 2;  
    }  
    signal(SIGUSR1,sig_usr1);/*connet signal with thread function*/
    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = SIGUSR1;
    mq_notify(mqd,&sigev); /*register mq notification .here ,we only get signal if empty mq arrive new data*/
    while(1)
    {
        pause();/*wait until signal arrive*/
        break;
    }
    mq_close(mqd);
    mq_unlink(MQ_FILE);
    exit(0);
}
