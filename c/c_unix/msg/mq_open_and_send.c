#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#define FILE_MODE S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH
#define MAX_MSG 30
/* gcc need -lrt*/
int main()
{
    mqd_t mqd;
    char msg[MAX_MSG]="hello";
    system("sudo mount -t mqueue none /dev/mqueue"); /*must mount mqueue before use*/
    mqd = mq_open("/mq_tmp1",O_RDWR|O_CREAT,FILE_MODE,NULL); /*create or open mq file:/tmp/mq_tmp1 , NULL means use default mq_attr*/
    if (mqd == -1) 
    {  
        perror("mq_open");  
        return 2;  
    }  
    printf("len:%d\n",(int)(sizeof(char)*(strlen(msg)+1)));
    if(mq_send(mqd,msg,(int)(sizeof(char)*(strlen(msg)+1)),0)<0) /*here is data length,but real buffer size is set by mq_open,receive side should use mq_getattr to get real mq_msgsize instead of using data length here*/
    {  
        perror("mq_send");  
        return 2;  
    }  
    mq_close(mqd);
    exit(0);
}
