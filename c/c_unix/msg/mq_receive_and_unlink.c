#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#define FILE_MODE S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH
#define MAX_MSG 30
/* gcc need -lrt*/
int main()
{
    mqd_t mqd;
    char msg[MAX_MSG]="";
    struct mq_attr attr;
    int prio;
    mqd = mq_open("/mq_tmp1",O_RDWR|O_CREAT,FILE_MODE,NULL); /*create or open mq file:/tmp/mq_tmp1*/
    if (mqd<0) 
    {  
        perror("mq_open");  
        return 2;  
    }  
    mq_getattr(mqd,&attr);
    if(mq_receive(mqd,msg,attr.mq_msgsize,&prio) < 0)
    {  
        perror("mq_receive");  
        return 2;  
    }  
    printf("mq_recive msg:%s\n",msg);
    mq_close(mqd);
    mq_unlink("/mq_tmp1");
    exit(0);
}
