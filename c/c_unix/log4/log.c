#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <unistd.h>
#include <time.h>
#include "log4c.h"  
#define L_FMT   "%-15s:%-15s[%05d]"
#define L_PAR    __FILE__,__func__,__LINE__
#define	L_FATA LOG4C_PRIORITY_FATAL	
#define L_WARN LOG4C_PRIORITY_WARN	
#define L_INFO LOG4C_PRIORITY_INFO
void log4c_category_localtm(const log4c_category_t* a_category,
				      int a_priority,
				      const char* a_format,
						...)
{
    if (log4c_category_is_priority_enabled(a_category, a_priority)) {
	va_list va;
	va_start(va, a_format);
	char format_with_tm[strlen(a_format)+32];

//	char tmBuf[32];  
	time_t t = time(0);  
	strftime(format_with_tm, sizeof(format_with_tm), "%d,%H:%M:%S.", localtime(&t));
	strcat(format_with_tm,a_format);

	log4c_category_vlog(a_category, a_priority, format_with_tm, va);
	va_end(va);
    }
}

int main()
{   
	log4c_init();  
//	log4c_category_t* mycat = log4c_category_get("WLAN_Console");  
	log4c_category_t* log4cat = log4c_category_get("LOG_File");  
	  
	fork();
	// 用该category进行日志输出，日志的类型为DEBUG，输出信息为 "Hello World!"，  
//	printf("",__LINE__);
	while(1)
	{
		usleep(1000000);
	//	log4c_category_log(log4cat, L_WARN, L_FMT"%d,%s", L_PAR,getpid(),"Hello \nWorld!");  
		log4c_category_localtm(log4cat, L_WARN, L_FMT"%d,%s", L_PAR,getpid(),"Hello \nWorld!");  
	}
	  
	// 去初始化  
	log4c_fini();   
    return 0; 

}
