#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/sysinfo.h>
int main()
{
    printf("current process pagesize:%d,total system:%ld,total free:%ld\n",getpagesize(),get_phys_pages(),get_avphys_pages());
    printf("cpu processor number:%d,current available:%d\n",get_nprocs_conf(),get_nprocs()); 
    return 0;
}
