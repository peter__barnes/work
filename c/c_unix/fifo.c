#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

#define FIFO1   "/tmp/fifo.1"
#define FIFO2   "/tmp/fifo.2"

int main()
{
    int readfd,writefd;
    pid_t   childpid;
    char buf[100];
    memset(buf,0,100);
    //创建FIFO
    if((mkfifo(FIFO1,FILE_MODE) < 0) && (errno != EEXIST))
    {
        perror("mkfifo() error");
        exit(-1);
    }
    if((mkfifo(FIFO2,FILE_MODE) < 0) && (errno != EEXIST))
    {
        unlink(FIFO1);
        perror("mkfifo() error");
        exit(-1);
    }
     //创建子进程
    childpid = fork();
    if(childpid == 0)
    {
        //fifo的open是阻塞的!写进程会阻塞等待读进程，此处是两个进程一起open，如果没有fork，这里会直接卡住！这和普通文件读写不同
        readfd = open(FIFO1,O_RDONLY,0);
        writefd = open(FIFO2,O_WRONLY,0);
        printf("Server input a message: ");
        fgets(buf,100,stdin);
        write(writefd,buf,strlen(buf));
        read(readfd,buf,100);
        printf("Server received a message from Client: %s\n",buf);
        exit(0);
    }
    if(childpid == -1)
    {
        perror("frok() error");
        exit(-1);
    }
     //防止死锁，注意顺序
    writefd = open(FIFO1,O_WRONLY,0);
    readfd = open(FIFO2,O_RDONLY,0);
    read(readfd,buf,100);
    printf("Client received a message form Server: %s\n",buf);
    printf("Client input a mesage: ");
    fgets(buf,100,stdin);
    write(writefd,buf,strlen(buf));
    waitpid(childpid,NULL,0);
    close(readfd);
    close(writefd);
    unlink(FIFO1);
    unlink(FIFO2);
    return 0;
}
