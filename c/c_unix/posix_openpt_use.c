#define _XOPEN_SOURCE  600 /*this should be define before other include file*/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int main()
{
	int masterfd, slavefd;
	char *slavedevice;

	masterfd = posix_openpt(O_RDWR|O_NOCTTY);
	if (masterfd == -1
			|| grantpt (masterfd) == -1
			|| unlockpt (masterfd) == -1
			|| (slavedevice = ptsname (masterfd)) == NULL)
	{
		return -1;
	}
	printf("slave device is: %s\n", slavedevice);
	slavefd = open(slavedevice, O_RDWR|O_NOCTTY);
	if (slavefd < 0)
	{
		return -1;
	}
	return 0;
}
