//brk用绝对的地址指定移到哪个位置。
#include <stdio.h>   
#include <unistd.h>   
int main()  
{   
    void* p = sbrk(0);  
    printf("start position:%p\n",p);
    int* p1 = p;  
    brk(p1+4);//分配了16个字节的空间   
    p1[0] = 10;  
    p1[1] = 20;  
    p1[2] = 30;  
    p1[3] = 40;  
    p1[4] = 50;  
    //increase 4 bytes.sbrk().returns previous program break.  (If increased, return a pointer to the start of the newly allocated memory). 
    int* p2 = sbrk(4);  
    printf("after p2,%p\n",p2); 
    p2 = sbrk(0);  
    printf("after p2,%p\n",p2); // this will be 4 bytes more than the last one
    brk(p1+1024);//分配整个页面的空间   
    printf("change size to to 1024,%p\n",sbrk(0));
    brk(p1+512);//释放一半空间   
    printf("change size to to 512,%p\n",sbrk(0));
    brk(p1);//释放所有空间  
    printf("free all space,%p\n",sbrk(0)); //can' run this line correctly cause no space;
	return 0;
}  
