/*
gcc4.8及以上版本支持地址越界，野指针检查.只需要在编译时使用-fsanitize=address选项即可,程序崩溃时会给出信息
另外还有一个-fsanitize=leak东东，可以检测内存泄露,程序结束会打印出关于泄露的log
使用这个会导致执行变慢一些，影响不是很大，但是还是建议程序测试期使用。
*/
#include "stdio.h"

int main(int argc , char **argv)
{
	int *p = NULL;
	*p = 10;
	printf("%d",*p);

	return 0;
}
