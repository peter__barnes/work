#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/stat.h>
#include <fcntl.h>
void set_daemon()
{
	int pid = fork();
	if (pid < 0)
	{
		printf("Fail to fork: can not start process -- appserver\n");
		exit(-1);
	}
	if (pid > 0)
		exit(0);
	umask(0);                          // 设置权限掩码 

	setsid();                          // 在子进程中创建新会话。
	//chdir(getenv("ITC_HOME"));             // 设置工作目录用户HOME目录
	chdir(".");                     // 设置工作目录ITC_HOME    
	umask(0);                          // 设置权限掩码 

	int i;
	for(i=0;i<getdtablesize();i++)     //getdtablesize返回子进程文件描述符表的项数
	{
		close(i);                     // 关闭这些不将用到的文件描述符
	}
	int fd = open("/dev/null", O_RDWR);
	if (fd > 0) 
	{
		//close(0);
		//close(1);
		//close(2);
		dup(fd);
		dup(fd);
		dup(fd);
		close(fd);
	}
	return;
}

int main()
{
	set_daemon();
	while(1)
	{
		usleep(10000);
	}
}
