【talib环境安装】
talib不适合用pip直接安装！因为默认下载的是32位不支持64位系统。
需要去talib官网下载，如：“TA_Lib-0.4.19-cp37-cp37m-win_amd64.whl” 其中的“cp37“表示支持python3.7，
“win”是windows，“amd64”是X86_64架构的缩写。
pip安装“.whl”文件：
pip install TA_Lib-0.4.19-cp37-cp37m-win_amd64.whl  
-----------------------------------------------------------------------------------------
【文件说明】
test_talib.py 最简单的talib程序，根据随机数计算EMA指标。

test_talib_gm.py 掘金gm结合talib运行，需要开第三方终端，具体的环境和部署参考官网和书籍。
