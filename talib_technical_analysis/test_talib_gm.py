#!/usr/bin/env python
# coding=utf-8
from __future__ import print_function, absolute_import
from gm.api import *
def init(context):
    schedule(schedule_func=algo, date_rule='1d', time_rule='14:50:00')
def algo(context):
    order_volume(symbol='SHSE.600000', volume=200, side=OrderSide_Buy,order_type=OrderType_Market, position_effect=PositionEffect_Open, price=0)
def on_backtest_finished(context, indicator):
    print(indicator)
if __name__ == '__talib__':
    run(strategy_id='strategy_id',
        filename='main.py',
        mode=MODE_BACKTEST,
        token='051133a6caa1348695ec1d9856760a83ea043cad',
        backtest_start_time='2020-11-01 08:00:00',
        backtest_end_time='2020-11-10 16:00:00',
        backtest_adjust=ADJUST_PREV,
        backtest_initial_cash=10000000,
        backtest_commission_ratio=0.0001,
        backtest_slippage_ratio=0.0001)
