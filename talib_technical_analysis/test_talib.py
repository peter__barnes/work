#talib和pandas原生SMA运行效率比较（网上抄的，底层逻辑未验证）
import pandas as pd
import numpy as np
import talib as ta
import time 
 
close_price = np.random.random(100000000)
print(close_price)
 
start = time.process_time()
ta.SMA(close_price, timeperiod=20)
end = time.process_time()
 
print("\n talib 计算耗时：")
print('耗时 %6.3f' %(end - start))
 
# 创建 dataframe
df = pd.DataFrame(close_price, columns=['close_price'])
#print(df.head())
start = time.process_time()
df.rolling(20).mean()
end = time.process_time()
 
print("\n DataFrame 计算耗时：")
print('耗时 %6.3f' %(end - start))
