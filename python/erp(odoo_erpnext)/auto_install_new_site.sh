#!/bin/bash
 
function is_domain_name() {
    local domain=$1
    # 正则表达式匹配域名
    local domain_regex='^([a-zA-Z0-9][a-zA-Z0-9\-]{1,61}[a-zA-Z0-9]\.)+[a-zA-Z0-9][a-zA-Z0-9\-]{1,61}[a-zA-Z0-9]$'
    
    if [[ $domain =~ $domain_regex ]]; then
        return 1
    else
        return 0
    fi
}
# 调用函数
is_domain_name "$1"
# 获取函数返回值
is_domain=$?
echo "$is_domain"
 
if [ -z "$1" ]; then
    echo "指令有误，第一个参数应为站点域名"
elif [ "$is_domain" == 1 ]; then
	echo "创建站点：$1"
else
    echo "指令有误，第一个参数应为站点域名"
fi

bench config dns_multitenant on
bench new-site $1
bench --site $1 install-app erpnext
bench --site $1 install-app erpnext_chinese
bench --site $1 install-app erpnext_oob
bench --site $1 install-app zelin_permission
bench --site $1 add-to-hosts
