from pandas.io.parsers import read_csv
import numpy as np
#see <<python data analysis>>
df = read_csv("WHO.csv")
print ("Dataframe", df)
print ("Column Headers", df.columns)
print ("Data types", df.dtypes)

print("2===================")
country_col = df["Country"]
print ("Type df", type(df))
print ("Type country col", type(country_col))

print("3===================")
last_col = df.columns[8]
print ("Last df column signs", last_col, np.sign(df[last_col]))