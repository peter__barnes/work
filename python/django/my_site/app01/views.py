from django.shortcuts import render
from django.http import HttpResponse
import pymysql.cursors

# Create your views here.
def test_view(request):
	print("run ......:",request)
	return HttpResponse("Hello, world. You're at the polls app01.") 
def login_view(request):
	"""
	html=
		<form method="post">
			<input type="text" name="username">
			<input type="password" name="password">
			<input type="submit" name="登录">
		</form>
	return HttpResponse(html) 
	"""
	return render(request,'form.html')
def database_view(request):
	conn = pymysql.connect('127.0.0.1',port=3306,user='root',passwd='1',db='sys')
	cursor = conn.cursor()
	cursor.execute("select * from version");
	data = cursor.fetchall()
	return HttpResponse(str(data)) 
