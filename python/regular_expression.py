import re
m = re.search('foo','foo')
if m is not None:
    print m.group()
n = re.search('.o','foo')
if n is not None:
    print n.group()
print;
print(re.match('www', 'www.runoob.com').span())
print(re.match('www', 'www.runoob.com').group())
print(re.search('www', 'www.runoob.com').group())
print(re.search('www', 'www.runoob.www').group())
print;
print(re.match('runoob', 'www.runoob.com'))# can't match,cause match() only search at beginning part!
print(re.search('^runoob', 'www.runoob.com'))# can't match,there is no runoob at beginning part!
print;
print;
print(re.sub(r'day','night', 'it is sunday,a good day!'))#sub(r'pattern new','old','str') 
print "back reference";
print(re.sub(r'(da)(y)','\g<1> night \g<2>', 'it is sunday,a good day!'))
