#!/usr/bin/python
# -*- coding: UTF-8 -*-

import MySQLdb
# 打开数据库连接
db = MySQLdb.connect("localhost","root","oor6843kme","test" )
# 使用cursor()方法获取操作游标 
cursor = db.cursor()

# 使用execute方法执行SQL语句
cursor.execute("SELECT VERSION();")

# 使用 fetchone() 方法获取一条数据库。
data = cursor.fetchone()

print "Database version : %s " % data

try:
    # 使用execute方法执行SQL语句
    cursor.execute("SELECT * FROM co;")

    # 使用 fetchone() 方法获取一条数据库。
    re = cursor.fetchall()
    print "co : \n";
    for row in re:
        co_id = row[0];
        course = row[1];
        name = row[2];
        print "co_id: %s,\tcourse: %s,\tname: %s." % (str(co_id),str(course),str(name));


except:
    print "error\n";

# 关闭数据库连接
db.close()
