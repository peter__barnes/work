#!/usr/bin/python
# -*- coding: UTF-8 -*-
from ctypes import cdll
from ctypes import *
import ctypes
clib = cdll.LoadLibrary("clib.so") #load clib.so libaray of current path

print "factorial of %d is %d"%(4,clib.fac(4))
print clib.reverse("Hello World") #it can not print the result we want ,cause it is utf-8 not decoded yet
rst = ctypes.string_at(clib.reverse("Hello World"), -1)
print(rst.decode('utf-8'))  # good result
