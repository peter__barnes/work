"""
setup.py
"""
 
from distutils.core import setup, Extension
 
 
example_module = Extension('_clib',
                           sources=['clib_wrap.c', 'clib.c'],
                           )
 
setup (name = 'clib',
       version = '0.1',
       author      = "Terrance__Chen",
       description = """Simple swig example """,
       ext_modules = [example_module],
       py_modules = ["example"],
       )
