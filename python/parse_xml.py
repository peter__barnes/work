#!/usr/bin/env python

try:
    from io import BytesIO as StringIO
except ImportError:
    try:
        from cStringIO import StringIO
    except ImportError:
        from StringIO import StringIO

try:
    from itertools import izip as zip
except ImportError:
    pass

try:
    from urllib2 import urlopen
except ImportError:
    from urllib.request import urlopen

from pprint import pprint
from xml.etree import ElementTree

g = urlopen('http://news.yahoo.com/rss') #1.open url
f = StringIO(g.read())#2.read data to iostream, f is something like filedescriptor
g.close() 

#or use this static xhtml in local folder,which can replace 'http://news.yahoo.com/rss'
'''
f = open("yahoo_news.xhtml","r");    
'''

tree = ElementTree.parse(f)#3.parse xml file data to elements which stored in tree
f.close()

def topnews(count=5):
    pair = [None, None]
    for elmt in tree.getiterator():#4. iterator which point to single element
        if elmt.tag == 'title': #5.get tag by iterator
            skip = elmt.text.startswith('Top Stories') #6.get text by iterator
            if skip:
                continue
            pair[0] = elmt.text
        if elmt.tag == 'link':
            if skip:
                continue
            pair[1] = elmt.text
            if pair[0] and pair[1]:
                count -= 1
                yield(tuple(pair))
                if not count:
                    return
                pair = [None, None]

for news in topnews():
    pprint(news)

#topnews = lambda count=5: [(x.text, y.text) for x, y in zip(tree.getiterator('title'), tree.getiterator('link')) if not x.text.startswith('Top Stories')][:count]
