import time


while True:
    #起始时间
    start_min = 1
    #播放倍速
    times_of_speed = 1.0
    starttime = time.time()
    print('start')
    try:
        while True:
            print('time: ', start_min + round((time.time() - starttime) * times_of_speed/60 , 1), 'min')
            time.sleep(60)
    except KeyboardInterrupt:
        print('stop')
        endtime = time.time()
        print('total:', start_min + round((time.time() - starttime) * times_of_speed/60, 1), 'min')
        break