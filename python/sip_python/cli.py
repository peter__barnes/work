#!/usr/bin/python
# -*- coding: utf-8 -*-  
import os
import socket
import time
port = 5060
host = '192.168.0.104'
s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
#s.sendto("hello world",(host,port)) 

path = os.getcwd()
file = os.path.join(path, 'play_104.msg')

# 由于python在进行明文读取，默认会将回车换行强制转换为/n，为了防止sip server解析错误，
# 在读取时增加了newline参数，并设置为空，则不进行转换，windows下读取为/r/n
with open(file, "rb+") as sm:
    data = sm.read()

print(data)
s.sendto(data,(host,port))
print('send over')
'''
while 1:
	buf = s.recv(2048)
	print(buf.decode('utf-8'))
	break
time.sleep(1)
'''
