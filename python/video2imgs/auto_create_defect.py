import xml.etree.ElementTree as ET
import os
import imgaug as ia
import numpy as np
import shutil
from tqdm import tqdm
from PIL import Image
from imgaug import augmenters as iaa
import random
import cv2 as cv
from mix_img import *
from cfg import *
import imutils
ia.seed(4)

DEFECT_IMG_PATH = 'E:/PStest/defect/'
DIR_OUT ='E:/PStest/contrast_out/'
IMG_DIR = 'E:/PStest/good_img/'
ANNO_DIR = 'E:/PStest/good_anno/'


def rotate_img(img):
    #h,w=img.shape[:2]
    #M=cv.getRotationMatrix2D((w/2,h/2),180*random.random(),0.6+1*random.random())
    #M=cv.getRotationMatrix2D((w/2,h/2),180*random.random(),1)
    #rotate=cv.warpAffine(img,M,(w,h))

    rotate = imutils.rotate_bound(img, 180*random.random())
    img_h, img_w = rotate.shape[:2]
    #for y in range(0,img_h):
        #for x in range(0,img_w):
            #if(rotate[y, x, 0]<80 and rotate[y, x, 1]<80 and rotate[y, x, 2]<80):
                #rotate[y, x, 3] = 0
    #cv.imshow('rotate',rotate)
    #cv.waitKey()
    #cv.destroyAllWindows()
    return rotate

def add_img(img_bg,img_fg,resizable=True):
    # 1.30 图像的叠加
    img_out = img_bg

    bg_h, bg_w = img_bg.shape[:2]
    fg_h, fg_w = img_fg.shape[:2]
    if (fg_h > bg_h*0.6) or (fg_w > bg_w*0.6):
        if (fg_h > bg_h * 1.2) or (fg_w > bg_w * 1.2):
            return False,img_out
        else:
            size_decrease = (int(fg_w /2), int(fg_h / 2))
            img_fg = cv.resize(img_fg, size_decrease, interpolation=cv.INTER_AREA)
            fg_h, fg_w = img_fg.shape[:2]
    while (fg_h < bg_h*0.2) and (fg_w < bg_w*0.2) and resizable:
        size_increase = (int(fg_w * 2), int(fg_h * 2))
        img_fg = cv.resize(img_fg, size_increase, interpolation=cv.INTER_AREA)
        fg_h, fg_w = img_fg.shape[:2]

    img_fg_3chan = cv.cvtColor(img_fg, cv.COLOR_BGRA2BGR)
    #print(fg_h,bg_h,fg_w,bg_w)
    merge_pos_x = int(bg_w*0.2 + random.random()*(bg_w*0.6 - fg_w))
    merge_pos_y = int(bg_h*0.2 + random.random()*(bg_h*0.6 - fg_h))
    # 首先获取原始图像roi
    rows, cols, channels = img_fg.shape
    bord_pix = 0
    roi = img_bg[merge_pos_y+bord_pix:merge_pos_y+rows-bord_pix, merge_pos_x+bord_pix:merge_pos_x+cols-bord_pix]
    #cv.imshow('img_fg_3chan',img_fg_3chan)
    #cv.waitKey()
    #'''
    for y in range(bord_pix,fg_h - bord_pix):
        for x in range(bord_pix,fg_w - bord_pix):
            if 0 == img_fg[y,x][3]:
                pass
            else:
                #print(img_fg[y, x, 0])
                #print(img_fg[y, x, 3])
                roi[y,x,0:2] = img_fg_3chan[y,x,0:2]
    #'''
    #cv.imshow('roi',roi)
    #cv.waitKey()
    #cv.addWeighted(img_fg, 0.5, roi, 0.5, 0, roi)
    img_out[merge_pos_y+bord_pix:merge_pos_y+rows-bord_pix, merge_pos_x+bord_pix:merge_pos_x+cols-bord_pix] = roi
    #cv.imshow('img_out', img_out)
    #cv.waitKey(0)
    return True,img_out


def create_defect_cell(img_cell,img_defect,resizable=True):
    #cv.imshow('img_defect', img_defect)
    #cv.waitKey(0)
    #随机调整角度和大小
    img_defect = rotate_img(img_defect)
    #result,defect_cell = add_img(img_cell, img_defect)
    #随机调整位置
    #合成图像
    return add_img(img_cell, img_defect, resizable)

def create_defect(file_bg,defect_label, cnt ):
    print(file_bg)
    resizable =True
    if defect_label =='scratch':
        file_defect_list = random.sample(defect_scratch_list, len(defect_scratch_list))
    elif defect_label =='dirty':
        file_defect_list = random.sample(defect_dirty_list, len(defect_dirty_list))
    elif defect_label =='press':
        file_defect_list = random.sample(defect_press_list, len(defect_press_list))
        resizable=False

    image_pre, ext = os.path.splitext(file_bg)
    img_file = IMG_DIR + image_pre + ".jpg"
    xml_file = ANNO_DIR + image_pre + '.xml'

    img_bg = cv.imread(img_file)
    tree = ET.parse(xml_file)
    root = tree.getroot()
    # if root.find('object') == None:
    #     return
    obj_i = 0
    for obj in root.iter('object'):
        #cls = obj.find('name').text
        xmlbox = obj.find('bndbox')
        b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
             int(float(xmlbox.find('xmax').text)),
             int(float(xmlbox.find('ymax').text))]
        try:
            img_cut = img_bg[b[1]:b[3], b[0]:b[2], :]

            lable_name = obj.find('name')
            if (lable_name is not None):
                if 'good' == lable_name.text or 'revert' == lable_name.text:
                    img_defect = cv.imread(DEFECT_IMG_PATH + file_defect_list[obj_i], cv.IMREAD_UNCHANGED)
                    merge_success,img_cut = create_defect_cell(img_cut,img_defect,resizable)
                    if merge_success:
                        img_bg[b[1]:b[3], b[0]:b[2], :] = img_cut
                        lable_name.text = defect_label
                    else:
                        pass
        except Exception as e:
            print('except:%s'%e)
            pass
        obj_i += 1
    cv.imwrite(os.path.join(DIR_OUT, 'contrast_auto%04d.jpg'%(cnt)), img_bg)
    tree.write(os.path.join(DIR_OUT, 'contrast_auto%04d.xml'%(cnt)))



if __name__ == "__main__":
    #defect_dirty_list=['d0004.png']
    #create_defect('img0261.jpg', 'dirty', 1)
    #exit()

    cnt=1
    defect_scratch_list=[]
    defect_dirty_list=[]
    defect_press_list=[]
    for aroot, dirs, files in os.walk(DEFECT_IMG_PATH):
        for item in files:
            if item.startswith("s"):
                defect_scratch_list.append(item)
            elif item.startswith("d"):
                defect_dirty_list.append(item)
            elif item.startswith("p"):
                defect_press_list.append(item)
    for i in range(0,3):
        for aroot, dirs, files in os.walk(IMG_DIR):
            filelist = files
            for item in filelist:
                create_defect(item,'scratch',cnt)
                cnt+=1
        for aroot, dirs, files in os.walk(IMG_DIR):
            filelist = files
            for item in filelist:
                create_defect(item,'dirty',cnt)
                cnt+=1
        for aroot, dirs, files in os.walk(IMG_DIR):
            filelist = files
            for item in filelist:
                create_defect(item,'press',cnt)
                cnt+=1
    #cv.imwrite(os.path.join(IMG_DIR_OUT, 'contrast_%04d.jpg'%(cnt)), img_ret)