import cv2
import numpy as np
import math

#计算点与点的距离
def point_to_point(x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude

# 通过查找投影点，计算点到投影点距离，获得点到直线段的距离
def point_to_line_dist(px, py, x1, y1, x2, y2):
    line_magnitude = point_to_point(x1, y1, x2, y2)
    if line_magnitude < 0.00000001:
        #如果线段距离很近，直接返回P到A的距离
        return point_to_point(px, py, x1, y1)
    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1))) #向量AB·向量AP
    u = u1 / (line_magnitude * line_magnitude) #向量AP在向量AB方向的投影与向量AB模的比值
    if (u < 0) or (u > 1):
        # 点到直线的投影不在线段内, 计算点到两个端点距离的最小值即为"点到线段最小距离"
        return min(point_to_point(px, py, x1, y1),point_to_point_distance(px, py, x2, y2))
    # 投影点在线段内部, 计算方式同点到直线距离, u 为投影点距离x1在x1x2上的比例, 以此计算出投影点的坐标
    ix = x1 + u * (x2 - x1)
    iy = y1 + u * (y2 - y1)
    return point_to_point(px, py, ix, iy),(ix,iy)


# 创建一个空白的黑色图像
image = np.zeros((480, 640, 3), dtype=np.uint8)

# 设置点的坐标
point = (320, 240)

# 设置直线的两个端点
line_start = (100, 100)
line_end = (500, 500)

# 绘制直线
cv2.line(image, line_start, line_end, (255, 255, 255), 2)

# 调用函数绘制点到直线的最短线段
dist,pt_map=point_to_line_dist(point[0],point[1], line_start[0], line_start[1],line_end[0],line_end[1])

cv2.line(image, (point[0], point[1]), (int(pt_map[0]), int(pt_map[1])), (0, 0, 255), 1)

# 显示图像
cv2.imshow('Point to Line Segment', image)
cv2.waitKey(0)
cv2.destroyAllWindows()