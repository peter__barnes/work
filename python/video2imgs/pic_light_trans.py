# 使用cv2.addWeighted（）等函数，批量调整图像的亮度/对比度、颜色，实现数据增强
#
# 
import cv2 as cv
import numpy as np
import xml.etree.ElementTree as ET
import shutil
import os
import random
from edge_extract import canny_edge_extract
from cfg import MACHINE_ENUM,MACHINE_TYPE,DATA_EX_DIR
from PIL import Image


def contrast_brightness_demo(image, c, b):            # 定义方法， c @ contrast  对比度 ; b @ brightness 亮度
    h, w, ch = image.shape
    blank = np.zeros([h, w, ch], image.dtype)         # 定义一张空白图像
    dst = cv.addWeighted(image, c, blank, 1-c, b)     # 设定权重
    #cv.imshow("con-bri-demo", dst)
    return dst

def my_blur(image):
    out = cv.blur(image, (3, 3))
    return out

def color_blue(image):
    image[:, :, 0] = image[:, :, 0]*0.75 + 25+25*random.random()
    return image

def color_green(image):
    image[:, :, 1] = image[:, :, 1]*0.75 + 10+20*random.random()
    return image

def color_red(image):
    image[:, :, 2] = image[:, :, 2]*0.75 + 15+30*random.random()
    return image

def light_trans_test():
    src = cv.imread("E:/data/error_data/img1022.jpg")  # 读取图片
    #cv.imshow("input image", src)                # 显示原图片

    dst = color_green(src)
    cv.imwrite("E:/data/error_data/trans_green.jpg", dst)
    '''
    #contrast_brightness_demo(src, 1.2, 1.2)      # 调用方法，在原图的基础上改变亮度/对比度的值
    dst = contrast_brightness_demo(src, 0.4, 0.4)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite("E:/data/error_data/trans1.JPG",dst)
    dst = contrast_brightness_demo(src, 0.7, 0.7)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite("E:/data/error_data/trans2.JPG",dst)
    dst = contrast_brightness_demo(src, 1.2, 1.2)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite("E:/data/error_data/trans3.JPG",dst)
    dst = contrast_brightness_demo(src, 1.6, 1.6)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite("E:/data/error_data/trans4.JPG",dst)
    '''
    #cv.waitKey(0)                  # 保持对话框
    #cv.destroyWindow()             # 清除内存

#IMG_PATH ='D:/data/error_data/JPEGImages/'
#ANN_PATH ='D:/data/error_data/Annotations/'
IMG_PATH=''
if(MACHINE_TYPE.value == MACHINE_ENUM.B325.value):
    IMG_PATH =DATA_EX_DIR+'/JPEGImages/'
    ANN_PATH =DATA_EX_DIR+'/Annotations/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
    IMG_PATH =DATA_EX_DIR+'/JPEGImages/'
    ANN_PATH =DATA_EX_DIR+'/Annotations/'
else:
    IMG_PATH =DATA_EX_DIR+'/JPEGImages/'
    ANN_PATH =DATA_EX_DIR+'/Annotations/'

#IMG_PATH = 'E:/PStest/wallpaper/'

perms = ((0, 1, 2), (0, 2, 1),
              (1, 0, 2), (1, 2, 0),
              (2, 0, 1), (2, 1, 0))

def read_xml_annotation(root, image_id):
    in_file = open(os.path.join(root, image_id))
    tree = ET.parse(in_file)
    root = tree.getroot()
    bndboxlist = []
    lable_name_list = []
    for object in root.findall('object'):  # 找到root节点下的所有object节点
        lable_name = object.find('name')
        bndbox = object.find('bndbox')  # 子节点下节点rank的值

        xmin = int(bndbox.find('xmin').text)
        xmax = int(bndbox.find('xmax').text)
        ymin = int(bndbox.find('ymin').text)
        ymax = int(bndbox.find('ymax').text)
        # print(xmin,ymin,xmax,ymax)
        bndboxlist.append([xmin, ymin, xmax, ymax])
        # print(bndboxlist)
        lable_name_list.append(lable_name)
    try:
        bndbox = root.find('object').find('bndbox')
    except:
        pass
    return lable_name_list,bndboxlist

def has_badtap(lable_names):
    lable_name_rets = []
    bndboxs_ret = []
    #has_bad_tap =False
    for lable_name in lable_names:
        if('bad_tape'==lable_name.text):
            #has_bad_tap=True
            return True
    return False

def light_trans(src_name):
    src_file_name = src_name+'.jpg'
    src = cv.imread(IMG_PATH+src_file_name)  # 读取图片
    #cv.imshow("input image", src)                # 显示原图片

    #contrast_brightness_demo(src, 1.2, 1.2)      # 调用方法，在原图的基础上改变亮度/对比度的值
    dst = contrast_brightness_demo(src, 0.4, 0.4)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite(IMG_PATH+'bt_04_'+src_file_name,dst)
    dst = contrast_brightness_demo(src, 0.7, 0.7)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite(IMG_PATH+'bt_07_'+src_file_name,dst)
    dst = contrast_brightness_demo(src, 1.3, 1.3)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite(IMG_PATH+'bt_13_'+src_file_name,dst)
    dst = contrast_brightness_demo(src, 1.6, 1.6)      # 调用方法，在原图的基础上改变亮度/对比度的值
    cv.imwrite(IMG_PATH+'bt_16_'+src_file_name,dst)

    anno_file_name = src_name+'.xml'
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_04_'+anno_file_name)
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_07_'+anno_file_name)
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_13_'+anno_file_name)
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_16_'+anno_file_name)

'''
def color_demo(image):
    gray = image[:, :, -1]
    return gray
'''
list_randcolor = [  cv.COLORMAP_AUTUMN ,
        cv.COLORMAP_BONE ,
        cv.COLORMAP_WINTER ,
        cv.COLORMAP_OCEAN ,
        cv.COLORMAP_SUMMER ,
        #cv.COLORMAP_SPRING ,
        cv.COLORMAP_PINK ,
        cv.COLORMAP_HOT
]

def color_demo(image):
    '''
    change=image
    change[:, :, 0] = image[:, :, 1].copy()
    change[:, :, 1] = image[:, :, 2].copy()
    change[:, :, 2] = image[:, :, 0].copy()
    '''
    color_select = random.sample(list_randcolor, 1)
    change = cv.applyColorMap(image, color_select[0])
    #change = Image.blend(change, image, 0.3)
    cv.addWeighted(change, 0.4, image, 0.6, 0, change)
    #change = cv.cvtColor(change, cv.COLOR_XYZ2BGR)
    return change

def color_demo2(image):
    '''
    change=image
    change[:, :, 0] = image[:, :, 1].copy()
    change[:, :, 1] = image[:, :, 2].copy()
    change[:, :, 2] = image[:, :, 0].copy()
    '''
    color_select = random.sample(list_randcolor, 1)
    change = cv.applyColorMap(image, color_select[0])
    cv.addWeighted(change, 0.4, image, 0.6, 0, change)
    #change = cv.cvtColor(change, cv.COLORMAP_WINTER)
    return change

def color_demo3(image):
    '''
    change=image
    change[:, :, 0] = image[:, :, 2].copy()
    change[:, :, 1] = image[:, :, 1].copy()
    change[:, :, 2] = image[:, :, 0].copy()
    '''
    color_select = random.sample(list_randcolor, 1)
    change = cv.applyColorMap(image, color_select[0])
    cv.addWeighted(change, 0.4, image, 0.6, 0, change)
    #change = cv.cvtColor(change, cv.COLORMAP_WINTER)
    return change

def color_trans(src_name):
    src_file_name = src_name+'.jpg'
    src = cv.imread(IMG_PATH+src_file_name)  # 读取图片
    dst = color_demo(src)
    cv.imwrite(IMG_PATH+'bt_color1_'+src_file_name,dst)
    dst = color_demo2(src)
    cv.imwrite(IMG_PATH+'bt_color2_'+src_file_name,dst)

    anno_file_name = src_name+'.xml'
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_color1_'+anno_file_name)
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_color2_'+anno_file_name)

def gasuss_noise(image, mu=0.0, sigma=0.1):
    """
     添加高斯噪声
    :param image: 输入的图像
    :param mu: 均值
    :param sigma: 标准差
    :return: 含有高斯噪声的图像
    """
    image = np.array(image / 255, dtype=float)
    noise = np.random.normal(mu, sigma, image.shape)
    gauss_noise = image + noise
    if gauss_noise.min() < 0:
        low_clip = -1.
    else:
        low_clip = 0.
    gauss_noise = np.clip(gauss_noise, low_clip, 1.0)
    gauss_noise = np.uint8(gauss_noise * 255)
    return gauss_noise


def annotation_has_badtape(file_path):
    in_file = open(file_path)
    tree = ET.parse(in_file)
    root = tree.getroot()
    bndboxlist = []
    lable_name_list = []
    for object in root.findall('object'):  # 找到root节点下的所有country节点
        lable_name = object.find('name')
        if(lable_name.text=='bad_tape'):
            return True
    return False

def rand_trans(src_name, del_origin = False):
    #image = data_dict["image"]
    xml_file = os.path.join(ANN_PATH, src_name + '.xml')
    has_badtape = annotation_has_badtape(xml_file)
    if has_badtape:
        return

    src_file_name = src_name+'.jpg'
    src = cv.imread(IMG_PATH+src_file_name)  # 读取图片

    rnd_num = random.random()

    rand_brightness = 0.6 + random.random() * 0.6
    #print(rand_brightness)
    dst = contrast_brightness_demo(src, rand_brightness, rand_brightness)
    rand_brightness = 0.6 + random.random() * 0.6
    dst2 = contrast_brightness_demo(src, rand_brightness, rand_brightness)
    #rand_brightness = 0.8 + random.random() * 0.4
    #dst3 = contrast_brightness_demo(src, rand_brightness, rand_brightness)
    if rnd_num > 0.75:
    #if True:
        dst = color_demo(dst)
        #dst2 = canny_edge_extract(dst2)
        #dst2 = color_demo2(dst2)
    elif rnd_num > 0.5:
        dst = color_demo2(dst)
        #dst2 = canny_edge_extract(dst2)
        #dst2 = color_demo3(dst2)
    elif rnd_num > 0.25:
        dst = color_demo3(dst)
        #dst2 = color_demo(dst2)
    elif rnd_num > 0.125:
        dst = color_blue(dst)
    else:
        dst = color_green(dst)
        #dst2 = color_blue(dst)

    #dst2 = my_blur(dst2)
    #添加高斯噪声
    #dst = gasuss_noise(dst,sigma=0.002)
    #dst3 = canny_edge_extract(dst)
    cv.imwrite(IMG_PATH + 'bt_rnd1_' + src_file_name, dst)
    #cv.imwrite(IMG_PATH + 'bt_rnd2_' + src_file_name, dst2)
    #cv.imwrite(IMG_PATH + 'bt_rnd3_' + src_file_name, dst3)
    anno_file_name = src_name + '.xml'
    shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_rnd1_'+anno_file_name)

    #shutil.copyfile(ANN_PATH+anno_file_name, ANN_PATH+'bt_rnd2_'+anno_file_name)
    #shutil.copyfile(ANN_PATH + anno_file_name, ANN_PATH + 'bt_rnd3_' + anno_file_name)
    if del_origin:
        os.remove(IMG_PATH+src_file_name)
        os.remove(ANN_PATH+anno_file_name)


def auto_rename():
    for aroot, dirs, files in os.walk("D:\\data\\tmp\\"):
        for item in files:
            if item.startswith('common_test_'):
                (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(item))
                newNameWithoutExtention = nameWithoutExtention[12:]
                os.rename("D:\\data\\tmp\\"+nameWithoutExtention+extention, "D:\\data\\tmp\\bad_img_2_"+newNameWithoutExtention+extention)

def del_trans_img():
    for aroot, dirs, files in os.walk(IMG_PATH):
        for item in files:
            if item.startswith('bt_'):
                os.remove(IMG_PATH+item)
    for aroot, dirs, files in os.walk(ANN_PATH):
        for item in files:
            if item.startswith('bt_'):
                os.remove(ANN_PATH+item)
    #后缀统一处理为小写，避免后续预处理失败
    for aroot, dirs, files in os.walk(IMG_PATH):
        filelist = files
        for item in filelist:
            if item.endswith('.JPG'):
                (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(item))
                os.rename(IMG_PATH+nameWithoutExtention+'.JPG', IMG_PATH+nameWithoutExtention+'.jpg')
                print('rename:%s ok'%(nameWithoutExtention+'.JPG'))



def create_trans_img():
    for aroot, dirs, files in os.walk(IMG_PATH):
        # aroot是self.path目录下的所有子目录（含self.path）,dir是self.path下所有的文件夹的列表.
        filelist = files  # 注意此处仅是该路径下的其中一个列表
        # print('list', list)

        # filelist = os.listdir(self.path) #获取文件路径
        total_num = len(filelist)  # 获取文件长度（个数）
        img_list_need_trans = random.sample(filelist, int(len(filelist) / 2))
        for item in img_list_need_trans:
            if item.startswith('bt_'):
                # 跳过已转换的不处理
                continue
            if item.startswith('test_'):
                # 跳过测试集不处理
                continue
            # if item.startswith('test_'):
            # 跳过测试专用图片不处理
            # continue
            if item.endswith('.jpg'):  # 初始的图片的格式为jpg格式的（或者源文件是png格式及其他格式，后面的转换格式就可以调整为自己需要的格式即可）
                # src = os.path.join(os.path.abspath(aroot), item)
                # 读取标签信息
                (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(item))
                xml_nameA = nameWithoutExtention + '.xml'
                img_lables, _ = read_xml_annotation(ANN_PATH, xml_nameA)

                # 一次读取一对XML信息
                if has_badtap(img_lables):  # 跳过封带不良
                    continue
                src = os.path.splitext(item)[0]
                print(src)
                # light_trans(src)
                # color_trans(src)
                rand_trans(src,del_origin = True)

if __name__ == "__main__":
    #light_trans_test()
    #auto_rename()
    #exit()

    for aroot, dirs, files in os.walk(IMG_PATH):
        for item in files:
            if item.startswith('bt_'):
                os.remove(IMG_PATH+item)
    for aroot, dirs, files in os.walk(ANN_PATH):
        for item in files:
            if item.startswith('bt_'):
                os.remove(ANN_PATH+item)
    '''
    #后缀统一处理为小写，避免后续预处理失败
    for aroot, dirs, files in os.walk(IMG_PATH):
        filelist = files
        for item in filelist:
            if item.endswith('.JPG'):
                (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(item))
                os.rename(IMG_PATH+nameWithoutExtention+'.JPG', IMG_PATH+nameWithoutExtention+'.jpg')
                print('rename:%s ok'%(nameWithoutExtention+'.JPG'))
    '''

    del_trans_img()
    create_trans_img()

    #light_trans("raw")


