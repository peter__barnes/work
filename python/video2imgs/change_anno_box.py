import cv2 as cv
import numpy as np
import shutil
import os
import random
import xml.etree.ElementTree as ET
from obj_exchange import read_xml_annotation,read_xml_size
from cfg import *

DATA_DIR = 'D:/data/VOC_detectBHJ2/'
#DATA_DIR='D:/data/VOC_detectBHJ2/'
IMG_DIR = DATA_DIR+'JPEGImages/'
ANNO_DIR = DATA_DIR+'Annotations2/'

def if_has_x_in_range_right(lst_bndbox,width):
    for bbox in lst_bndbox:
        #if bbox[0] > 105 and bbox[0] < 197:
        #if bbox[2] > 150 and bbox[0] < 258:
        if (width - bbox[2]) < (640- 500):
            #print(bbox[2])
            return True
    return False

def skip_check(file_name):
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    xml_file = ANNO_DIR + nameWithoutExtention + '.xml'
    lable_name, bndbox = read_xml_annotation(ANNO_DIR, nameWithoutExtention + '.xml')
    width,height = read_xml_size(ANNO_DIR, nameWithoutExtention + '.xml')

    b_has_x_in_range = if_has_x_in_range_right(bndbox,width)
    return (not b_has_x_in_range)

def move_edge(file_name,move_pix=8,only_right=False):
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    xml_file = ANNO_DIR + nameWithoutExtention + '.xml'

    img_file = IMG_DIR + file_name
    img = cv.imread(img_file)
    img_h, img_w = img.shape[:2]

    obj_max_x = obj_min_x = int (img_w/2)
    tree = ET.parse(xml_file)
    root = tree.getroot()
    # if root.find('object') == None:
    #     return
    obj_cnt = 0
    #find max_x in all objects
    for obj in root.iter('object'):
        obj_cnt = obj_cnt + 1
        #cls = obj.find('name').text
        xmlbox = obj.find('bndbox')
        xmax = int(xmlbox.find('xmax').text)
        xmin = int(xmlbox.find('xmin').text)
        if xmax > obj_max_x:
            obj_max_x = xmax
        if xmin < obj_min_x:
            obj_min_x = xmin
        #b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
             #int(float(xmlbox.find('xmax').text)),
             #int(float(xmlbox.find('ymax').text))]
    for obj in root.iter('object'):
        xmlbox = obj.find('bndbox')
        xmax = int(xmlbox.find('xmax').text)
        xmin = int(xmlbox.find('xmin').text)
        if xmax == obj_max_x:
            xmax = max(xmax - move_pix,xmin)
            xmlbox = obj.find('bndbox')
            xmlbox.find('xmax').text = str(int(xmax))
            print('write:'+xml_file)
            tree.write(xml_file)
            return

if __name__ == "__main__":
    for aroot, dirs, files in os.walk(IMG_DIR):
        filelist = files
        for item in filelist:
            if item.endswith('.jpg'):
                if skip_check(item):
                    continue
                move_edge(item,move_pix=8,only_right = True)