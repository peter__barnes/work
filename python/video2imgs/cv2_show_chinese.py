import cv2
import numpy as np
import numpy
from PIL import Image, ImageDraw, ImageFont
def cv2ImgAddText(img, text, left, top, textColor=(0, 255, 0), textSize=20):
  if (isinstance(img, numpy.ndarray)): # 判断是否OpenCV图片类型
    img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
  # 创建一个可以在给定图像上绘图的对象
  draw = ImageDraw.Draw(img)
  # 字体的格式
  fontStyle = ImageFont.truetype(
    "font/simsun.ttc", textSize, encoding="utf-8")
  # 绘制文本
  draw.text((left, top), text, textColor, font=fontStyle)
  # 转换回OpenCV格式
  return cv2.cvtColor(numpy.asarray(img), cv2.COLOR_RGB2BGR)

# 创建一个黑色图像
img = np.zeros((512, 512, 3), np.uint8)



# 绘制中文文字
#cv2.putText(img, '中国智', (50, 50), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
img = cv2ImgAddText(img, "大家好11", 50, 50, (255, 255, 255), 20)

# 显示图像
cv2.imshow('Image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
