'''
Author: CodingWZP
Email: codingwzp@gmail.com
Date: 2021-08-06 10:51:35
LastEditTime: 2021-08-09 10:53:43
Description: Image augmentation with label.
'''
import distutils.errors
import xml.etree.ElementTree as ET
import os
import imgaug as ia
import numpy as np
import shutil
from tqdm import tqdm
from PIL import Image
from imgaug import augmenters as iaa
import random
import cv2 as cv
from mix_img import *
from rm_jpg_and_anno_by_label import rm_bad_tape_slight,rm_bad_tape_all
from cfg import *
ia.seed(1)

import imgaug
import imgaug.augmenters as iaa
def read_xml_size(root,image_id):
    in_file = open(os.path.join(root, image_id))
    tree = ET.parse(in_file)
    root = tree.getroot()
    width = 0
    height = 0
    for object in root.findall('size'):
        width = int(object.find('width').text)
        height = int(object.find('height').text)
    return width,height

def read_xml_annotation(root, image_id):
    in_file = open(os.path.join(root, image_id))
    tree = ET.parse(in_file)
    root = tree.getroot()
    bndboxlist = []
    lable_name_list = []
    for object in root.findall('object'):  # 找到root节点下的所有object节点
        lable_name = object.find('name')
        bndbox = object.find('bndbox')  # 子节点下节点rank的值

        xmin = int(bndbox.find('xmin').text)
        xmax = int(bndbox.find('xmax').text)
        ymin = int(bndbox.find('ymin').text)
        ymax = int(bndbox.find('ymax').text)
        # print(xmin,ymin,xmax,ymax)
        bndboxlist.append([xmin, ymin, xmax, ymax])
        # print(bndboxlist)
        lable_name_list.append(lable_name)
    try:
        bndbox = root.find('object').find('bndbox')
    except:
        pass
    return lable_name_list,bndboxlist

# (506.0000, 330.0000, 528.0000, 348.0000) -> (520.4747, 381.5080, 540.5596, 398.6603)
def change_xml_annotation(root, image_id, new_target):
    new_xmin = new_target[0]
    new_ymin = new_target[1]
    new_xmax = new_target[2]
    new_ymax = new_target[3]

    in_file = open(os.path.join(root, str(image_id) + '.xml'))  # 这里root分别由两个意思
    tree = ET.parse(in_file)
    xmlroot = tree.getroot()
    object = xmlroot.find('object')
    bndbox = object.find('bndbox')
    xmin = bndbox.find('xmin')
    xmin.text = str(new_xmin)
    ymin = bndbox.find('ymin')
    ymin.text = str(new_ymin)
    xmax = bndbox.find('xmax')
    xmax.text = str(new_xmax)
    ymax = bndbox.find('ymax')
    ymax.text = str(new_ymax)
    tree.write(os.path.join(root, str("%06d" % (str(id) + '.xml'))))

def change_xml_list_annotation(root, image_id, new_target, saveroot, id):
    in_file = open(os.path.join(root, str(image_id) + '.xml'))  # 这里root分别由两个意思
    tree = ET.parse(in_file)
    # 修改增强后的xml文件中的filename
    elem = tree.find('filename')
    elem.text = (str(id) + '.jpg')
    xmlroot = tree.getroot()
    # 修改增强后的xml文件中的path
    elem = tree.find('path')
    if elem != None:
        elem.text = (saveroot + str(id) + '.jpg')

    index = 0
    for object in xmlroot.findall('object'):  # 找到root节点下的所有country节点
        bndbox = object.find('bndbox')  # 子节点下节点rank的值

        # xmin = int(bndbox.find('xmin').text)
        # xmax = int(bndbox.find('xmax').text)
        # ymin = int(bndbox.find('ymin').text)
        # ymax = int(bndbox.find('ymax').text)

        new_xmin = new_target[index][0]
        new_ymin = new_target[index][1]
        new_xmax = new_target[index][2]
        new_ymax = new_target[index][3]

        xmin = bndbox.find('xmin')
        xmin.text = str(new_xmin)
        ymin = bndbox.find('ymin')
        ymin.text = str(new_ymin)
        xmax = bndbox.find('xmax')
        xmax.text = str(new_xmax)
        ymax = bndbox.find('ymax')
        ymax.text = str(new_ymax)

        index = index + 1

    tree.write(os.path.join(saveroot, str(id + '.xml')))


def mkdir(path):
    # 去除首位空格
    path = path.strip()
    # 去除尾部 \ 符号
    path = path.rstrip("\\")
    # 判断路径是否存在
    # 存在     True
    # 不存在   False
    isExists = os.path.exists(path)
    # 判断结果
    if not isExists:
        # 如果不存在则创建目录
        # 创建目录操作函数
        os.makedirs(path)
        print(path + ' 创建成功')
        return True
    else:
        # 如果目录存在则不创建，并提示目录已存在
        print(path + ' 目录已存在')
        return False



#补充数据路径
#SUPP_DIR = 'D:/data/VOC_detect/'

IMG_PATH = DATA_DIR+'JPEGImages/'
ANN_PATH = DATA_DIR+'Annotations/'
IMG_SAVE_PATH = DATA_DIR+'/exchanged/JPEGImages/'
ANN_SAVE_PATH = DATA_DIR+'/exchanged/Annotations/'

def imgBG_fill_imgCell(save_path, img_filename,img_bg,img_cell,start_x,start_y):
    np_img_bg = np.array(img_bg)
    np_img_cell = np.array(img_cell)

    orininal_h = np_img_cell.shape[0]  # 获得图片1的高
    orininal_w = np_img_cell.shape[1]  # 获得图片1的宽
    for j in range(orininal_h):  # 循环遍历所有像素点
        for k in range(orininal_w):
            #np_img_bg[bndboxA[0][1] + j, bndboxA[0][0] + k] = np_img_cell[j, k]  # 将img1的图像该位置像素替换成img2
            np_img_bg[start_y + j, start_x + k] = np_img_cell[j, k]  # 将img1的图像该位置像素替换成img2

    img_ret = Image.fromarray(np_img_bg.astype('uint8')).convert('RGB')
    #img_ret.save(os.path.join(save_path, img_filename))
    return img_ret

def change_xml_annotation_cell_name(tree, cnt, new_name):
    root = tree.getroot()
    objects = root.findall('object')
    #for object in objects:
        #print(object.find('name').text)
    lable_name = objects[cnt].find('name')
    lable_name.text = new_name
    #tree.write(os.path.join(write_path, str(image_name[:-4]) + '.xml'))
    #objects[cnt].setAttribute('name', new_name)
    #print(objects[cnt].find('name').text)
    #return tree

def has_badtap(lable_names):
    lable_name_rets = []
    bndboxs_ret = []
    #has_bad_tap = False
    for lable_name in lable_names:
        if('bad_tape'== lable_name.text or 'bad_tape_slight'== lable_name.text):
            #has_bad_tap=True
            return True
    return False


def exchange_img_and_ann(img_A,img_B):
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(img_A))
    xml_nameA = nameWithoutExtention + '.xml'
    lable_nameA, bndboxA = read_xml_annotation(XML_DIR, xml_nameA)
    #print(bndboxA)

    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(img_B))
    xml_nameB = nameWithoutExtention + '.xml'
    lable_nameB, bndboxB = read_xml_annotation(XML_DIR, xml_nameB)

    #print(bndboxB)

    exchange_num = min(max(int(len(lable_nameA) / 2), int(len(lable_nameB) / 2)), len(lable_nameA), len(lable_nameB))
    print('try to exchange:%s,%s, %d cells'%(xml_nameA,xml_nameB,exchange_num))
    change_idx_A = random.sample(range(0, len(lable_nameA)), exchange_num)
    change_idx_B = random.sample(range(0, len(lable_nameB)), exchange_num)
    print(change_idx_A, change_idx_B)

    # 交换图像：1.取两个元件图片
    # bndboxA[0]
    # 一次读取一对图片
    imgA = Image.open(os.path.join(IMG_DIR, img_A))
    imgB = Image.open(os.path.join(IMG_DIR, img_B))

    #一次读取一对XML信息
    in_fileA = open(os.path.join(XML_DIR, str(img_A[:-4]) + '.xml'))  # 这里root分别由两个意思
    treeA = ET.parse(in_fileA)
    in_fileB = open(os.path.join(XML_DIR, str(img_B[:-4]) + '.xml'))  # 这里root分别由两个意思
    treeB = ET.parse(in_fileB)

    #root = treeA.getroot()
    #objects = root.findall('object')
    #for object in objects:
        #print(object.find('name').text)

    '''
    if has_badtap(lable_nameA) or has_badtap(lable_nameB):
        print('has badtap , copy file and skip!!')
        imgA.save(os.path.join(IMG_SAVE_PATH, img_A))
        imgB.save(os.path.join(IMG_SAVE_PATH, img_B))
        treeA.write(os.path.join(ANN_SAVE_PATH, str(img_A[:-4]) + '.xml'))
        treeB.write(os.path.join(ANN_SAVE_PATH, str(img_B[:-4]) + '.xml'))
        return
    '''
    for idx_A, idx_B in zip(change_idx_A,change_idx_B):

        # sp = img.size
        # img = np.asarray(img)
        # bndbox 坐标增强
        croppedA = imgA.crop((bndboxA[idx_A][0], bndboxA[idx_A][1], bndboxA[idx_A][2], bndboxA[idx_A][3]))
        # croppedA.show()

        # sp = img.size
        # img = np.asarray(img)
        # 读取要替换的单元
        croppedB = imgB.crop((bndboxB[idx_B][0], bndboxB[idx_B][1], bndboxB[idx_B][2], bndboxB[idx_B][3]))

        A_size = croppedA.size
        B_size = croppedB.size

        croppedA = croppedA.resize(B_size)
        croppedB = croppedB.resize(A_size)

        #将图像B抠出来的元件旋转180度数据增强
        #改变方向标记
        if(lable_nameB[idx_B] is not None):
            if 'good' == lable_nameB[idx_B].text:
                lable_nameB[idx_B].text = 'revert'
            elif 'revert' == lable_nameB[idx_B].text:
                lable_nameB[idx_B].text = 'good'
        # 旋转图片
        croppedB = croppedB.transpose(Image.ROTATE_180)  # 引用固定的常量值
        #croppedB.show()

        imgA = np.array(imgA)
        croppedB = np.array(croppedB)

        imgB = np.array(imgB)
        croppedA = np.array(croppedA)

        imgA = imgBG_fill_imgCell(IMG_SAVE_PATH, img_A, imgA, croppedB, bndboxA[idx_A][0], bndboxA[idx_A][1])
        imgB = imgBG_fill_imgCell(IMG_SAVE_PATH, img_B, imgB, croppedA, bndboxB[idx_B][0], bndboxB[idx_B][1])

        #将变更的XML结果写入新路径
        change_xml_annotation_cell_name(treeA, idx_A, lable_nameB[idx_B].text)
        change_xml_annotation_cell_name(treeB, idx_B, lable_nameA[idx_A].text)

    imgA.save(os.path.join(IMG_SAVE_PATH, img_A))
    imgB.save(os.path.join(IMG_SAVE_PATH, img_B))
    treeA.write(os.path.join(ANN_SAVE_PATH, str(img_A[:-4]) + '.xml'))
    treeB.write(os.path.join(ANN_SAVE_PATH, str(img_B[:-4]) + '.xml'))
        #change_xml_annotation_cell_name(ANN_PATH, ANN_SAVE_PATH, img_select[1], 0, lable_nameA[idx_A].text)

def copy_dir(src_path,dest_path):
    for file in os.listdir(src_path):
        # 遍历原文件夹中的文件
        full_file_name = os.path.join(src_path, file)  # 把文件的完整路径得到
        print("要被复制的全文件路径全名:", full_file_name)
        if os.path.isfile(full_file_name):  # 用于判断某一对象(需提供绝对路径)是否为文件
            shutil.copy(full_file_name, dest_path)  # shutil.copy函数放入原文件的路径文件全名  然后放入目标文件夹

def copy_dir_rename_file(src_path,dest_path):
    for file in os.listdir(src_path):
        # 遍历原文件夹中的文件
        full_file_name = os.path.join(src_path, file)  # 把文件的完整路径得到
        print("要被复制的全文件路径全名:", full_file_name)
        if os.path.isfile(full_file_name):  # 用于判断某一对象(需提供绝对路径)是否为文件
            if(file.startswith('test')):
                continue
            else:
                shutil.copy(full_file_name, dest_path+"BQJ_"+ file)  # shutil.copy函数放入原文件的路径文件全名  然后放入目标文件夹

def flip_img_obj(img):
    image_pre, ext = os.path.splitext(img)
    img_file = IMG_DIR + img
    img = cv.imread(img_file)
    img_out = img
    xml_file = XML_DIR + image_pre + '.xml'

    tree = ET.parse(xml_file)
    root = tree.getroot()
    # if root.find('object') == None:
    #     return
    obj_i = 0
    for obj in root.iter('object'):
        cls = obj.find('name').text
        xmlbox = obj.find('bndbox')
        b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
             int(float(xmlbox.find('xmax').text)),
             int(float(xmlbox.find('ymax').text))]
        try:
            img_cut = img[b[1]:b[3], b[0]:b[2], :]
            img_cut = cv.rotate(img_cut, cv.ROTATE_180)
            img_out[b[1]:b[3], b[0]:b[2], :] = img_cut

            lable_name = obj.find('name')
            if (lable_name is not None):
                if 'good' == lable_name.text:
                    lable_name.text = 'revert'
                elif 'revert' == lable_name.text:
                    lable_name.text = 'good'
            # img_cut = cv2.resize(img_cut, (640, 480))
        except:
            print('except:', '{}_{:0>2d}.jpg'.format(image_pre, obj_i))
            continue
        # path = os.path.join(cut_path, cls)
        # 目录是否存在,不存在则创建
        # mkdirlambda = lambda x: os.makedirs(x) if not os.path.exists(x) else True
        # mkdirlambda(path)
        obj_i += 1

    tree.write(os.path.join(XML_DIR, '{}_flip.xml'.format(image_pre)))
    cv.imwrite(os.path.join(IMG_DIR, '{}_flip.jpg'.format(image_pre)), img_out)
    print('flip {}.jpg'.format(image_pre))

def flip_img_obj2(img,replace = False):
    image_pre, ext = os.path.splitext(img)
    img_file = IMG_DIR + img
    img = cv.imread(img_file)
    img_out = cv.rotate(img, cv.ROTATE_180)
    xml_file = XML_DIR + image_pre + '.xml'

    height = img.shape[0]  # 获得图片1的高
    width = img.shape[1]  # 获得图片1的宽

    tree = ET.parse(xml_file)
    root = tree.getroot()
    # if root.find('object') == None:
    #     return
    obj_i = 0
    for obj in root.iter('object'):
        cls = obj.find('name').text
        xmlbox = obj.find('bndbox')
        b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
             int(float(xmlbox.find('xmax').text)),
             int(float(xmlbox.find('ymax').text))]
        img_xmin =int(xmlbox.find('xmin').text)
        img_ymin =int(xmlbox.find('ymin').text)
        img_xmax =int(xmlbox.find('xmax').text)
        img_ymax =int(xmlbox.find('ymax').text)
        xmlbox.find('xmin').text = str(width - img_xmax)
        xmlbox.find('xmax').text = str(width - img_xmin)
        xmlbox.find('ymin').text = str(height - img_ymax)
        xmlbox.find('ymax').text = str(height - img_ymin)
        try:
            lable_name = obj.find('name')
            if (lable_name is not None):
                if 'good' == lable_name.text:
                    lable_name.text = 'revert'
                elif 'revert' == lable_name.text:
                    lable_name.text = 'good'
            # img_cut = cv2.resize(img_cut, (640, 480))
        except:
            print('except:', '{}_{:0>2d}.jpg'.format(image_pre, obj_i))
            continue
        obj_i += 1
    if not replace:
        tree.write(os.path.join(XML_DIR, '{}_flip.xml'.format(image_pre)))
        cv.imwrite(os.path.join(IMG_DIR, '{}_flip.jpg'.format(image_pre)), img_out)
    else:
        tree.write(os.path.join(XML_DIR, '{}.xml'.format(image_pre)))
        cv.imwrite(os.path.join(IMG_DIR, '{}.jpg'.format(image_pre)), img_out)
    print('flip {}.jpg'.format(image_pre))

def flip_imgs_obj():
    img_list = os.listdir(IMG_DIR)
    for img in img_list:
        if img.startswith('test_'):
            continue
        else:
            flip_img_obj2(img)

def rand_flip_imgs_obj():
    img_list = os.listdir(IMG_DIR)
    img_list_need_flip = random.sample(img_list, int(len(img_list)/2))
    for img in img_list_need_flip:
        if img.startswith('test_'):
            continue
        else:
            flip_img_obj2(img, replace=True)

def dup_img_without_bad(img,times = 1):
    image_pre, ext = os.path.splitext(img)
    img_file = IMG_DIR + img
    img = cv.imread(img_file)
    xml_file = XML_DIR + image_pre + '.xml'

    height = img.shape[0]  # 获得图片1的高
    width = img.shape[1]  # 获得图片1的宽

    tree = ET.parse(xml_file)
    root = tree.getroot()
    obj_i = 0
    b_need_dup = True
    for obj in root.iter('object'):
        cls = obj.find('name').text
        try:
            lable_name = obj.find('name')
            if (lable_name is not None):
                if ('good' != lable_name.text and 'revert' != lable_name.text and 'empty' != lable_name.text):
                    b_need_dup=False
        except:
            print('except:', '{}_{:0>2d}.jpg'.format(image_pre, obj_i))
            continue
        obj_i += 1
    if b_need_dup:
        for i in range(1,times+1):
            tree.write(os.path.join(XML_DIR, '{}_{}.xml'.format(image_pre, i)))
            cv.imwrite(os.path.join(IMG_DIR, '{}_{}.jpg'.format(image_pre, i)), img)
        print('dup {}.jpg'.format(image_pre))

def dup_img(img,times = 1):
    image_pre, ext = os.path.splitext(img)
    img_file = IMG_DIR + img
    img = cv.imread(img_file)
    xml_file = XML_DIR + image_pre + '.xml'
    tree = ET.parse(xml_file)
    for i in range(1, times + 1):
        tree.write(os.path.join(XML_DIR, '{}_{}.xml'.format(image_pre, i)))
        cv.imwrite(os.path.join(IMG_DIR, '{}_{}.jpg'.format(image_pre, i)), img)
    print('dup {}.jpg'.format(image_pre))

def dup_imgs_without_bad(times = 1):
    img_list = os.listdir(IMG_DIR)
    for img in img_list:
        if img.startswith('test_'):
            continue
        else:
            dup_img_without_bad(img, times)

def dup_imgs_slight_bad(times = 1):
    img_list = os.listdir(IMG_DIR)
    for img in img_list:
        if img.startswith('test_slight_bad') or img.startswith('slight_bad'):
            dup_img(img, times)

def dup_imgs_bg(times = 1):
    img_list = os.listdir(IMG_DIR)
    for img in img_list:
        if img.startswith('background_img') or img.startswith('test_background'):
            dup_img(img, times)

def random_exchange(exchange_test,round=2):
    for k in range(0,round):
        img_list = os.listdir(IMG_DIR)
        print(len(img_list))
        img_list_without_test = []
        img_list_test = []
        for img in img_list:
            # 读取标签信息
            (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(img))
            xml_nameA = nameWithoutExtention + '.xml'
            img_lables,_ = read_xml_annotation(XML_DIR, xml_nameA)

            # 一次读取一对XML信息
            if has_badtap(img_lables):  # 跳过封带不良
                continue
            prob = random.randint(1, 100)
            if prob <= 40:  #固定40%图片不互换元件，避免只有封带不良没互换，导致用扣图区分封带不良
                continue

            if img.startswith('background_img') or img.startswith('test_background_img') : #背景图片不参与交换
                pass
            elif img.startswith('test_') or img.startswith('rightcam_') or img.startswith('bt_rnd1_test_')or img.startswith('bt_rnd2_test_'):
                img_list_test.append(img)
            else:
                img_list_without_test.append(img)

        #训练集内部交换
        img_select = random.sample(img_list_without_test, len(img_list_without_test))
        #img_select = img_list
        #print(img_select)
        #for name in img_select:
        for i in range(0,len(img_select),2):
            if(i+1 == len(img_select)):
                continue
            exchange_img_and_ann(img_select[i],img_select[i+1])
            #exchange_img_and_ann('nomark_img0005.jpg', 'multimark_img0395.jpg')

        if(not exchange_test):#如果不需要对测试集进行变换
            continue

        # 测试集内部交换
        img_select = random.sample(img_list_test, len(img_list_test))
        #img_select = img_list
        #print(img_select)
        #for name in img_select:
        for i in range(0,len(img_select),2):
            if(i+1 == len(img_select)):
                continue
            print(i," exchange:",img_select[i],img_select[i+1])
            exchange_img_and_ann(img_select[i],img_select[i+1])

def random_fg():
    img_list = os.listdir(FG_PATH)
    for img in os.listdir(IMG_SAVE_PATH):
        if img.startswith('test_') or img.startswith('bt_rnd1_test_')or img.startswith('bt_rnd2_test_')or img.startswith('bt_rnd3_test_'):
            continue
        else:
            print("mixing:%s"%img)
            random_mix(img,img_list)

if __name__ == "__main__":
    #后缀统一处理为小写，避免后续预处理失败
    for aroot, dirs, files in os.walk(IMG_PATH):
        filelist = files
        for item in filelist:
            if item.endswith('.JPG'):
                (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(item))
                os.rename(IMG_PATH+nameWithoutExtention+'.JPG', IMG_PATH+nameWithoutExtention+'.jpg')
                print('rename:%s ok'%(nameWithoutExtention+'.JPG'))

    if ((not os.path.exists(IMG_SAVE_PATH)) or (not os.path.exists(ANN_SAVE_PATH)) ) :
        mkdir(IMG_SAVE_PATH)
        mkdir(ANN_SAVE_PATH)
        if (MACHINE_TYPE.value == MACHINE_ENUM.BQJ.value):
            copy_dir_rename_file(IMG_PATH, IMG_SAVE_PATH)
            copy_dir_rename_file(ANN_PATH, ANN_SAVE_PATH)
        else:
            copy_dir(IMG_PATH, IMG_SAVE_PATH)
            copy_dir(ANN_PATH, ANN_SAVE_PATH)

    IMG_DIR = IMG_SAVE_PATH
    XML_DIR = ANN_SAVE_PATH

    #rm_bad_tape_slight()
    if (MACHINE_TYPE.value != MACHINE_ENUM.BHJ2.value):
        rm_bad_tape_all()
    #img_tmp.save(os.path.join(IMG_SAVE_PATH, 'bad_img_2_img0200222 .jpg'))
    #exchange_img_and_ann('bad_img_2_img0200.jpg','bad_img_2_img0232.jpg')
    #增加良品比例，避免不良品占比过高
    #dup_imgs_without_bad(times=1)
    if (MACHINE_TYPE.value == MACHINE_ENUM.BHJ2.value):
        dup_imgs_slight_bad(times=2)
        dup_imgs_bg(times=5)
    #翻转一次并改标记
    #if (MACHINE_TYPE.value != MACHINE_ENUM.BHJ2.value):
    #flip_imgs_obj()
    rand_flip_imgs_obj()  #只flip一半的图片，flip后会自动覆盖原图
    if(MACHINE_TYPE.value == MACHINE_ENUM.GOOD.value): #or (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
        exit()

    #随机交换1-2轮
    random_exchange(exchange_test=False,round=1)

    #random_fg()
