import xml.etree.ElementTree as ET
import os
import imgaug as ia
import numpy as np
import shutil
from tqdm import tqdm
from PIL import Image
from imgaug import augmenters as iaa
import random
import cv2 as cv
from mix_img import *
from rm_jpg_and_anno_by_label import rm_bad_tape_slight,rm_bad_tape_all
from cfg import *
from obj_exchange import read_xml_annotation,change_xml_annotation_cell_name,imgBG_fill_imgCell,mkdir


IMG_PATH = DATA_DIR+'JPEGImages_with_tape/'
ANN_PATH = DATA_DIR+'Annotations_with_tape/'
IMG_SAVE_PATH = DATA_DIR+'/merge_bg/JPEGImages/'
ANN_SAVE_PATH = DATA_DIR+'/merge_bg/Annotations/'

BG_PATH = 'E:/data/VOC_detect325/real/'
BG_IMG_FILE = BG_PATH+'real00003.jpg'
BG_ANN_FILE = BG_PATH+'real00003.xml'

def exchange_img_and_ann(img_cell,img_bg,last_cell_cnt = 0):
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(img_cell))
    xml_nameA = nameWithoutExtention + '.xml'
    lable_nameA, bndboxA = read_xml_annotation(ANN_PATH, xml_nameA)
    #print(bndboxA)

    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(img_bg))
    xml_nameB = nameWithoutExtention + '.xml'
    lable_nameB, bndboxB = read_xml_annotation(BG_PATH, xml_nameB)

    #print(bndboxB)

    #exchange_num = min(max(int(len(lable_nameA) / 2), int(len(lable_nameB) / 2)), len(lable_nameA), len(lable_nameB))
    #change_idx_A = random.sample(range(0, len(lable_nameA)), exchange_num)
    #change_idx_B = random.sample(range(0, len(lable_nameB)), exchange_num)
    #print(change_idx_A, change_idx_B)

    # 交换图像：1.取两个元件图片
    # bndboxA[0]
    # 一次读取一对图片
    cell_cnt = 0
    for cell_cnt in range(0,len(lable_nameA)):
        print('try to exchange:%s,cell_cnt:%d'%(xml_nameA,cell_cnt))

        imgA = Image.open(os.path.join(IMG_PATH, img_cell))
        imgB = Image.open(os.path.join(BG_PATH, img_bg))

        #一次读取一对XML信息
        in_fileA = open(os.path.join(ANN_PATH, str(img_cell[:-4]) + '.xml'))  # 这里root分别由两个意思
        treeA = ET.parse(in_fileA)
        in_fileB = open(os.path.join(BG_PATH, str(img_bg[:-4]) + '.xml'))  # 这里root分别由两个意思
        treeB = ET.parse(in_fileB)

        #for idx_A, idx_B in zip(change_idx_A,change_idx_B):

        # sp = img.size
        # img = np.asarray(img)
        # bndbox 坐标增强
        croppedA = imgA.crop((bndboxA[cell_cnt][0], bndboxA[cell_cnt][1], bndboxA[cell_cnt][2], bndboxA[cell_cnt][3]))
        # croppedA.show()

        # sp = img.size
        # img = np.asarray(img)
        # 读取要替换的单元(背景图固定只有一个元件)
        croppedB = imgB.crop((bndboxB[0][0], bndboxB[0][1], bndboxB[0][2], bndboxB[0][3]))

        A_size = croppedA.size
        B_size = croppedB.size

        croppedA = croppedA.resize(B_size)
        croppedB = croppedB.resize(A_size)

        imgA = np.array(imgA)
        croppedB = np.array(croppedB)

        imgB = np.array(imgB)
        croppedA = np.array(croppedA)

        # img_retA = \
        #imgA = imgBG_fill_imgCell(IMG_SAVE_PATH, img_cell, imgA, croppedB, bndboxA[cell_cnt][0], bndboxA[cell_cnt][1])
        imgB = imgBG_fill_imgCell(IMG_SAVE_PATH, img_bg, imgB, croppedA, bndboxB[0][0], bndboxB[0][1])

        # img_ret.show()
        #将变更的XML结果写入新路径
        #treeA =
        #change_xml_annotation_cell_name(treeA, img_cell, lable_nameB[idx_B].text)
        #treeB =
        change_xml_annotation_cell_name(treeB, 0, lable_nameA[cell_cnt].text)

        #imgA.save(os.path.join(IMG_SAVE_PATH, img_cell))
        imgB.save(os.path.join(IMG_SAVE_PATH, str(img_cell[:-4]) + '_merge' +str(last_cell_cnt+cell_cnt) +'.jpg'))
        #treeA.write(os.path.join(ANN_SAVE_PATH, str(img_cell[:-4]) + '.xml'))
        treeB.write(os.path.join(ANN_SAVE_PATH, str(img_cell[:-4]) + '_merge' +str(last_cell_cnt+cell_cnt) + '.xml'))
    return last_cell_cnt+cell_cnt+1


def rm_all_merge_file():
    #遍历图片
    img_list = os.listdir(IMG_PATH)
    for img in img_list:
        if -1 != img.find('merge'):
            #print(img)
            print('rm:%s'%img)
            os.remove(IMG_PATH+img)
    xml_list = os.listdir(ANN_PATH)
    for anno in xml_list:
        if -1 != anno.find('merge'):
            #print(img)
            print('rm:%s'%anno)
            os.remove(ANN_PATH+anno)

def merge_cell_to_bg():
    #(nameWithoutExtention, extention) = os.path.splitext(os.path.basename(BG_IMG_FILE))
    #bg_file_name = nameWithoutExtention + '.jpg'
    #遍历图片
    img_list = os.listdir(IMG_PATH)
    for img in img_list:
        if -1 == img.find('merge'): #已经合成过一次的，不要再合成
            #print(img)
            bg_img_list = os.listdir(BG_PATH)
            last_cell_cnt = 0
            for bg_img in bg_img_list:
                if bg_img.endswith('.jpg'):
                    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(bg_img))
                    bg_file_name = nameWithoutExtention + '.jpg'
                    print('try to merge:%s,%s'%(img,bg_file_name))
                    last_cell_cnt = exchange_img_and_ann(img,bg_file_name,last_cell_cnt)


if __name__ == "__main__":

    for aroot, dirs, files in os.walk(IMG_PATH):
        filelist = files
        for item in filelist:
            if item.endswith('.JPG'):
                (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(item))
                os.rename(IMG_PATH+nameWithoutExtention+'.JPG', IMG_PATH+nameWithoutExtention+'.jpg')
                print('rename:%s ok'%(nameWithoutExtention+'.JPG'))

    if ((not os.path.exists(IMG_SAVE_PATH)) or (not os.path.exists(ANN_SAVE_PATH)) ) :
        mkdir(IMG_SAVE_PATH)
        mkdir(ANN_SAVE_PATH)

    merge_cell_to_bg()
    #rm_all_merge_file()