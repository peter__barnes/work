import cv2 as cv
import numpy as np
import shutil
import os
import random
import xml.etree.ElementTree as ET
from cfg import *

#IMG_DIR = DATA_EX_DIR+'JPEGImages/'
#ANNO_DIR = DATA_EX_DIR+'Annotations/'
#DATA_BI_DIR='D:/data/VOC_detect_good/'
#DATA_BI_DIR='E:/data/exp0919-1bad_tape_tmp/'
DATA_BI_DIR='E:/data/05-16/'
IMG_DIR = DATA_BI_DIR+'JPEGImages/'
ANNO_DIR = DATA_BI_DIR+'Annotations/'

def rm_label_by_pos(file_name,pos):
    xml_file = ANNO_DIR + file_name
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    img_file = IMG_DIR + nameWithoutExtention +'.jpg'
    tree = ET.parse(xml_file)
    root = tree.getroot()

    objects = root.findall('object')
    root.remove(objects[pos])
    tree.write(xml_file)

def rm_label_by_size(file_name):
    xml_file = ANNO_DIR + file_name
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    img_file = IMG_DIR + nameWithoutExtention +'.jpg'
    tree = ET.parse(xml_file)
    root = tree.getroot()

    objects = root.findall('object')
    for obj in objects:
        label = obj.find('name').text
        bndbox = obj.find('bndbox')
        xmin = bndbox.find('xmin')
        ymin = bndbox.find('ymin')
        xmax = bndbox.find('xmax')
        ymax = bndbox.find('ymax')
        obj_height = int(ymax.text) - int(ymin.text)
        obj_width = int(xmax.text) - int(xmin.text)
        #bad_tape 往往长度和普通的不一样,需要设更大的阈值
        if (obj_height/obj_width) > 1.5 and label != 'bad_tape':
            root.remove(obj)
        elif (obj_height/obj_width) > 1.7:
            root.remove(obj)
    tree.write(xml_file)

#当前此函数每张图只剔除第一个重叠的框
def rm_bndbox_overlap(file_name):
    xml_file = ANNO_DIR + file_name
    in_file = open(os.path.join(ANNO_DIR, file_name))
    tree = ET.parse(in_file)
    root = tree.getroot()
    bndboxlist = []
    lable_name_list = []
    for object in root.findall('object'):  # 找到root节点下的所有object节点
        lable_name = object.find('name')
        bndbox = object.find('bndbox')  # 子节点下节点rank的值

        xmin = int(bndbox.find('xmin').text)
        xmax = int(bndbox.find('xmax').text)
        ymin = int(bndbox.find('ymin').text)
        ymax = int(bndbox.find('ymax').text)
        # print(xmin,ymin,xmax,ymax)
        bndboxlist.append([xmin, ymin, xmax, ymax])
        # print(bndboxlist)
        lable_name_list.append(lable_name)

    need_rm_obj = []
    lst_bndbox = bndboxlist
    cnt = 0
    for bbox,obj in zip(lst_bndbox,root.findall('object')):
        for i in range(0,cnt):
            if cnt == i:
                continue
            if bbox[0] > lst_bndbox[i][0] and bbox[0] < lst_bndbox[i][2]:
                need_rm_obj.append(obj)
                #return True
            elif bbox[2] > lst_bndbox[i][0] and bbox[0] < lst_bndbox[i][2]:
                need_rm_obj.append(obj)
                #return True
        cnt = cnt + 1

    for obj in need_rm_obj:
        # objects.remove(obj)
        root.remove(obj)
    if len(need_rm_obj) > 0:
        print('remove objs:%s' % xml_file)
        tree.write(xml_file)
    return False

def rm_label(file_name):
    xml_file = ANNO_DIR + file_name
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    img_file = IMG_DIR + nameWithoutExtention +'.jpg'
    tree = ET.parse(xml_file)
    root = tree.getroot()
    need_rm_obj = []

    objects = root.findall('object')
    obj_cnt = 0
    remove_pic = False
    for obj in objects:
        cls = obj.find('name').text
        if(cls!='good' and cls!='revert'):
            remove_pic = True
            need_rm_obj.append(obj)
            '''
            if (cls != 'empty'):
                remove_pic = True
                need_rm_obj.append(obj)
            else:
                need_rm_obj.append(obj)
            '''
            obj_cnt = obj_cnt + 1
    if remove_pic and (False == file_name.startswith('test')):
        #没有有效元件的图片可以删除
        print('remove file:%s'%img_file)
        os.remove(xml_file)
        os.remove(img_file)
    elif (len(need_rm_obj)>0):
        print('remove objs:%s'%img_file)
        for obj in need_rm_obj:
            #objects.remove(obj)
            root.remove(obj)
        tree.write(xml_file)


if __name__ == "__main__":
    for aroot, dirs, files in os.walk(ANNO_DIR):
        filelist = files
        for item in filelist:
            if item.endswith('.xml'):
                #rm_label(item)
                #rm_label_by_pos(item,0)
                rm_bndbox_overlap(item)
                #rm_label_by_size(item)