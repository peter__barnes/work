import cv2
import xml.etree.ElementTree as ET
import numpy as np

import xml.dom.minidom
import os
import shutil
import argparse
from cfg import *

#DATA_DIR = 'D:/data/test/'
IMG_PATH = DATA_DIR+'JPEGImages/'
ANN_PATH = DATA_DIR+'Annotations/'
IMG_SAVE_PATH = DATA_DIR+'/crops/JPEGImages/'
ANN_SAVE_PATH = DATA_DIR+'/crops/Annotations/'

ann_mould_file = 'E:/data/anno_mould.xml'

def copy_test():
    # JPG文件的地址
    img_path = IMG_PATH
    # XML文件的地址
    anno_path = ANN_PATH
    # 存结果的文件夹

    cut_path_img = IMG_SAVE_PATH
    cut_path_ann = ANN_SAVE_PATH
    if not os.path.exists(cut_path_img):
        os.makedirs(cut_path_img)
    if not os.path.exists(cut_path_ann):
        os.makedirs(cut_path_ann)
    # 获取文件夹中的文件
    imagelist = os.listdir(img_path)
    # print(imagelist
    for image in imagelist:
        if image.startswith('test') :
            shutil.copy(img_path + image, cut_path_img + image)
            image_pre, ext = os.path.splitext(image)
            xml_file = image_pre + '.xml'
            shutil.copy(anno_path + xml_file, cut_path_ann + xml_file)

def kou_with_small_bg(ignore_test=False,auto_rename=False,border_pix=20,duplicate_good_test=0):
    # JPG文件的地址
    img_path = IMG_PATH
    # XML文件的地址
    anno_path = ANN_PATH
    # 存结果的文件夹

    cut_path_img = IMG_SAVE_PATH
    cut_path_ann = ANN_SAVE_PATH
    if not os.path.exists(cut_path_img):
        os.makedirs(cut_path_img)
    if not os.path.exists(cut_path_ann):
        os.makedirs(cut_path_ann)
    # 获取文件夹中的文件
    imagelist = os.listdir(img_path)
    # print(imagelist
    for image in imagelist:
        if image.startswith('test') and ignore_test:
            pass
        else:
        #if True:
            image_pre, ext = os.path.splitext(image)
            img_file = img_path + image
            img = cv2.imread(img_file)
            xml_file = anno_path + image_pre + '.xml'
            img_h, img_w = img.shape[:2]

            tree = ET.parse(xml_file)
            root = tree.getroot()
            # if root.find('object') == None:
            #     return
            if len(root.findall('object')) == 0: #背景图片才会无元件
                #shutil.copy(img_path + image, cut_path_img + image)
                image_pre, ext = os.path.splitext(image)
                #shutil.copy(anno_path + image_pre + '.xml', cut_path_ann + image_pre + '.xml')
            #continue

            obj_i = 0
            for obj in root.iter('object'):
                #img_out = np.zeros((img_h, img_w, 3), np.uint8)
                #img_out2 = np.zeros((480, 640, 3), np.uint8)

                obj_i += 1
                cls = obj.find('name').text
                if cls != 'good' :
                    continue
                xmlbox = obj.find('bndbox')
                b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
                     int(float(xmlbox.find('xmax').text)), int(float(xmlbox.find('ymax').text))]
                #背景区为元件区加上border_pix为边
                has_left_border = False
                has_right_border = False
                has_top_border = False
                has_bottom_border = False
                bg_pos =[0,0,0,0]
                if(b[0]-border_pix > 0):
                    bg_pos[0] = b[0]-border_pix
                    has_left_border = True
                else:
                    bg_pos[0] = 0
                if (b[1] - border_pix > 0):
                    bg_pos[1] = b[1]-border_pix
                    has_top_border = True
                else:
                    bg_pos[1] = 0
                if(b[2]+border_pix < img_w):
                    bg_pos[2] = b[2]+border_pix
                    has_right_border = True
                else:
                    bg_pos[2] = img_w
                if(b[3]+border_pix < img_h):
                    bg_pos[3] = b[3]+border_pix
                    has_bottom_border = True
                else:
                    bg_pos[3] =img_h

                try:
                    img_cut = img[bg_pos[1]:bg_pos[3], bg_pos[0]:bg_pos[2], :]
                    #img_out[b[1]:b[3], b[0]:b[2], :] = img_cut
                    #img_cut = cv2.resize(img_cut, (640, 480))
                except Exception as e:
                    print('except:','{}_{:0>2d}.jpg,error:{}'.format(image_pre, obj_i, e))
                    continue
                #path = os.path.join(cut_path, cls)
                # 目录是否存在,不存在则创建
                #mkdirlambda = lambda x: os.makedirs(x) if not os.path.exists(x) else True
                #mkdirlambda(path)
                if(auto_rename):
                    cv2.imwrite(os.path.join(cut_path_img, 'BQJ_{}_{:0>2d}.jpg'.format(image_pre, obj_i)), img_cut)
                else:
                    cv2.imwrite(os.path.join(cut_path_img, '{}_{:0>2d}.jpg'.format(image_pre, obj_i)), img_cut)
                    if image.startswith('test') and duplicate_good_test>1 and (cls=='good' or cls=='revert'):
                        for i in range(1, duplicate_good_test):
                            cv2.imwrite(os.path.join(cut_path_img, '{}_dup{:0>2d}_{:0>2d}.jpg'.format(image_pre,i, obj_i)), img_cut)
                print('{}_{:0>2d}.jpg'.format(image_pre, obj_i))

                tree_out = ET.parse(ann_mould_file)
                root_out = tree_out.getroot()
                size = root_out.find('size')
                img_cut_h, img_cut_w = img_cut.shape[:2]
                size.find('width').text = str(img_cut_w)
                size.find('height').text = str(img_cut_h)
                objects = root_out.findall('object')
                for obj in objects:
                    lable_name = obj.find('name')
                    lable_name.text = cls
                    bndbox = obj.find('bndbox')
                    if has_left_border:
                        bndbox.find('xmin').text = str(border_pix)
                    else:
                        bndbox.find('xmin').text = xmlbox.find('xmin').text
                    if has_top_border:
                        bndbox.find('ymin').text = str(border_pix)
                    else:
                        bndbox.find('ymin').text = xmlbox.find('ymin').text
                    if has_right_border:
                        bndbox.find('xmax').text = str(img_cut_w - border_pix)
                    else:
                        bndbox.find('xmax').text = str(int(xmlbox.find('xmax').text) - bg_pos[0])
                    if has_bottom_border:
                        bndbox.find('ymax').text = str(img_cut_h - border_pix)
                    else:
                        bndbox.find('ymax').text = str(int(xmlbox.find('ymax').text) - bg_pos[1])
                if(auto_rename):
                    tree_out.write(os.path.join(cut_path_ann, 'BQJ_{}_{:0>2d}.xml'.format(image_pre, obj_i)))
                else:
                    tree_out.write(os.path.join(cut_path_ann, '{}_{:0>2d}.xml'.format(image_pre, obj_i)))
                    if image.startswith('test') and duplicate_good_test>1  and (cls=='good' or cls=='revert'):
                        for i in range(1, duplicate_good_test):
                            tree_out.write(os.path.join(cut_path_ann, '{}_dup{:0>2d}_{:0>2d}.xml'.format(image_pre, i, obj_i)))

def kou_add_bg(ignore_test=False,auto_rename=False,duplicate_good_test=0):
    # JPG文件的地址
    img_path = IMG_PATH
    # XML文件的地址
    anno_path = ANN_PATH
    # 存结果的文件夹

    cut_path_img = IMG_SAVE_PATH
    cut_path_ann = ANN_SAVE_PATH
    if not os.path.exists(cut_path_img):
        os.makedirs(cut_path_img)
    if not os.path.exists(cut_path_ann):
        os.makedirs(cut_path_ann)
    # 获取文件夹中的文件
    imagelist = os.listdir(img_path)
    # print(imagelist
    for image in imagelist:
        '''
        if image.startswith('test'):
            if(ignore_test):
                continue
            shutil.copy(img_path + image, cut_path_img + image)
            image_pre, ext = os.path.splitext(image)
            xml_file = anno_path + image_pre + '.xml'
            shutil.copy(anno_path + xml_file, cut_path_ann + xml_file)
        else:
        '''
        if True:
            image_pre, ext = os.path.splitext(image)
            img_file = img_path + image
            img = cv2.imread(img_file)
            xml_file = anno_path + image_pre + '.xml'
            # DOMTree = xml.dom.minidom.parse(xml_file)
            # collection = DOMTree.documentElement
            # objects = collection.getElementsByTagName("object")
            img_h, img_w = img.shape[:2]

            tree = ET.parse(xml_file)
            root = tree.getroot()
            # if root.find('object') == None:
            #     return
            obj_i = 0
            if len(root.findall('object')) == 0: #背景图片才会无元件
                shutil.copy(img_path + image, cut_path_img + image)
                image_pre, ext = os.path.splitext(image)
                xml_file = anno_path + image_pre + '.xml'
                shutil.copy(anno_path + xml_file, cut_path_ann + xml_file)
            for obj in root.iter('object'):
                img_out = np.zeros((img_h, img_w, 3), np.uint8)
                #img_out2 = np.zeros((480, 640, 3), np.uint8)

                obj_i += 1
                cls = obj.find('name').text
                xmlbox = obj.find('bndbox')
                b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
                     int(float(xmlbox.find('xmax').text)),
                     int(float(xmlbox.find('ymax').text))]
                try:
                    img_cut = img[b[1]:b[3], b[0]:b[2], :]
                    img_out[b[1]:b[3], b[0]:b[2], :] = img_cut
                    #img_cut = cv2.resize(img_cut, (640, 480))
                except Exception as e:
                    print('except:','{}_{:0>2d}.jpg,error:{}'.format(image_pre, obj_i, e))
                    continue
                #path = os.path.join(cut_path, cls)
                # 目录是否存在,不存在则创建
                #mkdirlambda = lambda x: os.makedirs(x) if not os.path.exists(x) else True
                #mkdirlambda(path)
                if(auto_rename):
                    cv2.imwrite(os.path.join(cut_path_img, 'BQJ_{}_{:0>2d}.jpg'.format(image_pre, obj_i)), img_out)
                else:
                    cv2.imwrite(os.path.join(cut_path_img, '{}_{:0>2d}.jpg'.format(image_pre, obj_i)), img_out)
                    if image.startswith('test') and duplicate_good_test>1 and (cls=='good' or cls=='revert'):
                        for i in range(1, duplicate_good_test):
                            cv2.imwrite(os.path.join(cut_path_img, '{}_dup{:0>2d}_{:0>2d}.jpg'.format(image_pre,i, obj_i)), img_out)
                print('{}_{:0>2d}.jpg'.format(image_pre, obj_i))

                tree_out = ET.parse(ann_mould_file)
                root_out = tree_out.getroot()
                size = root_out.find('size')
                size.find('width').text = str(img_w)
                size.find('height').text = str(img_h)
                objects = root_out.findall('object')
                for obj in objects:
                    lable_name = obj.find('name')
                    lable_name.text = cls
                    bndbox = obj.find('bndbox')
                    bndbox.find('xmin').text = xmlbox.find('xmin').text
                    bndbox.find('ymin').text = xmlbox.find('ymin').text
                    bndbox.find('xmax').text = xmlbox.find('xmax').text
                    bndbox.find('ymax').text = xmlbox.find('ymax').text
                if(auto_rename):
                    tree_out.write(os.path.join(cut_path_ann, 'BQJ_{}_{:0>2d}.xml'.format(image_pre, obj_i)))
                else:
                    tree_out.write(os.path.join(cut_path_ann, '{}_{:0>2d}.xml'.format(image_pre, obj_i)))
                    if image.startswith('test') and duplicate_good_test>1  and (cls=='good' or cls=='revert'):
                        for i in range(1, duplicate_good_test):
                            tree_out.write(os.path.join(cut_path_ann, '{}_dup{:0>2d}_{:0>2d}.xml'.format(image_pre, i, obj_i)))
                '''
                # 旋转图片
                #croppedB = croppedB.transpose(Image.ROTATE_180)
                objects = root_out.findall('object')
                for obj in objects:
                    lable_name = obj.find('name')
                    if (lable_name is not None):
                        if 'good' == lable_name.text:
                            lable_name.text = 'revert'
                        elif 'revert' == lable_name.text:
                            lable_name.text = 'good'

                img_cut2 = cv2.rotate(img_cut, cv2.ROTATE_180) #cv2.flip(img_cut, -1)
                img_out2[b[1]:b[3], b[0]:b[2], :] = img_cut2
                if(auto_rename):
                    cv2.imwrite(os.path.join(cut_path_img, 'BQJ_{}_{:0>2d}_flip.jpg'.format(image_pre, obj_i)), img_out2)
                    tree_out.write(os.path.join(cut_path_ann, 'BQJ_{}_{:0>2d}_flip.xml'.format(image_pre, obj_i)))
                else:
                    cv2.imwrite(os.path.join(cut_path_img, '{}_{:0>2d}_flip.jpg'.format(image_pre, obj_i)), img_out2)
                    tree_out.write(os.path.join(cut_path_ann, '{}_{:0>2d}_flip.xml'.format(image_pre, obj_i)))
                '''

def kou_without_bg():
    # JPG文件的地址
    img_path = IMG_PATH
    # XML文件的地址
    anno_path = ANN_PATH
    # 存结果的文件夹

    cut_path_img = IMG_SAVE_PATH
    cut_path_ann = ANN_SAVE_PATH
    if not os.path.exists(cut_path_img):
        os.makedirs(cut_path_img)
    if not os.path.exists(cut_path_ann):
        os.makedirs(cut_path_ann)
    # 获取文件夹中的文件
    imagelist = os.listdir(img_path)
    # print(imagelist
    for image in imagelist:
        '''
        if image.startswith('test'):
            shutil.copy(img_path + image, cut_path_img + image)
            image_pre, ext = os.path.splitext(image)
            xml_file = anno_path + image_pre + '.xml'
            shutil.copy(anno_path + xml_file, cut_path_ann + xml_file)
        else:
        '''
        if True:
            image_pre, ext = os.path.splitext(image)
            img_file = img_path + image
            img = cv2.imread(img_file)
            xml_file = anno_path + image_pre + '.xml'
            # DOMTree = xml.dom.minidom.parse(xml_file)
            # collection = DOMTree.documentElement
            # objects = collection.getElementsByTagName("object")

            tree = ET.parse(xml_file)
            root = tree.getroot()
            # if root.find('object') == None:
            #     return
            obj_i = 0
            for obj in root.iter('object'):
                #img_cut = np.zeros((480, 640, 3), np.uint8)

                obj_i += 1
                cls = obj.find('name').text
                if cls != 'empty':
                    continue

                xmlbox = obj.find('bndbox')
                b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
                     int(float(xmlbox.find('xmax').text)),
                     int(float(xmlbox.find('ymax').text))]
                try:
                    img_cut = img[b[1]:b[3], b[0]:b[2], :]
                    #img_cut = cv2.resize(img_cut, (640, 480))
                except:
                    print('except:','{}_{:0>2d}.jpg'.format(image_pre, obj_i))
                    continue
                #path = os.path.join(cut_path, cls)
                # 目录是否存在,不存在则创建
                #mkdirlambda = lambda x: os.makedirs(x) if not os.path.exists(x) else True
                #mkdirlambda(path)
                cv2.imwrite(os.path.join(cut_path_img, '{}_{:0>2d}.jpg'.format(image_pre, obj_i)), img_cut)
                print('{}_{:0>2d}.jpg'.format(image_pre, obj_i))

                tree_out = ET.parse(ann_mould_file)
                root_out = tree_out.getroot()
                objects = root_out.findall('object')
                for obj in objects:
                    lable_name = obj.find('name')
                    lable_name.text = cls
                    bndbox = obj.find('bndbox')
                    bndbox.find('xmin').text = '1' #xmlbox.find('xmin').text
                    bndbox.find('ymin').text = '1' #xmlbox.find('ymin').text
                    bndbox.find('xmax').text = str(int(xmlbox.find('xmax').text) - int(xmlbox.find('xmin').text))
                    bndbox.find('ymax').text = str(int(xmlbox.find('ymax').text) - int(xmlbox.find('ymin').text))
                tree_out.write(os.path.join(cut_path_ann, '{}_{:0>2d}.xml'.format(image_pre, obj_i)))

if __name__ == '__main__':
    #kou_without_bg()
    #exit()
    if (MACHINE_TYPE.value == MACHINE_ENUM.BQJ.value):
        #kou_add_bg(ignore_test=True,auto_rename=True)
        kou_with_small_bg(ignore_test=True, auto_rename=False, duplicate_good_test=0, border_pix=30)
    elif (MACHINE_TYPE.value == MACHINE_ENUM.GOOD.value):
        kou_add_bg(ignore_test=False, auto_rename=False, duplicate_good_test = 30)
    elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ2.value):
        kou_with_small_bg(ignore_test=False, auto_rename=False, duplicate_good_test = 0)
    else:
        kou_with_small_bg(ignore_test=True, auto_rename=False, duplicate_good_test = 0,border_pix=30)


    #copy_test()
