import os
import pandas as pd
from obj_exchange import read_xml_annotation,read_xml_size,change_xml_annotation_cell_name
#YOLO XML标签统计功能
def check_bndbox_overlap(lst_bndbox):
    cnt = 0
    for bbox in lst_bndbox:
        for i in range(0,cnt):
            if cnt == i:
                continue
            if bbox[0] > lst_bndbox[i][0] and bbox[0] < lst_bndbox[i][2]:
                return True
            elif bbox[2] > lst_bndbox[i][0] and bbox[0] < lst_bndbox[i][2]:
                return True
        cnt = cnt + 1
    return False

def obj_count(anno_dir,print_bad_file=False):
    obj_count_by_class = pd.Series(data=0,dtype=int,index= ["good","revert","bad","bad_tape","empty"])
    lst_bndbox_x_range = []
    for anno_file in (os.listdir(anno_dir)):
        lable_name, bndbox = read_xml_annotation(anno_dir, anno_file)
        b_overlap = check_bndbox_overlap(bndbox)
        if b_overlap:
            print(anno_file)
        width,height = read_xml_size(anno_dir, anno_file)
        #if(len(lable_name)==0) :#and width>500:
            #print(anno_file)
        for i in range(0, len(lable_name)):
            if (lable_name[i] is not None):
                idx = obj_count_by_class.index
                class_lable = lable_name[i].text
                #if(print_bad_file and (lable_name[i].text!="good" and lable_name[i].text!="revert"and lable_name[i].text!="empty")):
                    #print(anno_file)
                if(class_lable in idx):
                    obj_count_by_class[class_lable] = obj_count_by_class[class_lable]+1
                    if(print_bad_file and lable_name[i].text == 'dirty'):
                        print(anno_file)
                        pass
                else:
                    obj_count_by_class[class_lable] = 1
                #if(class_lable !='bad' and class_lable !='good' and class_lable !='revert' and class_lable !='empty'):
                    #print(anno_file)
    return obj_count_by_class


if __name__ == "__main__":
    #obj_count_by_class = obj_count('D:/data/VOC_detectBHJ/Annotations_test/')
    #obj_count_by_class = obj_count('D:/data/VOC_detectBHJ/crops/Annotations/')
    #obj_count_by_class = obj_count('E:/data/12-19empty/Annotations/',True)
    #obj_count_by_class = obj_count('D:/code/framework/yolov5/VOC_detect/VOC_detect_bi2/Annotations/')
    #obj_count_by_class = obj_count('D:/code/framework/yolov5/VOC_detect/VOC_detect325/Annotations/')

    #obj_count_by_class = obj_count('D:/data/VOC_detectBHJ/Annotations/',True)
    #obj_count_by_class = obj_count('E:/data/VOC_detect325/Annotations_with_tape/', print_bad_file=True)
    #obj_count_by_class = obj_count('D:/data/VOC_detectBHJ2/Annotations/',print_bad_file=True)
    obj_count_by_class = obj_count('E:/data/06-15slight/Annotations/',True)

    #obj_count_by_class = obj_count('D:/data/VOC_detect_good/Annotations/')
    print(obj_count_by_class)