import cv2 as cv
import numpy as np
import shutil
import os
import random
import xml.etree.ElementTree as ET
from cfg import *

IMG_DIR = DATA_EX_DIR+'JPEGImages/'
ANNO_DIR = DATA_EX_DIR+'Annotations/'
#BASE_DIR='E:/data/exp3/'
#IMG_DIR = BASE_DIR+"JPEGImages/"
#ANNO_DIR = BASE_DIR+"Annotations/"

#遍历文件夹，文件夹对应的xml若包含某些标签，则将xml和对应的jpg删除

def rm_file_by_label(file_name,rm_label = 'bad_tape'):
    xml_file = ANNO_DIR + file_name
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    img_file = IMG_DIR + nameWithoutExtention + '.jpg'

    tree = ET.parse(xml_file)
    root = tree.getroot()

    objects = root.findall('object')
    need_remove = False
    for obj in objects:
        cls = obj.find('name').text
        if(cls==rm_label):
            need_remove = True
    if need_remove:
        print('remove:'+file_name)
        os.remove(xml_file)
        os.remove(img_file)

def rm_bad_tape_slight():
    for aroot, dirs, files in os.walk(ANNO_DIR):
        filelist = files
        for item in filelist:
            if item.endswith('.xml'):
                rm_file_by_label(item, rm_label='bad_tape_slight')


def rm_bad_tape_all():
    for aroot, dirs, files in os.walk(ANNO_DIR):
        filelist = files
        for item in filelist:
            if item.endswith('.xml'):
                rm_file_by_label(item, rm_label='bad_tape_slight')
    for aroot, dirs, files in os.walk(ANNO_DIR):
        filelist = files
        for item in filelist:
            if item.endswith('.xml'):
                rm_file_by_label(item, rm_label='bad_tape')

if __name__ == "__main__":
    rm_bad_tape_slight()