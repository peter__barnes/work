import os
import imgaug as ia
import cv2 as cv
from mix_img import *
from rm_jpg_and_anno_by_label import rm_bad_tape_slight,rm_bad_tape_all
from cfg import *


def rotate_img(img):
    #img = cv.imread(img_file)
    #img_out = cv.rotate(img, cv.ROTATE_180)

    (h, w) = img.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    M = cv.getRotationMatrix2D((cX, cY), -8, 1.0)
    rotated = cv.warpAffine(img, M, (w, h))
    return rotated
    #cv.imshow("Rotated by 45 Degrees", rotated)
    #cv.waitKey()

#梯形形变
def TRAPEZOID(img_ori,k=0.12):
    img = cv.rotate(img_ori, cv.ROTATE_90_COUNTERCLOCKWISE)

    height, width = img.shape[0], img.shape[1]
    value = k*height
    resized_img = np.zeros([height, width,3], np.uint8)
    for i in range (height):
        temp = int( value - k * i )
        for j in range (temp,width-temp) :
            #每行非黑色区域的长度
            distance = int(width-temp) - int(temp+0.5)
            #缩小的倍率
            ratio = distance / width
            #取点的步长
            stepsize = 1.0/ratio
            #将同意行缩小相同倍率
            resized_img[i][j] = img[i][int((j-temp)*stepsize)]


    #cv.imshow("old", img)
    img_out = cv.rotate(resized_img, cv.ROTATE_90_CLOCKWISE)
    #cv.imshow("Trapezoid", img_out)
    #cv.waitKey(0)
    return img_out

PIC_IN_PATH = 'E:/PStest/scratch_rotate/'

if __name__ == "__main__":
    img_list = os.listdir(PIC_IN_PATH)
    for img_file in img_list:
        #rotate_img(img_file)
        print(img_file)
        (nameWithoutExtention, extention) = os.path.splitext(img_file)

        img = cv.imread(PIC_IN_PATH+img_file)
        transed = TRAPEZOID(img)
        img_out = rotate_img(transed)
        cv.imwrite(PIC_IN_PATH + 'rotated_' + nameWithoutExtention + extention, img_out)
    '''
    img = cv.imread(PIC_PATH+'badtape05973.jpg')
    transed = TRAPEZOID(img)
    img_out = rotate_img(transed)
    cv.imwrite(PIC_PATH+'out.jpg',img_out)
    '''