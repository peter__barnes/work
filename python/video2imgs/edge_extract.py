import cv2 as cv


def cv_show(name, img):
    cv.imshow(name, img)
    cv.waitKey(0)
    cv.destroyAllWindows()



def canny_edge_extract(img):
    # 沿x方向的边缘检测
    img1 = cv.Sobel(img, cv.CV_64F, 1, 0)
    sobelx = cv.convertScaleAbs(img1)
    # 展示未进行取绝对值的图片
    #cv_show('img1', img1)
    sobely = cv.convertScaleAbs(img1)
    '''
    cv_show('sobelx', sobelx)

    # 沿y方向的边缘检测
    img1 = cv.Sobel(img, cv.CV_64F, 0, 1)
    sobely = cv.convertScaleAbs(img1)
    cv_show('sobely', sobely)

    # 沿x，y方向同时检测，效果不是特别好
    img1 = cv.Sobel(img, cv.CV_64F, 1, 1)
    sobelxy = cv.convertScaleAbs(img1)
    cv_show('sobelxy', sobelxy)

    # 一般在x，y方向分别检测，在进行与运算
    # sobelxy1 = cv.bitwise_and(sobelx, sobely)
    # cv_show('sobelxy1', sobelxy1)
    # 这种方法也行
    '''
    sobelxy1 = cv.addWeighted(sobelx, 0.5, sobely, 0.5, 0)
    edges1 = cv.Canny(img, 16, 64)
    #cv_show('img',img)
    #cv_show('sobelxy1', img)
    #mixed = cv.add(img, img)
    #cv_show('mixed', mixed)
    img[:, :, 0] = img[:, :, 0]+50*edges1[:, :]
    img[:, :, 1] = img[:, :, 1]+50*edges1[:, :]
    img[:, :, 2] = img[:, :, 2]+50*edges1[:, :]
    #cv_show('img2',img)
    return img

if __name__ == "__main__":
    img = cv.imread('E:/PStest/bad_img_2_img0217.jpg')
    if img is None:
        print("Failed to read the image")
    else:
        img_out = canny_edge_extract(img)
        cv_show('img_out', img_out)

