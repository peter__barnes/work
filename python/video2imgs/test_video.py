import cv2
import time
#视频播放

#VIDEO_PATH = 'E:/data/原始325/test325带晶片1-1.mp4'
cap = cv2.VideoCapture(0) #mac系统选择1   如果打开本地视频，就直接将0改为视频的地址就可以了
#cap = cv2.VideoCapture(VIDEO_PATH)
cap.set(cv2.CAP_PROP_EXPOSURE, -13)
success=cap.isOpened()#返回的是bool类型，用于查看是否可以打开本地的视频
fps = cap.get(cv2.CAP_PROP_FPS)
print(fps)
while success:
    frame_id = cap.get(cv2.CAP_PROP_POS_FRAMES)
    cap.set(cv2.CAP_PROP_POS_FRAMES,frame_id + 1)
    ret,frames=cap.read()
    frame_rgb=cv2.cvtColor(frames,cv2.COLOR_BGR2RGB)#因为有些任务需要rgb形式
    cv2.imshow("windom",frame_rgb)
    #time.sleep(0.001)
    #key = cv2.waitKey(1)
    if cv2.waitKey(100)&0xFF==ord("q"):
        break
cap.release()
cv2.destroyAllWindows()



