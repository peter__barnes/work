from enum import Enum, unique

class MACHINE_ENUM(Enum):
    BQJ = 1
    BHJ = 2
    B325 = 3
    GOOD = 4
    BHJ2 = 5

MACHINE_TYPE = MACHINE_ENUM.BHJ2

DATA_DIR=''
DATA_EX_DIR=''
IMG_PATH=''

ONLY_PRESS_AND_SCRATCH_AS_BAD = False

if(MACHINE_TYPE.value == MACHINE_ENUM.B325.value):
    DATA_DIR = 'E:/data/VOC_detect325/'
    DATA_EX_DIR = DATA_DIR+'exchanged/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
    DATA_DIR = 'D:/data/VOC_detectBHJ/'
    DATA_EX_DIR = DATA_DIR+'exchanged/'
    #DATA_EX_DIR = DATA_DIR + 'crops/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ2.value):
    DATA_DIR = 'D:/data/VOC_detectBHJ2/'
    DATA_EX_DIR = DATA_DIR+'exchanged/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.GOOD.value):
    DATA_DIR = 'D:/data/VOC_detect_good/'
    DATA_EX_DIR = DATA_DIR+'exchanged/'
else:
    DATA_DIR = 'D:/data/VOC_detect/'
    DATA_EX_DIR = DATA_DIR+'exchanged/'
    #DATA_EX_DIR = DATA_DIR+'crops/'


