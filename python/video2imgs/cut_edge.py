import cv2 as cv
import numpy as np
import shutil
import os
import random
import xml.etree.ElementTree as ET
from obj_exchange import read_xml_annotation,read_xml_size
from cfg import *

DATA_DIR = 'E:/data/05-16/'
#DATA_DIR='D:/data/VOC_detectBHJ2/'
IMG_DIR = DATA_DIR+'JPEGImages/'
ANNO_DIR = DATA_DIR+'Annotations/'

def if_has_x_in_range(lst_bndbox):
    for bbox in lst_bndbox:
        #if bbox[0] > 105 and bbox[0] < 197:
        #if bbox[0] > 150 and bbox[0] < 258:
        if bbox[0] < 260:
            return True
    return False

def if_has_x_in_range_right(lst_bndbox,width):
    for bbox in lst_bndbox:
        #if bbox[0] > 105 and bbox[0] < 197:
        #if bbox[0] > 150 and bbox[0] < 258:
        if (width - bbox[2]) < (640- 500):
            return True
    return False

def skip_check(file_name,only_right=False):
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    xml_file = ANNO_DIR + nameWithoutExtention + '.xml'
    lable_name, bndbox = read_xml_annotation(ANNO_DIR, nameWithoutExtention + '.xml')
    width,height = read_xml_size(ANNO_DIR, nameWithoutExtention + '.xml')

    if only_right==False:
        b_has_x_in_range = if_has_x_in_range(bndbox)
    else:
        b_has_x_in_range = if_has_x_in_range_right(bndbox,width)
    return b_has_x_in_range

def cut_edge(file_name,border_pix=30,only_right=False):
    (nameWithoutExtention, extention) = os.path.splitext(os.path.basename(file_name))
    xml_file = ANNO_DIR + nameWithoutExtention + '.xml'

    img_file = IMG_DIR + file_name
    img = cv.imread(img_file)
    img_h, img_w = img.shape[:2]

    obj_max_x = obj_min_x = int (img_w/2)
    tree = ET.parse(xml_file)
    root = tree.getroot()
    # if root.find('object') == None:
    #     return
    obj_cnt = 0
    for obj in root.iter('object'):
        obj_cnt = obj_cnt + 1
        #cls = obj.find('name').text
        xmlbox = obj.find('bndbox')
        xmax = int(xmlbox.find('xmax').text)
        xmin = int(xmlbox.find('xmin').text)
        if xmax > obj_max_x:
            obj_max_x = xmax
        if xmin < obj_min_x:
            obj_min_x = xmin
        #b = [int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)),
             #int(float(xmlbox.find('xmax').text)),
             #int(float(xmlbox.find('ymax').text))]
    if obj_cnt > 0:
        rigth_cut_pix = 0
        left_cut_pix = 0
        if(obj_max_x + border_pix < img_w):
            rigth_cut_pix = img_w - obj_max_x - border_pix
        #rigth_cut_pix = 0
        if(obj_min_x - border_pix > 0) and only_right==False:
            left_cut_pix = obj_min_x - border_pix
        if (rigth_cut_pix!=0 or left_cut_pix!=0):
            #print('file:%s, cut_pix:%d,%d'%(file_name,left_cut_pix,rigth_cut_pix))
            crop_img = img[:, left_cut_pix:img_w - rigth_cut_pix]

            #修改xml的尺寸标记
            size = root.find('size')
            img_cut_h, img_cut_w = crop_img.shape[:2]
            if(str(img_cut_w) != size.find('width').text):
                print('file:%s,:%s,%s'%(file_name,img_cut_w,size.find('width').text))
            size.find('width').text = str(img_cut_w)
            size.find('height').text = str(img_cut_h)

            if(left_cut_pix!=0):
                #同步更改xml中所有x坐标
                for obj in root.iter('object'):
                    xmlbox = obj.find('bndbox')
                    xmlbox.find('xmin').text = str(int(xmlbox.find('xmin').text) - left_cut_pix)
                    xmlbox.find('xmax').text = str(int(xmlbox.find('xmax').text) - left_cut_pix)
            tree.write(xml_file)
            cv.imwrite(img_file,crop_img)
            #cv.imshow('crop_img',crop_img)
            #cv.waitKey()

if __name__ == "__main__":
    for aroot, dirs, files in os.walk(IMG_DIR):
        filelist = files
        for item in filelist:
            if item.endswith('.jpg'):
                #if skip_check(item,only_right = False):
                    #continue
                cut_edge(item,border_pix=15,only_right = True)