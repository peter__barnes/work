# -*- coding: utf-8 -*-
import os
import cv2
import numpy as np
import time
from PIL import Image

path ='D:/chentr/Pictures/6.png'

def change_color(path):
    img = cv2.imread(path)
    h, w = img.shape[:2]
    #cv2.imshow("img", img)
    #cv2.waitKey(0)
    for i in range(h):
        for j in range(w):
            if img[i][j][0] !=255:
                img[i][j][0] = min(255,img[i][j][0] +20)
                img[i][j][1] = min(255, img[i][j][1] - 20)
                img[i][j][2] = min(255, img[i][j][2] + 209)
                print(i,j,img[i][j])
    #cv2.imshow("img", img)
    #cv2.waitKey(0)
    cv2.imwrite('D:/chentr/Pictures/7.png',img)

def change_color_to_black_white():

    # 打开原始彩色JPG图片
    image = Image.open('input_color.jpg')

    # 将图片转换为黑白模式
    black_white_image = image.convert("L")

    # 保存黑白图片
    black_white_image.save('output_bw.jpg')


if __name__ == '__main__':
    #change_color(path)
    change_color_to_black_white()