import os
import shutil
import xml.etree.ElementTree as ET
from cfg import *
from obj_exchange import read_xml_annotation,change_xml_annotation_cell_name,mkdir
#自动进行标签转换（用于多分类和二分类互转）

def change_anno_label2(dir,dir_out):
    if(not os.path.exists(dir_out)):
        mkdir(dir_out)
    for file in os.listdir(dir):
        print('change_anno_label:%s'%file)
        if file.endswith('.xml'):
            # 一次读取一对XML信息
            anno_file = open(os.path.join(dir, file))  # 这里root分别由两个意思
            tree = ET.parse(anno_file)

            label_names, bndbox = read_xml_annotation(dir,file)
            for i in range(0,len(label_names)):
                if (label_names[i] is not None):
                    #if 'revert' == label_names[i].text or 'bad' == label_names[i].text:
                    if 'bad' == label_names[i].text or 'bad_tape' == label_names[i].text:
                        label_names[i].text = 'good'
                        change_xml_annotation_cell_name(tree, i, 'good')
            tree.write(os.path.join(dir_out, file))


def change_anno_label(dir,dir_out):
    if(not os.path.exists(dir_out)):
        mkdir(dir_out)
    for file in os.listdir(dir):
        print('change_anno_label:%s'%file)
        if file.endswith('.xml'):
            # 一次读取一对XML信息
            anno_file = open(os.path.join(dir, file))  # 这里root分别由两个意思
            tree = ET.parse(anno_file)

            label_names, bndbox = read_xml_annotation(dir,file)
            for i in range(0,len(label_names)):
                if (label_names[i] is not None):
                    #change_xml_annotation_cell_name(tree, i, 'bad_tape')
                    #if 'bad_tape' == label_names[i].text:
                        #label_names[i].text = 'bad_tape_slight'
                        #change_xml_annotation_cell_name(tree, i, 'bad_tape_slight')
                    if not (MACHINE_TYPE.value == MACHINE_ENUM.BHJ2.value):
                        if 'bad_tape' == label_names[i].text or 'empty' == label_names[i].text:
                            pass
                        elif 'bad_tape_slight' == label_names[i].text:
                            label_names[i].text = 'bad_tape'
                            change_xml_annotation_cell_name(tree, i, 'bad_tape')
                        #elif 'press' == label_names[i].text or 'scratch' == label_names[i].text:
                            #label_names[i].text = 'bad_difficult'
                            # change_xml_annotation()
                            #change_xml_annotation_cell_name(tree, i, 'bad_difficult')
                        elif 'overlap' == label_names[i].text:#第一摄像头也可以不检测叠料
                            label_names[i].text = 'bad'
                            change_xml_annotation_cell_name(tree, i, 'bad')

                        elif 'good' != label_names[i].text and 'revert' != label_names[i].text:
                            if not ONLY_PRESS_AND_SCRATCH_AS_BAD:
                                label_names[i].text = 'bad'
                                #change_xml_annotation()
                                change_xml_annotation_cell_name(tree, i, 'bad')
                            else:
                                if 'scratch' == label_names[i].text or 'press' == label_names[i].text:
                                    label_names[i].text = 'bad'
                                    change_xml_annotation_cell_name(tree, i, 'bad')
                                else:
                                    label_names[i].text = 'good'
                                    change_xml_annotation_cell_name(tree, i, 'good')
                    elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ2.value):
                        if 'bad_tape' == label_names[i].text or 'empty' == label_names[i].text:
                            pass
                        elif 'bad_tape_slight' == label_names[i].text:
                            label_names[i].text = 'bad_tape'
                            change_xml_annotation_cell_name(tree, i, 'bad_tape')
                        elif 'overlap' == label_names[i].text or 'scratch' == label_names[i].text or 'press' == label_names[i].text:
                            label_names[i].text = 'bad'
                            # change_xml_annotation()
                            change_xml_annotation_cell_name(tree, i, 'bad')
                        else:
                            label_names[i].text = 'good'
                            # change_xml_annotation()
                            change_xml_annotation_cell_name(tree, i, 'good')

            tree.write(os.path.join(dir_out, file))

#DATA_EX_DIR='E:/data/05-16/'
if __name__ == "__main__":
    #if (MACHINE_TYPE.value == MACHINE_ENUM.BQJ.value):
        #change_anno_label(DATA_EX_DIR + 'Annotations_random/', DATA_EX_DIR + 'Annotations_random2/')
    #else:
    #change_anno_label(DATA_DIR + 'Annotations_new/', DATA_DIR + 'Annotations2/')
    #change_anno_label(DATA_DIR + 'Annotations/', DATA_DIR + 'Annotations2/')
    change_anno_label(DATA_EX_DIR + 'Annotations/', DATA_EX_DIR + 'Annotations2/')

