#触发检测功能，选定一个触发检测点，计算点为圆心的周边小范围的颜色均值。
#
# 在视频、图片组中，反复检测触发检测点的颜色变化，每次由暗变亮触发一次信号。
import os
import time
import cv2
import numpy as np
VIDEO_PATH="E:/data/实拍/Video对比EXP8漏元件.mp4"
IMG_PATH='E:/data/formated_test/'
trigger_x = 400
trigger_y = 40
TRIGGER_COLOR_THRES=5
def watch_point_color(img):
    # file_names = os.listdir(path)
    h, w = img.shape[:2]
    #trigger_x = int(w / 2)
    #trigger_y = int(h / 2)
    #trigger_x = gl_trigger_x
    #trigger_y = gl_trigger_y
    #trigger_x = int(108 * 1.33)
    #trigger_y = int(258 * 1.33)
    scope_pix = 5
    if (h > scope_pix * 2) and (w > scope_pix * 2):
        #R_mean = np.mean(
            #img[trigger_y - scope_pix:trigger_y + scope_pix, trigger_x - scope_pix:trigger_x + scope_pix, 0])
        G_mean = np.mean(
            img[trigger_y - scope_pix:trigger_y + scope_pix, trigger_x - scope_pix:trigger_x + scope_pix, 1])
        B_mean = np.mean(
            img[trigger_y - scope_pix:trigger_y + scope_pix, trigger_x - scope_pix:trigger_x + scope_pix, 2])
        print((G_mean + B_mean) / 2)
        if ((G_mean+B_mean)/2) > TRIGGER_COLOR_THRES:
            return True
        else:
            return False
    return False

def trigger_test():
    color_white = True
    last_color_white =True
    for aroot, dirs, files in os.walk(IMG_PATH):
        filelist = files
        for item in filelist:
            time.sleep(0.5)
            print(item)
            img = cv2.imread(IMG_PATH+item)
            imc_trigger = int(trigger_x), int(trigger_y)
            img = cv2.circle(img, imc_trigger, 10, (255, 255, 255), 1)

            color_white= watch_point_color(img)
            if(color_white == last_color_white):
                pass
            else:
                last_color_white = color_white
                if color_white == False:
                    pass
                else:
                    print('changed!!!!')

            cv2.imshow("windom", img)
            cv2.waitKey(1)


if __name__ == "__main__":
    trigger_test()
