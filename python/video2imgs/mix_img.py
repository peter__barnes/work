import numpy as np
import cv2 as cv
from PIL import Image
import os
import random
#from obj_exchange import IMG_SAVE_PATH,ANN_SAVE_PATH
from cfg import *
import shutil


IMG_PATH = 'E:/PStest/'
FG_PATH = 'E:/PStest/BG/'
SPLITE_INPUT_PATH = 'E:/PStest/Pepsized_Blur/'
SPLITE_OUTPUT_PATH = 'E:/PStest/my_bg/'

if (MACHINE_TYPE.value == MACHINE_ENUM.BQJ.value):
    IMG_MIX_SAVE_PATH = DATA_EX_DIR+'/JPEGImages/'
    ANN_MIX_SAVE_PATH = DATA_EX_DIR+'/Annotations/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
    IMG_MIX_SAVE_PATH = DATA_DIR+'/exchanged/JPEGImages/'
    ANN_MIX_SAVE_PATH = DATA_DIR+'/exchanged/Annotations/'
else:
    IMG_MIX_SAVE_PATH = DATA_DIR+'/exchanged/JPEGImages/'
    ANN_MIX_SAVE_PATH = DATA_DIR+'/exchanged/Annotations/'

def auto_split_pic():
    j=1
    for i in range(0,4):
        for each_image in os.listdir(SPLITE_INPUT_PATH):
            out_name = "my_bg%04d.jpg"  % (j)
            j=j+1
            # 每个图像全路径
            image_input_fullname = SPLITE_INPUT_PATH + "/" + each_image
            # PIL库打开每一张图像
            img = Image.open(image_input_fullname)
            # 定义裁剪图片左、上、右、下的像素坐标
            x_max = img.size[0]
            y_max = img.size[1]

            mid_point_x = int(x_max / 2)
            mid_point_y = int(y_max / 2)

            down = mid_point_y + random.randint(0, mid_point_y)
            up = mid_point_y - (down - mid_point_y)
            right = mid_point_x + (down - mid_point_y)
            left = mid_point_x - (down - mid_point_y)

            BOX_LEFT, BOX_UP, BOX_RIGHT, BOX_DOWN = left, up, right, down
            # 从原始图像返回一个矩形区域，区域是一个4元组定义左上右下像素坐标
            box = (BOX_LEFT, BOX_UP, BOX_RIGHT, BOX_DOWN)
            # 进行roi裁剪
            roi_area = img.crop(box)
            # 裁剪后每个图像的路径+名称
            image_output_fullname = SPLITE_OUTPUT_PATH + "/" + out_name
            # 存储裁剪得到的图像
            roi_area.save(image_output_fullname)
            print('{0} crop done.'.format(each_image))

def random_mix(img_name,fg_list,overwrite=True,out_num=2,max_mix_rate = 0.2):
    img1 = Image.open(IMG_MIX_SAVE_PATH+img_name).convert("RGB")
    #img2 = Image.open(FG_PATH + 'Pepsized_Blur_03.jpg').convert("RGB")
    img2list = random.sample(fg_list, 2)

    for i in range(0,out_num):
        if(i==0 and overwrite==True):
            # 修改尺寸
            img_fg = Image.open(FG_PATH+img2list[i]).convert("RGB")
            img_fg = img_fg.resize(img1.size)
            # 混合图片
            #img = Image.blend(img1, img_fg, 0.5)
            img = Image.blend(img1, img_fg, random.random() * max_mix_rate)
            #img.show()
            fnameWithoutExt = os.path.splitext(img_name)[0]
            xmlname = fnameWithoutExt +'.xml'
            xml_out_name = fnameWithoutExt +'.xml'
            img.save(IMG_MIX_SAVE_PATH+fnameWithoutExt+'.jpg')

            #xmlfnameWithoutExt = os.path.splitext(fnameWithoutExt)[0] + '_' + str(i)
            #shutil.copy(ANN_SAVE_PATH+xmlname, ANN_SAVE_PATH+xml_out_name)
        else:
            # 修改尺寸
            img_fg = Image.open(FG_PATH+img2list[i]).convert("RGB")
            img_fg = img_fg.resize(img1.size)
            # 混合图片
            img = Image.blend(img1, img_fg, random.random() * max_mix_rate)
            #img.show()
            fnameWithoutExt = os.path.splitext(img_name)[0]
            xmlname = fnameWithoutExt +'.xml'
            xml_out_name = 'mix_' +str(i)+fnameWithoutExt +'.xml'
            img.save(IMG_MIX_SAVE_PATH+'mix_' +str(i)+fnameWithoutExt +'.jpg')

            #xmlfnameWithoutExt = os.path.splitext(fnameWithoutExt)[0] + '_' + str(i)
            shutil.copy(ANN_MIX_SAVE_PATH+xmlname, ANN_MIX_SAVE_PATH+xml_out_name)

def random_fg():
    img_list = os.listdir(FG_PATH)
    #for img in os.listdir(IMG_MIX_SAVE_PATH):
    for img in os.listdir(IMG_MIX_SAVE_PATH):
        if img.startswith('test_') or img.startswith('bt_rnd1_test_')or img.startswith('bt_rnd2_test_')or img.startswith('bt_rnd3_test_'):
            continue
        else:
            print("mixing:%s"%img)
            random_mix(img,img_list,overwrite=True,out_num=1)

if __name__ == "__main__":
    '''
    srcA = cv.imread(IMG_PATH + 'bad_img_2_img0217.jpg')
    srcB = cv.imread(IMG_PATH + 'test.jpeg')
    mixed_x, y_a, y_b, lam = mixup_data(srcA,srcB,alpha=0.5)
    cv.imshow("input image", mixed_x)
    '''
    random_fg()
    #fg_list = os.listdir(FG_PATH)

    #random_mix(IMG_PATH + 'bad_img_2_img0217.jpg', fg_list)
    '''
    #img1 = Image.open(IMG_PATH + 'bad_img_2_img0217.jpg').convert("RGB")
    img2 = Image.open(FG_PATH + 'Pepsized_Blur_03.jpg').convert("RGB")

    # 修改尺寸
    img2 = img2.resize(img1.size)

    # 混合图片
    img = Image.blend(img1, img2, 0.5)
    img.show()
    '''