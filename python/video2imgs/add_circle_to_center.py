import os
import sys
from cfg import *
import cv2 as cv
from obj_exchange import read_xml_annotation,change_xml_annotation_cell_name,mkdir

PIC_PATH = 'E:/data/03-09-badtape/exp/'
SAVE_PATH = 'E:/data/03-09-badtape/exp2/'

def add_circle_to_center(pic_path, file):
    img = cv.imread(pic_path+file)
    imc_center = int(img.shape[1] / 2), int(img.shape[0] / 2)
    img = cv.circle(img, imc_center, 10, (255, 255, 255), 1)
    cv.imwrite(SAVE_PATH+file, img)

if __name__ == "__main__":
    if(not os.path.exists(SAVE_PATH)):
        mkdir(SAVE_PATH)
    for file in os.listdir(PIC_PATH):
        print('add circle to file:%s'%(PIC_PATH + file))
        add_circle_to_center(PIC_PATH, file)
