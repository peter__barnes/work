#对比annotation与image文件夹，列出不一致的

import cv2 as cv
import numpy as np
import shutil
import os
import random
from cfg import MACHINE_ENUM,MACHINE_TYPE

if (MACHINE_TYPE.value == MACHINE_ENUM.B325.value):
    IMG_PATH ='E:/data/VOC_detect325/JPEGImages/'
    ANN_PATH ='E:/data/VOC_detect325/Annotations/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
    IMG_PATH ='D:/data/VOC_detectBHJ/JPEGImages/'
    ANN_PATH ='D:/data/VOC_detectBHJ/Annotations/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ2.value):
    IMG_PATH ='D:/data/VOC_detectBHJ2/JPEGImages/'
    ANN_PATH ='D:/data/VOC_detectBHJ2/Annotations/'
elif (MACHINE_TYPE.value == MACHINE_ENUM.GOOD.value):
    IMG_PATH = 'D:/data/VOC_detect_good/JPEGImages/'
    ANN_PATH = 'D:/data/VOC_detect_good/Annotations/'
else:
    IMG_PATH ='D:/data/VOC_detect/JPEGImages/'
    ANN_PATH ='D:/data/VOC_detect/Annotations/'

#IMG_PATH = 'E:/data/06-15slight/JPEGImages/'
#ANN_PATH = 'E:/data/06-15slight/Annotations/'


if __name__ == "__main__":
    for aroot, dirs, files in os.walk(IMG_PATH):
        filelist = files  # 注意此处仅是该路径下的其中一个列表
        # print('list', list)

        # filelist = os.listdir(self.path) #获取文件路径
        total_num = len(filelist)  # 获取文件长度（个数）

        for item in filelist:
            fnameWithoutExt = os.path.splitext(item)[0]
            #print(fnameWithoutExt)
            if(not os.path.exists(ANN_PATH+fnameWithoutExt+".xml")):
                print('img without anno:'+item)

    for aroot, dirs, files in os.walk(ANN_PATH):
        filelist = files  # 注意此处仅是该路径下的其中一个列表
        # print('list', list)

        # filelist = os.listdir(self.path) #获取文件路径
        total_num = len(filelist)  # 获取文件长度（个数）

        for item in filelist:
            fnameWithoutExt = os.path.splitext(item)[0]
            #print(fnameWithoutExt)
            if(not os.path.exists(IMG_PATH+fnameWithoutExt+".jpg")):
                print('anno without img:' + item)