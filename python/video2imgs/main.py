#视频自动截图片
# -*- coding:utf-8 _*-
import cv2
import numpy as np
import glob
import shutil
import os
from cfg import MACHINE_ENUM,MACHINE_TYPE

IS_DEEP_SORT = False
print("MACHINE_TYPE: " ,MACHINE_TYPE.name,",",MACHINE_TYPE.value)
def video2imgs_by_sec(vd_dir, second_period):
    video_dir = vd_dir

    path_pre, ext = os.path.splitext(vd_dir)
    #path_pre = path_pre.replace(' ', '')
    if not os.path.exists(path_pre):
        os.makedirs(path_pre)

    cap = cv2.VideoCapture(video_dir)
    assert cap.isOpened(), "视频读取失败！"

    success = True
    frame_count = 0
    img_cnt = 1

    #部分视频不能靠cap.read()的返回是否为success自动判定视频结束，这种情况只能算出结束时间，在超出范围强制结束
    # count the number of frames
    frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = int(cap.get(cv2.CAP_PROP_FPS))

    # calculate dusration of the video
    total_time = int(frames *1000/ fps)
    print(total_time)

    while (success):
        cap.set(cv2.CAP_PROP_POS_MSEC, 1000 * second_period * img_cnt)
        if total_time < 1000 * second_period * img_cnt:
            print("结束了,next img id:%d" % (img_cnt))
            success = False
            break
        success, frame = cap.read()

        if success:
            write_path = str("%s/img%05d.jpg" % (path_pre.rstrip(' '), img_cnt))
            cv2.imencode(write_path, frame)[1].tofile(write_path)
            img_cnt = img_cnt + 1
            frame_count = frame_count + 1
        else:
            print("结束了,next img id:%d"%(img_cnt))

    cap.release()

def video2imgs(vd_dir, vd_idx):
    video_dir = vd_dir

    cap = cv2.VideoCapture(video_dir)
    assert cap.isOpened(), "视频读取失败！"

    success = True
    frame_count = 0
    img_cnt= 1
    start_frame = 0
    video_index = vd_idx

    while (success):
        success, frame = cap.read()

        if success:
            if frame_count < start_frame:
                frame_count = frame_count + 1
                continue
            if frame_count % vd_idx == 0:
                print("读取一帧......")
                '''视频帧保存'''
                '''video_index是视频的id，frame_count帧的id，frame是一帧图像'''
                if IS_DEEP_SORT:
                    # deepsort data
                    cv2.imwrite("E:/data/formated_deep_img/img%05d.jpg" % (img_cnt), frame)
                elif (MACHINE_TYPE.value == MACHINE_ENUM.B325.value):
                    cv2.imwrite("E:/data/formated_img325-2/raw_cam2_img%05d.jpg" % (img_cnt), frame)
                elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
                    cv2.imwrite("E:/data/formated_test/real_img%05d.jpg" % (img_cnt), frame)
                else:
                    #common yolo data
                    cv2.imwrite("E:/data/formated_img/capoff_img%05d.jpg" % (img_cnt), frame)
                img_cnt = img_cnt + 1
            frame_count = frame_count + 1
        else:
            print("结束了,next img id:%d"%(img_cnt))

    cap.release()


def pick_imgs(gap):
    k = 0
    for img_dir in glob.glob("E:/DL/yolov5/litchi_video/images/" + "*.jpg"):
        img_save_dir = os.path.join("E:/DL/yolov5/litchi_video", 'imgs')

        k = k + 1
        if k == gap:
            k = 0
            print("---分隔帧采样---")
            if os.path.exists(img_save_dir):
                shutil.copy(img_dir, img_save_dir)

#F:\BaiduNetdiskDownload\CFA一级备考资料\CFA一级2023\CFA一级JC2023.2\2.基础班\1.Quantitative Methods  –Irene
if __name__ == "__main__":
    #for root, dirs, files in os.walk(r'E:/BaiduNetdiskDownload/courses/3.数量与计算器前导/'):
    #for root, dirs, files in os.walk(r'F:/BaiduNetdiskDownload/CFA二级备考资料/2.基础课/'):
    for root, dirs, files in os.walk(r'E:/Pictures/'):
        for name in files:
            if name.endswith(".mp4"):
                file_name = os.path.join(root, name)
                print("find File: " + file_name)
                video2imgs_by_sec(file_name,3)
    exit()
    if IS_DEEP_SORT:
        '''第一个参数是视频路径，第二个参数是视频的id（用于每一帧图片的命名）'''
        video2imgs("E:/data/原始数据/Video有封带不良.mp4", 22)
    elif (MACHINE_TYPE.value == MACHINE_ENUM.B325.value):
        video2imgs("E:/data/原始325/cam2-1.mp4", 3)
    elif (MACHINE_TYPE.value == MACHINE_ENUM.BHJ.value):
        #video2imgs("E:/data/原始BHJ/VBHJ翻转4-5.mp4", 6)
        video2imgs("E:/data/实拍/Video对比EXP8漏元件.mp4", 1)
    else:
        video2imgs("E:/data/原始数据/Video压伤9-4.mp4", 52)

    '''这是对视频帧做隔帧采样，2是指隔1帧取一张'''
    # pick_imgs(2)


