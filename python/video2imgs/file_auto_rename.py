import os
import shutil
import random
from mix_img import *
from cfg import MACHINE_ENUM,MACHINE_TYPE,DATA_DIR,IMG_PATH

def mkdir(path):
    # 去除首位空格
    path = path.strip()
    # 去除尾部 \ 符号
    path = path.rstrip("\\")
    # 判断路径是否存在
    # 存在     True
    # 不存在   False
    isExists = os.path.exists(path)
    # 判断结果
    if not isExists:
        # 如果不存在则创建目录
        # 创建目录操作函数
        os.makedirs(path)
        print(path + ' 创建成功')
        return True
    else:
        # 如果目录存在则不创建，并提示目录已存在
        print(path + ' 目录已存在')
        return False

def auto_rename_dir(dir_path,ext,perfix='img_old_',start_i=1):
    img_list = os.listdir(dir_path)
    img_list.sort()
    print(len(img_list))
    #start_i = 1
    for img in img_list:
        if os.path.isfile(dir_path+img):  # 用于判断某一对象(需提供绝对路径)是否为文件
            dst_name = perfix + str('%05d'%start_i) + ext
            shutil.copy(dir_path+img,dir_path+dst_name)
            print(dir_path+img,dir_path+dst_name)
            start_i=start_i+1
    return start_i

def auto_rename_dir_remain_tail(dir_path,ext,perfix='img_old_',start_i=1):
    img_list = os.listdir(dir_path)
    img_list.sort()
    print(len(img_list))
    #start_i = 1
    for img in img_list:
        if os.path.isfile(dir_path+img):  # 用于判断某一对象(需提供绝对路径)是否为文件
            dst_name = perfix + str('%05d'%start_i) +'_'+img
            shutil.copy(dir_path+img,dir_path+dst_name)
            print(dir_path+img,dir_path+dst_name)
            start_i = start_i+1
    return start_i

def rename_file_by_replace_char(dir_path):
    file_list = os.listdir(dir_path)
    for file in file_list:
        if True:#file.startswith('contrast_img'):
            file_new = file.replace('img','img0')
            #lst_file = list(file)
            #file_new = ''.join(lst_file.insert(insert_pos, char_add))
            shutil.move(dir_path+file, dir_path+file_new)
            if(file == file_new):
                print(file)

def random_copy(dir_path,select_rato):
    mkdir(dir_path+'JPEGImages_random')
    mkdir(dir_path+'Annotations_random')
    i = 1
    j = 1
    for img in os.listdir(dir_path+'JPEGImages'):
        prob = random.randint(1, 100)
        print("Probability: %d,img:%s" % (prob,img))

        xml_name = os.path.splitext(img)[0] + '.xml'
        if(prob>select_rato):
            continue
        if img.startswith('bt_rnd'):
            pass
        elif (img.startswith('test_')):
            continue
            dst_jpg_name = "test_random_" + str(j) + ".jpg"
            shutil.copy(dir_path+'JPEGImages/'+img, dir_path+'JPEGImages_random/'+dst_jpg_name)
            dst_xml_name = "test_random_" + str(j) + ".xml"
            shutil.copy(dir_path+'Annotations/'+xml_name, dir_path+'Annotations_random/'+dst_xml_name)
            j=j+1
        else:
            dst_jpg_name = "img_random_" + str(i) + ".jpg"
            shutil.copy(dir_path+'JPEGImages/'+img, dir_path+'JPEGImages_random/'+dst_jpg_name)
            dst_xml_name = "img_random_" + str(i) + ".xml"
            shutil.copy(dir_path+'Annotations/'+xml_name, dir_path+'Annotations_random/'+dst_xml_name)
            #i=i+1

            dst_jpg_name = "bt_rnd1_img_random_" + str(i) + ".jpg"
            shutil.copy(dir_path+'JPEGImages/bt_rnd1_'+img, dir_path+'JPEGImages_random/'+dst_jpg_name)
            dst_xml_name = "bt_rnd1_img_random_" + str(i) + ".xml"
            shutil.copy(dir_path+'Annotations/bt_rnd1_'+xml_name, dir_path+'Annotations_random/'+dst_xml_name)
            #i=i+1

            dst_jpg_name = "bt_rnd2_img_random_" + str(i) + ".jpg"
            shutil.copy(dir_path+'JPEGImages/bt_rnd2_'+img, dir_path+'JPEGImages_random/'+dst_jpg_name)
            dst_xml_name = "bt_rnd2_img_random_" + str(i) + ".xml"
            shutil.copy(dir_path+'Annotations/bt_rnd2_'+xml_name, dir_path+'Annotations_random/'+dst_xml_name)

            #dst_jpg_name = "bt_rnd3_img_random_" + str(i) + ".jpg"
            #shutil.copy(dir_path+'JPEGImages/bt_rnd3_'+img, dir_path+'JPEGImages_random/'+dst_jpg_name)
            #dst_xml_name = "bt_rnd3_img_random_" + str(i) + ".xml"
            #shutil.copy(dir_path+'Annotations/bt_rnd3_'+xml_name, dir_path+'Annotations_random/'+dst_xml_name)
            i = i+1

#IMG_MIX_SAVE_PATH = DATA_DIR+'/exchanged/JPEGImages_random/'
#ANN_MIX_SAVE_PATH = DATA_DIR+'/exchanged/Annotations_random/'

def random_fg():
    img_list = os.listdir(FG_PATH)
    for img in os.listdir(IMG_MIX_SAVE_PATH):
        if img.startswith('test_') or img.startswith('bt_rnd1_test_')or img.startswith('bt_rnd2_test_')or img.startswith('bt_rnd3_test_'):
            continue
        else:
            print("mixing:%s"%img)
            random_mix(img,img_list)

if __name__ == "__main__":
    #rename_file_by_replace_char('E:/data/VOC_detect325/JPEGImages_with_tape/')
    #rename_file_by_replace_char('E:/data/VOC_detect325/Annotations_with_tape/')
    #rename_file_by_replace_char('D:/data/VOC_detectBHJ/Annotations/')
    #auto_rename_dir("E:\\data\\formated_img325-2_rm_black\\",".jpg", perfix='real_cam2_img',start_i=1)
    #auto_rename_dir("E:\\data\\05-30no_cell\\Annotations\\", ".xml", perfix='background_img', start_i=310)
    #auto_rename_dir("E:\\data\\05-30no_cell\\ann_ori\\", ".xml", perfix='test_background', start_i=111)
    #auto_rename_dir("E:/data/06-06no_cell/Annotations_train/", ".xml", perfix='background_img', start_i=1400)
    #auto_rename_dir("E:/data/06-12slight/JPEGImages_train/", ".jpg", perfix='slight_bad_img', start_i=1669)
    auto_rename_dir("E:/data/06-15slight/Annotations_train/", ".xml", perfix='test_img', start_i=3267)
    auto_rename_dir("E:/data/06-15slight/JPEGImages_train/", ".jpg", perfix='test_img', start_i=3267)
    '''
    start_i = auto_rename_dir("E:/data/06-02no_cell/JPEGImages_train1/", ".jpg", perfix='slight_bad_img',  start_i=284)
    start_i = auto_rename_dir("E:/data/06-02no_cell/JPEGImages_train2/", ".jpg", perfix='slight_bad_img',  start_i=start_i)
    start_i = auto_rename_dir("E:/data/06-02no_cell/JPEGImages_test1/", ".jpg", perfix='test_slight_bad',  start_i=67)
    start_i = auto_rename_dir("E:/data/06-02no_cell/JPEGImages_test2/", ".jpg", perfix='test_slight_bad',  start_i=start_i)
    '''

    #auto_rename_dir("E:/Program Files (x86)/MindVision/Image/", ".jpg", perfix='capoff_img', start_i=330)
    #exit()
    #auto_rename_dir_remain_tail("E:\\data\\02-20badtape_train5\\JPEGImages\\", ".jpg", perfix='badtape', start_i=16140)
    #auto_rename_dir_remain_tail("E:\\data\\02-20badtape_train5\\Annotations\\", ".xml", perfix='badtape', start_i=16140)
    #auto_rename_dir_remain_tail("E:\\data\\02-13capoff_badtape_test3\\JPEGImages_cam1\\", ".jpg", perfix='test_img', start_i=55)
    #auto_rename_dir_remain_tail("E:\\data\\02-13capoff_badtape_test3\\Annotations_cam1\\", ".xml", perfix='test_img', start_i=55)
    #auto_rename_dir("E:/Program Files (x86)/MindVision/Image/", ".jpg", perfix='badtape',start_i=2007)
    exit()
    random_copy('D:/data/VOC_detect/exchanged/', 100)
    random_fg()


    #random_copy('E:\PStest\wallpaper', 100)