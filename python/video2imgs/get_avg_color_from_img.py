# -*- coding: utf-8 -*-
"""
Created on Thu Nov 1 10:43:29 2018
@author: Administrator
"""
import os
import cv2
import numpy as np
import time

path = 'E:/data/formated_img/capoff_img0304.jpg'
#path = 'E:/data/formated_img/capoff_img0297.jpg'


def compute(path):
    #file_names = os.listdir(path)
    t1 = time.time()
    img = cv2.imread(path, 1)
    h, w = img.shape[:2]
    scope_pix = 10
    if (h > scope_pix*2) and (w > scope_pix*2):
        R_mean = np.mean(img[int(h/2)-scope_pix:int(h/2)+scope_pix, int(w/2)-scope_pix:int(w/2)+scope_pix, 0])
        G_mean = np.mean(img[int(h/2)-scope_pix:int(h/2)+scope_pix, int(w/2)-scope_pix:int(w/2)+scope_pix, 1])
        B_mean = np.mean(img[int(h/2)-scope_pix:int(h/2)+scope_pix, int(w/2)-scope_pix:int(w/2)+scope_pix, 2])
        print(R_mean, G_mean, B_mean)
        print((R_mean+G_mean+B_mean)/3)
    t2 = time.time()
    print(t2-t1)
    #return R_mean, G_mean, B_mean


if __name__ == '__main__':
    compute(path)
    #print(R, G, B)