import tensorflow as tf

x = tf.placeholder(shape=[None,784],dtype=tf.float32)
y = tf.placeholder(shape=[None,10],dtype=tf.float32)
w = tf.Variable(tf.random_normal(stddev=0.01,shape=[784,10],dtype=tf.float32))
b = tf.Variable(tf.zeros(shape=[10],dtype=tf.float32))

init =tf.global_variables_initialier()
with tf.Session() as sess:
    sess.run(init)
    sess.run(w)