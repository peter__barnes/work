import torch
import torch.nn as nn
import torch.nn.functional as F

class ArcLoss(nn.Module):
    def __init__(self,feature_num=10,class_num=2):
        super(ArcLoss, self).__init__()
        self.W = nn.Parameter(torch.randn(feature_num,class_num),requires_grad = True)
    def forward(self,feature,m=0.5,s=64):
        x = F.normalize(feature,dim=1)
        w = F.normalize(self.W,dim=0)
        cosa = torch.matmul(x,w)/s
        a = torch.acos(cosa)
        arcsoftmax = torch.exp(s * torch.cos(a+m)) / torch.sum(torch.exp(s*cosa),dim=1,keepdim=True)
        - torch.exp(s*cosa) + torch.exp(s * torch.cos(a+m))
        return arcsoftmax