import torch
import torchtext

gv = torchtext.vocab.glove(name="6B",dim=50)

def get_wv(word):
    return gv.vectors[gv.stoi[word]]

def sim_10(word,n=10):
    all_list = []
    for i ,w in enumerate(gv.vectors):
        all_list.append([gv.itos[i],torch.dist(w,word)])
    return sorted(all_list,key=lambda x:x[1])[:n]

if __name__ == '__main__':
    wv1 = get_wv("China")
    wv2 = get_wv("Beijing")
    wv3 = get_wv("Japan")
    wv4 = wv3 - wv1 + wv2
    result = sim_10(wv4)
    print(result)
    print(result[0][0])
