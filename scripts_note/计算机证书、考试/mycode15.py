import numpy as np
from PIL import Image

def crop(img_file,mask_file):
    img_array = np.array(Image.open(img_file))
    mask_array = np.array(Image.open(mask_file))
    img_concated = np.concatenate(img_array,mask_array[:,:,[0]],-1)
    img = Image.fromarray(img_concated.astype('uint8'),mode = "RGBA")
    return img

if __name__ == '__main__':
    img = crop('1.jpg','2.jpg')
    img.show()
    img.save('3.jpg')