import torch
import torch.nn as nn
import torch.nn.functional as F

class UpSampling(nn.Module):
    def __init__(self):
        super(UpSampling,self).__init__()
    def forward(self,x):
        return F.interplote(x,scale_rate=2,mode='neareast')

class ConverlutionalLayer(nn.Module):
    def __init__(self,channel_in,channel_out,kernel_size,stride,padding,bias=False):
        super(ConverlutionalLayer, self).__init__()
        self.sub_module = nn.Sequential(
            nn.Conv2d(channel_in,channel_out,kernel_size,stride,padding,bias),
            nn.BatchNorm2d(channel_out),
            nn.LeakyReLU(0.1)
        )
    def forward(self,x):
        self.sub_module(x)

class DownSampling(nn.Module):
    def __init__(self,channel_in,channel_out):
        super(DownSampling, self).__init__()
        self.sub_module = nn.Sequential(
            ConverlutionalLayer(channel_in, channel_out, 3, 2, 1, False)
        )

    def forward(self,x):
        self.sub_module(x)