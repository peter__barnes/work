import PIL.Image as Image

def scale_img(img,scale_side):
    img.thumbnail((scale_side,scale_side))
    bg = Image.new("RGB",(scale_side,scale_side),(0,0,0))
    width,height = img.size
    if scale_side == width:
        bg.paste(img,(0,int((scale_side-height)/2)))
    elif scale_side == height:
        bg.paste(img, (int((scale_side - width) / 2), 0))
    else:
        bg.paste(img, (int((scale_side - width) / 2), int((scale_side-height)/2)) )
    return bg

if __name__ == '__main__':
    image = Image.open("img.png")
    ret = scale_img(image,400)
    ret.show()