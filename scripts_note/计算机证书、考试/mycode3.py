import numpy as np

def convert_matrix_2_square(boxes):
    box_out = boxes.copy()
    width = boxes[:,2] - boxes[:,0]
    height = boxes[:,3] - boxes[:,1]
    center_x = boxes[:,0]+ 0.5*width
    center_y = boxes[:,1]+0.5*height
    max_side = np.maximum(width,height)
    #box_out = [center_x-0.5*max_side,center_y-0.5*max_side, center_x+0.5*max_side,center_y+0.5*max_side]
    box_out[:,0] = center_x-0.5*max_side
    box_out[:,1] = center_y-0.5*max_side
    box_out[:,2] = center_x+0.5*max_side
    box_out[:,3] = center_y+0.5*max_side
    return box_out


if __name__ == "__main__":
    boxes = np.array([[16,36,25,94],[54,62,79,12]])
    ret = convert_matrix_2_square(boxes)
    print(ret)
    #[[-8 36 49 94]
    #[54 24 79 49]]