import torch
import torch.nn as nn

class ConverlutionalLayer(nn.Module):
    def __init__(self,in_channels,out_channels,kernel_size,stride,padding,bias=False):
        super(ConverlutionalLayer, self).__init__()
        self.sub_module = nn.Sequential(
            nn.Conv2d(in_channels=in_channels,out_channels=out_channels,kernel_size=kernel_size,stride=stride,padding=padding,bias=bias),
            nn.BatchNorm2d(out_channels=out_channels),
            nn.LeakyReLU(0.1),
        )
    def forward(self,x):
        return self.sub_module(x)

class ResidualNet(nn.Module):
    def __init__(self,in_channels):
        super(ResidualNet, self).__init__()
        self.sub_module = nn.Sequential(
            ConverlutionalLayer(in_channels,in_channels//2,1,1,0),
            ConverlutionalLayer(in_channels//2, in_channels, 3, 1, 1)
        )
    def forward(self,x):
        return x+self.sub_module(x)

class ConvNet(nn.Module):
    def __init__(self,in_channels,out_channels):
        super(ConvNet, self).__init__()
        self.sub_module = nn.Sequential(
            ConverlutionalLayer(in_channels, out_channels , 1, 1, 0),
            ConverlutionalLayer(out_channels , in_channels, 3, 1, 1),
            ConverlutionalLayer(in_channels, out_channels, 1, 1, 0),
            ConverlutionalLayer(out_channels, in_channels, 3, 1, 1),
            ConverlutionalLayer(in_channels, out_channels, 1, 1, 0),
        )
    def forward(self, x):
        return self.sub_module(x)