import torch
import torch.nn as nn

class Focus(nn.Module):
    def __init__(self,c1,c2,k=1):
        super(Focus, self).__init__()
        self.conv = nn.Conv2d(c1*4,c2,k,1)
    def forward(self,x):
        return self.conv(torch.cat([x[...,::2,::2],x[...,1::2,::2],x[...,::2,1::2],x[...,1::2,1::2]],1))