import torchaudio
import matplotlib.pyplot as plt

file = "1.wav"
audio,sample_rate = torchaudio.load(file)

def normalize(tensor):
    tensor_minus_mean = tensor - tensor.mean()
    return tensor_minus_mean/tensor.abs().max()

print(audio.mean())
print(audio.min())
print(audio.max())

def encoding_miu(audio):
    encoded_audio =  torchaudio.MulawEncoding(audio)
    plt.figure()
    plt.plot(encoded_audio[0,:].numpy())
    plt.show()
    return encoded_audio

def decoding_miu(audio):
    decoded_audio =  torchaudio.MulawDecoding(audio)
    plt.figure()
    plt.plot(decoded_audio[0,:].numpy())
    plt.show()
    return decoded_audio

if __name__ == "__main__":
    encoded_audio = encoding_miu(audio)
    decoded_audio = decoding_miu(encoded_audio)
    result = (decoded_audio-audio).abs() / audio.abs().medium()
    print()