import numpy as np

def iou(box,boxes,is_min=False):
    box_area = (box[2] - box[0])*(box[3] - box[1])
    boxes_area = (boxes[:,2] - boxes[:,0])*(boxes[:,3] - boxes[:,1])
    xx1 = np.maximum(box[0],boxes[:,0])
    yy1 = np.maximum(box[1],boxes[:,1])
    xx2 = np.minimum(box[2],boxes[:,2])
    yy2 = np.maximum(box[3],boxes[:,3])
    inter = (xx2-xx1)*(yy2-yy1)
    outer = boxes_area + box_area - inter
    if is_min:
        return np.true_divide(inter,np.minimum(box_area,boxes_area))
    else:
        return np.true_divide(inter,outer)

def nms(boxes, thres = 0.3, is_min=False):
    if boxes.shape[0] <= 0:
        return np.array([])

    boxes = boxes[(-boxes[:,4]).argsort()]
    r_boxes = []

    while boxes.shape[0] > 1:
        a_box = boxes[0]
        b_boxes = boxes[1:]
        r_boxes.append(a_box)

        boxes = b_boxes[iou(a_box,b_boxes,is_min) < thres]

    if boxes.shape[0] == 1:
        r_boxes.append(boxes[0])
    return np.stack(r_boxes)



if __name__ == '__main__' :
    box = np.array([1,1,11,11])
    boxs =np.array([[1,1,10,10],[11,11,20,20]])
    ret = iou(box,boxs)
    print(ret)

    bs = np.array([[1, 1, 10, 10, 0.98], [1, 1, 9, 9, 0.8], [9, 8, 13, 20, 0.7], [6, 11, 18, 17, 0.85]])
    print((-bs[:,4]).argsort())
    print(nms(bs))