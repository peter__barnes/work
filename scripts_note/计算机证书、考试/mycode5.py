import torch
import torch.nn as nn

class CenterLoss(nn.Module):
    def __init__(self, lambdas, feature_num, class_num):
        super(CenterLoss,self).__init__()
        self.lambdas = lambdas
        self.center = nn.Parameter(torch.randn(feature_num,class_num),requires_grad = True)
    def forward(self,features,labels):
        center_exp = self.center.index_select(dim=0,index = labels.long())
        count = torch.histc(labels,bins=int(max(labels).item()+1), min=0 , max=int(max(labels).item()))
        count_exp = count.index_select(dim=0,index=labels.long())
        loss = self.lambdas/2 * torch.mean(torch.div(torch.sum(torch.pow(features-center_exp,2),dim=1),count_exp))
        return loss


if __name__ == '__main__':
    data = torch.tensor([[3, 4], [5, 6], [7, 8], [9, 8], [6, 5]], dtype=torch.float32)
    label = torch.tensor([0, 0, 1, 0, 1], dtype=torch.float32)
    center_loss = CenterLoss(2,2,5)
    loss = center_loss(data, label)
    print(loss)
