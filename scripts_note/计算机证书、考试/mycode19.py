'''
[-1, -1, -1, -1, 0, -1],
[-1, -1, -1, 0, -1, 100],
[-1, -1, -1, 0, -1, -1],
[-1, 0, 0, -1, 0, -1],
[0, -1, -1, 0, -1, 100],
[-1, 0, -1, -1, 0, 100]
'''

import numpy as np

r_array = np.array([
    [-1, -1, -1, -1, 0, -1],
    [-1, -1, -1, 0, -1, 100],
    [-1, -1, -1, 0, -1, -1],
    [-1, 0, 0, -1, 0, -1],
    [0, -1, -1, 0, -1, 100],
    [-1, 0, -1, -1, 0, 100]
],dtype=np.float32)
q_array = np.zeros(6*6).reshape(6,6)
alpha = 1
gamma = 0.8
for round in range(0,10):
    for i in range(0,6):
        for j in range(0,6):
            if r_array[i][j]>=0:
                q_array[i][j] = q_array[i][j] + alpha*(r_array[i][j] + gamma*max(q_array[j]) - q_array[i][j])
            else:
                continue
print((q_array/np.max(q_array)*100).round())