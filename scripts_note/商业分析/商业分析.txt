以下内容源于沃顿商学院 商业分析课。
=============================================================================================================
【综合项目】
战略的4大核心步骤为，这4大核心步骤都可以从客户、运营、财务、人员四方面分析：
	1.     （问题陈述）Problem Statement. What problem(s) does adblocking software present to a firm like GYF in its ability to attract and maintain advertisement-buying customers?
		·       Customer Analytics - What does the loss of data caused by adblockers mean for market research, predicting consumer behavior, marketing decisions, and/or marketing strategy?

		·       Operations Analytics - What models would you use to predict the effects of adblockers on GYF’s ad-buying customers and why?

		·       Accounting Analytics - How do adblockers potentially affect GYF’s financial bottom line?

		·       People Analytics – What measures could be taken to change or strengthen GYF’s internal organization to deal with adblockers?
	2.    （战略计划） Your Strategy for addressing the problem.
		·       Customer Analytics – How will you implement a strategy driven by data, as opposed to intuition or speculation?

		·       Operations Analytics - What models would you use to test whether your strategy is working?

		·       Accounting Analytics – What financial or nonfinancial metrics would you look to in predicting ways that your adblockers strategy will improve financial outcomes? 

		·       People Analytics - What kinds of people and teams would you assemble to implement your strategy?

	3.    （战略预期）The anticipated Effects of your strategy.
		·       Customer Analytics – How will you implement a strategy driven by data, as opposed to intuition or speculation?

		·       Operations Analytics - What models would you use to test whether your strategy is working?

		·       Accounting Analytics – What financial or nonfinancial metrics would you look to in predicting ways that your adblockers strategy will improve financial outcomes? 

		·       People Analytics - What kinds of people and teams would you assemble to implement your strategy?

	4.   （战略评估）  How you will Measure the success of your strategy.
		·       Customer Analytics – What are the effects of your proposed strategy on GYF's customers, and how you will measure these effects and the changes they portend for market research, predictive analytics, marketing decisions, and marketing strategy?

		·       Operations Analytics - What models would you use to measure the effects of your strategy and why?

		·       Accounting Analytics - How would you calculate or predict any gain or loss of revenue from your strategy? Are there non-financial factors at play?

		·       People Analytics - What are the effects of your strategy on the internal organization? What are the best methods for measuring them?
=============================================================================================================
【用户分析】
精细化的用户分析才能达到精准营销，细粒度的数据往往可以提供更多价值。
	“So if you're still making decisions today with store level data or aggregate data, you're probably giving up a lot of money cuz there's data at a more granular level that's gonna allow you to make the same decisions”（沃顿商学院，商业分析课）

获取高质量、有价值的数据是商业分析、数据分析的基础，garbage in, garbage out，数据和模型都非常重要，如果只能二选一，还是数据更重要。
=============================================================================================================
【人员分析】
大部分人都有认知偏差，而客观数据加上适当的方法，可以一定程度上降低主观感受带来的偏差，但是，样本和模型往往也不完美，只能减少偏差，但依然存在很多偏差，而且使用不当，还会带来谬误。
	1.Non-regressive predictions（非回归预测）。这是一种非常常见的认知偏差，他们用线性的方式对问题进行预测与推断，而没有考虑到，现实中由于种种因素的影响，事情的发展趋势往往存在回归的倾向。
		很多绩效难以长期持续，会产生回归倾向，且伴随大量偶发和变数，需要多做跨时段分析，估计可持续性。现实的数据中，有太多看似可能有价值，但实际是噪音的数据，要多做可持续性分析，剔除掉噪音。
		很多曾经的世界顶尖企业（如50强）、顶尖基金已经沦为平庸，其长期综合水平和大市平均水平一致，某年美国棒球联赛顶尖球队在十年以后，水平依旧只是均值水平，这说明，很多竞赛中的优秀排位、表现是不可持续的，短期选出的那批顶尖选手，存在很大运气成分，而采用短期顶尖样本作为基础数据进行分析，也将包含巨大的噪声。

		When they actually sat down and looked at what predicted performance,they found that once people had been out of college for more than a couple of years, GPA had no much value as a predictor. 

	2.在其他条件相同的情况下，小样本统计结果常常含有更大的误差，对于小样本所能揭示的内容，一定要持谨慎态度，条件允许时要扩大样本。“The smaller the sample,the more we have to be cautious what kind of inference we draw from it. ”

	3.结果偏误（ Outcome Bias )，又称史学家偏误。
		指倾向于以结果来判断决策，而不是当时作决策的过程。然而，很多结果和关键条件是非显著性相关的，只是随机因素和随机结果，并不适合用来解释。

		相似的有，事后聪明式偏差(hindsight bias)，类似事后诸葛亮。是指在事后看待事件结果时,会觉得事件结果比事前预测时更不可避免、更容易预见的倾向。

	4.叙事偏见（Narrative bias）是指人们更容易接受和记住那些有吸引力的故事，而不是冷冰冰的事实和数据。
		例如，一个盲人乞讨者在牌子上写“我什么也看不见！”时，过往行人很少驻留；而诗人让·彼浩勒改为“春天来了，可是我什么也看不见！”后，乞讨者的收入显著增加。

	5.有效性幻觉
		在预测结果与输入信息之间良好吻合的基础上形成的没有根据的自信，被称为“有效性幻觉”（illusion of validity）。

		以前有军官培训活动，结课时会给出评价和预测，教官往往对自己的预测很自信，但后来长期的跟踪结果却说明，这些评价、预测几乎无效，未来军官的实际表现和培训活动中体现出的领导能力几乎是无关的（uncorrelated）。“And he calls this the illusion of validity,
a sense that we think we know much more about people than we actually do. ”
		
	6.巧合、相关性被误以为因果，或是因果关系被倒置
		雪糕销量增加和暴力犯罪同时增加，并不能说明两者有因果关系，可能只是巧合；女性肺癌少也不能说明是基因差异（实际原因是男性吸烟率高这一后天习惯导致）；优秀的员工被送去培训，有时会导致认知为培训导致优秀，事实上弄反了因果。

	进行双盲测试验证，并进行统计分析，是验证策略有效性的较好方法。
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
更多的独立信号、独立意见常能提高估算准度。企业可以让更多人参与决策。以前有个有趣的测试，农场主让游客估算奶牛的重量，单个客人的估算偏差常常很大、错得离谱，但当多个游客的独立估算结果平均时，则能接近真实值。
		这个方法有一个关键点：每个估算者要独立决策，不要提前相互交流，否则他们的估算会产生依赖和关联，将大大降低综合效用。另外，专业领域、培训途径的多元也能带来有效的独立意见。
			无关的独立意见效用是最好的！相较而言，即使相关性较低，但只要存在相关性，依然会大幅降低效用。
			“If the correlation is 0, in other words, if the experts are perfectly independent,then every expert you add creates that much new value.So the equivalent number independent expert is the same.But when those expert opinions becomes correlated even at 0.2 which is a pretty low correlation even at 0.2, you quickly lose the value of adding experts. ”当相关性为0.2，专家数量增加到7-8个时，继续增加专家数量的边际效用就已经非常微弱。（见"关联性对综合意见效用的影响.png"）
			"it shows that as you go from 0 experts to 9,if you have a correlation of 0.2, you never quite get above 4.You asymptote there, where you have nine judges, but because there's a little correlation between them the effective number of judges,there's only three, three and a half.If the correlation's 0.4, it plateaus much more around 2. "当相关性为0.2，即便有9个专家，其实际效用也永远不会超过4个完全独立意见的专家，而当相关性为0.4时，9个专家效用甚至不及2个完全独立的专家。（见"关联性对综合意见效用的影响.png"）
				
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
过程衡量往往比结果衡量更精细、精准。
	最终结果往往具有较大变数，受环境影响起伏不定，短期极端现象未必可以持续，但关键的过程有时却易于衡量，具有更大的可持续性。因此，我们可以把波动较大的最终目标，按关键过程分解为KPI，这些KPI往往更客观、少噪音、可持续，根据KPI进行投资、决策、过程改进。
	例如，足球进球这类最终结果往往有很大变数，但可以通过衡量球员实力、控球率、射门率、射门质量等等长期有效的指标，站在赢面大的一方。
	例如，市场往往波动较大，含有较大噪音，这导致利润起伏不定，现有绩效难以长期维持，但我们可以根据成本、质量、品牌美誉度、效率等指标衡量企业的可持续发展能力。
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
雇佣策略及准度
 	job knowledge tests, 0.48
	cognitive ability tests. 0.51
	use personality test,0.31
	reference checks, 0.26
		靠推荐很难获得真正有效的评价，主要在于，美国法律严格，企业和管理者都怕因给前雇员差评而导致诉讼、追责等问题，所以即便有负面评价，往往也不会告知。
	structured interviews, 0.51
	unstructured interviews（面谈对话等）, 0.31
		以前有军官培训活动，结课时会给出评价和预测，教官往往对自己的预测很自信，但后来长期的跟踪结果却说明，这些评价、预测几乎无效，未来军官的实际表现和培训活动中体现出的领导能力几乎是无关的（uncorrelated）。非结构性面谈有时会产生这种结果，让人误以为自己很了解对方，但其实并非如此。因此，更推荐结构化面试，因为结构化面试更客观。
	work samples, 0.54
	integrity test，0.41

	各种评估都有偏差，相对而言的，工作样本和实战表现或许是最准确的独立方案（非组合方案）。
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
雇佣、晋升、平级转岗，哪一种获取人才的方式更优？
	有人调查过新岗位刚上任不久的员工的表现。得出结论内部晋升依旧是较稳妥的选项，而外部雇佣往往缺乏了解且需要较高成本：
		The people who are hired into the job perform much worse than the people who are promoted for about the first two to three years.Much more likely to get poor ratings, much less likely to get the highest ratings.

		It was also the case that people who are hired were getting paid more.
	
		这一研究仅仅指概率上的优劣，并不是说不需要考虑外部招聘、放弃优秀的外部人才，相反，各个途径都应当综合考虑、对比，只是，重心应当放在内部人才的培养上。
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
经调查，对雇员产出影响的因素从高到低排序为：上级关系0.25、工作满意度0.22、角色冲突0.22、晋升机会0.16、压力0.13、协作者关系0.13、薪酬0.11。
	值得注意的是，我们通常所认为的对产出影响最大的薪酬，其实远没我们以为的那般重要。
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
团队协作中，有些成员处于团队交流的中心位置，会接触到各方面的信息，并传递各种信息，这些在绩效中时常被忽略，但却极为重要，绩效KPI中，应当考核每个成员在团队协作中的作用。

	团队成员的协作可以靠问卷调查的方式收集，如“Network Data-信息收集1.png、Network Data-信息收集2.png”,包括信息的方向（接收信息通常为低评分，发出信息通常为高评分）、信息广度（是否有跨团队、跨部门交流）、信息密度、交流效率/效果等，

	以此可识别团队协作中重要的成员，以及成员的协作贡献绩效。

类似“Network of Collaboration.png”所示，可以观察某点的网络结构的特性：
	1. Network size
		联系的数量
	2. Network strength
		联系的频率、时长
	3. Network range
		此点是否有跨部门联系
	4. Network density
		此点的联系方之间是否有联系，联系方之间也大量相互联系的为稠密，反之为稀疏。同样联系数量和频率、时长下，稀疏的更容易收集信息，因为稀疏的联系方之间缺乏关联（缺乏相关性），利于网络的拓展（获取非重复信息、过滤重复信息）。
	5. Network centrality
		是否处于联系的中心位置，越处于中心的位置，此点越重要。
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Talent Analytics难点：
	1.Context, 
		环境相关因素，我们经常错误的把员工业绩视为员工能力的表现，但实际上，这很大程度上与环境、局势/情景、平台相关，而非员工个人能力。
		环境差异较大的两个角色，通常更难做出比较，胡乱比较的结果往往会失真，只有放在相同、相似的环境下，才利于做比较。
		除非是人为制造相似环境用于比较，否则永远无法剔除环境因素的影响。
	2.interdependence, 
		任何人的工作，都依赖其他人。
		某项统计调查，明星职员跳槽以后的平均绩效水平，从未达到过之前被评为明星时的水平（不是指个体，而是整体评分），这是因为，这些明星员工的优秀业绩，并非完全是他们自己的，也依赖环境和其他同事。
	3.self-fulfilling prophecies,
		预言常常会自我实现，这是因为，你认为这个人很有能力，就会更加注重对其培养，给予其资源和帮助，受重视的人也会有更强的动力，这些都提高了其成功率。
	4.reverse causality
		两个相关因素A和B，有时我们以为是A->B，但实际相反，是B->A。
=============================================================================================================
【财务分析】
应收账款是常见的操纵收入的工具，观测方法是发现将应收账款增长率与收入增长率对比，如果应收增长率大幅高于收入增长率，则可能被操纵，但为了避免季节、周期本身带来的波动被误判为操纵收入，还需要进行同行对比。

R&D支出是常见的操纵利润的工具，观测方法是，R&D第四季度（年报最后一季度）通常是操纵发生的季度：

	1.假设保持年度利润不变（与去年一样），需要让第四季度R&D下降20%以内，这种情况为达目标的企业可能选择削减第四季R&D。

	2.假设保持年度利润不变（与去年一样），需要让第四季度R&D超过20%，甚至超过100%，这种情况很难靠削减单季R&D实现，所以干脆把来年的支出也挪到本年第四季，会出现R&D暴涨，糟糕的本年利润会变得更糟糕，但来年的支出已挪到今年花掉，来年财报会更好看。

	3.假设保持年度利润不变（与去年一样），第四季度R&D可以显著超过去年，则说明企业利润超预期，但是为了保障来年出现不利局面依然留有安全边际，企业可能选择本年第四季把多出的利润通过R&D花掉，会出现R&D暴涨。

	出现2、3两种情况的企业，实际利润质量差别很大，区分的方法在于：假设R&D维持去年水平，观察利润是否达标，如果利润超预期，则是情况3；如果利润显著低于预期，则是情况2。

本福特定律，也称为本福德法则，说明一堆从实际生活得出的数据中，以1为首位数字的数的出现机率约为总数的三成，接近期望值1/9的3倍。推广来说，越大的数，以它为首位的数出现的机率就越低。它可用于检查各种数据是否有造假。
	在十进制首位数字的出现概率（%，小数点后一个位）：
	d	p
	1	30.1%
	2	17.6%
	3	12.5%
	4	9.7%
	5	7.9%
	6	6.7%
	7	5.8%
	8	5.1%
	9	4.6%
	由上表所示，1出现在首位的概率高于30%，而9这样的大数，出现在首位的概率仅仅是4.6%。
		EXCEL具体计算过程参考“work\scripts_note\商业分析\DogDonutBeagleBagel-Example.xlsx”：
			EXCEL取某单元格首字母的公式为"=LEFT(ABS(B6*1000),1)",B6对应数据区,乘以1000是为了将小数变为整数，如0.023，首位为2，而非0。
			EXCEL统计个数为"COUNTIF(区域,K6)"，K6对应单元格数字1-9。
			按照对照表，统计预期和实际的偏差并累加，累加结果高于Cut-off则较高可能造假，反之则真实性较高。

财务和非财务数据的关联性是复杂而难以预测的，没有广泛统一的标准，即便有相关性，也往往不是线性的。不要完全听信他人的经验之谈、类比，这些类比非常容易失真。
	例如，在中国适用的相关性，在其他国家可能不适用，在低端市场适用的相关性，在中高端可能不适用。
	例如，某细分赛道，在尾部市场，花更高价格升级设备提升产品质量可以提高利润，但在头部市场，可能反而导致利润降低。

	因此，战略到执行，再到最终财务体现，其过程往往不同于战略制定时计划的那样，必须对之前战略路径假设的各个环节推理的有效性进行数据分析层面的验证，才能确认战略模型的有效性。
=============================================================================================================
【战略】
商业环境越来越复杂，技术变革越来越快，企业战略也需要有较好的适应性，而非一成不变的教条主义，良好的实践主要有：
	1.敏捷方法，拥抱变化，频繁迭代。
	2.实践试错，类似MVP最小可用、原型。
	3.生态化。让生态中的个体不断变异、试错、进化。

失败案例教训总结：
	高层也需要关注战略执行，而不是把执行抛给基层就完事了：
	 	There are a number of mistakes I've observed over the years. One is that strategy execution or implementation is viewed as a lower-level task or concern. Top managers with this view believe that making strategy work — the decisions and activities associated with this task — is somehow “below them,” literally and figuratively. This often creates a “caste” or class system in which upper management feels that it’s done the hard work — strategic planning — and that the lower-level people then can do the easier work of execution. This is a huge mistake, one that can create cultural rifts and poor communication across organizational levels, leading to ineffective performance and other serious problems. 

	高层要注重战略执行计划，而不仅仅是制定完战略目标和几个里程碑就完事了：
		There are additional pitfalls that threaten the strategy execution process in addition to those suggested above.An important one emanates from not having a solid plan of execution or implementation. Every strategic plan requires an implementation or execution component or plan.

	战略需要拥抱变化：
		Also, a major pitfall with all sorts of related problems is inadequate or inappropriate attention to the management of change. 

	战略制定时，要特别注意设定监测标准，即包括财务指标，也要包括非财务指标。如果没有监测、优化，没形成完整的闭环，通常不是一个全面而有效的战略。
		财务指标往往着眼于当前，战略的财务效益往往滞后，而非财务指标可以很好的补充对战略一致性和长远效益的监控，如客户满意度、产品质量、市场占有率等。

		非财务指标的监控更难，因为数据更难获得，且非标准化、长期化，和最终财务效果的关系难以判定，且统计可靠性不足。但即便如此，非财务指标依旧不可或缺。

