#include <iostream>
#include <string>
using namespace std;
class Shape {
    public:
        virtual void draw() = 0;
        virtual void erase() = 0;
        virtual ~Shape() {}

};
class Circle : public Shape {
    public:
        Circle() {}
        virtual ~Circle() { cout << "\tCircle::~Circle\n";  }
        void draw() { cout << "\tCircle::draw\n"; }
        void erase() { cout << "\tCircle::erase\n"; }

};
class Square : public Shape {
    public:
        Square() {}
        virtual ~Square() { std::cout << "\tSquare::~Square\n";  }
        void draw() { std::cout << "\tSquare::draw\n"; }
        void erase() { std::cout << "\tSquare::erase\n"; }

};
class Line : public Shape {
    public:
        Line() {}
        virtual ~Line() { cout << "\tLine::~Line\n";  }
        void draw() { cout << "\tLine::draw\n"; }
        void erase() { cout << "\tLine::erase\n"; }

};
class LB :public Line
{
    public:
        LB(){}
        virtual ~LB() { cout << "\tLB::~LB\n";  }
        void draw() { cout << "\tLB::draw\n"; }
        void erase() { cout << "\tLB::erase\n"; }
};
int main()
{
    cout<<"let's draw something."<<endl;
    Line l1;
    Square s1;
    LB lb1;
    cout<<"l1 draw :"<<endl;
    l1.draw();
    cout<<"s1 draw :"<<endl;
    s1.draw();
    cout<<"lb1 draw :"<<endl;
    lb1.draw();
    cout<<"lb1 upcasting draw :"<<endl;
    ((Line)(lb1)).draw();
    cout<<"destroy:"<<endl;
    return 0;
}
