#include<iostream>
/*upcasting can let child use parent's function
 * if parent has copy-constructor,copy child will automatic call this copy-constructor
 */
using namespace std;
enum note { middleC, Csharp, Cflat  }; // Etc.
class Instrument {
    public:
        Instrument()
        {
            cout<<"\trun Instrument's common constractor"<<endl;
        }
        Instrument(const Instrument& i)
        {
            cout<<"\trun Instrument's copy constractor"<<endl;
        }
        void play(note) const 
        {
            cout<<"\tplay music"<<endl;
        }
};
// Wind objects are Instruments
// because they have the same interface:
class Wind : public Instrument {
    public:
    Wind(): Instrument()
    {
        cout<<"\trun Wind's constractor"<<endl;
    }
};
void tune(Instrument& i) {
    i.play(middleC);
}
int main() {
    cout<<"create flute:"<<endl;
    Wind flute;
    cout<<"create flute2 by copy:"<<endl;
    Wind flute2 = flute;//copy child will call parent(Instrument)'s copy-constructor
    cout<<"tune(flute):"<<endl;
    tune(flute); // Upcasting
    cout<<"((Instrument)flute).play(middleC):"<<endl;
    ((Instrument)flute).play(middleC);  // Upcasting
    cout<<"use pointer to upcastin:"<<endl;
    Instrument *f = &flute;
    f->play(middleC);
}
