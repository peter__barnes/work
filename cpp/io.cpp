//: C02:Numconv.cpp
// Converts decimal to octal and hex
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
using namespace std;
int main() {
    int number;
    cout << "Enter a decimal number: ";
    cin >> number;
    cout << "value in octal = 0"
    << oct << number << endl;
    cout << "value in hex = 0x"
    << hex << number << endl;
    cout << "value in decimal:";
    cout << setw(8) ; //set width 8
    cout << setfill('&') ; // fill white space with '&'
    cout << setprecision(2); 
    cout << dec << number << endl;  //back to decimal style
    cout << flush; //flush the buffer
    cout << endl; // flushe the buffer and inserts a newline character.

    ofstream fout("write_by_ofstream.txt");//1.open file
    //or set flag in format  "ofstream fout(filename, c++mode or cmode);":
    //ofstream fout("write_by_ofstream.txt",ios_base::out|ios_base::app); //append
    fout << "For your eyes only!\n";	//2.write file
    fout.close();			//3.close file

    ifstream fin("write_by_ofstream.txt"); //1.open file (can check fail open by "!fin")
    char ch;
    while (fin.get(ch))			//2.read file and print by char
        cout << ch;
    fin.close();			//3.close file
    return 0;
} ///:~
/*
 * other io functions:
 * cin.peek() look ahead 1 character, but do not pop next input
 */
/*
File Mode           Constants Constant Meaning                  C equivalent
ios_base::in        Open file for reading.                      "r"
ios_base::out       Open file for writing.                      "w"
ios_base::ate       Seek to end-of-file upon opening file.
ios_base::app       Append to end-of-file.                      "a"
ios_base::trunc     Truncate file if it exists.
ios_base::binary    Binary file.
*/
