// sort algorithm example
#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector

using namespace std;
int main () {
    int myints[] = {1,3};
    std::vector<int> myvector (myints, myints+1);               // 32 71 12 45 26 80 53 33


    // print out content:
    std::cout << "myvector size:"<<endl;
    std::cout << sizeof(myvector)<<endl;

    return 0;
}
