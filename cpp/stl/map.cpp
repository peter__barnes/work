#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector
#include <map>       // std::vector
#include <iterator>
#include <boost/assign.hpp>
using namespace std;
int main()
{
	map<int,string> stud;
	stud.insert(pair<int,string>(5,"james"));
	stud.insert(pair<int,string>(6,"zhangchen"));
	stud.insert(pair<int,string>(3,"liping"));


	//follow 3 lines use boost assign init/add data;
	using namespace boost::assign;
	map<int,string> stud2=map_list_of(5,"james")(6,"zhangchen");
	stud2+=make_pair(3,"liping"),make_pair(7,"liuxiao");

	cout<<"1======================"<<endl;
	cout<<stud[3]<<endl;
	cout<<stud[6]<<endl;
	cout<<stud[0]<<endl;

	map<int,string>::iterator map_iter;
	cout<<"2======================"<<endl;
	for(map_iter=stud.begin();map_iter!=stud.end();++map_iter)
	{
		cout<<map_iter->first<<endl;
		cout<<map_iter->second<<endl;
	}
	return 0;
}
