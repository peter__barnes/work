#include<iostream>
/* Because a friend function is not actually a class member, it’s not inherited. 
 * a friend to a derived class can use base class's private members by upcasting. 
 */
using namespace std;
class pet
{
    protected:
    void eat()
    {
        cout<<"eat"<<endl;
    }
    public:
    void sleep()
    {
        cout<<"sleep"<<endl;
    }
};
class fish:public pet // must use public,or fish can't use pet's members
{
    public:
    // create fish::eat(),it is public and can be used by main()
    using pet::eat;  
};
int main()
{
    fish f;
    f.eat(); //fish::eat(), can't run protected pet::eat() directly in main()!
    f.sleep();//because pet::sleep() already public, can used directly by derived class.
    return 0;
}
