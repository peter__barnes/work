#include<iostream>
using namespace std;
class Engine {
    public:
        void start() const {}
        void rev() const {}
        void stop() const {}
};
class Wheel {
    public:
        void inflate(int psi) const {}
};
class Car {
    public:
        Engine engine;
        Wheel wheel[4];
};
int main() {
    Car car;
    car.wheel[0].inflate(72);
    car.engine.start();
    return 0;
}
