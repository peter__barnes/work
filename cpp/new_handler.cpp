/*
#include<iostream>
#include<cstdlib>
#include<new>
using namespace std;
int main()
{
    new_handler();
    while(1)
    {
        new int[10000];
    }
}
*/
// new_handler example
#include <iostream>     // std::cout
#include <cstdlib>      // std::exit
#include <new>          // std::set_new_handler
#include<unistd.h>
 
using namespace std;
void no_memory () {
  std::cout << "Failed to allocate memory!\n";
  std::exit (1);
}
 
int main () {
  std::set_new_handler(no_memory);
  char *p;
  while(1)
  {
	  std::cout << "Attempting to allocate 1 GiB...";
	  p = new char [(long)40*1024*1024*1024];
	  if(NULL!=p)
	  {
		  std::cout << "Ok\n";
	  }
	  usleep(100000);
  }
//  delete[] p;
  return 0;
}
