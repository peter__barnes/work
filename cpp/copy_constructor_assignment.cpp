#include<iostream>
#include<stdio.h>
using namespace std;
class Test
{
    char * style;
    public:
        Test() 
        {
            cout<<"\tDefault constructor called "<<endl;
        }
        Test(const Test &t)
        {
            cout<<"\tCopy constructor called "<<endl;
        }
        Test(Test && t)  //flag need in compile time: -std=c++0x  or -std=C++11
        {
            cout<<"\tMove constructor called "<<endl;
        }
        Test& operator = (const Test &t)
        {
            cout<<"\tAssignment operator called "<<endl;
        }
};
Test f(Test t) {
    return t;
}

int main()
{
    cout<<"run:Test t1, t2"<<endl;
    Test t1, t2; //Default constructor called 2 times
    cout<<"run:t2 = t1"<<endl;
    t2 = t1;//Assignment operator called
    cout<<"run:Test t3 = t1"<<endl;
    Test t3 = t1;//Copy constructor called

    //Move constructor: assign rvalue temporary or xvalue to object in declaration
    cout<<"run:Test t4 = f(Test())"<<endl;
    Test t4 = f(Test());//Default constructor and Move constructor called
    cout<<"run:Test t5 = std::move(t3)"<<endl;
    Test t5 = std::move(t3);//Move constructor called by xvalue, need flag
        //Test t5 ; t5= std::move(t3); //only call assignment operator
    return 0;
}
