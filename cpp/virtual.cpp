#include <iostream>
#include <typeinfo>
using namespace std;
enum note { middleC, Csharp, Cflat  }; // Etc.
class Instrument {
    public:
        //void play(note) const {
        virtual void play(note) const {
            cout << "Instrument::play" << endl;

        }
};
class Wind : public Instrument {
    public:
       // Override interface function:
            void play(note) const {
                cout << "Wind::play" << endl;
            }
};
void tune(Instrument& i) {
    i.play(middleC);
}
int main() {
    Wind flute;
    tune(flute); // Upcasting
    //Instrument *pp = dynamic_cast<Instrument>(flute);
    cout<<"flute.play():"<<endl;
    cout<<"type:"<< typeid(flute).name() << endl;
    flute.play(middleC);
}
