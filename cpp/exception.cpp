// bad_alloc standard exception
#include <iostream>
#include <exception>
using namespace std;

int main () {
  try
  {
      void * myarray=new int[10000000000][100000000];//bad_alloc by system
      throw 20;//throw int directly
  }
  catch (int e) // can catch throw of int(20)
  {
      cout << "An exception occurred. Exception Nr. " << e << '\n';
  }
  catch (exception& e)//can catch throw of exception(bad_alloc)
  {
      cout << "Standard exception: " << e.what() << endl;
  }
  catch (...)		// catch whatever is left
  {
      cout << "other thing happend"  ;
  }
  return 0;
}
/*
other exceptions of the C++ Standard library:
bad_alloc		thrown by new on allocation failure
bad_cast		thrown by dynamic_cast when it fails in a dynamic cast
bad_exception		thrown by certain dynamic exception specifiers
bad_typeid		thrown by typeid
bad_function_call	thrown by empty function objects
bad_weak_ptr		thrown by shared_ptr when passed a bad weak_ptr
=======================================================================================
throw objects:
A base-class reference can catch all objects of a family, but a derived-class
object can only catch that object and objects of classes derived from that class.
---------------------------------------------------------------------------------------
class bad_1 {...};
class bad_2 : public bad_1 {...};
class bad_3 : public bad 2 {...};
...
void duper()
{
	...
	if (oh_no)
		throw bad_1(); //use default constructor create an object
	if (rats)
		throw bad_2();
	if (drat)
		throw bad_3();
}
...
try {
	duper();
}
catch(bad_3 &be)        //can only catch bad_3
{ // statements }
catch(bad_2 &be)	//can catch bad_2 and bad_3
{ // statements }
catch(bad_1 &be)	//can catch bad_1 bad_2 bad_3
{ // statements }
*/
