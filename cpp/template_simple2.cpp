#include<iostream>
using namespace std;
template <typename Type>  //1
class Box
{
public:
    Type t1;
    void print(Type t);
    void print_t1(void);
};
template <typename Type>  //2 add this to each member of function
void Box<Type>::print(Type t) //3 Box<Type> ,not Box
{
    cout<<t<<endl;
}
template <typename Type>  
void Box<Type>::print_t1(void) 
{
    cout<<this->t1<<endl;
}
template <typename Type>
class SuperBox : public Box<Type> {}; //inheritance
int main()
{
    SuperBox<int> sb;
    Box<double> b;
    b.t1 = 32;
    b.print(100.00);
    b.print_t1();
    sb.print(11);
    return 0;
}
