#include <iostream>
#include <string>
#include <new>
/* 
 * core concept of this file: 
 * do not "delete" things created by "placement new"
 * if it is an object, use destructor to destroy them
 */
using namespace std;
const int BUF = 512;
class JustTesting
{
    private:
        string words;
        int number;
    public:
        JustTesting(const string & s = "Just Testing", int n = 0)
        {words = s; number = n; cout << words << " constructed\n"; }
        virtual ~JustTesting() { cout << words << " destroyed\n"; }
        void Show() const { cout << words << ", " << number << endl; }

};
int main()
{
    char * buffer = new char[BUF]; // get a block of memory
    JustTesting *pc1, *pc2;
    pc1 = new (buffer) JustTesting; // place object in buffer(placement new)
    pc2 = new JustTesting("Heap1", 20); // place object on heap
    cout << "Memory block addresses:\n" << "buffer: " << (void *) buffer << " heap: " << pc2 <<endl;
    cout << "Memory contents:\n";
    cout << pc1 << ": ";
    pc1->Show();
    cout << pc2 << ": ";
    pc2->Show();
    JustTesting *pc3, *pc4;
    // it is not realy new an object,it is create object by constructor
    // then place object to buffer ,which is already created.
    // so do not use delete to destroy it. 
    pc3 = new (buffer+ sizeof (JustTesting)) JustTesting("Bad Idea", 6);//placement new 
    pc4 = new JustTesting("Heap2", 10);
    delete pc2;
    delete pc4;
    pc1->~JustTesting();
    pc3->~JustTesting();
    /*do not delete pc3 and pc1*/
    delete [] buffer;
    cout << "Done\n";
    return 0;
}
