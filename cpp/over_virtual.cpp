#include<iostream>
#include<string>
using namespace std;
class Base{
public:
    virtual int f() const{
        cout<<"Base::f()"<<endl;
        return 1;
    }
    virtual long f(string) const{
        cout<<"Base::f()"<<endl;
    }
};
class Derived3:public Base{
public:
    long f(long) const{
    cout<<"Derived3::f()"<<endl;
    }
};
class Derived4:public Base{
public:
    void f(string,long) const{
    cout<<"Derived4::f()"<<endl;
    }
};
int main()
{
    return 0;
}
