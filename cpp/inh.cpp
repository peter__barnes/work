#include<iostream>
using namespace std;
class pet
{
    protected:
    void eat()
    {
        cout<<"eat"<<endl;
    }
    public:
    void sleep()
    {
        cout<<"sleep"<<endl;
    }
};
class fish:public pet
{
    public:
    using pet::eat;
};
int main()
{
    fish f;
    f.eat();
    f.sleep();
    return 0;
}
