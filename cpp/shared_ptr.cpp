#include <unistd.h>
#include <iostream>
#include <array>
#include <vector>
#include <memory>
using namespace std;

class obj 
{
public:
    ~obj() { std::cout <<"destroying implementation\n"; }
    void do_something() { std::cout << "did something\n"; }
};

int main ()
{
	shared_ptr<int> ptr1(new int);
	*ptr1=1;
//	while(1)
	{
		usleep(100000);//should use nanosleep.usleep is not safe
		int *a=new int[10000000];
		a[0]=1;
		a[1]=2;
		//if remove following 3 lines,mem will leak,use "top" command check it 
		shared_ptr<int[]> ptr2(a);
		//	ptr2[0]=1;
		ptr2[1]=21;
		cout<<"a[0]="<<a[0]<<";a[1]"<<a[1]<<endl;
	}
    shared_ptr<obj> impl(new obj());
    impl->do_something();
	shared_ptr<obj> impl2= impl;
	//share_ptr<FILE> can auto run fclose 
	shared_ptr<FILE> fp(fopen("1.txt","r"),fclose);
}
