#include <iostream>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
using namespace std;
using namespace boost;
 
int main()
{
    string str("readme.txt");
    if (ends_with(str,"txt"))//判断后缀
    {
		//to_upper 改变原字符串，to_upper_copy不改变
        cout << to_upper_copy(str) + " UPPER" <<endl;//大写
    }
    replace_first(str,"readme","followme");//替换
    cout << str <<endl;
    vector<char> v(str.begin(),str.end());
    vector<char> v2 = to_upper_copy(erase_first_copy(v,"txt"));
    for (int i = 0;i  < v2.size();++i)
    {
        cout << v2[i];
    }
	cout<<endl;

	//判断
    string str2("Power Bomb");
    if (iends_with(str2,"bomb"))//ignore case end with ?
    {
        cout << "success iends_with" <<endl;
    }
    if (ends_with(str2,"bomb"))// end with ?
    {
        cout << "success ends_with" <<endl;
    }
    if (contains(str2,"er"))//contain ?
    {
        cout << "success contains" <<endl;
    }
	if (is_equal()("a","a"))
	{
        cout << "success equal" <<endl;
	}

	//trim_left,trim_right,trim删除左右两端的空格，
    string str3 = " samus aran";
    cout << trim_copy(str3)<<endl;//加copy不变原始串,只变拷贝
    cout << trim_left_copy(str3)<<endl;
	cout<< str3 <<endl;

	//repace/erase
    string str4 = "Samus beat the monster.\n";
    cout << replace_first_copy(str4,"Samus","samus");
    replace_first(str4,"beat","kill");
    cout <<str4;
	//search and use sub string
	iterator_range<string::iterator> rge;
	cout << str4.substr(find_first(str4, "the").begin() - str4.begin()) <<endl; 
    return 0;
}
