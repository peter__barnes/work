#include <iostream>
#include<boost/pool/pool.hpp>
using namespace std;
using namespace boost;
int main()
{
	pool<> p(1024*sizeof(int));
	//pool do not need to free manually,boost system will auto free
	int* space=(int*)(p.malloc()); 
	if(nullptr==space)
	{
		cout<<"pool malloc failed"<<endl;
		return -1;
	}
	int i;
	for(i=0;i<1024;i++)
	{
		space[i]=i;
	}
	for(i=0;i<1024;i++)
	{
		cout<<space[i]<<endl;
	}
	return 0;
}
