#include<iostream>
#include<boost/tokenizer.hpp>
using namespace std;
using namespace boost;
int main()
{
	string str("this is a useless string for testing");
	tokenizer <>tok(str);
	//for(auto s:tok)
	for(auto &s:tok)//"&s" copy faster than use "s"
	{
		cout << s <<endl;
	}
	return 0;
}
