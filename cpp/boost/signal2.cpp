#include <signal.h>
#include <iostream>
#include <boost/signals2.hpp>//threads safe
#include <boost/function.hpp>
#include <boost/bind.hpp>
 
using namespace std;
class Button{
private:
	typedef boost::signals2::signal<void(int,int)> signalDef;
	typedef boost::signals2::signal<void(int,int)>::slot_type slotType;
private:
	signalDef mysignal;
	boost::signals2::connection mConnection;
public:
	boost::signals2::connection connectFun(const slotType& type){
		mConnection = mysignal.connect(type);
		return mConnection;
	}
	void sendSignal(){ mysignal(10,20); }
 
};
 
void fun1(int a, int b){
	cout << "add result :" << a + b << endl;
}
void fun2(int a, int b){
	cout << "sub result :" << a - b << endl;
}
int main()
{
 
	Button button;
	boost::signals2::connection con1 = button.connectFun(fun1);
	boost::signals2::connection con2 = button.connectFun(fun2);
	//con3.disconnect();
	while(1){
		button.sendSignal();
		usleep(1000000);
	}
	return 0;
}
