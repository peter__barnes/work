#include<iostream>
#include<boost/multi_array.hpp>
#include <boost/assign.hpp>
using namespace std;
using namespace boost;
using namespace boost::assign;
int main()
{
	multi_array<int,2> ma(extents[2][3]);

	int myints[] = {0,1,2,3,4,5};
	ma.assign(myints,myints+5);//equal to the follow for assignment
	/*
	for (int i = 0,  v = 0; i < 2; ++i)
	{
		for (int j = 0; j < 3;++j)
		{
			ma[i][j] = v++;
		}
	}
	*/
	/* array_data: 
	0 1 2 
	3 4 5 
	*/
	typedef multi_array<int, 2>::index_range range;
    auto view = ma[indices[range(0,2)][range(0,2)] ];

    for (int i = 0; i < 2; ++i)
    {
        for (int j = 0; j < 2;++j)
        {
            cout << view[i][j] << ",";
        }
        cout << endl;
    }
	/*view: 0 1 
			3 4 
	 */
	cout<<"info:"<<endl;
    cout << view.num_elements() << endl;
    cout << *view.shape() << endl;
}
