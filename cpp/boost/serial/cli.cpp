#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
using namespace std;
using namespace boost::asio;
 
//using namespace boost::asio;
 
int main(int argc, char* argv[])
{
	try
	{
		boost::asio::io_service io;
		boost::asio::serial_port sp(io,"/dev/pts/11");
 
		//设置串口参数
		sp.set_option(boost::asio::serial_port::baud_rate(9600));
		sp.set_option(boost::asio::serial_port::flow_control());
		sp.set_option(boost::asio::serial_port::parity());
		sp.set_option(boost::asio::serial_port::stop_bits());
		sp.set_option(boost::asio::serial_port::character_size(8));
 
		boost::system::error_code err;
		while(true)
		{
			boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(2));
			char buf[1024]="";
			sp.read_some(boost::asio::buffer(buf),err);
			if(!err)
			{
					cout<<"[recv data]"<<buf<<endl;
			}
			char buf2[]="client:hello";
			int nWrite = sp.write_some(boost::asio::buffer(buf2),err);
			if(err)
			{
				cout<<"write_some err,errmessage:"<<err.message()<<endl;
				break;
			}
		}
		io.run();
 
	}
	catch (exception& err)
	{
		cout << "Exception Error: " << err.what() << endl;
	}
	
	return 0;
}
