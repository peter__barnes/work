#include<iostream>
#include<boost/format.hpp>
#include<boost/algorithm/string.hpp>
using namespace std;
using namespace boost;
int main()
{
	char text[]="hello";    
    bool is_all_lower = boost::algorithm::all(text, is_lower());
	format fmt = boost::format("<%s> %s in the lower case") % text % (is_all_lower? "is": "is not");
    cout<<fmt.str()<<endl; 

	format fmt2("%s \t %05d");
	cout<<fmt2%"My id is "%125<<endl;
	cout<<fmt2%"james's id is "%127<<endl;
}
