#include<ctime>
#include<boost/random.hpp>
using namespace std;
using namespace boost;
int main()
{
	mt19937 rng(time(NULL));
	cout<<"min="<<mt19937::min()<<"\t"
		<<"max="<<mt19937::max()<<endl;
	for(int i=0;i<10;++i)
	{
		cout<<rng()<<endl;
	}
	cout<<"====================="<<endl;
	random::uniform_int_distribution<> ui(0,255);
	for(int i=0;i<10;++i)
	{
		cout<<ui(rng)<<endl;
	}

	vector<int> v(10);
	rng.generate(v.begin(),v.end());//fill v with random numbers
}
