#include<iostream>
#include<boost/assert.hpp>
using namespace std;
int main()
{
	static_assert(1==sizeof(char));//assert during compile time
	assert(16==0x10);//assert during running time
	assert(16==0x11);
	return 0;
}
