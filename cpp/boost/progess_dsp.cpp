#include <boost/progress.hpp>
#include <iostream>
#include <vector>
using namespace std;
using namespace boost;


int main()
{
	std::vector<string> v(100);

	progress_display pd(v.size());//申明进度条   参数即为进度条一行的总个数 本例为100

	for (auto &x:v)
	{
		cout<<x;
		//int i=pd.count();
		//cout<<i<<endl;
		pd+=50;
		//++pd;  //循环共100次 每循环一次 pd+1  pd/v.size()*100% 即为进度的百分数
		usleep(1000000);
		/*
		for(int i = 0 ;i <6000;i++)
		{
			for(int j = 0 ;j<6000;j++);
		}                          //延时 看的更清楚而已
		*/
	}


	return 0;
}
