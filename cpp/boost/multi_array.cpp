#include<iostream>
#include<boost/multi_array.hpp>
using namespace std;
using namespace boost;
int main()
{
	/// 申明一个3维数组
	multi_array<int, 3> ma(extents[2][3][4]);

	auto shape = ma.shape();
	for (size_t i = 0; i < ma.num_dimensions(); ++i)
	{
		cout << shape[i] << ",";
	}
	cout << endl << ma.num_elements() << endl;
	/// 可以像普通数组一样遍历
	for (int i = 0,  v = 0; i < 2; ++i)
		for (int j = 0; j < 3;++j)
			for (int k = 0;k < 4;++k)
			{
				ma[i][j][k] = v++;
			}
	/// 输出多维数组内所有元素
	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 3;++j)
		{
			for (int k = 0;k < 4;++k)
			{
				cout << ma[i][j][k] << ",";
			}
			cout << endl;
		}
		cout << endl;
	}
	//cout << ma[2][3][4];
	std::array<size_t, 3> idx = {0,1,2};/// 使用array
	ma(idx) = 10; /// 使用operator（）
	cout << ma(idx) << endl;
	cout << "===================================="<< endl;
	//following code resize multi_array from [2][3][4] to [4][3][2]
    multi_array<int, 3> mb(extents[2][3][4]);
    assert(mb.shape()[0] == 2);

    std::array<std::size_t, 3> arr = {4,3,2};
    mb.reshape(arr); /// [2][3][4]->[4][3][2]
    assert(mb.shape()[0] == 4);

    mb.resize(extents[2][9][9]);
    assert(mb.num_elements() == 2*9*9);
    assert(mb.shape()[1] == 9);
	return 0;
}
