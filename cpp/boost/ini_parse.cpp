#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <iostream>
using namespace std;
using namespace boost::property_tree;
 
int main()
{
	ptree pt;
	//open xml and read information to pt
	read_ini("ini_parse.ini", pt);
	//read value to val need a path 
	string ip = pt.get<string>("ip");
	cout<<"ip:"<<ip<<endl;
	int core =pt.get<int>("core");
	cout<<"core:"<<core<<endl;
	return 0;
}
