#include <iostream>
#include <boost/circular_buffer.hpp>
#include <numeric>
#include <assert.h>
using namespace std;
int main(int /*argc*/, char* /*argv*/[])
{
    // 创建一个容量为3的循环缓冲区
    boost::circular_buffer<int> cb(3);

    // 插入一些元素到循环缓冲区
    cb.push_back(1);
    cb.push_back(2);

    // 断言
    assert(cb[0] == 1);
    assert(cb[1] == 2);
    assert(!cb.full());
    assert(cb.size() == 2);
    assert(cb.capacity() == 3);

    // 再插入其它元素
    cb.push_back(3);
    cb.push_back(4);//因为只有3格空间，此次覆盖掉了最早的1
	auto pos = cb.begin();//创建迭代器

    // 求和
    int sum = std::accumulate(cb.begin(), cb.end(), 0);

    // 断言
    assert(cb[0] == 2);
    assert(cb[1] == 3);
    assert(cb[2] == 4);
    assert(*cb.begin() == 2);
    assert(cb.front() == 2);
    assert(cb.back() == 4);
    assert(sum == 9);
    assert(cb.full());
    assert(cb.size() == 3);
    assert(cb.capacity() == 3);
	cout<<"all assert finished"<<endl;

    return 0;
}
