#include<iostream>
//#include<exception>
#include<boost/exception/all.hpp>
using namespace std;
using namespace boost;
struct my_exception:virtual boost::exception //,virtual std::exception
{
};
typedef boost::error_info<struct tag_err_no, int> err_no;
typedef boost::error_info<struct tag_err_str,std::string> err_str;

int main()
{
	try
	{
		try 
		{
			/* 抛出异常，存储错误码 */ 
			throw my_exception() << err_no(10);
		}
		catch (my_exception& e) /* 捕获异常，使用引用形式 */ 
		{
			cout << *get_error_info<err_no>(e) << endl;
			//		cout << e.what() << endl;
			e << err_str("other info"); /* 向异常追加信息 */ 
			throw; /* 再次抛出异常 */ 
		}
	}
	catch (my_exception& e)
	{
		cout << *get_error_info<err_str>(e) << endl;
		//cout << e.what() << endl;
	}
}
