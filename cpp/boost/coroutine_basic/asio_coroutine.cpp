/*
首先，我们需要声明一个corountine，然后将需要重复进入的代码用reenter包括起来。第一次调用foo的时候，代码执行到第一个yield，此时，foo直接返回。

第二次调用的时候，程序直接执行上一次yield之后的代码，即fork，此时，程序调用foo，需要注意的是，被调用的fork不再执行第一个yield，而是直接从当前语句开始执行，于是得到输出foo2 101，返回之后，程序调用fork之后的yield，得到输出foo2 3。

第三次调用的时候，由于不再有任何未执行的yield，因此不再产生任何输出。
*/
/*输出:
foo1 1
foo2 101
foo2 3
*/
#include <boost/asio/yield.hpp>
#include <boost/asio/coroutine.hpp>
#include <iostream>

boost::asio::coroutine c;

void foo(int i)
{
    reenter(c)
    {
        yield std::cout<<"foo1 "<<i<<std::endl;
        fork foo(100);
        yield std::cout<<"foo2 "<< i+1<<std::endl;
    }
}
int main()
{
    foo(1);
    foo(2);
    foo(3);
    return 0;
}
