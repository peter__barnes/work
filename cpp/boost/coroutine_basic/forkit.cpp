/*coroutine变量用this，否则是变量名，最好别在body中定义变量，否则有麻烦（reenter的实现是用switch来做的）。
yield的用法大致如下：
	yield return yield后加return，则reenter块后的代码不执行了。
	yield; 本次进入reenter块什么都不做，但计数会增加
	yield break 终止，再也不会进入reenter块
fork
fork用于“复制”协程，类似于unix的fork，程序先执行fork的代码，完毕后，再接着运行fork下一行代码。
*/
/* 
输出：
forkit1 1
foo1 1
foo2 10
=====
foo2 2
=====
forkit2 100
foo3 3
=====
=====
=====
*/
#include <boost/asio/yield.hpp>
#include <boost/asio/coroutine.hpp>
#include <iostream>
boost::asio::coroutine c;	
boost::asio::coroutine c2;	
void forkit(int i)
{
	reenter(c2) 
	{
		yield std::cout<<"forkit1 "<< i<<std::endl;		
		yield std::cout<<"forkit2 "<< i<<std::endl;					
	}		
}
 void foo(int i)
{
	reenter(c) 
	{
		yield 
		{
			std::cout<<"foo1 "<<i<<std::endl;
			return;
		}
		fork foo(10);
		yield std::cout<<"foo2 "<< i<<std::endl;
		fork forkit(100);		
		yield std::cout<<"foo3 "<< i<<std::endl;
		yield break;
		yield std::cout<<"foo4 "<< i<<std::endl;			
	}
	printf("=====\n");
}
int main()
{
	forkit(1);
	
	foo(1);
	foo(2);
	foo(3);	
	foo(4);	
	foo(5);			
	return 0;
}
