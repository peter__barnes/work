#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/yield.hpp>
#include <boost/asio/coroutine.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind/bind.hpp>
using namespace boost::asio::ip;
using namespace std;
struct session : boost::asio::coroutine
{
  boost::shared_ptr<tcp::socket> socket_;
  boost::shared_ptr<std::vector<char> > buffer_;
 
  session(boost::shared_ptr<tcp::socket> socket)
    : socket_(socket), buffer_(new std::vector<char>(1024))
  {
  }
 
  void operator()(boost::system::error_code ec = boost::system::error_code(), std::size_t n = 0)
  {
    if (!ec) reenter (this)
    {
      for (;;)
      {
        yield socket_->async_read_some(boost::asio::buffer(*buffer_), *this);
        yield boost::asio::async_write(*socket_, boost::asio::buffer(*buffer_, n), *this);
      }
    }
  }
};
int main()
{
	return 0;
}
