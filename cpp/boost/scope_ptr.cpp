#include <string>
#include <iostream>
#include <boost/scoped_ptr.hpp>

using namespace std;
using namespace boost;
class obj 
{
public:
    ~obj() { std::cout <<"destroying implementation\n"; }
    void do_something() { std::cout << "did something\n"; }
};

int main()
{
    scoped_ptr<obj> impl(new obj());
    impl->do_something();
//	scoped_ptr<obj> impl2= impl;//error
	return 0;
}

 
