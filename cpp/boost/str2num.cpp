#include<iostream>
#include<boost/lexical_cast.hpp>
using namespace std;
using namespace boost;
int main()
{
	try
	{
		//C++ std version
		int i2 = stoi("123");
		int l2 = stol("1000");
		string s2=to_string(776L);
		cout<<i2<<"\t"<<l2<<"\t"<<s2<<"\t"<<endl; 

		//boost version
		int i=lexical_cast<int>("123");
		long l=lexical_cast<long>("1000");
		double d=lexical_cast<double>("3.14159e5");
		string s=lexical_cast<string>(123.5);
		cout<<i<<"\t"<<l<<"\t"<<d<<"\t"<<s<<endl; 

		int x= lexical_cast<int>("abc");//exception
	}
	catch(boost::bad_lexical_cast& e)
	{
		cout << e.what() << endl;
		return -1;
	}
	catch(...)
	{
		cout << "other error"  ;
	}
	return 0;
}
