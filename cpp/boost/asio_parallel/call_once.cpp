#include <boost/thread/thread.hpp>
#include <boost/thread/once.hpp>
#include <iostream>

int i = 0;
boost::once_flag flag = BOOST_ONCE_INIT;

void init()
{
	++i;
}

void thread_func()
{
	std::cout<<1<<std::endl;
	boost::call_once(&init, flag);
}

int main(int argc, char* argv[])
{
	boost::thread thrd1(&thread_func);
	boost::thread thrd2(&thread_func);
 
	boost::thread_group grp;
	for (int i = 0; i < 10; ++i)
		grp.create_thread(thread_func);

	thrd1.join();
	thrd2.join();
	grp.join_all( );
	std::cout << i << std::endl; //should show 1,only run once
	return 0;
}
