#include<boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <string>
#include <ctime>
#include <iostream>
#include <boost/enable_shared_from_this.hpp>
using namespace std;
using namespace boost::asio;
typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;

void client_session(socket_ptr sock) {
	while ( true) {
		char data[512];
		size_t len = sock->read_some(buffer(data));
		cout<< data<<endl;
		if ( len > 0)
			write(*sock, buffer("server:ok", 9));
	}
}
int main(int argc, char* argv[])
{
	io_service service;
	ip::tcp::endpoint ep( ip::tcp::v4(), 2001); // listen on 2001
	ip::tcp::acceptor acc(service, ep);
	while ( true) {
		socket_ptr sock(new ip::tcp::socket(service));
		acc.accept(*sock);
		boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(2));boost::thread( boost::bind(client_session, sock));
	}
}
