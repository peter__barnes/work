#include<boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <string>
#include <ctime>
#include <iostream>
#include <boost/enable_shared_from_this.hpp>
using namespace std;
using namespace boost::asio;
using boost::asio::ip::tcp;

int main(int argc, char* argv[])
{
	io_service service;
	ip::tcp::endpoint ep( ip::address::from_string("127.0.0.1"), 2001);
	ip::tcp::socket sock(service);
	//socket can't be copied by = or (),use socket_ptr copy it:
	//typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;
	sock.connect(ep);

	//char data[512]="client:hello";
	//char data2[512];
	boost::array<char,128> w_buf = {"client:hello"};
	boost::array<char,128> r_buf ;
	for(;;)
	{
		boost::system::error_code ignored_error;
		//tcp: [async_]read/write_some
		//udp and icmp: [async_]receive_from/send_to
		sock.write_some(buffer((w_buf)));
		size_t len = sock.read_some(buffer((r_buf)));
		cout<< r_buf.data()<<endl;
		boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(2));
	}
	return 0;
}
