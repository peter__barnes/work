#include <iostream>
#include <string>
#include <list>
#include <thread>
#include <future>
#include <cassert>

// 1.promise 对象可以保存某一类型T的值，该值可被future对象读取（可能在另外一个线程中）

static void PrintValue(std::future<int>& value) {
	std::cout << "Wait..." << std::endl;
	// 线程会阻塞在这里，等待promise去set_value，以此可以达到线程同步，以及线程通信。
	int result = value.get();
	std::cout << "Value: " << result << std::endl;
}

int main()
{
	std::promise<int> promise;
	std::future<int> value = promise.get_future();

	std::thread thread(PrintValue, std::ref(value));

	for (int i = 0; i < 5; ++i) {
		std::this_thread::sleep_for(std::chrono::seconds(1));
		std::cout << i + 1 << "s" << std::endl;
	}

	promise.set_value(101);

	thread.join();
}
