#include <iostream>       // std::cout
#include <boost/thread/thread_guard.hpp>
#include <boost/thread/thread.hpp>
//#include <boost/assign.hpp>
using namespace std;
using namespace boost;
//using namespace boost::assign;
void dummy(int num)
{
	for(int i =0;i<num;i++)
	{
		cout <<i<<endl;
	}
}
int main()
{
	thread t1 = thread(dummy,20);
	thread t2(dummy,10);
	thread t3[3];
	thread_guard<detach> g1(t1);
	thread_guard<detach> g2(t2);
	for(int i=0;i<3;i++)
	{
		thread_guard<detach> tmp(t3[i]);
		t3[i]= thread(dummy,5);
	}
	return 0;
}
