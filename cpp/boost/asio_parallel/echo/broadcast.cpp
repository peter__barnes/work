#define ASIO_STANDALONE
#include <cstdlib>
#include <iostream>
#include "boost/asio.hpp"
using namespace boost;

#define BC_PORT 5000
enum { max_length = 1024 };
int main()
{
    namespace ip = asio::ip;
    asio::io_service io_service;
  
    // Server binds to any address and any port.
    ip::udp::socket socket(io_service,
        ip::udp::endpoint(ip::udp::v4(), 0));
    socket.set_option(asio::socket_base::broadcast(true));
  
    // Broadcast will go to port BC_PORT.
    ip::udp::endpoint broadcast_endpoint(ip::address_v4::broadcast(), BC_PORT);
  
    // Broadcast data.
    //boost::array<char, 4> buffer;
//    char* buf = "this is a broadcast";
	char buf[256];
	strcpy(buf,"this is a broadcast");
    socket.send_to(asio::buffer(buf,strlen(buf)+1), broadcast_endpoint);

    char reply[max_length];
    asio::ip::udp::endpoint sender_endpoint;
    size_t reply_length = socket.receive_from(
        asio::buffer(reply, max_length), sender_endpoint);
    std::cout << "Reply is: ";
    std::cout.write(reply, reply_length);
    std::cout << "\n";
}
