#include <iostream>       // std::cout
#include <chrono>         // std::chrono::milliseconds
#include <boost/chrono/include.hpp>
//#include <thread>         // std::thread
#include <mutex>          // std::timed_mutex
#include <boost/thread/thread_guard.hpp>
#include <boost/thread/thread.hpp>
using namespace std;
using namespace boost;
boost::timed_mutex mtx;

void fireworks() {
	// waiting to get a lock: each thread prints "-" every 200ms:
	cout << boost::this_thread::get_id()<<endl;
	while (!mtx.try_lock_for(boost::chrono::milliseconds(200))) {
		cout << "-";
		// adopt_lock won't lock again if already locked
	//	boost::lock_guard<boost::timed_mutex> gd(mtx,boost::adopt_lock); 
	}
	// got a lock! - wait for 1s, then this thread prints "*"
	this_thread::sleep_for(boost::chrono::milliseconds(1000));
	cout << "*\n";
	mtx.unlock();
}

int main ()
{
	thread threads[10];
	// spawn 10 threads:
	for (int i=0; i<10; ++i)
	{
		threads[i] = thread(fireworks);
	}
	for (auto& th : threads) th.join();

	return 0;
}
