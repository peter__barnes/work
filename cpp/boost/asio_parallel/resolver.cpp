#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include <boost/asio.hpp>
using namespace boost::asio;
//example: find ip by domain("www.yahoo.com"), and print ip
int main(int argc, char* argv[])
{
	io_service service;
	ip::tcp::resolver resolver(service);
	ip::tcp::resolver::query query("www.yahoo.com", "80");
	ip::tcp::resolver::iterator iter = resolver.resolve( query);
	ip::tcp::endpoint ep = *iter;
	std::cout << ep.address().to_string() << std::endl;
	return 0;
}
