#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <mutex>          // std::mutex
#define RAII_VERSION 
using namespace std;
volatile int counter(0); // non-atomic counter
mutex mtx;           // locks access to counter

void attempt_10k_increases() {
    for (int i=0; i<10000; ++i) {
		//try_lock() is non-block mode ,skip if currently locked
        if (mtx.try_lock()) {   // only increase if currently not locked:
#ifdef RAII_VERSION
			//auto lock(block mode),won't skip
			lock_guard<mutex> gd(mtx,adopt_lock); // adopt_lock won't lock again if already locked
			++counter;
#else
            ++counter;
            mtx.unlock();
#endif
        }
    }
}

int main (int argc, const char* argv[]) {
    thread threads[10];
    for (int i=0; i<10; ++i)
	{
        threads[i] = thread(attempt_10k_increases);
//		threads[i].detach();
	}

    for (auto& th : threads) th.join();
    cout << counter << " successful increases of the counter.\n";

    return 0;
}
