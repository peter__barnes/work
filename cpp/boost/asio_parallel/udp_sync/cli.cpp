#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <boost/enable_shared_from_this.hpp>
using namespace std;
using namespace boost::asio;
using boost::asio::ip::tcp;
ip::udp::endpoint ep( ip::address::from_string("127.0.0.1"), 8001);
void sync_echo(std::string msg) {
	io_service service;
	ip::udp::socket sock(service, ip::udp::endpoint(ip::udp::v4(), 0)
			);
	sock.send_to(buffer(msg), ep);
	char buff[1024];
	ip::udp::endpoint sender_ep;
	int bytes = sock.receive_from(buffer(buff), sender_ep);
	std::string copy(buff, bytes);
	std::cout << "server echoed our " << msg << ": "
		<< (copy == msg ? "OK" : "FAIL") << std::endl;
	sock.close();
}
int main(int argc, char* argv[]) {
	char const* messages[] = { "John says hi", "so does James", "Lucy got home", 0 };
	boost::thread_group threads;
	for ( char const** message = messages; *message; ++message) {
		threads.create_thread( boost::bind(sync_echo, *message));
		boost::this_thread::sleep( boost::posix_time::millisec(100));
	}
	threads.join_all();
}
