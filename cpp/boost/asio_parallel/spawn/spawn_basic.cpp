#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <string>
#include <ctime>
#include <iostream>
#include <boost/enable_shared_from_this.hpp>
using namespace std;
using namespace boost::asio;
using std::cerr; using std::endl;
//coroutine function,参数yield_context由spawn创建
//yield_context作为handle来调用的async_函数都会有同步编程的特性
void do_something(yield_context yield)
{
	cout<<"do_something"<<endl;
}
int main(int argc, char* argv[])
{

	io_service io_service_;
/* see:  boost.org official manual
   void spawn(
    Function && function,
    const boost::coroutines::attributes & attributes = boost::coroutines::attributes());

	Parameters:
		void function(basic_yield_context<Handler> yield);

	参考此官方文档spawn原型，有两个参数，参数二为coroutine函数
	coroutine函数也能用bind(见spawn do_echo):
	bind第一个参数传函数名，第二个传shared_ptr,
	后面的参数全按照官网Parameters填对应占位符(do_echo yield对应_1)。
*/
	//create coroutine function
	boost::asio::spawn(io_service_, do_something);
	//此处先于协程执行，只有到run()时，协程才开始运行
	cout<<"main:spawn finished"<<endl;
	//The run() function blocks until all work has finished and there are no more handlers to be dispatched, or until the io_service has been stopped. 
	io_service_.run(); // main blocked,and coroutines are running.
		// "io_service.run();"  equals to: 
		//while ( !io_service.stopped() ) io_service.run_once();
}
