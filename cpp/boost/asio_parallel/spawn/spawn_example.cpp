#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <string>
#include <ctime>
#include <iostream>
#include <boost/enable_shared_from_this.hpp>
using namespace boost::asio;
using std::cerr; using std::endl;
io_service io_service_;
class session: public boost::enable_shared_from_this<session>
{
	public:
		explicit session(ip::tcp::socket socket):
			sock_(std::move(socket)),
			strand_(io_service_),
			uuid_(std::rand())
	{}
		~session() { cerr << "~sessoin ->" << uuid_ << endl; }
		void go()
		{
			//此处为了防止spawn异步操作返回后将自身(session)释放
			//因此增加一个shared_ptr指向自身,并传入到do_echo
			auto self(shared_from_this());
			//创建协程do_echo,协程内async会立即让出权限、spawn返回
			boost::asio::spawn(strand_,
					boost::bind(&session::do_echo, self, _1));
		}
		//协程函数体,参数yield_context由spawn创建
		//yield_context作为handle来调用的async_函数都会有同步编程的特性
		void do_echo(yield_context yield)
		{
			while(1)
			{
				char data[128];
		//协程异步编写,思维方式却和同步类似，不用再写回调函数处理返回
		//而是直接类似同步的方式处理返回值,如下面的data。
	//带yield的async_函数都是立刻yield让出权限,协程也挂起不往后执行
	//系统自动监听异步操作，异步操作执行完后，coroutine也继续运行
	//yield也可被handler函数替代，即指定write事件结束的监听函数
				std::size_t n = sock_.async_read_some(boost::asio::buffer(data), yield);
				cerr << "RECVED:【" << data << "】->" << uuid_ <<endl;
				std::time_t now = std::time(nullptr);
				std::string time_str = std::ctime(&now);
				async_write(sock_, buffer(time_str), yield);
				//sock_.shutdown(ip::tcp::socket::shutdown_send);
			}
		}
	private:
		ip::tcp::socket sock_;
		io_service::strand strand_;
		std::size_t uuid_;
};
//coroutine function,参数yield_context由spawn创建
//yield_context作为handle来调用的async_函数都会有同步编程的特性
void start_accept(yield_context yield)
{
	//port number 2016 
	ip::tcp::acceptor acceptor(io_service_, ip::tcp::endpoint(ip::tcp::v4(), 2016));
	for (;;) {
		boost::system::error_code ec;
		ip::tcp::socket socket(io_service_);
		//协程中所有async_函数都是立刻yield让出权限,协程也挂起不往后执行
		//系统自动监听异步操作，异步操作执行完后，coroutine也继续运行
		acceptor.async_accept(socket, yield[ec]);
		//非协程写法(需要自己写回调函数handle_accept)：
		//acceptor.async_accept(socket, boost::bind( handle_accept, sock, _1 ) );
		if(!ec)
		{
		// make_shared动态分配对象并初始化，返回指向此对象的shared_ptr
			boost::make_shared<session>(std::move(socket))->go();
		}
	}
}
int main(int argc, char* argv[])
{
/* see:  boost.org official manual
   void spawn(
    Function && function,
    const boost::coroutines::attributes & attributes = boost::coroutines::attributes());

	Parameters:
		void function(basic_yield_context<Handler> yield);

	参考此官方文档spawn原型，有两个参数，参数二为coroutine函数
	coroutine函数也能用bind(见spawn do_echo):
	bind第一个参数传函数名，第二个传shared_ptr,
	后面的参数全按照官网Parameters填对应占位符(do_echo yield对应_1)。
*/
	//create coroutine function
	boost::asio::spawn(io_service_, start_accept);
	//The run() function blocks until all work has finished and there are no more handlers to be dispatched, or until the io_service has been stopped. 
	io_service_.run(); // main blocked,and coroutines are running.
		// "io_service.run();"  equals to: 
		//while ( !io_service.stopped() ) io_service.run_once();
}
