#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include "boost/asio.hpp"
using namespace boost;
using asio::ip::tcp;
std::string make_daytime_string()
{
    using namespace std;
    time_t now = time(0);
    return ctime(&now);
}

int main(int argc, char* argv[])
{
    try
    {
        boost::asio::io_service io_service;
        tcp::acceptor acceptor(io_service,tcp::endpoint(tcp::v4(),13));
        for(;;)
        {
            tcp::socket socket(io_service);//创建一个套接字
            acceptor.accept(socket);//接受一个客户端socket
			//acceptor.async_accept(...)则为异步
            std::string message = make_daytime_string();
            boost::system::error_code ignored_error;
            boost::asio::write(socket,boost::asio::buffer(message),ignored_error);
        }
    }
    catch(std::exception & e)
    {
        std::cerr<<e.what()<<std::endl;
    }

    return 0;
}
