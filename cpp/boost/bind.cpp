#include <iostream>
#include <string>
#include <functional>
using namespace std;
using namespace std::placeholders;
  
void test(int i, double d, const string &s)
{
    cout << "i= " << i << " d= " << d <<" s= " << s << endl;
}
int main(int argc, const char *argv[])
{
    function<void( void )> fp;
    string s = "foo";
    int a = 3;
    double b = 6.7;
    fp = bind(&test, a, b, s);
    fp();
  
    function <void (int , const string&)> fp1;
    double b2 = 4.6;
    fp1 = bind(test, _1, b2, _2); //bind可取代函数指针
    fp1(4, "kity");

	auto fp2 = bind(test, _1, b2, _2);//bind+auto取代函数指针
    fp2(5, "superman");
  
    return 0;
}
