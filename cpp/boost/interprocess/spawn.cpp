#include <cstdlib> //std::system
#include <sstream>
#include <iostream>
#include <boost/process/spawn.hpp>
#include <boost/process/search_path.hpp>
using namespace boost::process;
using namespace boost::algorithm;
//bp::spawn function starts a process and immediately detaches it. It thereby prevents the system from creating a zombie process, but will also cause the system to be unable to wait for the child to exit.
int main (int argc, char *argv[])
{
	if(2==argc)
	{
		std::string arg(argv[1]);
		if(0==arg.compare("call"))
		{
			std::cout<<"this is child"<<std::endl;
			std::cout<<getpid()<<std::endl;
			exit(0);
		}
	}
	else
	{
		spawn("spawn.out", "call");
		std::cout<<"this is parent"<<std::endl;
		std::cout<<getpid()<<std::endl;
	}
	exit(0);
}
