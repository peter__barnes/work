#include<iostream>
#include<boost/xpressive/xpressive_dynamic.hpp>
using namespace std;
using namespace boost::xpressive;
int main()
{
	//find
	cregex reg=cregex::compile("a.c");
	bool ret1=regex_match("a+c",reg);
	bool ret2=regex_match("abc",reg);
	cout<<ret1<<"\t"<<ret2<<endl;

	//ignore case
	cregex reg2=cregex::compile("\\d{2}a.c",icase);
	bool ret3=regex_match("33Anc",reg2);
	cout<<ret3<<endl;

	//loop search
	string str("this is a useless string abc anc for test");
	cmatch sub ;
	while(regex_search(str.c_str(),sub,reg))
	{
		cout<<sub[0]<<endl;
		str=sub.suffix().str();
	}

	//loop search by iterator
	string str2("this is a useless string abc anc for test");
	sregex reg4=sregex::compile("\\w* ");
	sregex_iterator pos(str2.begin(),str2.end(),reg4);
	sregex_iterator end;
	for(;pos!=end;++pos)
	{
		cout<<"["<<(*pos)[0]<<"]"<<endl;
	}
	return 0;
}
