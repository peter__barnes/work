#include<iostream>
#include<boost/ref.hpp>
using namespace std;
using namespace boost;
int main()
{
	double height = 1.73;
	auto rw = ref(height); //ref create reference
	cout << rw.get()<<endl;

	string str("hello world");
	auto rw2 = cref(str);
	cout << rw2.get()<<endl;//cref create const reference
	cout << rw2.get().size()<<endl;
	return 0;
}
