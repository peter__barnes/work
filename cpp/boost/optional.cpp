#include <boost/optional.hpp>
#include <iostream>
int a()
{
    int i;
    std::cin >> i;
    return (i > 10) ? i : -1;
}

boost::optional<int> b()
{
    int i;
    std::cin >> i;
    return (i > 10) ? i : boost::optional<int>{};
}

int main()
{
	/*non-optional version*/
	int i = a();
	if (i != -1)
		std::cout << i << std::endl;
	else
		std::cout << "not valid input" << std::endl;

	/*optional version*/
	boost::optional<int> j = b();
	if (j)
		std::cout << *j << std::endl;
	else
		std::cout << "not valid input" << std::endl;
}
