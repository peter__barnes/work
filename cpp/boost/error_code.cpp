#define BOOST_ERROR_CODE_HEADER_ONLY
#include <boost/system/error_code.hpp> 
#include <boost/asio.hpp> 
#include <iostream> 
#include <string> 
using namespace std;
using namespace boost;

class application_category :  //user defined error code
	public system::error_category 
{ 
	public: 
		const char *name() const BOOST_SYSTEM_NOEXCEPT
		{ return "application"; } 
		string message(int ev) const { return "error message"; } 
}; 
application_category cat; 

int main() 
{ 
	//system/boost defined error code
	system::error_code ec; 
	string hostname = boost::asio::ip::host_name(ec); 
	cout << ec.value() << endl; 
	cout << ec.category().name() << endl; 
	cout << ec.message() << endl; 
	cout << endl;

	//user defined error code  
	boost::system::error_code ec2(-6, cat); 
	cout << ec2.value() << endl; 
	cout << ec2.category().name() << endl; 
	cout << ec2.message() << endl; 
} 
