#include <iostream>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
using namespace std; 
int main()
{
	boost::uuids::uuid a_uuid = boost::uuids::random_generator()(); // 这里是两个() ，因为这里是调用的 () 的运算符重载
	const string tmp_uuid = boost::uuids::to_string(a_uuid);
	cout<<tmp_uuid<<endl;
	return 0;
}
