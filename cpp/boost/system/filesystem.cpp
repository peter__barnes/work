#include "boost/filesystem.hpp"   // 包含所有需要的 Boost.Filesystem 声明
#include <iostream>               // 使用 std::cout
#include <string>
#include <sstream>
namespace fs = boost::filesystem;
using namespace std; 
int main(int argc, char **argv)
{
	fs::path f("/usr/local/bin/name.txt");
 
	cout<<"f.string()	"<<f.string()<<endl;
	cout<<"f.root_name()	"<<f.root_name()<<endl;
	cout<<"f.root_directory()	"<<f.root_directory()<<endl;
	cout<<"f.root_path()	"<<f.root_path()<<endl;
	cout<<"f.relative_path()	"<<f.relative_path()<<endl;
	cout<<"f.filename()	"<<f.filename()<<endl;
	cout<<"f.parent_path()	"<<f.parent_path()<<endl;
	cout<<"f.stem()	"<<f.stem()<<endl;
	cout<<"f.extension()	"<<f.extension()<<endl;
	cout<<"f.replace_extension	"<<f.replace_extension("new")<<endl;
	char buf[] = "hello";
	cout<<"append	"<<f.append(buf, buf + sizeof(buf))<<endl;
	cout<<"f.remove_filename()	"<<f.remove_filename()<<endl;
	cout << "convert to std::string()=>" << fs::initial_path().string()<<endl;
 
	return 0;
}
/* 其他path相关用法：
system_complete(path); 返回完整路径(相对路径 + 当前路径)
exists(path); 目录是否存在
is_directory(path);
is_directory(file_status); 是否是路径
is_empty(path); 文件夹是否为空，必须保证路径存在，否则抛异常
is_regular_file(path);
is_regular_file(file_status); 是否是普通文件
is_symlink(path);
is_symlink(file_status); 是否是一个链接文件
file_status status(path); 返回路径名对应的状态
initial_path(); 得到程序运行时的系统当前路径
current_path(); 得到系统当前路径
current_path(const Path& p); 改变当前路径
space_info space(const Path& p); 得到指定路径下的空间信息，space_info 有capacity, free 和 available三个成员变量，分别表示容量，剩余空间和可用空间。
last_write_time(const Path& p); 最后修改时间
last_write_time(const Path& p, const std::time_t new_time); 修改最后修改时间
bool create_directory(const Path& dp); 建立路径
create_hard_link(const Path1& to_p, const Path2& from_p);
error_code create_hard_link(const Path1& to_p, const Path2& from_p, error_code& ec); 建立硬链接
create_symlink(const Path1& to_p, const Path2& from_p);
create_symlink(const Path1& to_p, const Path2& from_p, error_code& ec); 建立软链接
remove(const Path& p, system::error_code & ec = singular); 删除文件
remove_all(const Path& p); 递归删除p中所有内容，返回删除文件的数量
rename(const Path1& from_p, const Path2& to_p); 重命名
copy_file(const Path1& from_fp, const Path2& to_fp); 拷贝文件
omplete(const Path& p, const Path& base = initial_path<Path>()); 以base以基，p作为相对路径，返回其完整路径
create_directories(const Path & p); 建立路径
*/
