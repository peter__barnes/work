#include "boost/filesystem.hpp"   // 包含所有需要的 Boost.Filesystem 声明
#include <iostream>               // 使用 std::cout
#include <string>
#include <sstream>
using namespace std; 
using namespace boost::filesystem; 
int main()
{
	path dir("/usr/local/");
	for (recursive_directory_iterator iter(dir); iter != recursive_directory_iterator(); iter++)
	{
		cout << *iter << endl;
		if (is_directory(*iter) && iter.level() > 0)// 只在路径的基础上再打开一层文件夹，此时level = 1，遇到文件夹设置no_push()不再深入
		{
			iter.no_push();
		}
		/*
		else if (!is_directory(*iter))
		{
			std::cout << *iter << std::endl;
		}// 同理还有pop()方法跳出该目录
		*/
	}

}
