#include <boost/timer/timer.hpp>
#include <memory>
#include <vector>
#include <string>
#include <iostream>

using namespace std;
using namespace boost::timer;


vector<string> createVector_98()
{
    vector<string> vec;
    for (int i = 0; i < 10; ++i){
            vec.emplace_back("helloworld");
    }
    return vec; //no move version
}

vector<string> createVector_11()
{
    vector<string> vec;
    for (int i = 0; i < 100; ++i){
        vec.emplace_back("helloworld");
    }
    return move(vec); //move version
}

int main()
{
    const int TEST_TIMES = 100;

    vector<string> result;
	//compare time usage with cpu_timer 
    cpu_timer timer;
    timer.start();
    for (int i = 0; i < TEST_TIMES; ++i){
        result = createVector_98();
    }
    cout << "no move" << timer.format(6) << endl;

    timer.start(); // don't call resume()
    
    for (int i = 0; i < TEST_TIMES; ++i){
        result = createVector_98();
    }
    cout << "use move" << timer.format(6) << endl;
}
