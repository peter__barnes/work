#include<iostream>
using namespace std;
template<class TypeX>
class Box
{
    public:
        int i;
        void print(TypeX t);
};

template<class TypeX>
void Box<TypeX>::print(TypeX t)
{
    cout<<t<<endl;
}

int main()
{
    Box<int> box;
    box.print(100);
    return 0;
}
