#include<iostream>
#include<stdio.h>
#include<vector>
#include<valarray>
#include<array>
#include<stdlib.h>
using namespace std;
int main(void)
{
    vector<int> v2(8); // an array of 8 int elements
    vector<int> v3(8,10); // an array of 8 int elements,each set to 10
    valarray<int> v4(10,8);
    cout<<v3[5]<<endl;
    cout<<v3.size()<<endl;
    cout<<v4[5]<<endl;
    cout<<v4.size()<<endl;
    cout<<"1111111111"<<endl;
    vector <int> a;
    a.push_back(1);
    a.push_back(2);
    cout<<a.back()<<endl;
    cout<<a.at(0)<<endl;
    cout<<a.at(1)<<endl;
    a[0]=100;
    cout<<a[0]<<endl;
    cout<<a[1]<<endl;
    cout<<"2222222222"<<endl;
    a.erase(a.begin());
    a.insert(a.begin(),3);
    cout<<a[0]<<endl;
    cout<<a[1]<<endl;

    return 0;
}
