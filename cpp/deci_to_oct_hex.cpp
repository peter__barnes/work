//: C02:Numconv.cpp
// Converts decimal to octal and hex
#include <iostream>
#include <cstdlib>
using namespace std;
int main() {
    int number;
    cout << "Enter a decimal number: ";
    cin >> number;
    cout << "value in octal = 0"
    << oct << number << endl;
    cout << "value in hex = 0x"
    << hex << number << endl;
    cout << "value in decimal:";
    cout << dec << number << endl;
    cout << true << "!\n";
    cout << flush; //flush the buffer
    cout << endl; // flushe the buffer and inserts a newline character.
} ///:~
