#include <fstream>
#include <iostream>
#include <string>
using namespace std;
ofstream out("HowMany.out");
class HowMany {
    static int objectCount;
    public:
    HowMany() { objectCount++;  }
    static void print(const string& msg = "") {
        if(msg.size() != 0) cout << msg << ": ";
        cout << "objectCount = "
            << objectCount << endl;

    }
    virtual ~HowMany() {
        objectCount--;
        print("~HowMany()");

    }

};
int HowMany::objectCount = 0;
HowMany f(HowMany x) {
    x.print("x argument inside f()");
    return x;
}
int main() {
    HowMany h;
    HowMany::print("after construction of h");
    HowMany h2 = f(h);
    HowMany::print("after call to f()");
    return 0;
}
