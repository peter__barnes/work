/*
原题如下(实际编码只实现了核心功能，为了快速试用，简化了题目约束):

	题目是模拟二维的捕食-被捕者，在一个20x20的格子中，两种生物，一个是蚂蚁，一个是狮蚁；
	狮蚁吃蚂蚁;

	蚂蚁：一个计数单位time，每过三个time，每个time移动一次，随机上下左右，碰到随机的方向有生物就不动，
	碰到边界也是这样，当第三个time（移动完之后）时蚂蚁随机在一个方向繁殖，
	再次繁殖要在过三个time，没有可用的格子就不繁殖。

	狮蚁：一个计数单位time，每过三个time，每个time移动一次，
	随机上下左右（如果附近有蚂蚁，就移动到那个格子，吃掉蚂蚁），同类不能相吃。
	存活8个time进行繁殖，方向与蚂蚁相同，
	如果在连续3个time中没有吃到蚂蚁就好饥饿而死，并把这个拿掉，不占格子。

	有个基类定义两个物种通用的数据，基类有move的虚函数，派生蚂蚁和狮蚁两个类，
	开始放5只狮蚁和100只蚂蚁进行模拟，在time后显示数量的变化，并且按下enter进行下个time。
*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <unistd.h>
const int FULL_AFTER_EAT = 5;
using namespace std;
class ant* space[10][10];
struct POS
{
	int x;
	int y;
};
class ant
{
	private:
		struct POS pos;
		int born_cnt;
		int type;
	protected:
		void set_type(int ant_type)
		{
			type = ant_type;
		}
		void set_born_cnt(int cnt)
		{
			born_cnt = cnt;
		}
		void plus_born_cnt()
		{
			++born_cnt;
		}
	public :
		ant(int x,int y)
		{
			pos.x=x;
			pos.y=y;
			born_cnt=0;
		}
		~ant()
		{
		}
		int get_type()
		{
			return type;
		}
		int get_born_cnt()
		{
			return born_cnt;
		}
		struct POS get_pos()
		{
			return pos;
		}
		struct POS try_move()
		{
			struct POS move_pos = pos;
			int direct=rand() % 4;
			if(0 == direct)
			{
				move_pos.x=move_pos.x - 1;
			}
			else if(1 == direct)
			{
				move_pos.x=move_pos.x + 1;
			}
			else if(2 == direct)
			{
				move_pos.y=move_pos.y - 1;
			}
			else if(3 == direct)
			{
				move_pos.y=move_pos.y + 1;
			}
			return move_pos;
		}
		void set_pos(struct POS move_pos)
		{
			pos = move_pos;
		}
		void move(class ant* space[10][10])
		{
			//plus_born_cnt();
			cout <<"try moving"<<endl;
			struct POS move_pos = try_move();
			if(move_pos.x<0  || move_pos.x>9 ||move_pos.y<0  || move_pos.y>9 )
			{
				cout<<"reach border\n";
				return;
			}
			if(NULL != space[move_pos.x][move_pos.y])
			{
				cout<<"not empty\n";
				return;
			}
			space[move_pos.x][move_pos.y] = this;
			space[pos.x][pos.y]=NULL;
			set_pos(move_pos);
		}
		virtual void act(class ant* space[10][10]){};
};

class common_ant : public ant
{
	private:
	protected:
		~common_ant();
	public:
		common_ant(int x,int y):ant(x,y)
		{
			set_type(0);
		}
		void born()
		{
			cout <<"try born"<<endl;
			set_born_cnt(0);
			struct POS born_pos = try_move();
			if(born_pos.x<0  || born_pos.x>9 ||born_pos.y<0  || born_pos.y>9 )
			{
				cout<<"reach border\n";
				return;
			}
			if(NULL != space[born_pos.x][born_pos.y])
			{
				cout<<"not empty\n";
				return;
			}
			space[born_pos.x][born_pos.y]=new common_ant(born_pos.x,born_pos.y) ;
		}
		void act(class ant* space[10][10])
		{
			int cnt = get_born_cnt();
			cout<<"cnt="<<cnt<<endl;
			plus_born_cnt();
			if(cnt >=3)
			{
				cout<<"try moving\n";
				((class ant *)this)->move(space);
				born();
			}
		}
};

class big_ant : public ant
{
	private:
		int food;
	protected:
		~big_ant(){};
		void set_food(int cnt)
		{
			food = cnt;
		}
		void reduce_food()
		{
			--food ;
		}
	public:
		big_ant(int x,int y):ant(x,y)
		{
			food = FULL_AFTER_EAT ;
			set_type(1);
		}
		int get_food()
		{
			return food;
		}
		void born()
		{
			//set_born_cnt(0);
			struct POS born_pos = try_move();
			if(born_pos.x<0  || born_pos.x>9 ||born_pos.y<0  || born_pos.y>9 )
			{
				cout<<"reach border\n";
				return;
			}
			if(NULL != space[born_pos.x][born_pos.y])
			{
				cout<<"not empty\n";
				return;
			}
			cout<<"borning\n";
			space[born_pos.x][born_pos.y]=new big_ant(born_pos.x,born_pos.y) ;
		}
		bool eat()
		{
			struct POS self_pos=get_pos();
			struct POS eat_pos;
			bool find = false;
			if(self_pos.x-1>=0 && NULL != space[self_pos.x-1][self_pos.y])
			{
				if(0==space[self_pos.x-1][self_pos.y]->get_type())
				{
					eat_pos.x=self_pos.x-1;
					eat_pos.y=self_pos.y;
					find = true;
				}
			}
			else if(self_pos.x+1<10 && NULL != space[self_pos.x+1][self_pos.y])
			{
				if(0==space[self_pos.x+1][self_pos.y]->get_type())
				{
					eat_pos.x=self_pos.x+1;
					eat_pos.y=self_pos.y;
					find = true;
				}
			}
			else if(self_pos.y-1>=0 && NULL != space[self_pos.x][self_pos.y-1])
			{
				if(0==space[self_pos.x][self_pos.y-1]->get_type())
				{
					eat_pos.x=self_pos.x;
					eat_pos.y=self_pos.y-1;
					find = true;
				}
			}
			else if(self_pos.y+1<10 && NULL != space[self_pos.x][self_pos.y+1])
			{
				if(0==space[self_pos.x][self_pos.y+1]->get_type())
				{
					eat_pos.x=self_pos.x;
					eat_pos.y=self_pos.y+1;
					find = true;
				}
			}
			if(true == find)
			{
				cout<<"eat\n";
				delete space[eat_pos.x][eat_pos.y];
				space[eat_pos.x][eat_pos.y] = NULL;
				int food=FULL_AFTER_EAT;
			}
			return find;
		}
		void act(class ant* space[10][10])
		{
			food--;
			if(0==food)
			{
				POS pos = get_pos();
				space[pos.x][pos.y] = NULL;
				delete this;
				cout << "hungried to death\n";
				return;
			}
			int cnt = get_born_cnt();
			if(2 == cnt%3)
			{
				((class ant *)this)->move(space);
			}
			eat();
			if(7 == cnt%8)
			{
				born();
			}
			plus_born_cnt();
		}
};

int action_space(class ant* space[10][10])
{
	int i,j;
	for(i=0;i<10;i++)
	{
		for(j=0;j<10;j++)
		{
			if(NULL == space[i][j])
			{
			}
			else if(0 == space[i][j]->get_type())
			{
				//((class common_ant *)space[i][j])->act();
				space[i][j]->act(space);
			}
			else if(1 == space[i][j]->get_type())
			{
				space[i][j]->act(space);
			}
		}
	}
}

int print_space(class ant* space[10][10])
{
	int i,j;
	for(i=0;i<10;i++)
	{
		for(j=0;j<10;j++)
		{
			if(NULL == space[i][j])
			{
				cout <<"- ";
			}
			else if(0 == space[i][j]->get_type())
			{
				cout <<"0 ";
			}
			else if(1 == space[i][j]->get_type())
			{
				cout <<"1 ";
			}
		}
		cout <<endl;
	}
	cout <<endl;
	cout <<"======================================="<<endl;
}

int main()
{
	//	cout<<"hello world\n";
	srand((unsigned)time(0));
	memset(space,0,100);

	space[1][8]=new common_ant(1,8) ;
	space[2][8]=new big_ant(2,8) ;
	while(1)
	{
		usleep(1000000);
		print_space(space);
		//space[2][8]->move(space);
		action_space(space);
	}
}

