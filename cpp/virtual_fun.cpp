#include<iostream>
#include<string>
using namespace std;
class Pet{
public:
    virtual string speak() const {
    return "speak";
    }
};

class Dog : public Pet{
public:
    string speak() const{
    return "Bark!";
    }
};
int main()
{
    Dog bob;
    Pet* p1= &bob;
    Pet p2;
    cout<<"p1->speak:"<<p1->speak()<<endl;
    cout<<"p2->speak:"<<p2.speak()<<endl;
}
