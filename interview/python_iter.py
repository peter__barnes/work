#预测打印结果，说明为什么
import os
lst = [1, 1, 2, 3]
for item in lst:
    if item == 1:
        lst.remove(item)
print(lst)
#打印[1, 2, 3]，因为迭代器失效问题，导致第二个1漏删。python禁止在迭代器遍历的过程中删除元素