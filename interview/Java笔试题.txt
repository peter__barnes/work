职位类型：Java
姓名：


提示：以下皆为闭卷
【选择题】
　　1．下列语句哪一个正确（ ）
　　A． Java程序经编译后会产生machine code
　　B． Java程序经编译后会产生byte code
　　C． Java程序经编译后会产生DLL
　　D． 以上都不正确　　

　　2．下列是合法的运算符的是（ ）
　　A．&& B．<> C．if D．:=　　

      3．执行如下程序代码
　　a=0;c=0;
　　do{
　　--c;
　　a=a-1;
　　}while(a>0);
　　后，C的值是（ ）
　　A．0 B．1 C．-1 D．死循环　　

      4．下列语句正确的是（ ）
　　A． 形式参数可被视为local variable
　　B． 形式参数可被字段修饰符修饰
　　C． 形式参数为方法被调用时，真正被传递的参数
　　D． 形式参数不可以是对象　　





【简答题】
1、在java中如果声明一个类为final，表示什么意思？



2、父类的构造方法是否可以被子类覆盖（重写）？



3、请讲述String 和StringBuffer的区别。



4、结合分析JAVA sleep()和wait()方法的区别。




5、简述ORM，并说明其适用场景和优缺点





6、#SQL编程，请从person表中查询出grade级别最小的人员
数据表person结构如下：
person表		
id	int	主键id
name	varchar	姓名
year	int	出生年份
		
数据表grades结构如下：		
id	int	主键id
grade	int	级别
personID		person表的主键




7、（1）简述一种常见消息队列中间件的功能和优缺点，对比Redis
（2）介绍4个以上Redis数据类型和数据结构，并简要概括其优缺点



8.列出20个常用Linux命令（不同参数只算一个）



