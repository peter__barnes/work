#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //ui->text1->setText("11111");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_rb1_toggled(bool checked)
{
    if(checked)
    {
        ui->text1->setText("football toggled");
    }
}

void MainWindow::on_rb2_toggled(bool checked)
{
    if(checked)
    {
        ui->text1->setText("basketball toggled");
    }
}
