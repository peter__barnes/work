#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_cbb1_currentIndexChanged(int index)
{
    int cbb1_index=ui->cbb1->currentIndex();
    switch(cbb1_index){
        case 0:
            ui->cbb2->clear();
            ui->cbb2->addItem("数学");
            ui->cbb2->addItem("物理");
            break;
        case 1:
            ui->cbb2->clear();
            ui->cbb2->addItem("计算机");
            ui->cbb2->addItem("土木工程");
            break;
        case 2:
            ui->cbb2->clear();
            ui->cbb2->addItem("语言文学");
            ui->cbb2->addItem("历史");
            break;
        case 3:
            ui->cbb2->clear();
            ui->cbb2->addItem("政治");
            ui->cbb2->addItem("法律");
            break;
    }
}
