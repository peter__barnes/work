import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Text {
        id: text1
        x: 203
        y: 24
        width: 77
        height: 26
        text: qsTr("Hello World")
        font.pixelSize: 12
    }

    BorderImage {
        id: borderImage
        x: 208
        y: 131
        width: 225
        height: 255
        source: "kasha.jpeg"
    }
}
