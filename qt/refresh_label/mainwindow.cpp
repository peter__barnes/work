#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label->setText("ui->setupUi finished");

    //创建定时器
    QTimer *testTimer = new QTimer(this);
    //将定时器超时信号与槽(功能函数)联系起来
    connect( testTimer, SIGNAL(timeout()), this, SLOT(on_refresh()) );
    //开始运行定时器，定时时间间隔为1000ms
    testTimer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_refresh()
{
    static int i=0;
    if(i<1024)
    {
        i++;
    }
    else
    {
        i=0;
    }
    char str[6] ;
    sprintf(str, "%d", i);
    ui->label->setText(str);
}
