#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QDir>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   
    ui->setupUi(this);
    QWebEngineView *view = ui->webEngineView;
    //view->load(QUrl("http://www.baidu.com"));
    //view->load(QUrl("file:///home/chentr/work/qt/echarts_show_html/dynamic_bar.html"));

    char* path=QDir::currentPath().toLatin1().data();
    qDebug(path);
    //build_path + "../echarts_show_html/"  will return to src_path
    view->load(QUrl("file://"+QDir::currentPath()+"/../echarts_show_html/dynamic_bar.html"));

    view->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}
