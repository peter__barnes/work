#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        printf("Here is child process\n");
    }
    else if (pid > 0)
    {
        printf("Here is parent process\n");
    }
    else
    {
        // fork failed
        printf("fork() failed!\n");
        return 1;
    }
    return 0;
}
