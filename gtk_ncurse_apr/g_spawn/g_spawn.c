#include <stdio.h>
#include <glib.h>
int main()
{
    int counter = 0;
    GPid        pid;
    gboolean    ret;
    gchar      *args[]={"print",NULL};//print in current path
    /* Spawn child process */
    ret = g_spawn_async_with_pipes( NULL, args, NULL,
            0, NULL, NULL, &pid, NULL, NULL, NULL, NULL );
    if( ! ret )
    {
        g_error( "SPAWN FAILED" );
        return 1;
    }
    printf("parent_process:hello\n");
    getchar();//in windows,need stop to watch stdout
    return 0;
}
