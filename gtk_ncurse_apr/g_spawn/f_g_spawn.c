#include <stdio.h>
#include <glib.h>
int print_process()
{
    printf("child_process:hello\n");
    return 0;
}
gboolean my_spawn(gchar **argv,GPid *child_pid)
{
    gboolean    ret;
    ret = g_spawn_async_with_pipes( NULL, argv, NULL,
            0, NULL, NULL, child_pid, NULL, NULL, NULL, NULL );
    return ret;
}
int main(int argc, char *argv[])
{
    int counter = 0;
    GPid        child_pid;
    gboolean    ret;
    gchar      *args[]={"f_g_spawn","child",NULL};/*then child have 1 more args "child" than parent*/
    if(argc == 1) /*if parent process*/
    {
        /* Spawn child process */
        ret = my_spawn(args,&child_pid);
        if( ! ret )
        {
            g_error( "SPAWN FAILED" );
            return 1;
        }
        printf("parent_process:hello\n");
        getchar();/*in windows,need stop to watch stdout*/
    }
    else /*if child process*/
    {
        print_process();
    }
    return 0;
}
