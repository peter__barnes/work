#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<glib.h>
enum 
{
    MY_DATA_TYPE 
}Data_type;
struct my_data
{
    int index;
    char data[10];
};
void print_data(gpointer data, gpointer type)
{
    if(type==MY_DATA_TYPE)
    {
        if(data!=NULL)
        {
            printf("index=%d,data=%s\n",((struct my_data *)data)->index,((struct my_data *)data)->data);
        }
    }
    else
    {
        printf("undefined data type\n");
    }

}

gint cmp_data(gconstpointer a, gconstpointer b) 
{
    const struct my_data* a_tmp = a;
    const struct my_data* b_tmp = b;
    return a_tmp->index - b_tmp->index ;/*compare MY_DATA by index*/
/*    return (((struct my_data *)a)->index > ((struct my_data *)b)->index );//compare MY_DATA by index*/
}
gboolean traverse_print(gpointer key, gpointer value, gpointer data)
{
    int *index_tmp = key;
    char *data_tmp = value;
    printf("index=%d,data=%s\n",*index_tmp,data_tmp);
    return false;
}
int main()
{
    struct my_data tmp;
    GTree * gtree = g_tree_new (cmp_data);
    struct my_data st1 ={0,"math"};
    struct my_data st2 ={1,"physics"};
    struct my_data st3 ={2,"biology"};
    g_tree_insert (gtree, &st1.index, &st1.data);
    g_tree_insert (gtree, &st2.index, &st2.data);
    g_tree_insert (gtree, &st3.index, &st3.data);
    printf("begin traverse_print\n");
    g_tree_foreach(gtree,traverse_print,NULL);
    printf("end traverse_print\n\n");
    print_data(&tmp, MY_DATA_TYPE);
    printf("\n");
    tmp.index = 1;
    strcpy(tmp.data,g_tree_lookup (gtree, &tmp.index));
    print_data(&tmp, MY_DATA_TYPE);
    g_tree_destroy(gtree);
    return 0;
}
