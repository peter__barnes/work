#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<glib.h>
static char *data[] = { "alpha", "bravo", "charlie", "delta",
    "echo", "foxtrot", "golf", "hotel", "india", "juliet",
    "kilo", "lima", "mike", "november", "oscar", "papa",
    "quebec", "romeo", "sierra", "tango", "uniform",
    "victor", "whisky", "x-ray", "yankee", "zulu","aa"
};
enum 
{
    MY_DATA_TYPE 
}Data_type;
struct my_data
{
    char key[10];
    char value[100];
};
gboolean cmp_data(gconstpointer a, gconstpointer b) 
{
    return 0==strcmp(((struct my_data *)a)->key ,((struct my_data *)b)->key );//compare by key, return true if 2 key equals
}
void print_data(gpointer key, gpointer value, gpointer type)
{
    printf("key=%s,\t\tvalue=%s\n",(char *)key,(char *)value);
}
int main()
{
    int i;
    int j;
    gpointer value = NULL;
    gpointer key = NULL;
    GHashTable *ht = g_hash_table_new (g_str_hash,cmp_data);// if key are not char*,we may need other algorithm than g_str_hash
    struct my_data my_data_st[27];
    for(i=0;i<27;i++)
    {
        strcpy (my_data_st[i].key,data[i]);
        strcpy (my_data_st[i].value,data[i]);
        for(j=0;j<9;j++)
        {
            strcat (my_data_st[i].value,data[i]);
        }
        g_hash_table_insert (ht, my_data_st[i].key,&(my_data_st[i].value));  //insert data (key,value)
    }
    g_hash_table_foreach (ht,print_data,NULL);//run print_data() for each element in hash table

    printf("\n");
    key = "echo";
    printf("try to find key'%s',result:\n",(char *)key);
    value = g_hash_table_lookup (ht, key);
    print_data(my_data_st[1].key, value, NULL);
    key = "echo1";
    printf("try to find key'%s',result:\n",(char *)key);
    value = g_hash_table_lookup (ht, key);
    print_data(key, value, NULL);

    g_hash_table_destroy (ht);

    return 0;
}
