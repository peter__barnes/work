#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<glib.h>
enum 
{
    MY_DATA_TYPE 
}Data_type;
struct my_data
{
    int index;
    char data[10];
};
void print_data(gpointer data, gpointer type)
{
    if(type==MY_DATA_TYPE)
    {
        if(data!=NULL)
        {
            printf("index=%d,data=%s\n",((struct my_data *)data)->index,((struct my_data *)data)->data);
        }
    }
    else
    {
        printf("undefined data type\n");
    }

}

gint cmp_data(gconstpointer a, gconstpointer b, gpointer type) 
{
    if(type==MY_DATA_TYPE)
    {
        return (((struct my_data *)a)->index > ((struct my_data *)b)->index );//compare MY_DATA by index
    }
    else
    {
        printf("undefined data type\n");
    }
    return 0;
}
int main()
{
    GSequence * seq = g_sequence_new (NULL);
    struct my_data st1 ={0,"math"};
    struct my_data st2 ={1,"physics"};
    struct my_data st3 ={2,"biology"};
    GSequenceIter *begin;
    GSequenceIter *end;

    
    if(g_sequence_is_empty (seq))
    {
        printf("seq is empty now!\n");
    }
    g_sequence_append (seq, &st1);
    g_sequence_append (seq, &st3);
    g_sequence_append (seq, &st2);
    begin = g_sequence_get_begin_iter (seq);
    end = g_sequence_get_end_iter (seq);
    g_sequence_foreach_range(begin,end,print_data,MY_DATA_TYPE); //run print_data() for each element in seq

    g_sequence_sort (seq,cmp_data,MY_DATA_TYPE); //sort data by cmp_data()
    g_sequence_foreach(seq,print_data,MY_DATA_TYPE);//run print_data() for each element in seq

    g_sequence_free (seq);
    return 0;
}
