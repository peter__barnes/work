#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<glib.h>
enum 
{
    MY_DATA_TYPE 
}Data_type;
struct my_data
{
    int index;
    char data[10];
};
void print_data(gpointer data, gpointer type)
{
    if(type==MY_DATA_TYPE)
    {
        if(data!=NULL)
        {
            printf("index=%d,data=%s\n",((struct my_data *)data)->index,((struct my_data *)data)->data);
        }
    }
    else
    {
        printf("undefined data type\n");
    }

}
gint cmp_data(gconstpointer a, gconstpointer b, gpointer type) 
{
    if(type==MY_DATA_TYPE)
    {
        return (((struct my_data *)a)->index > ((struct my_data *)b)->index );//compare MY_DATA by index
    }
    else
    {
        printf("undefined data type\n");
    }
    return 0;
}
int main()
{
    GQueue *gq = g_queue_new();

    struct my_data st1 ={0,"math"};
    struct my_data st2 ={1,"physics"};
    struct my_data st3 ={2,"biology"};
    struct my_data st4 ={3,"english"};
    g_queue_push_head (gq,&st1);
    g_queue_push_head (gq,&st2);
    g_queue_push_tail (gq,&st3);
    g_queue_foreach(gq,print_data,MY_DATA_TYPE);//run print_data() for each element in seq
    printf("\n");
    struct my_data* tmp = g_queue_pop_head (gq);
    print_data(tmp, MY_DATA_TYPE);
    printf("\n");
    g_queue_foreach(gq,print_data,MY_DATA_TYPE);//run print_data() for each element in seq

    g_queue_free (gq);
    return 0;

}
