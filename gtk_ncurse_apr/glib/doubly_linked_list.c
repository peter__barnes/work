#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<glib.h>
enum 
{
    MY_DATA_TYPE 
}Data_type;
struct my_data
{
    int index;
    char data[10];
};
void print_data(gpointer data, gpointer type)
{
    if(type==MY_DATA_TYPE)
    {
        if(data!=NULL)
        {
            printf("index=%d,data=%s\n",((struct my_data *)data)->index,((struct my_data *)data)->data);
        }
    }
    else
    {
        printf("undefined data type\n");
    }

}
gint cmp_data(gconstpointer a, gconstpointer b, gpointer type) 
{
    if(type==MY_DATA_TYPE)
    {
        return (((struct my_data *)a)->index > ((struct my_data *)b)->index );//compare MY_DATA by index
    }
    else
    {
        printf("undefined data type\n");
    }
    return 0;
}
int main()
{
    GList * gl=g_list_alloc();

    struct my_data st1 ={0,"math"};
    struct my_data st2 ={1,"physics"};
    struct my_data st3 ={2,"biology"};
    struct my_data st4 ={3,"english"};

    gl = g_list_append (gl, &st1);
    gl = g_list_append (gl, &st3);
    gl = g_list_append (gl, &st2);
    g_list_foreach(gl,print_data,MY_DATA_TYPE);//run print_data() for each element in seq
    printf("\nnow we reverse GList gl\n");
    gl = g_list_reverse (gl);
    g_list_foreach(gl,print_data,MY_DATA_TYPE);//run print_data() for each element in seq
    printf("\nnow add st4\n");
    gl = g_list_prepend (gl, &st4);
    g_list_foreach(gl,print_data,MY_DATA_TYPE);//run print_data() for each element in seq

    printf("\nprint head,index=%d,data=%s\n",((struct my_data *)gl->data)->index,((struct my_data *)gl->data)->data);
    if(NULL==gl->prev)
    {
        printf("\ngl is the head,prev is NULL\n");
    }
    gl = gl->next; // move head to next (then the old first will be out of list)
    gl->prev = NULL;
    printf("print head,index=%d,data=%s\n",((struct my_data *)gl->data)->index,((struct my_data *)gl->data)->data);

    g_list_foreach(gl,print_data,MY_DATA_TYPE);//run print_data() for each element in seq
    g_list_free (gl);

    return 0;
}
