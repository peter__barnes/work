#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <glib.h>
/*
(part1)simulate shell filename match :
        match pattern "*.c" and "*.h"  from "a.c b.h c.cpp"
(part2)simulate vi string replacement:
        string "a.c b.h c.cpp" to "A.c B.h c.cpp"   // :%s/\(\a*\)\(.[c|h]\s\)/\U\1\L\2/g 
*/
#define MAX_PIECE 100
int main()
{
    //(part1)
    int i;
    GRegex * reg = NULL;
    GError *err = NULL;
    GMatchInfo *match_info = NULL;
    gchar *pattern=".*[c|h]$";
    gchar *str="a.c b.h c.cpp";
    gchar **str_splited=NULL;

    reg = g_regex_new(pattern,0,0,&err); //create regex
    if(NULL==reg)
    {
        printf("set reg failed:%s\n",err->message);
    }
    str_splited = g_regex_split_simple(" ",str,0,0); //split string by space(" ")

    for(i=0;i<MAX_PIECE&&str_splited[i]!=NULL;i++)
    {
        printf("str_splited[%d]:%s\n",i,str_splited[i]); //print str after split

        g_regex_match (reg, str_splited[i], 0, &match_info); //search pattern
        while (g_match_info_matches (match_info)) //print info if match
        {
            gchar *word = g_match_info_fetch (match_info, 0); 
            g_print ("Found: %s\n", word);
            g_free (word);
            g_match_info_next (match_info, NULL);
        }
        g_match_info_free (match_info);
    }
    g_regex_unref (reg);

    //(part2)
    printf("before replacement:%s\n",str);
    gchar *pattern2="(\\w*)(.[c|h])\\s";
    reg = g_regex_new(pattern2,0,0,&err); //create regex
        g_regex_match (reg, str, 0, &match_info); //search pattern
        while (g_match_info_matches (match_info)) //print info if match
        {
            gchar *word = g_match_info_fetch (match_info, 0); 
            g_print ("Found: %s\n", word);
            g_free (word);
            g_match_info_next (match_info, NULL);
        }
    //gchar *result = g_regex_replace (reg, str, strlen(str), 0, "\\U\\1\\L\\2 ", 0, &err); 
    //use "g<1>" is better(more clear) than "\\1",though the result is the same
    gchar *result = g_regex_replace (reg, str, strlen(str), 0, "\\U\\g<1>\\L\\g<2> ", 0, &err); // \\1 and \\2 match 1st 2nd "(...)" of pattern2
    printf("after replacement:%s\n",result);

    g_strfreev(str_splited);
    g_regex_unref (reg);

    return 0;
}
