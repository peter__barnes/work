windows下msys模式安装Gtk+开发环境，编译运行Gtk+程序

1.从http://msys2.github.io/ 下载 msys2xxx.exe并安装到默认路径c:\msys64(本文以64位系统为例)
	开始菜单里面有三个命令行工具： 
	1.MinGW-w64 Win32 Shell是指32位程序开发环境。 2.MinGW-w64 Win64 Shell 是指64位程序开发环境。 3.MSYS2 Shell是MSYS2管理环境。 
	只有在前两个 MinGW-w64的命令行里才能使用gcc、g++等编译工具。第三个MSYS2 Shell 一般仅用于软件包安装管理和更新，无法编译。

2.更新系统库，安装软件包（pacman 将下载的软件包保存在 /var/cache/pacman/pkg/ 并且不会自动清理）：
 pacman -Sy pacman
 pacman -Syu			#此步骤运行后需要重启msys
 pacman -S mingw-w64-x86_64-toolchain  #选择GCC的项目，在前文“1.”提到的前两个MinGW-w64环境运行，千万不要在第三个环境单独装gcc，那个gcc有严重bug。
 pacman -S mingw-w64-x86_64-gedit    #gedit有依赖GTK，或 mingw-w64-x86_64-gtk3亦可。
 pacman -S pkg-config 
pacman -S libreadline gdb  #配置gdb调试工具，gdb需要libreadline（此步骤为可选）
	

3.此时将hello_world.c和C:\msys64\usr\bin\msys-2.0.dll移到同一文件夹（如C:\msys64\home\admin\）。
（msys-2.0.dll是关键文件，不同版本或许位置不一样，需要搜索。支持unistd.h，win7/10下亲测完美支持sleep()，但不支持fork）

gcc hello_world.c,会生成a.exe的程序。
--------------------
#include<stdio.h>
#include<stdlib.h>
int main()
{
    printf("hello world!\n");
    getchar();
    return 0;
}
---------------------
4.配置gtk+环境,编译程序
搜索gtk+-3.0.pc，即pkg-config管理文件，默认路径：
	C:\msys64\mingw64\lib\pkgconfig\gtk+-3.0.pc
export PKG_CONFIG_PATH=/mingw64/lib/pkgconfig	(不要写成windows路径)

gcc -o gtk_hello gtk_hello.c `pkg-config --libs --cflags gtk+-3.0`
生成gtk_hello.exe
---------------------
#include<gtk/gtk.h>
int main(int argc,char *argv[])
{
    GtkWidget    *window;
    gtk_init(&argc,&argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window),"hello!GTK");
    gtk_widget_show(window);
    gtk_main();
    return 0;
}
---------------------

5.运行
C:\msys64\mingw64\bin	是关键目录，此目录下有大量gtk-*.exe的demo，可以直接运行，有些甚至可以查看源码。这里包含的dll支持很多linux功能，如pthread.h的大部分主要功能。
将gtk_hello.exe、msys-2.0.dll和C:\msys64\mingw64\bin下所有的dll一起移到其他位置，win7（64位）能完美运行。
但上一行的方法在win10会丢失图标、字体变难看。C:\msys64\mingw64\share目录下fontconfig和icons需要一起移动（维持share和bin平级目录关系，即exe的../share/下有icons等）。
