1.安装codeblocks-MinGW版。
将 C:\Program Files (x86)\CodeBlocks\MinGW\bin  添加到PATH中。cmd命令行敲入：gcc -v会看到编译器的版本情况。

2.网上下载并安装旧有gtk+-bundle版，自带mingw-gcc兼容模式,鉴于官方不提供二进制版，此处专门保留exe文件备份。

3.设置环境变量 "用户变量"、"系统变量"二者选一即可
变量名      变量值 
GTK_HOME   C:\gtk+ 
INCLUDE    C:\gtk+\include 
LIB        C:\gtk+\lib 
PATH       C:\gtk+\bin 

4：配置codeblock （以下方案实测有效，而且小程序能支持按钮、文字弹窗等基本功能）
   4.1启动codeblock，点击"File"->"New"->"Empty Project"创建一个空项目,
  
   4.2点击"Project"->"build options" 
  	4.2.1:选择“Compiler settings”标签中的Other options子标签 
    	加入编译选项： 
    	-mms-bitfields  
    	选项解释：编译GTK程序必须的选项 


  	4.2.2:选择“Linker settings”标签 
    	在"Link libraries"中点击"Add"按钮加入gtk+ lib（全选c:\gtk+\lib\目录下的文件，点确认）。 
    	在Other Linker options加入链接选项：    
    	-mwindows 
    	选项解释：告诉编译器去编译一个窗口程序，生成的程序可以去掉控制台。 
              如果你正在写一个终端程序或需要在终端调试信息，那么不要加 
              这个选项！ 

  	4.2.3:选择“Search directories”标签 
    	在"Compiler"子标签中加入GTK+ include的路径，这里比较郁闷，只能一条条的添加,所以用到那个功能加那个功能吧. 
		C:\gtk+\include 
		C:\gtk+\include\atk-1.0 
		C:\gtk+\include\gtk-3.0 
		C:\gtk+\include\gtk-3.0\gtk 
		C:\gtk+\include\glib-2.0\glib 
		C:\gtk+\include\glib-2.0 
		C:\gtk+\lib\glib-2.0\include 
		C:\gtk+\include\pango-1.0 
		C:\gtk+\include\cairo 
		C:\gtk+\include\gdk-pixbuf-2.0\gdk-pixbuf
		C:\gtk+\include\gdk-pixbuf-2.0

    	4.2.4:点击"Setting"->"Global Variables..."会弹出一个"Global Variable“的窗口，在这里配置gtk的编译选项, 
    	base       C:\gtk+ 
    	include    C:\gtk+\include 
   	 lib        C:\gtk+\lib 
    	cflags     `pkg-config --cflags gtk+-3.0` 
    	lflags     `pkg-config --libs gtk+-3.0` 
  	然后就可以编译gtk+程序了.如果没有安装GTK+运行时，运行gtk+程序还会报错，将C:\gtk+\bin下的所有DLL文件都复制到项目的bin\Debug目录下，之后就可以运行了。 
