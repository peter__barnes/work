#include <pthread.h>
#include <stdio.h>
#include <apr_thread_proc.h>

/* this function is run by the second thread */
apr_thread_start_t inc_x(apr_thread_t *new_thread,void *x_void_ptr)
{
    /* increment x to 100 */
    int *x_ptr = (int *)x_void_ptr;
    while(++(*x_ptr) < 100);
    {
    }
    printf("x increment finished\n");
    /* the function must return something - NULL will do */
    return NULL;
}
int main()
{
    int x = 0, y = 0;
    /* show the initial values of x and y */
    printf("x: %d, y: %d\n", x, y);
    /* this variable is our reference to the second thread */
//    pthread_t inc_x_thread;
    apr_thread_t *new_thread;
//    apr_threadattr_t *new_attr;
    apr_status_t ret;//=   apr_threadattr_create (&new_attr, NULL);
    apr_pool_initialize();
    apr_pool_t *pool;
    apr_status_t rc;
    if (apr_pool_create(&pool, NULL) <0 ) {
        printf("Could not allocate pool\n");
        return -1;
    }
/*
    if(((int)ret)!=0)
    {
        fprintf(stderr, "Error creating thread\n");
        return 1;
    }
    */
    /* create a second thread which executes inc_x(&x) */
    if(ret = apr_thread_create(&new_thread, NULL, inc_x, &x,pool)) {
        fprintf(stderr, "Error creating thread\n");
        return 1;
    }
    /* increment y to 100 in the first thread */
    while(++y < 100);
    printf("y increment finished\n");
    /* wait for the second thread to finish */
    if(apr_thread_join(&ret , new_thread)<0) {
        fprintf(stderr, "Error joining thread\n");
        return 2;
    }
    /* show the results - x is now 100 thanks to the second thread */
    printf("x: %d, y: %d\n", x, y);
    return 0;
}
