#include <ncurses.h>			/* ncurses.h includes stdio.h */  
#include <string.h> 

int main()
{
    char mesg[]="Enter a string: ";    
    char str[80];
    int row,col;				/* to store the number of rows and *
                                 * the number of colums of the screen */
    initscr();				/* start the curses mode */
    getmaxyx(stdscr,row,col);		/* get the number of rows and columns */
    /* print the message at the center of the screen */
    mvprintw(row-2,0,"This screen has %d rows and %d columns\n",row,col);
    printw("Try resizing your window(if possible) and then run this program again");
    mvprintw(row/2,(col-strlen(mesg))/2,"%s",mesg);
    getstr(str);
    printw("You Entered: %s", str);
    refresh();



    getch();
    endwin();

    return 0;
}
