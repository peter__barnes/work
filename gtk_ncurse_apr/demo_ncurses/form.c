#include <curses.h>
#include <string.h>
#include <menu.h>
#include <form.h>

void print_field(char *input)
{	
    move(20, 0);
    clrtoeol();
    mvprintw(20, 0, "field input is : %s", input);
}	
int main()
{	
    FIELD *field[3];
    FIELD *cur;
    FORM  *my_form;
    int ch;
    void (*p)(char *);
    char *buf;

    /* Initialize curses */
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    /* Initialize few color pairs */
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    init_pair(2, COLOR_WHITE, COLOR_BLUE);

    /* Initialize the fields */
    field[0] = new_field(1, 10, 4, 18, 0, 0);
    field[1] = new_field(1, 10, 6, 18, 0, 0);
    field[2] = NULL;

    /* Set the user pointer */
    set_field_userptr(field[0], print_field);/*signal/pointer connection*/ 
    set_field_userptr(field[1], print_field);/*signal/pointer connection*/

    /* Set field options */
    set_field_fore(field[0], COLOR_PAIR(1));/* Put the field with blue background */
    set_field_back(field[0], COLOR_PAIR(2));/* and white foreground (characters */
    /* are printed in white 	*/
    field_opts_off(field[0], O_AUTOSKIP);  	/* Don't go to next field when this */
    /* Field is filled up 		*/
    set_field_back(field[1], A_UNDERLINE); 
    field_opts_off(field[1], O_AUTOSKIP);

    /* Create the form and post it */
    my_form = new_form(field);
    post_form(my_form);
    refresh();

    set_current_field(my_form, field[0]); /* Set focus to the colored field */
    mvprintw(4, 10, "Value 1:");
    mvprintw(6, 10, "Value 2:");
    mvprintw(LINES - 2, 0, "Use UP, DOWN arrow keys to switch between fields");
    refresh();

    /* Loop through to get user requests */
    while((ch = getch()) != KEY_F(1))
    {	switch(ch)
        {	case KEY_DOWN:
            /* Go to next field */
            form_driver(my_form, REQ_NEXT_FIELD);
            /* Go to the end of the present buffer */
            /* Leaves nicely at the last character */
            form_driver(my_form, REQ_END_LINE);
            break;
            case KEY_UP:
            /* Go to previous field */
            form_driver(my_form, REQ_PREV_FIELD);
            form_driver(my_form, REQ_END_LINE);
            break;
            case 10: /* Enter */
            form_driver(my_form, ch);
            cur = current_field(my_form);
            /*field_buffer()/field_status() on field currently have input may not get a correct buffer/status*/
            /*must run them after REQ_VALIDATION finished,form_driver() can block at the current REQ_VALIDATION until it finished*/
            form_driver(my_form, REQ_NEXT_FIELD);
            form_driver(my_form, REQ_END_LINE);
            p = field_userptr(cur);  /*set field that want to use signal*/
            buf=field_buffer(cur,0);
            p(buf); /*run signal function*/
            //p((char *)("111"));
            break;
            default:
            /* If this is a normal character, it gets */
            /* Printed				  */	
            form_driver(my_form, ch);
            break;
        }
    }

    /* Un post form and free the memory */
    unpost_form(my_form);
    free_form(my_form);
    free_field(field[0]);
    free_field(field[1]); 

    endwin();
    return 0;
}
