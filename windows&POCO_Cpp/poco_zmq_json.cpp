//poco zmq JSON收发、拼装解析例子，可用VS建新工程，覆盖即可使用，虽为windows开发，但实际内容多为跨平台库，移植简单
#include "stdafx.h"
#include<iostream>
#include <sstream>
#include <thread>
#include"zmq.h"
#include "zmq.hpp"
#include "Poco/JSON/Parser.h"
#include "Poco/JSON/ParseHandler.h"
#include "Poco/JSON/JSONException.h"
#include "Poco/StreamCopier.h"
#include "Poco/Dynamic/Var.h"
#include "Poco/JSON/Query.h"
#include "Poco/JSON/PrintHandler.h"
using namespace std;
using namespace Poco::Dynamic;
using namespace Poco;
using namespace Poco::JSON;
void send_hello(zmq::socket_t& socket)
{
	zmq::message_t request(5);
	memcpy(request.data(), "Hello", 5);
	std::cout << "Sending Hello " << std::endl;
	socket.send(request);
}
void send_RequsetZoneInfo(zmq::socket_t& socket)
{
	Poco::JSON::Array ZoneList;
	ZoneList.add(1);
	ZoneList.add(2);

	JSON::Object obj;
	JSON::Object obj2;
	obj2.set("MsgID", Dynamic::Var("RequsetZoneInfo"));
	obj2.set("Result", Dynamic::Var(1));
	obj2.set("Para", Dynamic::Var(obj));
	obj2.set("Note", Dynamic::Var(""));
	obj2.set("Body", Dynamic::Var(""));
	Dynamic::Var i_var(obj2);
	cout << i_var.toString() << endl;
	zmq::message_t request(i_var.toString().size());
	memcpy(request.data(), i_var.toString().c_str(), i_var.toString().size());
	socket.send(request);
}
void ParseRespZoneInfo(zmq::socket_t& socket)
{
	//  Get the reply.
	zmq::message_t reply;
	socket.recv(&reply);

	string js;
	js.assign((char*)reply.data(), reply.size());
	std::cout << "Received:" << js << std::endl;

	Object::Ptr objPtr;
	JSON::Parser parser;
	Dynamic::Var varObj = parser.parse(js);
	objPtr = varObj.extract<Object::Ptr>();

	Poco::JSON::Object::Ptr JsPara = objPtr->getObject("Para");
	string JsMsgID = objPtr->getValue<string>("MsgID");
	int JsResult = objPtr->getValue<int>("Result");
	cout << "JsMsgID" << JsMsgID << endl;
	Poco::JSON::Array::Ptr Zone = JsPara->getArray("Zone");
	
	Sleep(2000);
}
void get_reply(zmq::socket_t& socket)
{
	//  Get the reply.
	zmq::message_t reply;
	socket.recv(&reply);
	char buffer[20480];
	memcpy(buffer, reply.data(), reply.size());
	buffer[reply.size()] = '\0';
	if (0 == strncmp(buffer, "JsonServerOnline", strlen("JsonServerOnline")))
	{
		return;
	}
	if (0 == strncmp(buffer, "JsonZoneStatusChange", strlen("JsonZoneStatusChange")))
	{
		return;
	}
	std::cout << "Received:" << buffer << std::endl;
	ParseRespZoneInfo(socket);
	Sleep(500);
}
void zmq_conn_req(zmq::socket_t& socket)
{
	std::cout << "Connecting to hello world server..." << std::endl;
	socket.connect("tcp://localhost:61203");
}
static string zmq_sub_opt[] = {
  "JsonZoneStatusChange",
  "JsonTaskStatusChange",
  "JsonServerStatus"
};
void zmq_conn_sub(zmq::socket_t& socket)
{
	//  Prepare our context and socket
	std::cout << "Connecting to ZMQ_SUB server..." << std::endl;
	socket.connect("tcp://localhost:61202");
	char filter1[] = "";
	socket.setsockopt(ZMQ_SUBSCRIBE, filter1, strlen(filter1));
}
void sub_fun()
{
	zmq::context_t context2(1);
	zmq::socket_t socket2(context2, ZMQ_SUB);
	zmq_conn_sub(socket2);
	while (true)
	{
		get_reply(socket2);
	}

	getchar();
}
int _tmain(int argc, _TCHAR* argv[])
{
	std::thread sub(sub_fun);  //按照值传递 
	zmq::context_t context(1);
	zmq::socket_t socket(context, ZMQ_REQ);
	zmq_conn_req(socket);

	send_RequsetZoneInfo(socket);
	get_reply(socket);
	//  Do 10 requests, waiting each time for a response
	for (int request_nbr = 0; request_nbr != 10; request_nbr++) {
		send_hello(socket);
		get_reply(socket);
	}
	sub.join();
	getchar();
	return 0;
}