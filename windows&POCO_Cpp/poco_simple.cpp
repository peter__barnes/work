//简单的poco小程序，用于测试
#include "Poco/ByteOrder.h"
#include <iostream>
#include<thread>
#include <Poco/String.h>
#include "Poco/Event.h"
#include "Poco/Thread.h"
using Poco::Event;
using Poco::Thread;
using Poco::Runnable;
using Poco::trim;
using Poco::trimLeft;
using Poco::trimRight;
using Poco::trimRightInPlace;
using namespace std;
using Poco::ByteOrder;
using Poco::UInt16;

class EventRunnable :public Runnable
{
    public:
        EventRunnable(Event& ea, Event& eh) :m_EventAuto(ea), m_EventHandle(eh) {  }
        void print(){}
        void run()
        {   
            std::cout << "Enter Thread." << std::endl;
            m_EventAuto.wait();
            std::cout << "Through wait 1." << std::endl;
            m_EventAuto.wait();
            std::cout << "Through wait 2." << std::endl;
            m_EventHandle.wait();
            m_EventHandle.wait();
            std::cout << "Through wait 3." << std::endl;
        }
    private:
        Event& m_EventAuto;
        Event& m_EventHandle;
};

int main(int argc, char** argv)
{
    Event ea;
    Event eh(false);
    EventRunnable r(ea, eh);
    Thread t;
    t.start(r);
    r.print();

    Thread::sleep(2000);
    ea.set();
    Thread::sleep(2000);
    ea.set();
    Thread::sleep(2000);
    eh.set();
    t.join();
    return 0;
}
