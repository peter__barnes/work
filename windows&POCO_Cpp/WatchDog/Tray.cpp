#include <winsock2.h>
#include <windows.h>
#include "resource.h"
#include <Poco/String.h>
#include "Poco/AutoPtr.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Exception.h"
#include "Poco/Logger.h"
#include "logger_recorder.h"
#include <thread>

#define WATCHDOG_PORT 61032
#define KEEPALIVE_DEADLINE_SEC  6
#define WM_TRAY (WM_USER + 100)
#define WM_TASKBAR_CREATED RegisterWindowMessage(TEXT("TaskbarCreated"))

#define APP_NAME	TEXT("ITC广播看门狗托盘程序")
#define APP_TIP		TEXT("ITC广播看门狗程序运行中")

Poco::Mutex nAliveCntLock;//锁
int nAliveCnt = 20;		//心跳计数器
bool ProcExit = false;	//进程退出标志位
NOTIFYICONDATA nid;		//托盘属性
HMENU hMenu;			//托盘菜单
Poco::Net::ServerSocket *pServerSocket;

using namespace std;
using namespace Poco;
using Poco::Exception;

//实例化托盘
void InitTray(HINSTANCE hInstance, HWND hWnd)
{
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = hWnd;
	nid.uID = IDI_TRAY;
	nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP | NIF_INFO;
	nid.uCallbackMessage = WM_TRAY;
    nid.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TRAY));
	lstrcpy(nid.szTip, APP_NAME);

	hMenu = CreatePopupMenu();//生成托盘菜单
	//为托盘菜单添加两个选项
	AppendMenu(hMenu, MF_STRING, ID_SHOW, TEXT("提示"));
	AppendMenu(hMenu, MF_STRING, ID_EXIT, TEXT("退出"));
   	Shell_NotifyIcon(NIM_ADD, &nid);
}

//演示托盘气泡提醒
void ShowTrayMsg()
{
	lstrcpy(nid.szInfoTitle, APP_NAME);
	lstrcpy(nid.szInfo, TEXT("ITC广播看门狗启动！"));
	nid.uTimeout = 1000;
	Shell_NotifyIcon(NIM_MODIFY, &nid);
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_TRAY:
		switch (lParam)
		{		
        case WM_RBUTTONDOWN:
		{
			//获取鼠标坐标
			POINT pt; GetCursorPos(&pt);

			//解决在菜单外单击左键菜单不消失的问题
			SetForegroundWindow(hWnd);
			//显示并获取选中的菜单
			int cmd = TrackPopupMenu(hMenu, TPM_RETURNCMD, pt.x, pt.y, NULL, hWnd,
				NULL);
			if (cmd == ID_SHOW)
				MessageBox(hWnd, APP_TIP, APP_NAME, MB_OK);
			if (cmd == ID_EXIT)
				PostMessage(hWnd, WM_DESTROY, NULL, NULL);
		}
        break;
		case WM_LBUTTONDOWN:
			MessageBox(hWnd, APP_TIP, APP_NAME, MB_OK);
			break;
		case WM_LBUTTONDBLCLK:
			break;
		}
		break;
        case WM_DESTROY:
		ProcExit = true;
		//窗口销毁时删除托盘
		Shell_NotifyIcon(NIM_DELETE, &nid);
		PostQuitMessage(0);
		break;
        case WM_TIMER:
		ShowTrayMsg();
		KillTimer(hWnd, wParam);
		break;
	}
    if (uMsg == WM_TASKBAR_CREATED)
	{
		//系统Explorer崩溃重启时，重新加载托盘
		Shell_NotifyIcon(NIM_ADD, &nid);
	}
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void RestartProc()
{
    //终止原有进程
    SHELLEXECUTEINFO ShExecInfo = { 0 };
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = L"cmd.exe";//调用的程序名
	ShExecInfo.lpParameters = L"cmd.exe /c taskkill /im SouFire* ";//调用程序的命令行参数
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_HIDE;//窗口状态为隐藏
	ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo);
	WaitForSingleObject(ShExecInfo.hProcess, 5000);////等该进程结束,最多5秒
	Sleep(3000);

    //启动新进程
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = L"cmd.exe";//调用的程序名
	ShExecInfo.lpParameters = L"cmd.exe /c SouFire.exe ";//调用程序的命令行参数
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_HIDE;//窗口状态为隐藏
	ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo);
	WaitForSingleObject(ShExecInfo.hProcess, 8000);////等该进程结束,最多8秒
}


int WatchDogCounter()
{
	while (ProcExit != true)
	{
		Sleep(1000);
		Poco::Mutex::ScopedLock LockEp(nAliveCntLock);
		nAliveCnt++;
        //检查心跳
		if (nAliveCnt > KEEPALIVE_DEADLINE_SEC)
		{
			poco_information(LOG, "nAliveCnt reached deadline，Call RestartProc!");
			RestartProc();
			nAliveCnt = -4;//给4秒的启动时间作为计时缓冲
		}
    }
	ExitThread(0);
	//return 0;
}

int WatchDog()
{
	//KillProc();
	Poco::Net::SocketAddress oSa("127.0.0.1", WATCHDOG_PORT);
	Poco::Net::ServerSocket srv(oSa); // does bind + liste
	pServerSocket = &srv;
	Poco::Net::StreamSocket ss;
    while (ProcExit != true)
	{
		try
		{
			ss = srv.acceptConnection(); 
		}
		catch (Exception &e)
		{
			poco_debug(LOG, "acceptConnection failed, ExitThread!");
			ExitThread(0);
		}
        ss.setBlocking(false);
		char Buffer[256];
		while (ProcExit != true)
		{
			Sleep(1000);
            Poco::Mutex::ScopedLock LockEp(nAliveCntLock);
			try
			{
				int nDataSize = ss.receiveBytes(Buffer, sizeof(Buffer));
				if (nDataSize > 0)
				{
					nAliveCnt = 0;
				}
                else if (nDataSize == 0)//正常断开连接
				{
					nAliveCnt = KEEPALIVE_DEADLINE_SEC;
					ss.close();
					poco_debug(LOG, "ss closed");
					break;
				}
			}
            catch (Exception &e)//超时等异常
			{
				poco_debug_f1(LOG, "Execption:%s", e.message());
				//nAliveCnt++;
			}
    	}
	}
	//return 0;
	ExitThread(0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/,
	LPSTR /*lpCmdLine*/, int iCmdShow)
{
	SetupLogger();
	poco_information(LOG, "WatchDog start");
	HWND hWnd;
	MSG msg;
    WNDCLASS wc = { 0 };
	wc.style = NULL;
	wc.hIcon = NULL;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.lpfnWndProc = WndProc;
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = APP_NAME;
   	wc.hCursor = NULL;
       
	if (!RegisterClass(&wc)) return 0;

	hWnd = CreateWindowEx(WS_EX_TOOLWINDOW, APP_NAME, APP_NAME, WS_POPUP, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

	std::thread thWatchDog(WatchDog);
	std::thread thWatchDogCounter(WatchDogCounter);
	//thWatchDogCounter.detach();

	ShowWindow(hWnd, iCmdShow);
	UpdateWindow(hWnd);

	InitTray(hInstance, hWnd);			//实例化托盘
	SetTimer(hWnd, 3, 1000, NULL);		//定时发消息，演示气泡提示功能

    while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	if (NULL != pServerSocket)
	{
		pServerSocket->close();
	}
    thWatchDog.join();
	thWatchDogCounter.join();
	//return msg.wParam;
	ExitProcess(0);
}