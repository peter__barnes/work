#pragma once
#include "Poco/Logger.h"
#include <string>
using namespace std;
using  namespace Poco;	
#define logger_handle   (Logger::get("logger"))
#define LOG		logger_handle
void SetupLogger(unsigned int MaxSize = 3);
string GetLogFilename();
void LogHex(const char *buff, int DataLen);
void LogDec(const char *buff, int DataLen);
void LogCh(const char *buff, int DataLen);