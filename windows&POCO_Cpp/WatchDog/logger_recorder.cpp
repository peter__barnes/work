#include "logger_recorder.h"
#include "Poco/PatternFormatter.h"
#include "Poco/FormattingChannel.h"
#include "Poco/FileChannel.h"
#include "Poco/AutoPtr.h"
#include "Poco/Format.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/SplitterChannel.h"
#include "Poco/Message.h"
#include "Poco/Path.h"
#include "Poco/File.h"
#include <windows.h>
#include <iostream>
#define POCO_LOG_LEVEL "debug"
using namespace Poco;

string AnsiToUtf8(LPCSTR Ansi)
{
	int WLength = MultiByteToWideChar(CP_ACP, 0, Ansi, -1, NULL, 0);
	LPWSTR pszW = (LPWSTR)_alloca((WLength + 1) * sizeof(WCHAR));
	MultiByteToWideChar(CP_ACP, 0, Ansi, -1, pszW, WLength);
	int ALength = WideCharToMultiByte(CP_UTF8, 0, pszW, -1, NULL, 0, NULL, NULL);
	LPSTR pszA = (LPSTR)_alloca(ALength + 1);
	WideCharToMultiByte(CP_UTF8, 0, pszW, -1, pszA, ALength, NULL, NULL);
	pszA[ALength] = 0;
	string ps = pszA;
	return ps;
}
string getCurrentFileName()
{
	char cBuffer[MAX_PATH];
	GetModuleFileNameA(NULL, cBuffer, MAX_PATH);
	return std::string(cBuffer);
}

string GetLogFilename()
{
	string	sFilename = getCurrentFileName();
	Path Path(sFilename);
	string sLogName;
	string sPath = Path.parent().toString();
	sLogName.append(sPath);
	sLogName.append("log\\timetask\\");
    Poco::File f(AnsiToUtf8(sLogName.c_str()));
	f.createDirectories();
	sPath = Path.getBaseName();
	sLogName.append(sPath);
	sLogName.append(".log");
	return AnsiToUtf8(sLogName.c_str());
}

void Init_mixchannelogger(const string TFile , unsigned int MaxSize)
{
	string sFileSize = format("%uM", MaxSize);
	AutoPtr<SplitterChannel> splitterChannel(new SplitterChannel());
	AutoPtr<Channel> consoleChannel(new ConsoleChannel());
    AutoPtr<FileChannel> rotatedFileChannel(new FileChannel("rotated.log"));

	rotatedFileChannel->setProperty("rotation", sFileSize);
	rotatedFileChannel->setProperty("path", TFile);
	rotatedFileChannel->setProperty("archive", "timestamp");

    splitterChannel->addChannel(consoleChannel);
	splitterChannel->addChannel(rotatedFileChannel);

    AutoPtr<Formatter> formatter(new PatternFormatter("%Y/%n/%d-%L%H:%M:%S.%c  thread: %T%I, Position: %U(%u)\r\n%p: %t "));
	AutoPtr<Channel> formattingChannel(new FormattingChannel(formatter, splitterChannel));
	Logger::get("logger").setChannel(formattingChannel);
	Logger::get("logger").setLevel(POCO_LOG_LEVEL);
}

void Init_filechannelogger(const string t_file, unsigned int t_max_size)
{
	string sFileSize = format("%uK", t_max_size);
	AutoPtr<FileChannel> file_channel(new FileChannel());
	file_channel->setProperty("rotation", "daily");  // rotate daily 
    file_channel->setProperty("archive", "timestamp");
	file_channel->setProperty("purgeAge", "30days");
	file_channel->setProperty("path", t_file);
	AutoPtr<PatternFormatter> pattern_formatter(new PatternFormatter("%Y/%n/%d-%L%H:%M:%S  thread: %T%I, Position: %U(%u)\r\n%p: %t "));
	AutoPtr<FormattingChannel> formatter_channle(new FormattingChannel(pattern_formatter, file_channel));
	Logger::get("logger").setChannel(formatter_channle);
	Logger::get("logger").setLevel(POCO_LOG_LEVEL);
}
void SetupLogger(unsigned int MaxSize /*= 3*/)
{
	static bool bSetup = false;		// only allow run once time 
	if (!bSetup)
	{
        bSetup = true;
		if (MaxSize == 0)
		{
			MaxSize = 10;
		}
#if (defined SIM_TEST) ||(defined PRINT_SCREEN) 
		Init_mixchannelogger(GetLogFilename(), 2);
#else
		Init_filechannelogger(GetLogFilename(), 2);
#endif
	}
}