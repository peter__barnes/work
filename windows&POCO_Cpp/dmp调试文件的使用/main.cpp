#此用例实测在VS2013上release模式有效
#参考https://blog.csdn.net/wangzhen_cv/article/details/40656409

// main.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include "Dump.h
"
//测试函数，用于制造异常，验证dmp文件能生成
void TestDump(int* p)
{
    for(int i = 0; i < 100; i++)
    {
        p[i] = i * i;
    }
}
int _tmain(int argc, _TCHAR* argv[])
{
	//dmp文件生成句柄
	SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)ApplicationCrashHandler);	
    //后续正常填入其他代码即可
    TestDump(0);//调用测试函数制造异常，生成dmp文件
    
    return 0;

}