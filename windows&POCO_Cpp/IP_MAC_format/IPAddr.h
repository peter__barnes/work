#pragma once
#include<iostream>
#include<string>
#include<vector>
class CIPAddr
{
public:
	CIPAddr(const char* pBuf, const int nLen = IP_LEN);
	CIPAddr(const string& strDotFormat);
	virtual ~CIPAddr();
public:
	string ToDotString();
	string ToHexString();
private:
	string m_strDotFormat;
	string m_strHex;
};
