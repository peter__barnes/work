#include "IPAddr.h"

CIPAddr::CIPAddr(const char* pBuf, const int nLen /*= IP_LEN*/)
{
	m_strHex.assign(pBuf, nLen);
	Poco::Net::IPAddress oIpa(pBuf,nLen);
	m_strDotFormat = oIpa.toString();
}
CIPAddr::CIPAddr(const string& strDotFormat)
{
	m_strDotFormat = strDotFormat;
	Poco::Net::IPAddress oIpa(strDotFormat);
	m_strHex.assign((char*)oIpa.addr(), oIpa.length());
}
CIPAddr::~CIPAddr()
{
}
string CIPAddr::ToDotString()
{
	return m_strDotFormat;
}
string CIPAddr::ToHexString()
{
	//最多四个字节
	return m_strHex;
}
