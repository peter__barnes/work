#pragma once
#include<iostream>
#include<string>
#include<vector>
class CMac
{
public:
	CMac();
	CMac(const string& strMac);
	CMac(const char* pHexMac, const int nLen = 6);

public:
	string ToString();
	string ToHex();

private:
	//"AA-BB-CC-DD-EE-FF"
	string  m_strMac;

	//0XAA 0XBB 0XCC 0XDD 0XEE 0XFF
	string  m_strHexMac;
};
