#include "Mac.h"
CMac::CMac() : m_strHexMac(6, -1), m_strMac("FF-FF-FF-FF-FF")
{
}
CMac::CMac(const string& strMac)
{
	//strMac : "AA-BB-CC-11-22-33"
	if (strMac.empty())
	{
		m_strHexMac.assign(6,0);
		return;
	}

	if (strMac.length()>18)
	{
		m_strHexMac.assign(6, 0);
		return;
	}
	m_strMac = strMac;

	char pBuf[18] = { 0 };
	m_strMac._Copy_s(pBuf, _countof(pBuf), _countof(pBuf));
	char pMacByte[9][1] = { 0 };
	sscanf_s(pBuf, "%x-%x-%x-%x-%x-%x", pMacByte[0], pMacByte[1], pMacByte[2], pMacByte[3], pMacByte[4], pMacByte[5]);

	m_strHexMac.assign((char*)pMacByte, MAC_LEN);
}

CMac::CMac(const char* pHexMac, const int nLen /*= 6*/)
{
	m_strHexMac.assign(pHexMac, nLen);

	char szMac[18] = { 0 };

	char macByte[6][3] = { 0 };
	for (int i = 0; i < 6; i++)
	{
		_itoa_s((unsigned char)pHexMac[i], (char*)(macByte + i), 3, 16);
	}

	sprintf_s(szMac, "%02s-%02s-%02s-%02s-%02s-%02s", macByte[0], macByte[1], macByte[2], macByte[3], macByte[4], macByte[5]);
	//szMac : "aa-bb-cc-11-22-33"

	_strupr_s(szMac);
	//szMac : "AA-BB-CC-11-22-33"

	m_strMac = szMac;
}
string CMac::ToString()
{
	return m_strMac;
}
string CMac::ToHex()
{
	return m_strHexMac;
}
