#include <iostream>
using namespace std;
 
#include "Poco/Util/Application.h"
#include "Poco/Path.h"
using namespace Poco::Util;
 
#include "Poco/AutoPtr.h"
#include "Poco/Util/IniFileConfiguration.h"
using Poco::AutoPtr;
using Poco::Util::IniFileConfiguration;

//解析config.ini
int ReadCFG()
{
	try
	{
		AutoPtr<IniFileConfiguration> pConf(new IniFileConfiguration("config.ini"));
		std::string sMainHostIp = pConf->getString("myApplication.MainHostIp");
		int nMainHostDataPort = pConf->getInt("myApplication.MainHostDataPort");
		//svalue = pConf->getInt("myApplication.asomeValue", 456);

		poco_information_f2(LOG, "sMainHostIp=%s,MainHostDataPort=%s",
			sMainHostIp, to_string(nMainHostDataPort));
            	}
	catch (Poco::Exception& ex)
	{
		poco_error(logger_handle, "ReadCFG failed!");
		poco_error(logger_handle, ex.displayText());
		return -1;
	}
	return 0;
}


 
int main(int argc, char** argv)
{
    SetupLogger();//启动poco日志，见WatchDog示例（此功能删掉改用std::cout不影响结果）
    //解析config.ini
    ReadCFG();
    //解析Text.ini
	AutoPtr<IniFileConfiguration> pConf(new IniFileConfiguration("Test.ini"));
	std::string path = pConf->getString("myApplication.somePath");
	int svalue = pConf->getInt("myApplication.someValue");
	svalue = pConf->getInt("myApplication.asomeValue",456);
	std::cout << path << endl;
	cout << svalue << endl;
 
	return 0;
}