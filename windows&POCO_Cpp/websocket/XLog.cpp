#include <memory>
using std::shared_ptr;
//#include <shlobj.h>
#include "XLog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/daily_file_sink.h" // support for basic file logging
#include "stdafx.h"
#include "linux/limits.h"
shared_ptr<spdlog::logger> spd;
shared_ptr<spdlog::logger> console;
bool InitLog()
{
    /*
    wchar_t szPath[PATH_MAX] = { 0 };
    //GetModuleFileNameW(NULL, szPath, PATH_MAX);
    //szPath = L"C:\\demo\\test\\app.exe"
    wchar_t* p = wcsrchr(szPath, '\\');
    *++p = 0;
    wcscat(szPath, L"log\\RelayServer\\");
    */
    char strPath[PATH_MAX] ="log/RelayServer/rs.txt";
    //szPath = L"C:\\demo\\test\\log\\RelayServer\\"

    //创建日志目录
    /*
    int nRet = SHCreateDirectory(nullptr, szPath);
    if (nRet != ERROR_SUCCESS && nRet != ERROR_ALREADY_EXISTS)
    {
        fprintf_s(stderr, "SHCreateDirectory failed , error code %d\n", nRet);
        return false;
    }*/

    //wcscat(szPath, L"rs.txt");
    //创建日志对象
    spd = spdlog::daily_logger_mt("RS", strPath, 0, 0, false);
    if (spd)
    {
        spd->flush_on(spdlog::level::info);
        spd->set_pattern("%l : %Y-%m-%d %H:%M:%S:%e tid:%t : %v");
        spdlog::flush_every(std::chrono::seconds(3));
    }
    console = spdlog::stdout_color_mt("console");
    if (console)
    {
        console->flush_on(spdlog::level::info);
        console->set_pattern("%l : %Y-%m-%d %H:%M:%S:%e tid:%t : %v");
        spdlog::flush_every(std::chrono::seconds(3));
    }
    return spd != nullptr;
}

void CloseLog()
{
    spdlog::shutdown();
}

CThreadLog::CThreadLog(const char* func):
_func(func)
{
    char buf[256] = { 0 };
    sprintf(buf, "%s __THREAD__ Enter.", _func);
    spd->info("{}\r\n", buf);
    console->info("{}\r\n", buf);
}
CThreadLog::~CThreadLog()
{
    char buf[256] = { 0 };
    sprintf(buf, "%s __THREAD__ Exist.", _func);
    spd->info("{}\r\n", buf);
}
