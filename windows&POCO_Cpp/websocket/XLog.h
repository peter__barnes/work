#ifndef XLOG_H
#define XLOG_H
#include "spdlog/spdlog.h"
#include "stdafx.h"
//初始化日志库
bool InitLog();
//关闭日志 注：必须要有执行这个函数，否则会导致线程无法正常退出
void CloseLog();

//日志对象
extern shared_ptr<spdlog::logger> spd;
extern shared_ptr<spdlog::logger> console;
//记录消息X(X = 字符串)
#define ILOG(x) spd->info( "{} line : {}\r\n{}\r\n\r\n", __FILE__, __LINE__, x); \
                console->info( "{} line : {}\r\n{}\r\n\r\n", __FILE__, __LINE__, x);
#define WLOG(x) spd->warn( "{} line : {}\r\n{}\r\n\r\n", __FILE__, __LINE__, x); \
                console->warn( "{} line : {}\r\n{}\r\n\r\n", __FILE__, __LINE__, x);
#define ELOG(x) spd->error("{} line : {}\r\n{}\r\n\r\n", __FILE__, __LINE__, x); \
                console->error("{} line : {}\r\n{}\r\n\r\n", __FILE__, __LINE__, x);
class CThreadLog
{
public:
    CThreadLog(const char* func);
    ~CThreadLog();
private:
    const char* _func;
};
#define LT   CThreadLog xxThreadLog(__FUNCTION__)

//声明日志缓冲区[多线程安全]
#define LOG_BUF(nSize)	char _logBuf[nSize]; MSL_(m_xLog);
#define ILOG1(fmt,x)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x);\
    ILOG(_logBuf);\
}
#define ILOG2(fmt,x,y)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x,y);\
    ILOG(_logBuf);\
}
#define ILOG3(fmt,x,y,z)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x,y,z);\
    ILOG(_logBuf);\
}
#define ELOGE(ex)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,"%s %s", ex.name(), ex.message().c_str());\
    ELOG(_logBuf);\
}
#define ELOG1(fmt, x)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x);\
    ELOG(_logBuf);\
}
#define ELOG2(fmt,x,y)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x,y);\
    ELOG(_logBuf);\
}
#define ELOG3(fmt,x,y,z)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x,y,z);\
    ELOG(_logBuf);\
}
#define WLOG1(fmt,x)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x);\
    WLOG(_logBuf);\
}
#define WLOG2(fmt,x,y)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x,y);\
    WLOG(_logBuf);\
}
#define WLOG3(fmt,x,y,z)\
{\
    SSL_(m_xLog);\
    sprintf(_logBuf,fmt,x,y,z);\
    WLOG(_logBuf);\
}



#endif // XLOG_H
