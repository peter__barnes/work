#include "stdafx.h"
#include "XSpinLock.h"

XSpinLock::XSpinLock(bool bRecursive /* = false */)
{
	m_bRecursive = bRecursive;
	m_slock = 0;
	m_nRef = 0;
	//m_dwTid = 0;
    m_dwTid=(std::thread::id)0;
}

//加锁
void XSpinLock::Lock()
{
	if (m_bRecursive)
	{
		//DWORD dwTid = GetCurrentThreadId();
		std::thread::id nTid = std::this_thread::get_id();
		if (nTid == m_dwTid)
		{
			//相同线程
			m_nRef++;
			return;
		}
	}
	do
	{
		long n = 0;
		if (m_slock.compare_exchange_strong(n, 1))
		{
			if (m_bRecursive)
			{
				//记录当前线程ID
				//m_dwTid = GetCurrentThreadId();
                m_dwTid = std::this_thread::get_id();

				m_nRef++;
			}
			break;
		}
		//__asm pause
	} while (true);
}

//解锁
void XSpinLock::Unlock()
{
	if (m_bRecursive)
	{
		//DWORD dwTid = GetCurrentThreadId();
        std::thread::id dwTid = std::this_thread::get_id();

		if (dwTid == m_dwTid)
		{
			m_nRef--;
			if (m_nRef == 0)
			{
				m_dwTid=(std::thread::id)0;
				m_slock = 0;
			}
		}
	}
	else
	{
		m_slock = 0;
	}
}

XSpinLock::XScopedLock::XScopedLock(XSpinLock& xSpinLock) :_xSpinLock(xSpinLock)
{
	_xSpinLock.Lock();
}

XSpinLock::XScopedLock::~XScopedLock()
{
	_xSpinLock.Unlock();
}


