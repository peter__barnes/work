#include "stdafx.h"
#include "WebSocketClientMgr.h"
#include "IPDirector.h"
//#include "JsonHandler.h"
//#include "Config.h"
//#include "Logic.h"
#include "WebSocketServer.h"

CWebSocketServer::CWebSocketServer()
{
	m_pHttpServer = nullptr;
	//HTTPStreamFactory::registerFactory();
}

CWebSocketServer::~CWebSocketServer()
{
	//HTTPStreamFactory::unregisterFactory();
}

void CWebSocketServer::Start(int nPort)
{
	if (m_pHttpServer == nullptr)
	{
		HTTPServerParams::Ptr  param = new HTTPServerParams();
		param->setMaxThreads(5);
		param->setThreadIdleTime(1000);
		m_pHttpServer = new HTTPServer(new RequestHandlerFactory, nPort, param);
		if (m_pHttpServer != nullptr)
		{
			m_pHttpServer->start();
		}
	}
}

void CWebSocketServer::Stop()
{
	m_pHttpServer->stopAll();
}

void CWSRequestHandler::handleRequest(HTTPServerRequest& request, HTTPServerResponse& response)
{
	response.set("Sec-WebSocket-Protocol", "EVAC-6100");
	WebSocket ws(request, response);
	CWebSocketClient client(ws);//根据请求的wsSocket，建立一个客户端处理对象
	theClientMgr.AddClient(&client);
	
	//CFG.PublishAll();//推送当前全部信息

	client.RecvLoop();//循环接收处理客户端报文
}

HTTPRequestHandler* RequestHandlerFactory::createRequestHandler(const HTTPServerRequest& request)
{
	Mutex::ScopedLock   lock(m_mutex);
	string sReqInfo="";
	for (HTTPServerRequest::ConstIterator itReq = request.begin(); itReq != request.end(); ++itReq)
	{
		//std::cout << itReq->first << ": " << itReq->second << std::endl;
		//poco_information_f2(LOG, "%s:%s", itReq->first, itReq->second);
		sReqInfo += itReq->first + ":" + itReq->second+"\n";
	}
	//poco_information(LOG, sReqInfo);
	if (request.find("Upgrade") != request.end() &&
		Poco::icompare(request["Upgrade"], "websocket") == 0 &&
		request.find("Sec-WebSocket-Protocol") != request.end() &&
		Poco::icompare(request["Sec-WebSocket-Protocol"], "EVAC-6100") == 0
		)
		return new CWSRequestHandler;
	else
		return nullptr;
}
