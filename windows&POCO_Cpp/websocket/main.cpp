#include "stdafx.h"
//#include "Utility.h"
#include "IPDirector.h"
//#include "IPConfig.h"
#include "XLog.h"
//#include "PocoRedis.h"
using namespace std;
//using namespace U;
 

int main(int argc, char** argv)
{
    //初始化日志系统
    if (!InitLog())
    {
        printf("InitLog error, system need restart\n");
        return -1;
    }
    ELOG("InitLog OK!");

    //相关资源初始化
            /*
    {
        //测试代码
        //PocoRedisTest("127.0.0.1",6379);
        
        bool bOK = false;
        do
        {
            //加载配置
            if (!IPCON.Load())
            {
                ELOG("Conf load failed.");
                break;
            }
            //加载语言资源
#ifndef NO_LANG
            if (!LANG.Load())
            {
                ELOG("Language pack load failed.");
                break;
            }
#endif
            //初始化组播地址池
            IPMAM.Init();
            bOK = true;
        } while (0);
        if (!bOK)
        {
            ELOG("Relay Server Exiting... RES Init Failed");

            //关闭日志
            CloseLog();

            return -1;
        }
    }
*/
    ILOG("Relay Server Starting......");
    bool bOK = true;
    do
    {
        //启动中继服务器
        if (!IPD.Start())
        {
            bOK = false;
            break;
        }
        ILOG("Relay Server Start OK......");
        //退出指令处理
        while (true)
        {
            //if (_kbhit())
            {
                char c = (char)getchar();
                putchar(c);
                if (c == 'x' || c == 'X')
                {
                    break;
                }
            }
            Sleep(50);
        }
    } while (0);

    IPD.Stop();
    //CloseHandle(_single);

    if (bOK)
    {
        //IPDBS.WriteStopMsg();
        ELOG("Relay Server Exit......Normal");
    }
    else
    {
        ELOG("Relay Server Start Failed......");
    }

    CloseLog();

	return 0;
}
