
#ifndef STDAXF_H
#define STDAXF_H

#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <cstring>

#include "unistd.h"//usleep等无法跨平台等功能
#define Sleep(a) usleep(a)


//cpp
#include <iostream>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <vector>
#include <utility>
#include <map>
#include <set>
#include <unordered_map>
#include <sstream>
#include <exception>
#include <cctype>
#include <memory>
#include <array>
#include <atomic>
#include <thread>
#include <mutex>
#include <bitset>
#include <functional>

using std::string;
using std::wstring;
using std::to_string;
using std::stoi;
using std::queue;
using std::deque;
using std::list;
using std::vector;
using std::pair;
using std::map;
using std::set;
using std::unordered_map;
using std::stringstream;
using std::exception;
using std::unique_ptr;
using std::make_unique;
using std::shared_ptr;
using std::make_shared;
using std::array;
using std::atomic;
using std::thread;
using std::mutex;
using std::lock_guard;
using std::make_pair;
using std::bitset;

//poco basic
#include "Poco/Types.h"
#include "Poco/Exception.h"
#include "Poco/AutoPtr.h"
#include "Poco/Timespan.h"
#include "Poco/LocalDateTime.h"
#include "Poco/Util/JSONConfiguration.h"
using Poco::Exception;
using Poco::Event;
using Poco::AutoPtr;
using Poco::Timespan;
using Poco::LocalDateTime;
using Poco::Int64;
using Poco::Util::JSONConfiguration;

//POCO NETWORK
#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/MulticastSocket.h"
#include "Poco/Net/NetException.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/WebSocket.h"
#include "Poco/Thread.h"
using Poco::Net::SocketAddress;
using Poco::Thread;
using Poco::Net::DatagramSocket;
using Poco::Net::MulticastSocket;
using Poco::TimeoutException;
//POCO 线程
#include "Poco/Runnable.h"
#include "Poco/ThreadPool.h"
using Poco::Runnable;
using Poco::ThreadPool;
using Poco::Thread;
using Poco::Event;
using Poco::Mutex;
//POCO WebSocket
#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/WebSocket.h"
using Poco::Net::HTTPServer;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServerParams;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPMessage;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPCredentials;
using Poco::Net::WebSocket;

//POCO JSON-主要用于封装
#include "Poco/JSON/Parser.h"
using Poco::Dynamic::Var;
using Poco::JSON::Object;
using Poco::JSON::Array;
using Poco::JSON::Parser;
using Poco::JSON::JSONException;

//rapidjson-主要用于解析 , poco解析功能没有对内存管理进行优化，因此采用rapidjson
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
using namespace rapidjson;

//typedef wchar_t wchar;
//windows兼容
typedef unsigned long       DWORD;
typedef int                 BOOL;
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
typedef long LONG;
typedef Poco::Int64 LONGLONG;
typedef Poco::UInt64 ULONGLONG;
//typedef unsigned long ULONG_PTR;
typedef Poco::UInt32 ULONG_PTR;
typedef ULONG_PTR DWORD_PTR;
//typedef unsigned long DWORD_PTR, *PDWORD_PTR;
#define MAKEWORD(a, b)      ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MAKELONG(a, b)      ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
#define LOWORD(l)           ((WORD)(((DWORD_PTR)(l)) & 0xffff))
#define HIWORD(l)           ((WORD)((((DWORD_PTR)(l)) >> 16) & 0xffff))
#define LOBYTE(w)           ((BYTE)(((DWORD_PTR)(w)) & 0xff))
#define HIBYTE(w)           ((BYTE)((((DWORD_PTR)(w)) >> 8) & 0xff))


#define CSR const string&

//单例(windows有效，Linux需要改写)
#define SINGLETON(className) public: static className##& GetInstance(){	static className ins; return ins;}
#include "XLog.h"
#include "XSpinLock.h"
//#include "Utility.h"


#endif
