#ifndef COMMON_H
#define COMMON_H

#include"stdafx.h"

#define NO_LANG
//日志级别
#define LOG_INF 0
#define LOG_WAR 3
#define LOG_ERR 5
//打印类开关
//#define DIG_AUDIO_TEST
#define JSON_RELY_ALL_TEST
#define PRINT_VOL_CHANGE
#define PRINT_EP_TASK_CHANGE
//#define JSON_NOTIFY_TEST
//#define ZMQ_TEST
//#define VOL_TEST
#define JSON_NOISE_DEV

#define TMP_TEST //临时版本，兼容版：允许主机、主控卡少一字节音量
//版本兼容(V1.1功能)
#define VERSION_1_1
#ifdef VERSION_1_1
#define OLD_STYLE_EP_STAT_LS
#define NOISE_CARD_BIND_LIST_IN_LS
#define OTHER_VERSION_1_1
#endif

//继电器与音量调节合一指令
#define CMD_CTRL_AUDIO_WITH_VOL
#define CMD_ONLY_BUFFER_SLEEP

#define LED_CHECK_PERIOD 5
#define MAX_AMP_CARD_MAIN_PER_GROUP 8
#define MAX_AMP_CARD_BK_PER_GROUP 1
#define UDP_PKT_SIZE	1500

#define MAX_STEAL_TASK_LIST_SIZE 100
#define LS_OFFLINE_TIME 15
#define OFFLINE_TIME_BY_SECOND 5
#define DEFAULT_UDP_DEV_PORT 6500
#define DEFAULT_AUDIO_PORT 8802
#define MAC_LEN		6
//公司标志位 ASCII BL（保伦）
#define COMPANY_MARK 0x424c
//终端协议标志位
//#define IP_MARK 0X5A
#define PROJ_MARK 0xba0b

#define HOST_DEV_ZONE_NUM 4		//设备真实通道数
#define AMP_DEV_ZONE_NUM 4
#define BOOSTER_ZONE_NUM 4
#define NOISE_DEV_ZONE_NUM 4
#define VIR_HOST_DEV_ZONE_NUM 5		//比真实通道数多一路虚拟监听分区
#define FLOATING_CARD_AMP_NUM 5
#define FLOATING_CARD_OUTPUT_NUM 4
#define WIRELESS_PHONE_ZONE_NUM 2
#define MAX_ZONE_NAME_NUM_PER_DEV 4 //单个设备最多包含分区名个数（有分区名的设备，按数量最多的类型算）
#define CONFIG_TOOL_ZONE_MARK 3
//套接字缓冲区大小
#define UDP_SNDBUF_SIZE	(10*1024*1024)
#define UDP_RCVBUF_SIZE	(10*1024*1024)

//终端LED显示状态
enum MIC_LED_STAT
{
    LED_FREE = 1,
    LED_TALK,
    LED_ALARM,
    LED_MUSIC,
    LED_BROADCAST,
    LED_MONITOR,
    LED_RING,
    LED_EMG,
    LED_TOTAL_NUM,
};
enum TASK_STEP
{
    TASK_STEP_RING,
    TASK_STEP_SHOW,
    TASK_STEP_OTHER,
    TASK_TOTAL_NUM,
};
enum IO_TYPE
{
    EP_OUTPUT,//输出终端（分区/通道）
    EP_INPUT,	//输入终端（分区/通道）
    EP_NO_FIX_CHANNEL,//非固定IO通道设备
    EP_IO_BOTH,	//对讲类通道
};
enum EP_TASK_TYPE
{
    EP_BG_TASK=0,//背景音
    EP_MIC_TASK=1,//前景音
    EP_SRC_TASK=2,//音源
    EP_MAX_TASK_NUM=3,	
};

enum NOISE_CARD_CONFIG
{
    NOISE_REFER_NOISE,
    NOISE_RATIO_GAIN,
    NOISE_CONFIG_TOTAL_NUM,
};
enum AMP_GROUP_TYPE
{
    AMP_MAIN,//独立使用功放或主功放（只含服务器直接管理的，不含浮动卡管理的主功放）
    AMP_BK,//备用功放（只含服务器直接管理的，不含浮动卡管理的备用功放）
    AMP_FLOATING,//浮动卡管理的功放
};
//新版本
enum RS_TASK_TYPE
{
    IP_TASK_NULL,
    IP_TASK_CLIENT_PLAY_LOCAL_MUSIC,
    IP_TASK_SERVER_PLAY_LOCAL_MUSIC,
    IP_TASK_BROADCAST,
    IP_TASK_RING,
    IP_TASK_MONITOR_ENV,
    IP_TASK_BUSINESS_TALK,
    IP_TASK_SIP_BROADCAST,
};
enum LS_TASK_TYPE
{
    LS_TASK_NULL,
    LS_TASK_TEMP_TASK = 3,
    LS_TASK_QUEUE_TASK = 4,
    LS_TASK_CLIENT_PLAY_LOCAL_MUSIC = 5,
    LS_TASK_TIMETASK_PLAY_SERVER_MUSIC = 6,
    LS_TASK_BG_MUSIC = 7,
    LS_TASK_BROADCAST=8,
    LS_TASK_LINE = 9,
    LS_TASK_FAS_COLLECT = 10,
    LS_TASK_FIRE_ALARM_MP3 = 11,
    LS_TASK_TRIG_IN = 12,
    LS_TASK_TRIG_BG_MUSIC = 13,
    LS_TASK_MANUAL_ALARM = 14,
    LS_TASK_MOBILE_BROADCAST = 15,
    LS_TASK_MOBILE_TALK=16,
    LS_TASK_BUSINESS_TALK=17,
    LS_TASK_RING=18,
    LS_TASK_MONITOR_ENV=19,
    LS_TASK_MOBILE_TTS_PLAY = 20,
    LS_TASK_SIP_BROADCAST=21,
};
const int EP_SOURCE_CHANNEL = EP_MIC_TASK;
enum FLAG_WORKABLE
{
    WORKABLE_NULL,
    WORKABLE_OK,
    WORKABLE_ABNORMAL,//异常
    WORKABLE_BREAK,
};
enum FLAG_CODE
{
    FLAG_NULL =0x00,
    //执行结果
    RET_SUCCESS = 0x01,
    RET_FAIL = 0x02,
    //工作状态
    STAT_WORK = 0x03,//正在工作
    STAT_FREE = 0x04,//空闲
    //故障状态
    STAT_OK = 0x05,//正常
    STAT_BREAK = 0x06,//故障
    STAT_OFFLINE = 0x07,
    //按钮状态（紧急）
    BTN_RELEASE = 0x08,//松开（紧急）
    BTN_PRESS = 0x09,//按下（紧急）
    //功放状态
    AMP_BREAK = 0x0A,//故障
    AMP_OK = 0x0B,//正常
    AMP_SLEEP = 0x0C,//睡眠
    //回路状态
    CIRC_OPEN = 0x0D,//开路
    CIRC_SHORT = 0x0E,//短路
    CIRC_OK = 0x0F,//正常
    CIRC_GROUND = 0x10,//接地
    //输入触点状态
    INPUT_OPEN = 0x11,//开路
    INPUT_SHORT = 0x12,//短路
    INPUT_OK = 0x13,//正常
    INPUT_TRIG = 0x14,//触发
    //输出触点状态
    OUTPUT_BREAK = 0x15,//断开
    OUTPUT_CONNECT = 0x16,//闭合
    //控制位
    CTRL_OPEN = 0x17,//控制位打开
    CTRL_CLOSE = 0x18,//控制位关闭
    CTRL_NO_CHANGE = 0x19,//控制位不改变
    //触发模式
    SHORT_TRIG_MODE ,//短路触发（正常,触发）
    ELE_LEVE_TRIG_MODE ,//电平触发（开路,短路,正常,触发）
    //寻呼控制
    BTN_BROADCAST_START,//广播按钮按下（非紧急）
    BTN_SINGLE_START,	//呼叫按钮按下（非紧急）
    BTN_COMMON_START,//普通按钮按下（非紧急）
    BTN_END,//按钮结束（非紧急）
    BTN_REJECT,//按钮拒接（非紧急，只在对讲设备收到请求）
    BTN_ACCEPT,//按钮拒接（非紧急）

    //检测时间模式
    CHECK_MODE_TIME_TAB=0x24,//检测模式-时间表
    CHECK_MODE_PERIOD,//检测模式-周期
    CHECK_MODE_NONE,//检测模式不-检测
    //触发方式
    SINGLE_TRIG_IN_MODE,//单次触发（用于触点输入）
    MULTI_TRIG_IN_MODE,//多次触发（用于触点输入）
    FLOATING_AMP_MAIN = 0x2b,//主功放
    FLOATING_AMP_BK = 0x2c, //备功放

    SERIAL_5bit = 0x30,//5位
    SERIAL_6bit = 0x31,//6位
    SERIAL_7bit = 0x32,//7位
    SERIAL_8bit = 0x33,//8位
    SERIAL_1bit = 0x34,//1位
    SERIAL_1_5bit = 0x35,//1.5位
    SERIAL_2bit = 0x36,//2位
    SERIAL_NO_CHECK = 0x37,//无校验
    SERIAL_ODD,//奇
    SERIAL_EVEN ,//偶
    SERIAL_HIGH ,//高
    SERIAL_LOW ,//低
    SERIAL_422 ,//422总线
    SERIAL_485 ,//485总线

    SERIAL_BAUD_600 = 0x40,//600
    SERIAL_BAUD_1200 ,//1200
    SERIAL_BAUD_2400 ,//2400
    SERIAL_BAUD_4800 ,//4800
    SERIAL_BAUD_9600,//9600
    SERIAL_BAUD_19200 ,//19200
    SERIAL_BAUD_38400 ,//38400
    SERIAL_BAUD_57600 ,//57600
    SERIAL_BAUD_115200,//115200（默认配置）
    //数传模式
    DEV_SINGLECAST = 0x50,//单播
    DEV_MULTICAST = 0x51,//多播
    //数传IP获取方式
    SET_IP_DYNAMIC = 0x52,//单播
    SET_IP_STATIC = 0x53,//多播

    //监听状态
    MONITOR_OK = 0x60,//监听成功
    MONITOR_CLOSE = 0x61,//服务器关闭
    MONITOR_FAILED = 0x62,//失败（故障、离线）
    MONITOR_USED = 0x63,//已被其他监听
    //客户端软件状态（操作台 / 业务话筒）
    MIC_SOFT_LOGOUT = 0xa0,//操作台软件退出
    MIC_SOFT_LOGIN = 0xa1, //登陆
    MIC_SOFT_LOCKED = 0xa2, //锁屏
    //电源状态
    POWER_STAT_OK =0xb0,//正常
    POWER_STAT_BREAK=0xb1,//故障
    POWER_STAT_OVER_VOLTAGE=0xb2,//过压
    POWER_STAT_LACK_VOLTAGE = 0xb3,//欠压
    POWER_STAT_WAIT = 0xb4,//待机
    //无线模块状态
    WIRELESS_STAT_WAIT = 0xb8,//待机
    WIRELESS_STAT_RECVING = 0xb9,//接收
    WIRELESS_STAT_SENDING = 0xba,//发送
    WIRELESS_STAT_BREAK = 0xbb,//故障
};
// 设备与服务器协议
enum DEV_TYPE
{
    DEV_MAIN_CARD = 0x80,// 主控板卡
    DEV_AUDIO_CARD = 0x81,// 音频板卡
    DEV_RECORD_CARD = 0x82,// 录音板卡
    DEV_PHONE_CARD = 0x83,//电话板卡
    DEV_NOISE_CHECK_CARD = 0x84,//噪检板卡
    DEV_FAS_CARD = 0x85,//FAS短路板卡
    DEV_AMP_CARD = 0x86,//功放板卡
    DEV_AC_POWER = 0x87,//功放电源卡：原交流电源(Alternating Current)板卡
    DEV_DC_POWER = 0x88,//主控电源卡：原直流电源(Direct Current)板卡
    DEV_BACKUP_POWER = 0x89,//直流电源卡：原备用电源板卡
    DEV_BUSSINESS_MIC = 0x8A,//业务话筒
    DEV_DIG_MIC = 0x8B,//数传话筒
    DEV_SITE_SERV = 0x8C,//站点服务器（用于显示设备列表）
    DEV_CENTER_SERV = 0x8D,//中心服务器（用于显示设备列表）
    DEV_HOST_DEV,//主机（类似海南主机-一体式)
    DEV_AMP_DEV,//分区功放（类似海南分区-一体式)
    DEV_TALK_PANEL,
    DEV_BOOSTER,//广播升压器
    DEV_NOISE_DEV,//噪检探测器(一体式噪检设备)
    DEV_FLOATING_CARD,//浮动卡
    DEV_WIRELESS_PHONE,//小区广播(无线手持机)
    DEV_ABRIDGED_MIC,//简化版操作台
};

//extern map<int, int> mapLS2RSDevType;
//extern map<int, int> mapRS2LSDevType;

//终端协议（大小端问题，反转顺序）
enum EndPointCMD
{
    /* Command: 0x00~0x09 is used by IPAddressWriter */
    IP_KEEP_ALIVE = 0x0100, //设备心跳
    IP_NEED_DATA = 0X0200,			//设备需要更新数据
    IP_PTT_BTN_STAT = 0X0300,			//PTT按钮状态变化
    IP_EMG_BTN_STAT = 0X0400,			//紧急按钮状态变化
    IP_MONITOR = 0x0500,			//监听
    IP_SYNC_STAT = 0x0600,	//同步设备状态
    IP_SYNC_NAME = 0x0700,	//同步设备名称
    IP_ZONE_NAME = 0x0800,//同步分区设备分区名称
    IP_CTRL_AUDIO_IN = 0x0900,	//控制音频输入
    IP_CTRL_AUDIO_OUT = 0x0a00,	//控制音频输出
    IP_SET_AMP_VOLUME = 0x0b00, //设置输出音量
    IP_DIAL_STAT = 0x0c00,//电话拨通状态
    IP_DIAL_CODE = 0x0d00,//电话操作符
    IP_FAS_TRIG_STAT = 0x0e00,//FAS触点状态
    IP_SET_TRIG_OUT_MULTI = 0x0f00, //设置触点输出
    IP_SET_AUTO_CHECK = 0x1000,//自检
    IP_MIC_STAT = 0x1100,//0011 话筒寻呼
    IP_SET_DEV_LAMP = 0x1200,//设备模式状态设置
    IP_TEST_BOOSTER_START = 0x1300,//升压器开始检测
    IP_TEST_BOOSTER_RESULT = 0x1400,//升压器上送检测结果
    IP_CTRL_BOOSTER_OUT = 0x1500,//升压器输出控制
    IP_TRIG_STAT_OUT = 0x1600,	//新版触点输出（用于对讲设备）
    IP_TRIG_STAT_IN = 0x1700,	//新版触点输入（用于对讲设备）
    IP_REQ_ACCEPT_CALL = 0x1800,
    IP_SET_PEER_TYPE = 0x1900,//设备模式状态设置
    IP_CTRL_ONE_TRIGGER_OUT = 0x1a00,//控制单个触点输出
    IP_CTRL_PHONE = 0x1b00,//电话控制
    IP_REQ_PHONE_RING = 0x1c00,//提示音请求(电话板卡)
    IP_MONITOR_RESULT = 0x1d00,//下发监听结果
    IP_REQUEST_NOISE_VALUE = 0x1e00,//下发监听结果
    IP_REPLY_NOISE_VALUE = 0x1f00,//下发监听结果
    IP_NOTIFY_EMG_TASK_FAILED = 0x2000,//下发紧急任务建立失败。
    IP_FLOATING_CARD_TASK_CHANGE = 0x2300,//浮动卡上报单个分区状态
    IP_SYNC_SYS_TIME = 0x2400,//设备请求同步系统时间
    IP_BLOCK_BROADCAST_CTRL = 0x2500,//站台小区广播控制指令
    IP_BLOCK_TRIG_CTRL = 0x2600,//站台小区广播按键状态
    IP_SET_AMP_GROUP = 0x0102,//设置主备功放分组
    IP_GET_AMP_GROUP = 0x0202,//读取主备功放分组
    IP_SET_AMP_BALANCE_SWITCH = 0x0402,//设置功放均衡切换
    IP_GET_AMP_BALANCE_SWITCH = 0x0502,//读取功放均衡切换

    IP_SET_DEV_NAME = 0x0305,
    IP_CLEAR_OFFLINE_DEV = 0x1105,//清除中继服务器所有离线设备信息
    IP_SET_FLOATING_CARD_BIND = 0x1505,//设置（下发）浮动卡分组

    IP_SET_PROXY = 0x0505,	//配置转发端口参数
    IP_FAS_TRIG_MODE_BASE = 0x0705,//FAS触点模式
    IP_SET_CHECK_SWITCH = 0x0905,//配置检测开关
    IP_GET_CHECK_SWITCH = 0x0a05,//读取检测开关
    IP_SET_TRIG_MODE_IN = 0x0d05,//设置FAS触点输入模式
    IP_GET_TRIG_MODE_IN = 0x0e05,//读取FAS触点输入模式
};
#include "stdafx.h"
//终端标识符
typedef struct tagEpGroupInfo
{
    vector<string> setMainEpPk;
    vector<string> setBackupEpPk;
}EP_GROUP_INFO;
//协议解析
#define PK_STR_LEN		20
#define PK_LEN		7
#define MAC_LEN		6
#define AUDIO_ID_LEN 7
#define IP_LEN		4
#define MAX_NAME_LEN 64
#define DIG_PROTO_AUDIO_ID_OFFSET  6 //数传协议音频流音频ID位置
#define COMPANY_OFFSET			(0)
#define PROJ_OFFSET				(COMPANY_OFFSET + 2)
#define DATA_LEN_OFFSET			(PROJ_OFFSET + 2)
#define EXTEND_LEN_OFFSET		(DATA_LEN_OFFSET + 2)
#define CRC_ENABLE_FLAG_OFFSET	(EXTEND_LEN_OFFSET + 1)
#define CRC_CODE_OFFSET			(CRC_ENABLE_FLAG_OFFSET + 1)
#define PK_OFFSET				(CRC_CODE_OFFSET + 2)
#define ACKNUM_OFFSET			(PK_OFFSET + PK_LEN)
#define SRCMAC_OFFSET			(ACKNUM_OFFSET + 2)
#define IP_ADDR_OFFSET			(SRCMAC_OFFSET + MAC_LEN)
#define DEV_TYPE_OFFSET			(IP_ADDR_OFFSET + IP_LEN)
#define PROTO_TYPE_OFFSET		(DEV_TYPE_OFFSET + 1)
#define RESERVE_OFFSET			(PROTO_TYPE_OFFSET + 1)
#define ENTITY_OFFSET			(RESERVE_OFFSET + 4)
#define DATA_OFFSET				(ENTITY_OFFSET+2)//实体起始位置+2字节命令码


#define MSGID(id) #id

#define NAME_DEV_MAIN_CARD  "主控卡"
#define NAME_DEV_AUDIO_CARD "音频卡"
#define NAME_DEV_RECORD_CARD "录音卡"
#define NAME_DEV_PHONE_CARD "电话终端"
#define NAME_DEV_NOISE_CHECK_CARD "噪检卡"
#define NAME_DEV_FAS_CARD "FAS卡"
#define NAME_DEV_AMP_CARD "功放卡"
#define NAME_DEV_POWER "电源卡"
#define NAME_DEV_BACKUP_POWER "备用电源"
#define NAME_DEV_BUSSINESS_MIC "操作台"
#define NAME_DEV_DIG_MIC "音频话筒"
#define NAME_DEV_TALK_PANEL "插播盒"
#define NAME_DEV_SERV "服务器"
#define NAME_DEV_HOST_DEV "广播主机"
#define NAME_DEV_AMP_DEV "广播功放"
#define NAME_DEV_NOISE_CHECK_DEV "噪检探测器"//一体式
#define NAME_DEV_FLOATING_CARD "浮动卡"
#define NAME_DEV_WIRELESS_PHONE "小区广播"
#define NAME_DEV_ABRIDGED_MIC "控制盒"

#define NAME_DEV_COMMON "设备"

#define NAME_DEV_RS "中继服务器"
#define NAME_DEFAULT_AMP_ZONE "功放分区"
#define NAME_DEFAULT_FLOATING_ZONE "浮动分区"

#define LOG_SERV_LOGOUT "后台服务退出"
#define LOG_SERV_LOGIN "后台服务启动"








#define MSGID(id) #id
//
//任务相关及终端设置相关，逻辑服务器向中继服务器发送zmqRequest
//		LSR : Logic Server Request
//
#define LSR_CreateNoiseDev \
		MSGID(ls2rs_create_noise_dev)

#define LSR_PublishTerminalStatus \
		MSGID(ls2rs_publish_terminals_status)

#define LSR_PublishModBusTerminalInfo \
		MSGID(ls2rs_publish_modbus_terminal_info)

#define LSR_PublishEndPointsName \
		MSGID(ls2rs_publish_terminals_name)

#define LSR_PublishDeviceName \
		MSGID(ls2rs_publish_device_name)

//创建任务
#define	LSR_CreateTask \
			MSGID(zmq_request_task_terminal)

//创建任务
//#define	LSR_CreateTask \
//			MSGID(ls2rs_create_task)

//销毁任务(单个)
#define	LSR_DestroyTask \
			MSGID(zmq_request_stop_task)
			//MSGID(ls2rs_destroy_task)

//销毁任务(批量)
#define	LSR_DestroyTasks \
			MSGID(zmq_request_stop_tasks)

//销毁任务(批量:根据LSID)
#define	LSR_DestroyTasksByLSID \
			MSGID(zmq_stop_all_task)

//请求接听对讲
#define	LSR_RequestAcceptCall \
			MSGID(ls2rs_request_accept_call)


//服务器强制控制小区广播工作状态
#define	LSR_BlockBroadCastForceCtrl \
			MSGID(ls2rs_block_broad_cast_force_ctrl)

//向任务中添加终端
#define	LSR_PushEndPoints \
			MSGID(ls2rs_push_terminals_to_task)

//从任务中删除终端
#define	LSR_PopEndPoints \
			MSGID(ls2rs_pop_terminals_from_task)

//设置任务音量
#define	LSR_SetTaskVolume \
			MSGID(ls2rs_set_task_volume)

//配置转发端口参数
#define	LSR_SetDeviceProxyCfg \
			MSGID(ls2rs_set_device_ProxyCfg)

//设置任务灯光模式
#define	LSR_SetTaskLightMode	\
			MSGID(ls2rs_set_task_light_mode)

//设置终端音量
#define	LSR_SetEndPointsVolume \
			MSGID(ls2rs_set_terminal_volume)

//设置车站名称
#define	LSR_SetServerName \
			MSGID(ls2rs_set_server_name)

//获取车站名称
#define	LSR_GetServerName \
			MSGID(rs2ls_get_server_name)

//设置设备名称
#define	LSR_SetDeviceName \
			MSGID(ls2rs_set_device_name)

//设置分区名称
#define	LSR_SetZoneName \
			MSGID(ls2rs_set_zone_name)

//中继服务器获取设备名称
#define	LSR_GetZoneName \
			MSGID(rs2ls_get_zone_name)

//设置触点模式
#define	LSR_SetTriggerModeBase \
			MSGID(ls2rs_set_trigger_mode_base)

//设置触点输入模式
#define	LSR_SetTriggerModeIn \
			MSGID(ls2rs_set_trigger_mode_in)

//设置触点输出
#define	LSR_BinTriggerOutput \
			MSGID(ls2rs_bind_trigger_output)

//设置噪检阈值（暂未使用）
#define	LSR_SetNoiseCheckValue \
			MSGID(ls2rs_set_noise_check_level)

//推送噪检值
#define	RSR_NoiseCheckValue \
			MSGID(zmq_noise_check_value)

//设备模式状态设置（LED灯）
#define	LSR_SetLedMode \
			MSGID(ls2rs_set_led_mode)


//读取检测开关
#define	LSR_GetCheckSwitch \
			MSGID(ls2rs_get_check_switch)

//设备主备功放分组
#define	LSR_SetAmpGroup \
	MSGID(ls2rs_set_amp_group)

//设备浮动卡绑定
#define	LSR_SetFloatingBind \
	MSGID(ls2rs_set_floating_bind)

//下发自检指令
#define	LSR_SetAutoCheck \
	MSGID(ls2rs_set_auto_check)

//控制电话终端工作状态变化
#define	LSR_CtrlPhoneWorkStat \
	MSGID(ls2rs_ctrl_phone_work_stat)

//下发设置功放均衡切换
#define	LSR_SetAmpBalanceSwitch \
	MSGID(ls2rs_set_amp_balance_switch)

//获取主备功放分组
#define	LSR_GetAmpGroup \
	MSGID(rs2ls_get_amp_group)

//获取主备功放分组
#define	LSR_GetFloatingBind \
	MSGID(rs2ls_get_floating_bind)

//获取噪检值
#define	RSR_GetNoiseCheckList \
			MSGID(zmq_get_noise_check_list)

//逻辑服务器在任务间隔时下发
#define	LSR_NotifyTaskInterval \
			MSGID(ls2rs_notify_task_interval)

//下发绑定关系后，中继会根据绑定关系立即查一次噪检值并调节音量
#define	LSR_NoiseCheckBind \
			MSGID(ls2rs_noise_check_bind)

//检测主备功放分组（广播获取所有主备分组，与中继服务器内存不匹配的主键会被返回给逻辑服务器）
#define	LSR_CheckAmpGroup \
	MSGID(ls2rs_check_amp_group)


//对讲正式接通
#define	LSR_SetTaskShowInfo \
			MSGID(zmq_request_show_taskname)
			//MSGID(ls2rs_set_task_show_info)

//重新设置对讲任务的音频标签
#define	LSR_UpdateTalkTaskAudioLabel	\
			MSGID(ls2rs_reset_audio_lable)

//强制终端离线[当WEB端在终端管理里把终端删除后，如果终端在线的话，逻辑服务器需要让终端重新上线]
#define	LSR_ForceEndPointsOffline \
			MSGID(ls2rs_set_terminal_offline)

//逻辑服务器向中继服务器请求所有终端状态
#define	LSR_RequestEndPointsStatus \
			MSGID(ls2rs_get_terminals_status)

//逻辑服务器向中继服务器请求所有终端状态
#define	LSR_RequestEndPointStatus \
			MSGID(ls2rs_get_terminal_status)

//逻辑服务器向中继服务器请求所有任务状态
#define LSR_RequestAllTaskStatus \
			MSGID(ls2rs_get_all_task_status)

//发送短信
#define	LSR_SendSMS \
			MSGID(ls2rs_empty)

//
//终端请求命令：由中继服务器将此请求直接转发给逻辑服务器，以zmqRequest的方式
//     RSR : Relay Server Request
//

//电话终端对讲状态
#define	RSR_PhoneCardStat \
			MSGID(zmq_phone_card_work_stat)

//终端拨号
#define	RSR_DailCode \
			MSGID(zmq_dail_code)

//电话终端请求控制提示音
#define	RSR_PhoneCtrlRing \
			MSGID(zmq_phone_end_ring)

//站台小区广播控制指令 IP_BLOCK_BROADCAST_WORK_STAT
#define	RSR_BlockBroadcastCtrl \
			MSGID(zmq_block_broadcast_ctrl)

//站台小区广播按键指令
#define	RSR_BlockTriggerCtrl \
			MSGID(zmq_block_trigger_ctrl)

#define	RSR_FasModeChange \
			MSGID(zmq_fas_mode_change)

#define	RSR_FasInStatChange \
			MSGID(zmq_fas_in_stat_change)

//读取检测开关
#define RSR_IpCheckSwitch \
			MSGID(zmq_Ip_Check_Switch)

//返回主备功放分组检测结果
#define	RSR_CheckAmpGroup \
			MSGID(zmq_check_amp_group)

//终端同意对讲请求
#define	RSR_AcceptTalk \
			MSGID(zmq_accept_talk)

//终端拒绝对讲请求
#define	RSR_RejectTalk \
			MSGID(zmq_reject_talk)

//终端PTT按键改变
//#define	RSR_PTT_BTN_STAT_CHANGE \
			//MSGID(zmq_ptt_btn_stat_change)
//终端话筒按键改变（话筒寻呼）
#define	RSR_MIC_BTN_STAT_CHANGE \
			MSGID(zmq_mic_btn_stat_change)
//终端发起广播
#define	RSR_CreateBroadcast \
			MSGID(zmq_create_broadcast)

//终端取消广播
#define	RSR_CancelBroadcast \
			MSGID(zmq_cancel_broadcast)

//终端发起多段广播
#define	RSR_CreateBroadcastExt \
			MSGID(zmq_create_broadcast_ex)

//终端请求环境监听
#define	RSR_CreateEnvMon \
			MSGID(zmq_create_enviorenment_monitor)

//终端取消环境监听
#define	RSR_CancelEnvMon \
			MSGID(zmq_cancel_enviorenment_monitor)


//
//状态更新：中继服务器向逻辑服务器推送终端状态变化,以zmqPublish的方式
//   RSP : Relay Server Publish
//

#define	RSR_ModBusTerminalInfo \
			MSGID(zmq_publish_modbus_terminal_info)

//报告所有终端状态
#define	RSP_EndPointsStatus \
			MSGID(zmq_publish_terminals_status)

//报告单个终端状态
#define	RSP_EndPointStatus \
			MSGID(zmq_publish_terminal_status)

//报告所有任务状态
#define	RSP_AllTaskStatus \
			MSGID(zmq_publish_all_task_status)

//报告任务被抢占
#define	RSP_TaskStealInfo \
			MSGID(zmq_publish_task_steal_info)


//心跳
#define	RSP_KeepAlive \
			MSGID(zmq_publish_keep_alive)

//报告终端名称
#define	RSP_EndPointsName \
			MSGID(zmq_publish_terminals_Name)

#define	RSP_DeviceName \
			MSGID(zmq_publish_device_Name)

//终端离线
//#define	RSP_EndPointOffline \
			//MSGID(zmq_publish_terminal_offline)

//终端音量改变
#define	RSP_EndPointVolume \
			MSGID(zmq_publish_terminal_volume)

//报告所有终端状态
#define	RSP_FloatingOutputBind \
			MSGID(zmq_floating_output_bind)


//终端当前任务发生变化
#define	RSP_EndPointCurrentTask \
			MSGID(zmq_publish_terminal_current_task)


#endif // COMMON_H








