#ifndef WEB_SOCKET_SERVER_H
#define WEB_SOCKET_SERVER_H
#include "stdafx.h"
class CWSRequestHandler : public HTTPRequestHandler
{
public:
	void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response);
};

class RequestHandlerFactory : public HTTPRequestHandlerFactory
{
	Mutex  m_mutex;
public:
	HTTPRequestHandler* createRequestHandler(const HTTPServerRequest& request);
};

class CWebSocketServer
{
public:
	CWebSocketServer();
	virtual ~CWebSocketServer();

public:

	void Start(int nPort);

	void Stop();

private:
	HTTPServer*	m_pHttpServer;
};
#endif
