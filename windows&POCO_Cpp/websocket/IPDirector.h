#ifndef IPDIRECTOR_H
#define IPDIRECTOR_H
/*
#include "MulticastMgr.h"
#include "CommandRS.h"
#include "EndPointMgr.h"
#include "EPAudio.h"
#include "LSAudio.h"
*/
#include "WebSocketServer.h"
//#include "IPConfig.h"
class CMulticastMgr;
//class CEndPointMgr;
//class CCommandRS;
//class CLSAudio;
//class CEPAudio;
//class IPConfig;
//管理所有中继句柄
class IPDirector
{
private:
    IPDirector() = default;
    virtual ~IPDirector() = default;

    //SINGLETON(IPDirector);

public:
    static IPDirector& GetInstance()
    {
        static IPDirector ins;
        return ins;
    }

    //启动服务器
    bool Start();

    //停止服务器
    void Stop();
public:

/*
    //配置信息
    IPConfig m_ipConfig;

    //ZMQ发布
    //CZmqPublisher m_zmqPub;

    //终端实时信息管理器
    CEndPointMgr m_epMgr;

    //终端命令消息处理器ep(EndPoint)
    CCommandRS m_epCMD;
    //所有任务管理器
    CTaskMgr m_taskMgr;

    //终端任务管理器
    CEPTaskMgr m_epTaskMgr;
    //逻辑服务器音频数据与任务映射关系管理器
    CLSAudioMgr m_lsAudioMgr;
    //终端音频接收器
    CEPAudio m_epAudio;

    //逻辑服务器音频数据接收及转发
    CLSAudio m_lsAudio;
*/

	CWebSocketServer  m_wsSrv;

/*
    //多播地址管理器
    CMulticastMgr m_multicastMgr;
    //zmq响应派发
    CZmqDispatcher m_zmqDispatcher;

    //zmq响应管理器
    CZmqReplyMgr m_zmqReplyMgr;


    //缓存管理
    CCacheMgr m_cacheMgr;

    //终端当前任务记录
    CTaskRecord m_taskRecord;

    CDbSession m_dbSession;

    //zmq响应管理器
    CNoiseCardMgr m_noiseCardMgr;
    */
};

#define  IPWSS	(IPD.m_wsSrv)
#define  IPD	(IPDirector::GetInstance())
/*
#define  IPCON	(IPD.m_ipConfig)
#define  IPCMD	(IPD.m_epCMD)
#define  IPEPM	(IPD.m_epMgr)
#define  IPPUB	(IPD.m_zmqPub)
#define  IPRPM	(IPD.m_zmqReplyMgr)
#define  IPEA	(IPD.m_epAudio)
#define  IPTM	(IPD.m_taskMgr)
#define  IPETM	(IPD.m_epTaskMgr)
#define  IPLSAM	(IPD.m_lsAudioMgr)
#define  IPLSA	(IPD.m_lsAudio)
#define  IPMAM	(IPD.m_multicastMgr)
#define  IPTR	(IPD.m_taskRecord)
#define  IPDBS  (IPD.m_dbSession)
#define  IPNCM  (IPD.m_noiseCardMgr)
*/
#endif // IPDIRECTOR_H
