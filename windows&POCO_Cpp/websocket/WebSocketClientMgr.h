#ifndef WEB_SOCKET_CLIENT_MGR_H
#define WEB_SOCKET_CLIENT_MGR_H
#include "stdafx.h"
#include "WebSocketClient.h"

class CWebSocketClientMgr : public Runnable
{
public:
	CWebSocketClientMgr();
	virtual ~CWebSocketClientMgr();

public:

	//添加新的客户端
	void AddClient(CWebSocketClient* pClient);

	//删除已有客户端
	void RemoveClient(CWebSocketClient* pClient);

public:

	//启动发送线程
	void Start();

	//停止发送线程
	void Stop();

	//添加待发送消息
	void PushMsg(const Object& o);

private:

	virtual void run();

private:

	//同步对象
	Mutex   m_mutex;

	//客户端列表
	list<CWebSocketClient*>	m_lstClients;//客户端列表
	
	//消息队列，此信息需要随发随删
	queue<string>	m_msgQueue;

	//用于发送状态报告数据
	Thread  m_thread;
	bool m_bExit;
};

extern CWebSocketClientMgr theClientMgr;
#define WSM theClientMgr

#endif
