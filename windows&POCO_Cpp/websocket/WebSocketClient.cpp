#include "stdafx.h"
#include "Common.h"
#include "WebSocketClientMgr.h"
//#include "JsonHandler.h"
#include "WebSocketClient.h"

__inline bool Close(const int nFlag)
{
	return ((nFlag & 0x08) >> 3) == 1;
}
__inline bool Fin(const int nFlag)
{
	return ((nFlag & 0x80) >> 7) == 1;
}
__inline bool Ping(const int nFlag)
{
	return (nFlag & 0x09) == 0x09;
}
__inline bool Pong(const int nFlag)
{
	return (nFlag & 0x0A) == 0x0A;
}

CWebSocketClient::CWebSocketClient(WebSocket& sock):
m_sock(sock),
m_bExist(false)
{
	m_strAddress = m_sock.peerAddress().toString();
	//m_spUser = make_unique<CUser>(this);
}

CWebSocketClient::~CWebSocketClient()
{
	m_sock.close();
}

//循环接收WS客户端报文
void CWebSocketClient::RecvLoop()
{
	//注：POCO的WS服务器对大容量数据包的封装不是很好
	//暂时使用大容量内容空间来接收数据
	//火狐是发送的一次性数据，不管数据多么长
	//谷歌是发送的可连接数据，可以分多次接收
	//为了统一，设置最大接收缓冲区为12M，可以接收1000万长度的请求。
	const int nSize = 12 * 1024 * 1024;
	auto pBuff = make_unique<char[]>(nSize);
	char* pBuf = pBuff.get();

	string strMsg;
	int nLen = 0;
	int nFlag = 0;

	while (!m_bExist)
	{
		try
		{
			strMsg.clear();
			while (true)
			{
				//发送心跳
				//LG.Echo();
				if (!m_sock.poll(1000, Poco::Net::Socket::SELECT_READ))
				{
					continue;
				}

				int nFlag = 0;
				memset(pBuf, 0, nSize);
				nLen = m_sock.receiveFrame(pBuf, nSize, nFlag);

				if (::Close(nFlag) || nLen == 0)
				{//关闭连接
					m_bExist = true;
					break;
				}
				if (Ping(nFlag) || Pong(nFlag))
				{
					continue;
				}

				strMsg += pBuf;

				if (Fin(nFlag) && nLen > 0)
				{
					//接收到一个完整帧，对数据进行处理
					HandleMsg(strMsg);
					break;
				}
			}
		}
		catch (const TimeoutException&ex)
		{
			//poco_error_f1(LOG, "timeout:%s",ex.displayText());
			continue;
		}
		catch (const Exception&ex)
		{
			//poco_error_f1(LOG, "error:%s",ex.displayText());
			m_bExist = true;
		}
	}

	theClientMgr.RemoveClient(this);
}

void CWebSocketClient::SendMsg(const string& strMsg)
{
	if (!m_bExist)
	{
		m_sock.sendFrame(strMsg.data(), strMsg.length());
	}
}

//解析客户端发来的JSON报文，并交给User的逻辑处理流程
void CWebSocketClient::HandleMsg(const string& strMsg)
{
//	CJsonHandler  jsHandler;

	try
	{

		//接收到的请求
#ifdef JSON_RELY_ALL_TEST
		ILOG(strMsg);
#endif
		/*
		//处理
		jsHandler.Handle(strMsg);

		CSR strResult = jsHandler.GetResult();

		SendMsg(strResult);

		//发送的响应
#ifdef JSON_RELY_ALL_TEST
		if (LSR_NoiseCheckBind != jsHandler.GetMsgID()
				&& LSR_SetTaskVolume != jsHandler.GetMsgID()
				&& LSR_NotifyTaskInterval != jsHandler.GetMsgID())
		{
			ILOG(strResult);
		}
#endif
		*/
	}
	catch (const Exception& e)
	{
		//poco_error(LOG, e.displayText());
		return;
	}
}

void CWebSocketClient::Close()
{
	m_sock.close();
}
