#include "stdafx.h"
#include "WebSocketClientMgr.h"

CWebSocketClientMgr theClientMgr;

CWebSocketClientMgr::CWebSocketClientMgr() :
m_thread("CWebSocketClientMgr"),
m_bExit(false)
{
}

CWebSocketClientMgr::~CWebSocketClientMgr()
{
}

void CWebSocketClientMgr::AddClient(CWebSocketClient* pClient)
{
	Mutex::ScopedLock oLocker(m_mutex);
	m_lstClients.push_back(pClient);
}

void CWebSocketClientMgr::RemoveClient(CWebSocketClient* pClient)
{
	Mutex::ScopedLock oLocker(m_mutex);
	m_lstClients.remove(pClient);
}

void CWebSocketClientMgr::Start()
{
	m_thread.start(*this);
}

void CWebSocketClientMgr::run()
{
	vector<CWebSocketClient*>		vecRemove;

	while (true)
	{
		while (true)
		{
			Mutex::ScopedLock oLocker(m_mutex);
			if (m_msgQueue.empty())
			{
				break;
			}
			const string& strMsg = m_msgQueue.front();
			for (const auto elem : m_lstClients)
			{
				try
				{
					elem->SendMsg(strMsg);
				}
				catch (const exception&)
				{
					vecRemove.push_back(elem);
				}
			}
			m_msgQueue.pop();

			for (const auto ele : vecRemove)
			{
				RemoveClient(ele);
			}
			vecRemove.clear();
		}
		
		if (m_bExit)
		{
			break;
		}

		Sleep(200);
	}
}

void CWebSocketClientMgr::PushMsg(const Object& o)
{
	string strMsg = Var(o).toString();
	Mutex::ScopedLock oLocker(m_mutex);
	m_msgQueue.push(strMsg);
}

void CWebSocketClientMgr::Stop()
{
	m_bExit = true;
	m_thread.join();
	Mutex::ScopedLock oLocker(m_mutex);
	for (const auto& e : m_lstClients)
	{
		e->Close();
	}
	m_lstClients.clear();
}
