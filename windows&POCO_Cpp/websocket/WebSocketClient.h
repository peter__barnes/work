/*
*  File Name:WebSocketClient.h
*  Created on: 2019年5月24日
*  Author: chentr
*  description:  websocket客户端请求处理相关
*  Modify date: （最后修改时间）(可选项)
* 	Modifier Author:(可选项)
*  description :用于详细说明此程序文件完成的主要功能 (可选项)
* 业务操作等等(可选项)
*/

#ifndef WEB_SOCKET_CLIENT_H
#define WEB_SOCKET_CLIENT_H

//class CUser;

class CWebSocketClient
{
public:

	CWebSocketClient(WebSocket& sock);
	virtual ~CWebSocketClient();

public:
	void SendMsg(const string& strMsg);
	void HandleMsg(const string& strMsg);
	void RecvLoop();
	void Close();

private:

	//用于发送数据的套接字
	WebSocket&	m_sock;

	//客户端ip:port
	string	m_strAddress;

	//是否退出
	bool	m_bExist;

	//用户
	//unique_ptr<CUser>	m_spUser;
};
#endif
