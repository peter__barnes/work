#ifndef XSPINLOCK_H_INCLUDED
#define XSPINLOCK_H_INCLUDED

#include "stdafx.h"
#include <atomic>
#include <thread>
class XSpinLock
{
public:
	XSpinLock(bool bRecursive = false);
	XSpinLock(const XSpinLock&) = delete;
	XSpinLock(const XSpinLock&&) = delete;
	XSpinLock& operator=(const XSpinLock&) = delete;
	XSpinLock& operator=(const XSpinLock&&) = delete;

public:

	//上锁
	void Lock();

	//开锁
	void Unlock();

public:
	class XScopedLock
	{
	public:
		XScopedLock(XSpinLock& xSpinLock);
		~XScopedLock();
	private:
		XSpinLock&	_xSpinLock;
	};
private:

	//锁
    std::atomic<long>	m_slock;
    //std::atomic_long m_slock;

	//递归标志
	bool m_bRecursive;

	//当前线程ID
    std::thread::id m_dwTid;

	//当前锁引用计数，用于递归
    std::atomic<long>	m_nRef;
    //std::atomic_long m_nRef;
};
//为类声明自旋锁成员变量
#define MSL_(name)		XSpinLock  name
#define MSL				MSL_(m_xSpinLock)

//声明局部锁
#define SSL_(lockName)	XSpinLock::XScopedLock ssl(lockName)
#define SSL				SSL_(m_xSpinLock)

#endif // XSPINLOCK_H_INCLUDED
