#include "stdafx.h"
#include<iostream>
#include<string>
#include<vector>
#include<thread>
#include<sstream>
#include<iomanip>
#include <Poco/String.h>
#include "Poco/Event.h"
#include "Poco/Thread.h"
#include "Utility.h"

#include <Iphlpapi.h>
#pragma comment(lib,"ws2_32")
#pragma comment(lib,"Iphlpapi")

#define IP_BYTE_LEN 4
using Poco::Event;
using Poco::Thread;
using Poco::Runnable;
using Poco::trim;
using Poco::trimLeft;
using Poco::trimRight;
using Poco::trimRightInPlace;
using namespace std;
void SplitString(const string& s, vector<string>& v, const string& c)
{
	string::size_type sPos1, sPos2;
	sPos2 = s.find(c);
	sPos1 = 0;
	while (string::npos != sPos2)
	{
		v.push_back(s.substr(sPos1, sPos2 - sPos1));

		sPos1 = sPos2 + c.size();
		sPos2 = s.find(c, sPos1);
	}
	if (sPos1 != s.length())
		v.push_back(s.substr(sPos1));
}
static int GetBit(const BYTE nByte, const int nIndex, const int nLen = 1)
{
	if (nIndex > 7 || nIndex < 0 || (nIndex + nLen > 8))
	{
		_asm int 3
	}
	BYTE nData = nByte >> nIndex;

	int nAnd = 0;
	for (size_t i = 0; i < nLen; i++)
	{
		nAnd += 1 << i;
	}

	return nAnd & nData;
}

std::string binToHexStr(const unsigned char *data, size_t size, char sep)
{
	std::ostringstream strHex;
	strHex << std::hex << std::setfill('0');
	for (size_t i = 0; i < size; ++i)
	{
		strHex << std::setw(2) << static_cast<unsigned int>(data[i]);
		if (i != size - 1)
		{
			strHex << sep;
		}
	}
	return strHex.str();
}
string MacHex2Str(string MacHex)
{
	return binToHexStr((const unsigned char *)MacHex.c_str(), MacHex.size(), '-');
}
string MacStr2Hex(string MacHexStr)
{
	vector<string> vsMacFieldHex;
	string sMacHex;
	SplitString(MacHexStr, vsMacFieldHex, "-");
	for (int i = 0; i < vsMacFieldHex.size(); i++)
	{
		//cout << MacFieldHex[i] << endl;
		char tmp = strtol(vsMacFieldHex[i].c_str(), NULL, 16);
		sMacHex.push_back(tmp);
	}
	return sMacHex;
}
string IpHex2Str(string IpHex)
{
	string ret_sIp;
	if (IpHex.size() != IP_BYTE_LEN)
	{
		//poco_error_f1(LOG, "IpHex.size()=%d", IpHex.size());
		return "";
	}
	for (int i = 0; i < IP_BYTE_LEN; i++)
	{
		ret_sIp += to_string((unsigned char)IpHex[i]);
		if (i != (IP_BYTE_LEN - 1))
		{
			ret_sIp += ".";
		}
	}
	return ret_sIp;
}
vector<string> stringSplit(const string& strSrc, const char delimiter, bool bExcludeEmpty /* = true */)
{
	vector<string> vecResult;
	stringstream ss(strSrc);
	string strToken;
	while (getline(ss, strToken, delimiter))
	{
		if (strToken.empty() && bExcludeEmpty)
		{
			continue;
		}
		vecResult.push_back(strToken);
	}
	return vecResult;
}
int byte2int(int b)
{
	return (b >= 0) ? (b) : (256 + b);
}
//ANSI转成UTF16LE
wstring A2U16(const char* szAnsi)
{
	int nBuffLen = MultiByteToWideChar(CP_ACP, 0, szAnsi, -1, NULL, 0);
	wstring strU16LE(nBuffLen - 1, 0);

	wchar_t* pBuf = const_cast<wchar_t*>(strU16LE.c_str());

	MultiByteToWideChar(CP_ACP, 0, szAnsi, -1, pBuf, nBuffLen);

	return strU16LE;
}
//ANSI转成UTF16LE
wstring A2U16(const string& strAnsi)
{
	return A2U16(strAnsi.c_str());
}
//UTF16LE转成ANSI
string U162A(const wchar_t* szU16LE)
{
	int nBuffLen = WideCharToMultiByte(CP_ACP, 0, szU16LE, -1, NULL, 0, NULL, NULL);

	string  strAnsi(nBuffLen - 1, 0);
	char* pBuf = const_cast<char*>(strAnsi.c_str());

	WideCharToMultiByte(CP_ACP, 0, szU16LE, -1, pBuf, nBuffLen, NULL, NULL);

	return strAnsi;
}
//UTF16LE转成ANSI
string U162A(const wstring& strU16LE)
{
	return U162A(strU16LE.c_str());
}
//UTF16LE转成UTF8
string U162U8(const wchar_t* szU16LE)
{
	int nBuffLen = WideCharToMultiByte(CP_UTF8, 0, szU16LE, -1, NULL, 0, NULL, NULL);

	string strU8(nBuffLen - 1, 0);
	char* pBuf = const_cast<char*>(strU8.c_str());

	WideCharToMultiByte(CP_UTF8, 0, szU16LE, -1, pBuf, nBuffLen, NULL, NULL);

	return strU8;
}
//UTF16LE转成UTF8
string U162U8(const wstring& strU16LE)
{
	return U162U8(strU16LE.c_str());
}
//UTF8转成UTF16LE
wstring U82U16(const char* szU8)
{
	int nBuffLen = MultiByteToWideChar(CP_UTF8, 0, szU8, -1, NULL, NULL);

	wstring strU16LE(nBuffLen - 1, nBuffLen);
	wchar_t* pBuf = const_cast<wchar_t*>(strU16LE.c_str());;

	MultiByteToWideChar(CP_UTF8, 0, szU8, -1, pBuf, nBuffLen);

	return strU16LE;
}
//UTF8转成UTF16LE
wstring U82U16(const string& strU8)
{
	return U82U16(strU8.c_str());
}
//ANSI转成UTF8
string A2U8(const char* szAnsi)
{
	wstring strU16LE = A2U16(szAnsi);
	return U162U8(strU16LE.c_str());
}
//ANSI转成UTF8
string A2U8(const string& strAnsi)
{
	return A2U8(strAnsi.c_str());
}
//UTF8转成ANSI
string U82A(const char* szU8)
{
	wstring  strU16LE = U82U16(szU8);
	return U162A(strU16LE.c_str());
}

//UTF8转成ANSI
string U82A(const string& strU8)
{
	return U82A(strU8.c_str());
}
	void ICMPOff(const unique_ptr<MulticastSocket>& upSock)
	{
#define	SIO_UDP_CONNRESET	_WSAIOW(IOC_VENDOR, 12)

		if (!upSock)
		{
			return;
		}

		SOCKET s = upSock->impl()->sockfd();

		DWORD dwRet = 0;
		bool bIcmpOff = false;
		WSAIoctl(s, SIO_UDP_CONNRESET, &bIcmpOff, sizeof(bIcmpOff), nullptr, 0, &dwRet, nullptr, nullptr);

#undef SIO_UDP_CONNRESET
	}
	void ICMPOff(const unique_ptr<DatagramSocket>& upSock)
	{
#define	SIO_UDP_CONNRESET	_WSAIOW(IOC_VENDOR, 12)

		if (!upSock)
		{
			return;
		}
		SOCKET s = upSock->impl()->sockfd();

		DWORD dwRet = 0;
		bool bIcmpOff = false;
		WSAIoctl(s, SIO_UDP_CONNRESET, &bIcmpOff, sizeof(bIcmpOff), nullptr, 0, &dwRet, nullptr, nullptr);

#undef SIO_UDP_CONNRESET
	}
	Int64 SystemRunSec()
	{
		return GetTickCount64() / 1000;
	}
	//获取IP对应的MAC
	string IP2MAC(const string& strIP)
	{
		unsigned   char   pMac[6];
		ULONG   MacLen = 6;
		ULONG   DestIP = inet_addr(strIP.c_str());
		int rs = SendARP(DestIP, (ULONG)NULL, (PVOID)pMac, (PULONG)&MacLen);
		string MACs = "";
		if (rs == 0 && MacLen == 6)
		{
			char  pBuf[32];
			sprintf_s(pBuf, "%02X-%02X-%02X-%02X-%02X-%02X",
				(unsigned int)pMac[0],
				(unsigned int)pMac[1],
				(unsigned int)pMac[2],
				(unsigned int)pMac[3],
				(unsigned int)pMac[4],
				(unsigned int)pMac[5]);
			MACs = pBuf;
		}
		return(MACs);
	}
	unsigned short CRC16_XMODEM(char *pData, int nLen)
	{
		unsigned short wCRCin = 0x0000;
		unsigned short wCPoly = 0x1021;
		unsigned char wChar = 0;
		while (nLen--)
		{
			wChar = *(pData++);
			wCRCin ^= (wChar << 8);
			for (int i = 0; i < 8; i++)
			{
				if (wCRCin & 0x8000)
					wCRCin = (wCRCin << 1) ^ wCPoly;
				else
					wCRCin = wCRCin << 1;
			}
		}
		return (wCRCin);
	}







int _tmain(int argc, _TCHAR* argv[])
{
	system("pause");
	return 0;
}

