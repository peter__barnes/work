#pragma once

//辅助函数
namespace U
{
	template<typename C = vector<string> >
	C stringSplit(CSR strSrc, const char delimiter, bool bExcludeEmpty = true);
	vector<int> stringToIntVec(const string& strSrc, const char delimiter = ',');
	//编码转换
	wstring A2U16(const char* szAnsi);
	wstring A2U16(CSR strAnsi);

	string U162A(const wchar_t* szU16LE);
	string U162A(const wstring& strU16LE);
	wstring U82U16(const char* szU8);
	wstring U82U16(CSR strU8);

	string U162U8(const wchar_t* szU16LE);
	string U162U8(const wstring& strU16LE);
    
	string A2U8(const char* szAnsi);
	string A2U8(CSR strAnsi);

	string U82A(const char* szU8);
	string U82A(CSR strU8);
	//禁用ICMP 端口不可达
	void ICMPOff(const unique_ptr<MulticastSocket>& upSock);
	void ICMPOff(const unique_ptr<DatagramSocket>& upSock);

	//获取系统运行时长，单位秒
	Int64 SystemRunSec();
#define SRC SystemRunSec()
	string IP2MAC(const string& strIP);
	unsigned short CRC16_XMODEM(char *pData, int nLen);

    template <int n>
	vector<int> StringToIntVector(const string& str, const char c = ',')
	{
		vector<int>  arr = { 0 };
		stringstream ss(str);
		string strToken;
		int i = 0;
		while (getline(ss, strToken, c))
		{
			if (strToken.empty())
			{
				continue;
			}
			arr.push_back(stoi(strToken));
			//arr[i++] = stoi(strToken);
		}
		return arr;
	}
	template <int n>
	array<int, n> StringToIntArray(const string& str, const char c = ',')
	{
		array<int, n>  arr = { 0 };
		stringstream ss(str);
		string strToken;

		int i = 0;
		while (getline(ss, strToken, c))
		{
			if (strToken.empty())
			{
				continue;
			}
			arr[i++] = stoi(strToken);
			if (i == n)
			{
				return arr;
			}
		}
		return arr;
	}
}
