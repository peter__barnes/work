//此文件可单独使用，覆盖掉VS新工程即可
#include "stdafx.h"
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <WS2tcpip.h>
#include <cstdlib>
#include "minwindef.h"
#include<iostream>
#include <sstream>
#include <vector>
#include <array>
//#include "Poco/Foundation.h"
#pragma comment(lib,"ws2_32.lib")	
using namespace std;
//初始化winsock，获取udp sokcet
int getUdpSocket()
{
	WORD nVer = MAKEWORD(2, 2);
	WSADATA oLpData;
	int nErr = WSAStartup(nVer, &oLpData);
	if (nErr != 0)
		return -1;
	int nUdpSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (nUdpSocket == INVALID_SOCKET)
		return -2;
	return nUdpSocket;
}

//获取指定地址信息数据结构
sockaddr_in getAddr(char *ip, int port)
{
	sockaddr_in oAddr;
	oAddr.sin_family = AF_INET;
	oAddr.sin_port = htons(port);
	oAddr.sin_addr.S_un.S_addr = inet_addr(ip);
	return oAddr;
}
//组装请求下载报文
char *RequestDownloadPack(const char *pContent, int &nDatalen)
{
	int nLen = strlen(pContent);
	char *pBuf = new char[nLen + 2 + 2 + 5];
	pBuf[0] = 0x00;
	pBuf[1] = 0x01;//操作码
	memcpy(pBuf + 2, pContent, nLen);
	memcpy(pBuf + 2 + nLen, "\0", 1);
	memcpy(pBuf + 2 + nLen + 1, "octet", 5);
	memcpy(pBuf + 2 + nLen + 1 + 5, "\0", 1);
	nDatalen = nLen + 2 + 2 + 5;
	return pBuf;
}
//组装请求上传报文
char *RequestUploadPack(const char *pContent, int &nDatalen)
{
	int nLen = strlen(pContent);
	char *pBuf = new char[nLen + 2 + 2 + 5];
	pBuf[0] = 0x00;
	pBuf[1] = 0x02;//操作码
	memcpy(pBuf + 2, pContent, nLen);
	memcpy(pBuf + 2 + nLen, "\0", 1);
	memcpy(pBuf + 2 + nLen + 1, "octet", 5);
	memcpy(pBuf + 2 + nLen + 1 + 5, "\0", 1);
	nDatalen = nLen + 2 + 2 + 5;
	return pBuf;
}
void TftpUpload(string sServIp, int nServPort, string sFileName)
{
	SOCKET oSock = getUdpSocket();
	sockaddr_in oAddr = getAddr((char *)sServIp.c_str(), nServPort);//TFTP服务器地址
	int nDatalen;
	char *pSendData = RequestUploadPack((char *)sFileName.c_str(), nDatalen);//请求下载123.flac文件

	int nRes = sendto(oSock, pSendData, nDatalen, 0, (sockaddr*)&oAddr, sizeof(oAddr));
	if (nRes != nDatalen)
	{
		std::cout << "sendto failed" << std::endl;
		return;
	}
	delete[]pSendData;

	char pAck[4];
	sockaddr_in server;
	int nLen = sizeof(server);
	int nAcklen = recvfrom(oSock, pAck, 4, 0, (sockaddr*)&server, &nLen);
	if (nAcklen != 4 || pAck[1] != 4)
	{
		cout << "error!ack 1-3:" << (int)pAck[1] << "," << (int)pAck[2] << "," << (int)pAck[3] << "," << endl;
		return;
	}
	FILE *fFile;
	errno_t nErr;
	nErr = fopen_s(&fFile, (char *)sFileName.c_str(), "rb");
	//FILE *fFile = fopen((char *)sFileName.c_str(), "rb");//本地保存为123.flac
	if (fFile == NULL)
	{
		std::cout << "File open failed!" << std::endl;
		return;
	}
	//!试用，此处大文件可能超出一个char
	WORD nPackNum = 1;
	for (; true; nPackNum++)
	{
		char pBuf[512 + 4];
		pBuf[0] = 0x00;
		pBuf[1] = 0x03;//操作码
		pBuf[2] = HIBYTE(LOWORD(nPackNum));
		pBuf[3] = LOBYTE(LOWORD(nPackNum));
		nRes = fread(pBuf + 4, 1, sizeof(pBuf) - 4, fFile);
		if (nRes <= 0)
		{
			cout << "file end or error!" << endl;
			return;
		}
		//发送请求下载到服务器69端口后，服务端会随机找一个端口来发送数据给客户端，客户端返回确认报文时，不能再向69端口发送ACK，而是向该端口发送ACK，所以需要保存新的地址信息server，用该地址信息来进行数据传输
		if (nRes > 0)
		{
			int nSendlen = sendto(oSock, pBuf, nRes + 4, 0, (sockaddr*)&server, sizeof(server));
			cout << "send:" << nSendlen << endl;

			//发送确认报文
			char pAck[4];
			sockaddr_in server;
			nLen = sizeof(server);
			int nAcklen = recvfrom(oSock, pAck, 4, 0, (sockaddr*)&server, &nLen);
			cout << "ack 1-3:" << (int)pAck[1] << "," << (int)pAck[2] << "," << (int)pAck[3] << "," << endl;
			if (nAcklen == 4 &&
				(0x04 != pAck[1] || pAck[3] != pBuf[3]))
			{
				cout << "send again" << endl;
				//重传
				nSendlen = sendto(oSock, pBuf, nRes + 4, 0, (sockaddr*)&server, sizeof(server));
			}
		}
	}
	fclose(fFile);
}

void TftpDownload(string sServIp, int nServPort, string sFileName)
{
	SOCKET oSock = getUdpSocket();
	sockaddr_in oAddr = getAddr((char *)sServIp.c_str(), nServPort);//TFTP服务器地址
	int nDatalen;
	char *pSendData = RequestDownloadPack((char *)sFileName.c_str(), nDatalen);//请求下载123.flac文件

	int nRes = sendto(oSock, pSendData, nDatalen, 0, (sockaddr*)&oAddr, sizeof(oAddr));
	if (nRes != nDatalen)
	{
		std::cout << "sendto failed" << std::endl;
		return;
	}
	delete[]pSendData;

	FILE *fFile;
	errno_t nErr;
	nErr = fopen_s(&fFile, (char *)sFileName.c_str(), "wb");
	//FILE *fFile = fopen((char *)sFileName.c_str(), "wb");//本地保存为sFileName
	if (fFile == NULL)
	{
		std::cout << "File open failed!" << std::endl;
		return;
	}
	while (true)
	{
		char pBuf[1024];
		sockaddr_in server;
		int nLen = sizeof(server);
		nRes = recvfrom(oSock, pBuf, 1024, 0, (sockaddr*)&server, &nLen);
		//发送请求下载到服务器69端口后，服务端会随机找一个端口来发送数据给客户端，客户端返回确认报文时，不能再向69端口发送ACK，而是向该端口发送ACK，所以需要保存新的地址信息server，用该地址信息来进行数据传输
		if (nRes > 0)
		{
			short nFlag;
			memcpy(&nFlag, pBuf, 2);
			nFlag = ntohs(nFlag);//网络字节序转换为主机字节序，很重要
			if (nFlag == 3)
			{
				short nNo;
				memcpy(&nNo, pBuf + 2, 2);
				fwrite(pBuf + 4, nRes - 4, 1, fFile);
				if (nRes > 0 && nRes < 512)
				{
					std::cout << "download finished!" << std::endl;
					break;
				}
				std::cout << "Pack No=" << ntohs(nNo) << std::endl;
				//发送确认报文
				char pAck[4];
				pAck[0] = 0x00;
				pAck[1] = 0x04;
				memcpy(pAck + 2, &nNo, 2);
				int nSendlen = sendto(oSock, pAck, 4, 0, (sockaddr*)&server, sizeof(server));
				if (nSendlen != 4)
				{
					std::cout << "ack error" << WSAGetLastError() << std::endl;
					break;
				}
			}
			if (nFlag == 5)
			{
				//服务端返回了错误信息
				short nErrorCode;
				memcpy(&nErrorCode, pBuf + 2, 2);
				nErrorCode = ntohs(nErrorCode);
				char strError[1024];
				int nIter = 0;
				while ((*pBuf + nIter + 4) != 0)
				{
					memcpy(strError + nIter, pBuf + nIter + 4, 1);
					++nIter;
				}
				std::cout << "Error " << nErrorCode << "  " << strError << std::endl;
				break;
			}
		}
	}
	fclose(fFile);
}
int _tmain(int argc, _TCHAR* argv[])
{
	//TftpDownload("172.16.21.31", 69, "222.txt");
	TftpUpload("172.16.21.5", 69, "333.txt");
	while (true){ getchar(); }
	return 0;
}
