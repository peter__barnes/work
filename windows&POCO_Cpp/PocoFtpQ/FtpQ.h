#pragma once
//#include "UploadQ.h"
#include <string>
#include <Poco/String.h>
#include "Poco/Event.h"
#include "Poco/Mutex.h"
#include "Poco/AutoPtr.h"
#include "Poco/SharedPtr.h"
#include "Poco/Thread.h"
#include "Poco/ThreadPool.h"
#include "Poco/Runnable.h"
#include <memory>
using std::unique_ptr;
using std::make_unique;
using std::shared_ptr;
using std::make_shared;
using Poco::Runnable;
using Poco::Event;
using Poco::Thread;
using Poco::ThreadPool;
using Poco::AutoPtr;
using Poco::SharedPtr;
using Poco::Mutex;
using std::string;
using std::to_string;
using std::wstring;
//POCO FTP
#include "Poco/Net/FTPClientSession.h"
using Poco::Net::FTPClientSession;
#include "Poco/Net/NetException.h"
using Poco::TimeoutException;
using Poco::Exception;

#include <exception>
using std::exception;

#include <vector>
using std::vector;

#include <array>
using std::array;

#include <list>
using std::list;
#include<istream>
using std::istream;
#include<ostream>
using std::ostream;
#include<fstream>
using std::fstream;
class CUploadQ : public Runnable
{
public:
	CUploadQ();
	virtual ~CUploadQ();
public:

	//启动
	virtual void Start();

	//停止
	virtual void Stop();
	//线程函数
	virtual void run() override;

protected:

	bool m_bRunning;
	Thread	m_thread;
	Event	m_event;
};
class CFtpUri
{
public:

	CFtpUri(const string& strLocalFile, const string& strServerFile);

	//更新上传状态
	void UpdateStatus(const int nSize);

	//删除
	bool CFtpUri::Remove(const string& strServer, string sFileName);
	//下载
	bool CFtpUri::Download(const string& strServer);
	//上传
	bool Upload(const string& strServer);

private:

	//本机文件[绝对路径]
	string  m_strLocalFile;

	//服务器文件[/xxx/yyy]
	string  m_strServerFile;

	//文件总大小(B)
	__int64 m_nTotal;

	//当前上传大小(B)
	__int64 m_nUpload;
};

class CFtpQ : public CUploadQ
{
public:

	CFtpQ(const string& strServer);
	virtual ~CFtpQ();

public:

	virtual void Start() override;
	virtual void Stop() override;
	virtual void run() override;

	//添加
	void Push(const shared_ptr<CFtpUri>& spUri);

	//取消
	void Cancel(const wstring& wsLocalFile);

	//取消所有
	void CancelAll();

private:

	//FTP服务器IP
	string  m_strServer;

	//待上传资源
	list<shared_ptr<CFtpUri>>  m_lstUri;
	Mutex	m_mutex;
};
