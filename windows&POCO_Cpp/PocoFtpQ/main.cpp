#include "stdafx.h"
#include<iostream>
#include<thread>
#include <Poco/String.h>
#include "Poco/Event.h"
#include "Poco/Thread.h"
#include "FtpQ.h"
using Poco::Event;
using Poco::Thread;
using Poco::Runnable;
using Poco::trim;
using Poco::trimLeft;
using Poco::trimRight;
using Poco::trimRightInPlace;
using namespace std;
//UTF16LE转成UTF8
string U162U8(const wchar_t* szU16LE)
{
	int nBuffLen = WideCharToMultiByte(CP_UTF8, 0, szU16LE, -1, NULL, 0, NULL, NULL);

	string strU8(nBuffLen - 1, 0);
	char* pBuf = const_cast<char*>(strU8.c_str());

	WideCharToMultiByte(CP_UTF8, 0, szU16LE, -1, pBuf, nBuffLen, NULL, NULL);

	return strU8;
}
//ANSI转成UTF16LE
wstring A2U16(const char* szAnsi)
{
	int nBuffLen = MultiByteToWideChar(CP_ACP, 0, szAnsi, -1, NULL, 0);

	wstring strU16LE(nBuffLen - 1, 0);

	wchar_t* pBuf = const_cast<wchar_t*>(strU16LE.c_str());

	MultiByteToWideChar(CP_ACP, 0, szAnsi, -1, pBuf, nBuffLen);

	return strU16LE;
}
//ANSI转成UTF8
string A2U8(const char* szAnsi)
{
	wstring strU16LE = A2U16(szAnsi);
	return U162U8(strU16LE.c_str());
}
string A2U8(const string& strAnsi)
{
	return A2U8(strAnsi.c_str());
}
int _tmain(int argc, _TCHAR* argv[])
{
	string sFtpServIp = "127.0.0.1";
	CFtpQ q(sFtpServIp);
	cout << "FtpQ created"<< endl;
	vector<string> vL = {
		"E:\\LongMusic\\Dura1.mp3",
		"E:\\LongMusic\\song.mp3",
	};
	vector<string> vS = {
			"Dura.mp3",
			"song.mp3",
	};
	for (int i = 0; i < 2; i++)
	{
		//A2U8用于中文转换，但目前内部没按处理中午的模式写（实际解析中午会有问题，需修改）
		q.Push(make_shared<CFtpUri>(A2U8(vL[i]), vS[i]));
	}
	while (true){getchar();}
	return 0;
}
