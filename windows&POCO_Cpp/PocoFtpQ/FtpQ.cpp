#include "stdafx.h"

#include "FtpQ.h"
CUploadQ::CUploadQ() :
m_bRunning(false)
{
}
CUploadQ::~CUploadQ()
{
}

//启动
void CUploadQ::Start()
{

}
//停止
void CUploadQ::Stop()
{

}

//线程函数
void CUploadQ::run()
{

}
CFtpUri::CFtpUri(const string& strLocalFile, const string& strServerFile) :
m_strLocalFile(strLocalFile),
m_strServerFile(strServerFile),
m_nUpload(0)
{
	//todo 获取文件大小
	m_nTotal = 0;
}
bool CFtpUri::Remove(const string& strServer,string sFileName)
{
	Sleep(1500);
	char szBuf[2048] = { 0 };
	try
	{
		FTPClientSession ftp(strServer);

		ftp.login("anonymous", "anonymous@example.com");
		ftp.setPassive(true, false);

		ftp.setWorkingDirectory("ALERT");
		ftp.remove(sFileName);
	}
	catch (const Exception& e)
	{
		string s = e.displayText();
		return false;
	}
}
bool CFtpUri::Download(const string& strServer)
{
	Sleep(1500);
	char szBuf[2048] = { 0 };
	try
	{
		FTPClientSession ftp(strServer);

		ftp.login("anonymous", "anonymous@example.com");
		ftp.setPassive(true, false);

		ftp.setWorkingDirectory("ALERT");
		istream& in = ftp.beginDownload(m_strServerFile);
		//ftp.beginDownload();

		//wstring wsLocalFile = U82U16(m_strLocalFile);
		std::ofstream  out(m_strLocalFile, std::ios_base::out | std::ios_base::binary);

		bool bOK = true;

		while (!in.eof())
		{
			in.read(szBuf, sizeof szBuf);
			int len = (int)in.gcount();
			out.write(szBuf, len);//这里有时候会接收到服务器发来的RST
			if (!out.good())
			{//出错判断
				bOK = false;
				break;
			}

			//更新下载进度
			UpdateStatus(len);
			Sleep(0);
			//任务被取消
			//if (m_bCancel)
			//{
			//	break;
			//}
		}
		if (bOK)
		{
			out.flush();
			if (!out.good())
			{//出错判断
				bOK = false;
			}
		}
		else
		{
			//m_status.ResetCurrentFileInfo();
		}

		ftp.endDownload();
		//in.close();
	}
	catch (const Exception& e)
	{
		string s = e.displayText();
		return false;
	}

	return true;
}
bool CFtpUri::Upload(const string& strServer)
{
	Sleep(1500);
	char szBuf[2048] = { 0 };
	try
	{
		FTPClientSession ftp(strServer);
		
		ftp.login("anonymous", "anonymous@example.com");
		ftp.setPassive(true, false);
		
		ftp.setWorkingDirectory("ALERT");
		ostream& out = ftp.beginUpload(m_strServerFile);
		std::ifstream  in(m_strLocalFile, std::ios_base::in | std::ios_base::binary);

		bool bOK = true;

		while (!in.eof())
		{
			in.read(szBuf, sizeof szBuf);
			int len = (int)in.gcount();
			out.write(szBuf, len);//这里有时候会接收到服务器发来的RST
			if (!out.good())
			{//出错判断
				bOK = false;
				break;
			}
			//更新下载进度
			UpdateStatus(len);

			Sleep(0);
			//任务被取消
			//if (m_bCancel)
			//{
			//	break;
			//}
		}

		if (bOK)
		{
			out.flush();
			if (!out.good())
			{//出错判断
				bOK = false;
			}
		}
		else
		{
			//m_status.ResetCurrentFileInfo();
		}
		ftp.endUpload();
		in.close();
	}
	catch (const Exception& e)
	{
		string s = e.displayText();
		return false;
	}

	return true;
}

void CFtpUri::UpdateStatus(const int nSize)
{

}

CFtpQ::CFtpQ(const string& strServer) :
m_strServer(strServer)
{
	Start();
}
CFtpQ::~CFtpQ()
{
}

void CFtpQ::Start()
{
	if (m_bRunning)
	{
		return;
	}

	m_bRunning = true;
	m_thread.start(*this);
	m_event.wait();
}

void CFtpQ::Stop()
{
	m_bRunning = false;
	m_thread.join();
}

void CFtpQ::run()
{
	m_event.set();

	while (m_bRunning)
	{
		shared_ptr<CFtpUri>	spUri;
		bool bEmpty = false;
		{
			Mutex::ScopedLock locker(m_mutex);
			if (!m_lstUri.empty())
			{
				spUri = m_lstUri.front();
				m_lstUri.pop_front();
			}
			else
			{
				bEmpty = true;
			}
		}
		if (bEmpty)
		{
			Sleep(1000);
		}

		if (spUri)
		{
			spUri->Upload(m_strServer);
			//spUri->Download(m_strServer);
			//spUri->Remove(m_strServer, "Dura.mp3");
		}
	}
}

void CFtpQ::Push(const shared_ptr<CFtpUri>& spUri)
{
	Mutex::ScopedLock locker(m_mutex);
	m_lstUri.push_back(spUri);
}

//取消
void CFtpQ::Cancel(const wstring& wsLocalFile)
{

}
//取消所有
void CFtpQ::CancelAll()
{

}

