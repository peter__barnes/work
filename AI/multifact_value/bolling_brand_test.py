import tushare as ts
import talib
import pandas as pd
import numpy as np
import tushare as ts
import xgboost as xgb
import mplfinance as mpf
import datetime


def set_token():
    ts.set_token('35000bb7b3c3efb44ccce92dadf9c6d35f39119d761ac8a367166296')
    return ts.pro_api()#接口，token可在tushare官网里登录个人账号获取

def download_data():
    pro = set_token()
    # 通过tushare获取股票信息
    df = pro.daily(ts_code='601888.SZ',start='2018-01-30',end='2018-10-30') #以股票代码[601888]中国国旅为例，提取从2018-01-12到2018-10-30的收盘价
    df.to_csv('F:/ProgramData/tushare/test/stock_basic.csv', index=False)

def get_stock(num):
    stock=num
    #设置查询时间————一个月
    today = datetime.datetime.today()
    startday=today+datetime.timedelta(days=-365)
    today = today.strftime('%Y%m%d')
    startday =startday.strftime('%Y%m%d')
    #获取股票数据
    stock_df = pro.daily(ts_code=stock, start_date=startday,end_date=today)
    #将trade_date转换为时间格式
    stock_df['trade_date'] = pd.to_datetime(stock_df['trade_date'])
    #倒序排列 iloc[::-1]
    stock_df=stock_df.iloc[::-1]
    #将列vol改为volume
    stock_df=stock_df.rename(columns={'vol':'volume'})
    #保存为csv文件，不保存索引
    stock_df.to_csv('%s.csv'%stock,index=False)
    #读取csv文件,将trade_date作为行索引
    stock_df=pd.read_csv('%s.csv'%stock,index_col=1)
    #将索引转为时间格式
    stock_df.index = pd.to_datetime(stock_df.index)
    return stock_df

def load_stock(num):
    stock=num
    #读取csv文件,将trade_date作为行索引
    stock_df=pd.read_csv('%s.csv'%stock,index_col=1)
    #将索引转为时间格式
    stock_df.index = pd.to_datetime(stock_df.index)
    return stock_df

if __name__ == '__main__':

    #pro = set_token()
    #stock_df = get_stock('000543.SZ')

    stock_df = load_stock('000543.SZ')

    #提取收盘价
    closed = stock_df['close'].values[:]
    # 获取均线的数据，通过timeperiod参数来分别获取 5,10,20 日均线的数据。
    ma5 = talib.SMA(closed, timeperiod=5)
    ma10 = talib.SMA(closed, timeperiod=10)
    ma20 = talib.SMA(closed, timeperiod=20)

    # 获取MACD数据
    # MACD
    macd, macdsignal, macdhist = talib.MACD(stock_df['close'], fastperiod=12, slowperiod=26, signalperiod=9)
    macd = pd.DataFrame(macd, columns=['0'])
    macdsignal = pd.DataFrame(macdsignal, columns=['0'])
    macdhist = pd.DataFrame(macdhist, columns=['0'])

    macd=pd.DataFrame(macd,columns=['0'])
    macdsignal=pd.DataFrame(macdsignal,columns=['0'])
    macdhist=pd.DataFrame(macdhist,columns=['0'])




    # 设置mplfinance的蜡烛颜色
    # up为阳线颜色
    # down为阴线颜色
    my_color = mpf.make_marketcolors(
        up='darkslateblue',
        down='limegreen',
        edge='inherit',
        wick='inherit',
        volume='inherit'
    )

    # 设置图形风格
    # figcolor:设置图表的背景色
    # y_on_right:设置y轴位置是否在右
    # gridaxis:设置网格线位置
    # gridstyle:设置网格线线型
    # gridcolor:设置网格线颜色
    my_style = mpf.make_mpf_style(
        marketcolors=my_color,
        figcolor='#EEEEEE',
        y_on_right=False,
        gridaxis='both',
        gridstyle='-.',
        gridcolor='#E1E1E1'
    )

    # 设置基本参数
    # type:绘制图形的类型,有candle, renko, ohlc, line等
    # 此处选择candle,即K线图
    # mav(moving average):均线类型,此处设置5,10,30日线
    # volume:布尔类型，设置是否显示成交量，默认False
    # title:设置标题
    # y_label:设置纵轴主标题
    # y_label_lower:设置成交量图一栏的标题
    # figratio:设置图形纵横比
    # figscale:设置图形尺寸(数值越大图像质量越高)
    # datetime_format:设置日期显示格式
    # xrotation:设置x坐标的转角度
    kwargs = dict(
        type='candle',
        mav=(5, 10, 30),
        volume=True,
        title='%s' % (stock_df.iloc[0, 0]),
        ylabel='Price',
        ylabel_lower='Volume',
        figratio=(600 / 72, 480 / 60),
        figscale=3,
        datetime_format='%Y-%m-%d',
        xrotation=0
    )

    # 设置配图
    add_plot = [
        mpf.make_addplot(macdhist.tail(60), type='bar', panel=2, ylabel='MACD', color='darkslateblue'),
        mpf.make_addplot(macd.tail(60), panel=2, color='orangered'),
        mpf.make_addplot(macdsignal.tail(60), panel=2, color='limegreen')
        #mpf.make_addplot(rsi.tail(60), panel=3, ylabel='RSI'),
        #mpf.make_addplot(slowk.tail(60), panel=4, color='darkslateblue', ylabel='KDJ'),
        #mpf.make_addplot(slowd.tail(60), panel=4, color='limegreen'),
        #mpf.make_addplot(slowj.tail(60), panel=4, color='orangered')
    ]

    mpf.plot(stock_df.tail(60), **kwargs, addplot=add_plot, style=my_style)