import tushare as ts
import os

DATA_PATH = 'F:/ProgramData/tushare/'

def set_token():
    ts.set_token('35000bb7b3c3efb44ccce92dadf9c6d35f39119d761ac8a367166296')
    return ts.pro_api()#接口，token可在tushare官网里登录个人账号获取

def get_daily(pro,code,start_date='19900101', end_date='20230801'):
    df = pro.query('daily', ts_code=code, start_date=start_date, end_date=end_date)
    filename = code + '_daily.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join(DATA_PATH,'daily/',filename),index=False) #将获取到的数据保存为csv文件
    #print(df.head(3))