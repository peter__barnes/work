# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

#def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    #print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
import os
import time
import pandas as pd
import numpy as np
import tushare as ts
import xgboost as xgb
from sklearn.preprocessing import Binarizer
#from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import *


year_scope = 10

#获取交易日信息
def print_trade_date(pro):
    df = pro.trade_cal(exchange='', start_date='20180901', end_date='20181001',
                fields='exchange,cal_date,is_open,pretrade_date', is_open='0')
    print(df)

#获取股票列表及其基础数据
def comany_info(pro):
    data=pro.stock_basic(exchage='',list_status='L',
                     fields='ts_code,symbol,name,area,industry,market,list_date,is_hs')
                         #获取TS代码，股票代码，股票名称，所在地域，所属行业，市场类型等信息

    data=data[data['market'].isin(['主板'])]
    #利用.isin()函数过滤数据，只保留科创板的股票，如要添加其他版块则.isin(['科创板'，'xxx'])

    data.to_csv(os.path.join('F:/ProgramData/tushare','stock_basic.csv'),index=False) #将获取到的数据保存为csv文件
    #os.path.join(路径，文件名)为保存路径拼接函数。注：事先创建Data文件夹
    print('获取到上市公司：',len(data))

#获取个股财务指标并保存入文件
def fina_ind(pro,code):
    print("code:%s"%code)
    print('df1')
    df1 = pro.query('fina_indicator', ts_code=code, start_date = '20100101',end_date='20200101')#period='20201231')
    time.sleep(3)
    print('df2')
    df2 = pro.query('fina_indicator', ts_code=code, start_date = '20000101',end_date='20100101')#period='20201231')
    time.sleep(10)
    print('df3')
    df3 = pro.query('fina_indicator', ts_code=code, start_date = '19900101',end_date='20000101')#period='20201231')
    time.sleep(10)
    print('concating')
    df = pd.concat([df1, df2, df3], axis=0)
    filename = code+'_fina_ind.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join('F:/ProgramData/tushare',filename),index=False) #将获取到的数据保存为csv文件
    print(df)

def get_monthly(pro,code):
    df = pro.query('monthly', ts_code=code, start_date='19900101', end_date='20200101')
    filename = code+'_monthly.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join('F:/ProgramData/tushare',filename),index=False) #将获取到的数据保存为csv文件
    print(df)

def set_token():
    ts.set_token('35000bb7b3c3efb44ccce92dadf9c6d35f39119d761ac8a367166296')
    return ts.pro_api()#接口，token可在tushare官网里登录个人账号获取

def monthly_to_yearly():
    for code in ts_code_all:
        filename = 'F:/ProgramData/tushare/' + code + '_monthly.csv'
        monthly = pd.read_csv(filename)
        #monthly.loc[:, 'year'] = 0
        monthly.insert(monthly.shape[1], 'year', value=0)
        #print(monthly)
        for index, row in monthly.iterrows():
            #print(index, row['trade_date'], row['year'])
            monthly.loc[index, 'year'] = int(row['trade_date']/10000)
        yearly = monthly.groupby('year')['close'].mean().reset_index()
        filename = 'F:/ProgramData/tushare/' + code + '_yearly.csv'
        print("saving:%s"%filename)
        yearly.to_csv(os.path.join('F:/ProgramData/tushare',filename),index=False) #将获取到的数据保存为csv文件

def merge_data_to_csv():
    # 读取全部公司的财务指标数据（1990-2020），并且只筛选年报，并在最后追加单独一列表示年份
    fina_list = []
    for code in ts_code_all:
        print('loading fina_ind:%s'%code)
        filename = 'F:/ProgramData/tushare/fs/' + code + '_fina_ind.csv'
        fina = pd.read_csv(filename)
        # fina_list = []
        fina.insert(fina.shape[1], 'year', value=0)
        for index, row in fina.iterrows():
            # print(index, row['trade_date'], row['year'])
            if (1231 != row['end_date'] % 10000):
                # print(index)
                # print(int(row['end_date']/10000))
                fina = fina.drop(index)
                # fina.loc[index, 'end_date'] = int(row['end_date']/10000)
                # fina_list.append(row)
            else:
                fina.loc[index, 'year'] = int(row['end_date'] / 10000)
            # 重新从0开始编号，否则后续与年线表合并数据会出现index对不上的问题
        fina.index = range(0, len(fina))
        # print(fina)
        fina_list.append(fina)
    fina_pd = pd.concat(fina_list, keys=ts_code_all)
    # print(fina_pd.loc['000002.SZ'])

    '''
    #读取全部公司的财务指标数据（1990-2020）
    fina_list = []
    #逐个公司读取数据
    for code in ts_code_all:
        print(code)
        filename = 'F:/ProgramData/tushare/' + code + '_fina_ind.csv'
        fina = pd.read_csv(filename)
        fina_list.append(fina)
    #数据合并
    fina_pd = pd.concat(fina_list, keys=ts_code_all)
    #测试单个数据调用
    #print('-'*40)
    #print(fina_pd.loc['000002.SZ'])
    '''
    # 读取全部公司的月线数据（1990-2020）
    yearly_list = []
    fina_list = []
    # 逐个公司读取数据
    for code in ts_code_all:
        print('loading yearly:%s'%code)
        filename = 'F:/ProgramData/tushare/yearly/' + code + '_yearly.csv'
        yearly = pd.read_csv(filename)
        yearly_list.append(yearly)
    # 数据合并
    yearly_pd = pd.concat(yearly_list, keys=ts_code_all)
    # 测试单个数据调用
    print('-' * 40)

    data_merge = []
    # print(yearly_pd.loc['000002.SZ'])
    for code in ts_code_all:
        print('merging:%s'%code)
        if((code not in yearly_pd.index)):
            continue
        yearly = yearly_pd.loc[code]
        # yearly = yearly_pd.loc['000002.SZ']
        yearly.insert(yearly.shape[1], 'eval', value=9)
        fina_single = fina_pd.loc[code]
        fina_single.insert(fina_single.shape[1], 'eval', value=9)
        fina_single.insert(fina_single.shape[1], 'PE', value=0)
        fina_single.insert(fina_single.shape[1], 'PB', value=0)
        fina_single.insert(fina_single.shape[1], 'PS', value=0)
        for index, row in yearly.iterrows():
            # 如果5年以后的数据也在合法范围内
            if ((index + year_scope) < yearly.shape[0]):
                # 确认year真的是相差5年（避免因数据不规则用错其他年份的数据）
                if ((yearly.loc[index + year_scope, 'year'] - row['year']) == year_scope):
                    # 确认数据有效，股价应当大于0
                    price_now = row['close']
                    price_5y_later = yearly.loc[index + year_scope, 'close']
                    #'eps', 'bps', 'revenue_ps',  # 此行用于后续计算市盈率、市净率、市销率等估值指标
                    fina_PE = price_now / fina_single.loc[index, 'eps']
                    fina_PB = price_now / fina_single.loc[index, 'bps']
                    fina_PS = price_now / fina_single.loc[index, 'revenue_ps']
                    fina_single = fina_single.copy()
                    fina_single.loc[index, 'PE'] = fina_PE
                    fina_single.loc[index, 'PB'] = fina_PB
                    fina_single.loc[index, 'PS'] = fina_PS
                    if (price_now > 0 and price_5y_later > 0):
                        # 先只分两类以便测试，只选出能在10年成长8倍的，(无效数据分类为9，需要抛弃)
                        if ((price_5y_later / price_now) > 8):
                            yearly.loc[index, 'eval'] = 1
                            fina_single.loc[index, 'eval'] = 1
                        else:
                            yearly.loc[index,'eval'] = 0
                            fina_single.loc[index,'eval'] = 0
                        '''
                        #复杂分类法（加上无效数据，一共分5类）
                        if(price_5y_later/price_now > 3.3):
                            yearly.loc[index,'eval'] = 1
                            fina_single.loc[index,'eval'] = 1
                        elif(price_5y_later/price_now > 2.3):
                            yearly.loc[index,'eval'] = 2
                            fina_single.loc[index,'eval'] = 2
                        elif(price_5y_later/price_now > 1.3):
                            yearly.loc[index,'eval'] = 3
                            fina_single.loc[index,'eval'] = 3
                        else:
                            yearly.loc[index,'eval'] = 4
                            fina_single.loc[index,'eval'] = 4
                        '''
        #print(fina_single)
        fina_single=fina_single.drop(fina_single.index[fina_single['eval']>2])
        fina_list.append(fina_single)
    # 3维格式，第一维是ts_code,第二维是年份，第三维是各列财务信息
    print("concating data")
    #fina_pd = pd.concat(fina_list, keys=ts_code_all)
    # 2维格式，所有ts_code的各个年份全部汇总在一维里（见ts_code、year），第二维是各列财务信息
    data_merge = pd.concat(fina_list)
    # 比较适合用于测试选股的列：assets_turn、netprofit_margin、profit_to_gr、adminexp_of_gr、impai_ttm、roe、assets_to_eqt、ocf_to_shortdebt、roa_dp        ts_code、year、eval
    data_merge = data_merge.loc[:,
                 ['assets_turn', 'netprofit_margin', 'profit_to_gr', 'adminexp_of_gr', 'impai_ttm', 'roe',
                  'assets_to_eqt', 'ocf_to_shortdebt', 'debt_to_eqt', 'cfps_yoy', 'op_yoy', 'tr_yoy',
                  'PE',  'PB', 'PS',              #市盈率、市净率、市销率等估值指标
                  'ts_code', 'year', 'eval']]  #此行为基本信息
    print("drop unusable data")
    #data_merge=data_merge.drop(data_merge.index[data_merge['eval']>2])
    # for code in ts_code_all:
    # print(fina_pd.loc[code])
    print(data_merge)
    filename = 'F:/ProgramData/tushare/' + 'data_merge.csv'
    print('saving:%s'%filename)
    data_merge.to_csv(filename, index=False)  # 将获取到的数据保存为csv文件

# 定义模型训练参数
params = {
    "objective":"binary:logistic",
    "booster":"gbtree",
    "max_depth":9
}

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print('Start')
    #获取token，如果调用远程tushare数据而非离线数据，则需要运行此行
    #pro=set_token()

    #读取全部公司基本信息
    #print_trade_date(pro)
    comany_basic = pd.read_csv("F:/ProgramData/tushare/stock_basic.csv")
    #comany_basic = comany_basic[3715:]
    comany_basic=comany_basic[comany_basic['market'].isin(['主板'])]
    #print(comany_basic['ts_code'])
    ts_code_all = comany_basic['ts_code']
    #每次遍历整表耗时较久，前期调试只需要少量数据即可
    #ts_code_all = ts_code_all.head(5)
    #merge_data_to_csv()
    print(ts_code_all)

    merge_data = pd.read_csv("F:/ProgramData/tushare/data_merge.csv")
    print(merge_data)
    #缺失项填0处理
    merge_data = merge_data.fillna(0)
    merge_data = merge_data.replace([np.inf, -np.inf], 0)

    #选出训练集与测试集
    df_train_raw = merge_data.sample(frac=0.6)
    rowlist = []
    for indexs in df_train_raw.index:
        rowlist.append(indexs)
    df_test_raw = merge_data.drop(rowlist, axis=0)

    #对结果打标签，eval是前面通过对比5年跨度股价涨幅做的最终标记，1为选中
    train_label = df_train_raw['eval']
    test_label = df_test_raw['eval']
    df_train = df_train_raw.drop(['eval','ts_code','year'], axis=1)
    df_test = df_test_raw.drop(['eval','ts_code','year'], axis=1)

    print(test_label)
    print(df_test)

    xgb_train = xgb.DMatrix(df_train,train_label)
    xgb_test = xgb.DMatrix(df_test)
    print("-"*30)
    #df_train = df_train.loc[:,['platelets', 'creatinine_phosphokinase', 'high_blood_pressure','diabetes','sex','smoking','DEATH_EVENT']]

    # 训练轮数
    num_round = 10000

    # 训练过程中实时输出评估结果（取消此处和evals即可删除打印，减少IO可以加快速度）
    #watchlist = [(xgb_train,'train')]

    # 模型训练
    model = xgb.train(params = params,dtrain=xgb_train,num_boost_round = num_round)#,evals=watchlist)

    preds = model.predict(xgb_test)
    print(preds)

    print("="*30)
    bi_scaler = Binarizer(threshold=0.30)
    preds = bi_scaler.fit_transform(preds.reshape(-1, 1))
    preds = preds.flatten()
    #preds = binarizer(preds)
    print(preds)
    result = pd.DataFrame(preds,columns=['pred'])
    print('preds.value_counts:%s'%pd.Series(preds).value_counts())
    print(test_label)
    print('test_label.value_counts:%s'%pd.Series(test_label).value_counts())
    print("="*30)
    acc_score = accuracy_score(test_label, preds)
    prec_score = precision_score(test_label, preds)
    rec_score = recall_score(test_label, preds)
    print('acc_score=%s,查全率rec_score=%s,查准率prec_score=%s'%(acc_score,rec_score,prec_score))

    df_test_raw = df_test_raw.reset_index(drop=True)
    df_test_result = df_test_raw.join(result)
    #print(result.dtype)
    #print(df_test_raw.dtypes)
    #df_test_raw.insert(df_test_raw.shape[1], 'pred', value=9)

    #df_test_raw = pd.merge(df_test_raw, result,left_on=key_or_keys, right_index=True,how='left', sort=False)
    #pd.merge(result, df_test_raw, on='index')

    #将最终预测与初始信息录入文件
    df_test_result.to_csv("F:/ProgramData/tushare/preds.csv")





