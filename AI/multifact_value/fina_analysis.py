import os
import time
import pandas as pd
import numpy as np
import tushare as ts
import xgboost as xgb
from sklearn.preprocessing import Binarizer
from sklearn.metrics import *
from tushare_basic import *
import xlwt

DATA_PATH = 'F:/ProgramData/tushare/'




def fina_analysis_by_stock(stock_code,start_year,year_num=5,write_file=True):
    #创建用于输出的dataframe
    columns = ['指标名','指标类型-细分','指标类型-大类']
    for i in range(year_num):
        columns.append(str(start_year+year_num-1-i))
    columns.append('加总或平均')
    columns.append('参考值')
    columns.append('说明')
    #print(columns)
    index = ['_0','COO净额','CIO净额','流出占比','_1','流动比率','速动比率','现金流量比率','_2','资产负债率(%)','财务杠杆','经营杠杆','总杠杆','_3',
             '应收账款比率','非实物资产比率','折旧率比率','销售管理费用比率','应收账款周转天数（天）',
             '应收账款指数DSRI','资产质量指数AQI','折旧率指数DEPI','销售管理费用指数SGAI','应收账款周转天数增长率','_4','_5',
             'PE','PS','ROE','1-D','ROE*(1-D)','_6',
             'PXG权重综合','传统行业加成','0.06/PXG','营收增长率(%)','营收动量','利润增长率(%)','利润动量','营收、净利是否背离','为可比公司营业收入倍数','_7',
             '毛利率','毛利率动量','净利率','应付:应收','资本回报率ROIC','_8',
             '经营净现金流/营业总收入','经营净现金流/净利润','营业收入/营业总收入']
    df_output = pd.DataFrame(data='', columns=columns,index=index) #创建只有行名、列名的空表
    out_filename = 'output/fina_analysis_'+stock_code+'.xls'


    df_comany_basic = pd.read_csv(DATA_PATH + "/stock_basic.csv")
    comany_basic = df_comany_basic[df_comany_basic['ts_code']==stock_code]
    print('公司信息')
    print(comany_basic)

    #读取数据：1.三大报表，2.估值 3.指标
    df_balance = pd.read_csv(DATA_PATH + '/fs/' + stock_code + "_balancesheet.csv")
    df_cashflow = pd.read_csv(DATA_PATH + '/fs/' + stock_code + "_cashflow.csv")
    df_income = pd.read_csv(DATA_PATH + '/fs/'+ stock_code + "_income.csv")
    #df_dividend = pd.read_csv(DATA_PATH + '/fs/'+ stock_code + "_dividend.csv")
    df_fina_ind = pd.read_csv(DATA_PATH + '/fs/'+ stock_code + "_fina_ind.csv")
    df_daily_basic = pd.read_csv(DATA_PATH + '/daily/' + stock_code + "_daily_basic.csv")

    df_cashflow = df_cashflow[df_cashflow['end_type']==4] #只留年报，去除半年报、季报
    df_cashflow = df_cashflow.drop_duplicates(subset=['end_date'], keep='first') #多次同期财报除重，以更新最新的财报为准
    df_cashflow['end_year'] = df_cashflow['end_date'].apply(lambda x:int(x/10000))  #只留年份，去除日、月，以便操作

    df_balance = df_balance[df_balance['end_type']==4] #只留年报，去除半年报、季报
    df_balance = df_balance.drop_duplicates(subset=['end_date'], keep='first') #多次同期财报除重，以更新最新的财报为准
    df_balance['end_year'] = df_balance['end_date'].apply(lambda x:int(x/10000))  #只留年份，去除日、月，以便操作

    df_income = df_income[df_income['end_type'] == 4]  # 只留年报，去除半年报、季报
    df_income = df_income.drop_duplicates(subset=['end_date'], keep='first')  # 多次同期财报除重，以更新最新的财报为准
    df_income['end_year'] = df_income['end_date'].apply(lambda x: int(x / 10000))  # 只留年份，去除日、月，以便操作

    df_fina_ind = df_fina_ind[df_fina_ind['end_date'].apply(lambda x:int(x%10000)) == 1231] #只留年报，去除半年报、季报
    df_fina_ind = df_fina_ind.drop_duplicates(subset=['end_date'], keep='first') #多次同期财报除重，以更新最新的财报为准
    df_fina_ind['end_year'] = df_fina_ind['end_date'].apply(lambda x:int(x/10000))  #只留年份，去除日、月，以便操作

    df_daily_basic['end_year'] = df_daily_basic['trade_date'].apply(lambda x:int(x/10000)) #只留年份
    df_daily_basic = df_daily_basic.drop_duplicates(subset=['end_year'], keep='first') #多次同期财报除重，以更新最新的财报为准

    #print(df_cashflow['end_year'])
    #print(df_fina_ind['end_year'])
    #print(df_daily)

    #rate = []
    all_cio=0.0
    all_coo=0.0
    all_current_ratio=0
    all_quick_ratio=0
    all_cash_ratio=0
    all_debt_to_assets=0
    lst_opt_leverage = []
    all_netprofit_margin = 0
    all_grossprofit_margin = 0
    last_grossprofit_margin = 0
    total_momentum_grossprofit_margin = 0
    #经营净现金流/营业总收入
    all_ocf_div_gr = 0
    #经营净现金流/净利润
    all_ocf_div_ni = 0
    #营业收入/营业总收入
    all_revenue_to_total_revenue = 0
    # 股息率
    all_dv_ratio = 0
    #净资产收益率ROE(waa为加权平均）
    all_roe_waa = 0
    #营收增长率
    all_or_yoy = 0
    total_momentum_or_yoy =0 #动量
    #归属母公司扣分净利润增长率
    all_dt_netprofit_yoy = 0
    total_momentum_dt_netprofit_yoy = 0  #动量
    for i in range(5):
        year = start_year + i

        df_cashflow_year = df_cashflow[df_cashflow['end_year'] == year]
        df_income_year = df_income[df_income['end_year'] == year]
        df_balance_year = df_balance[df_balance['end_year'] == year]
        df_fina_ind_year = df_fina_ind[df_fina_ind['end_year'] == year]
        df_daily_basic_year = df_daily_basic[df_daily_basic['end_year'] == year]
        #历年投资活动现金流量净额
        #print(df_cashflow_year['n_cashflow_inv_act'])
        df_output.loc['CIO净额',str(year)] = float(df_cashflow_year['n_cashflow_inv_act'])
        all_cio += float(df_cashflow_year['n_cashflow_inv_act'])
        # 历年经营活动现金流量净额
        df_output.loc['COO净额', str(year)] = float(df_cashflow_year['n_cashflow_act'])
        df_output.loc['流出占比', str(year)] = -float(df_cashflow_year['n_cashflow_inv_act']*100)/float(df_cashflow_year['n_cashflow_act'])
        all_coo += float(df_cashflow_year['n_cashflow_act'])
        #各类比率
        df_output.loc['流动比率', str(year)] = float(df_fina_ind_year['current_ratio'])
        df_output.loc['速动比率', str(year)] = float(df_fina_ind_year['quick_ratio'])
        df_output.loc['现金流量比率', str(year)] = float(df_cashflow_year['n_cashflow_act'])/float(df_balance_year['total_cur_liab'])
        df_output.loc['资产负债率(%)', str(year)] = float(df_fina_ind_year['debt_to_assets'])
        all_current_ratio += float(df_fina_ind_year['current_ratio'])
        all_quick_ratio += float(df_fina_ind_year['quick_ratio'])
        all_cash_ratio += float(df_cashflow_year['n_cashflow_act'])/float(df_balance_year['total_cur_liab'])
        all_debt_to_assets += float(df_fina_ind_year['debt_to_assets'])
        #print(df_income['admin_exp'])
        opt_leverage = float((df_income_year['ebit'] + df_income_year['admin_exp']) / df_income_year['ebit'])
        df_output.loc['经营杠杆', str(year)] = opt_leverage
        lst_opt_leverage.append(opt_leverage)
        #print(float(df_cashflow[df_cashflow['end_year'] == year]['n_cashflow_inv_act']))
        #print(float(df_cashflow[df_cashflow['end_year'] == year]['n_cashflow_act']))
        #print(df_income_year['ebit'])
        #销售毛利率、净利率
        df_output.loc['毛利率', str(year)] = float(df_fina_ind_year['grossprofit_margin'])
        all_grossprofit_margin += float(df_fina_ind_year['grossprofit_margin'])
        if i != 0 :
            df_output.loc['毛利率动量', str(year)] = float(df_fina_ind_year['grossprofit_margin']) - last_grossprofit_margin
            total_momentum_grossprofit_margin += float(df_fina_ind_year['grossprofit_margin']) - last_grossprofit_margin
        last_grossprofit_margin =float(df_fina_ind_year['grossprofit_margin'])
        df_output.loc['净利率', str(year)] = float(df_fina_ind_year['netprofit_margin'])
        all_netprofit_margin += float(df_fina_ind_year['netprofit_margin'])

        df_output.loc['经营净现金流/营业总收入', str(year)] = float(100.0*df_cashflow_year['n_cashflow_act'])/float(df_income_year['revenue'])
        df_output.loc['经营净现金流/净利润', str(year)] = float(100.0*df_cashflow_year['n_cashflow_act'])/float(df_income_year['n_income'])
        df_output.loc['营业收入/营业总收入', str(year)] = float(100.0*df_income_year['revenue'])/float(df_income_year['total_revenue'])
        all_ocf_div_gr += float(df_cashflow_year['n_cashflow_act'])/float(df_income_year['revenue'])
        all_ocf_div_ni += float(df_cashflow_year['n_cashflow_act'])/float(df_income_year['n_income'])
        all_revenue_to_total_revenue += float(df_income_year['revenue'])/float(df_income_year['total_revenue'])

        #求5年平均派息率D、ROE
        df_output.loc['1-D', str(year)] = 1.0 - float(df_daily_basic_year['dv_ratio'])
        df_output.loc['ROE', str(year)] = float(df_fina_ind_year['roe_waa'])
        all_dv_ratio += float(df_daily_basic_year['dv_ratio'])
        all_roe_waa += float(df_fina_ind_year['roe_waa'])
        #营收增长率,营业利润增长率
        df_output.loc['营收增长率(%)', str(year)] = float(df_fina_ind_year['or_yoy'])
        df_output.loc['利润增长率(%)', str(year)] = float(df_fina_ind_year['dt_netprofit_yoy'])
        all_or_yoy += float(df_fina_ind_year['or_yoy'])
        all_dt_netprofit_yoy += float(df_fina_ind_year['dt_netprofit_yoy'])
        if i != 0 :
            df_output.loc['营收动量', str(year)] = float(df_fina_ind_year['or_yoy']) - last_or_yoy
            df_output.loc['利润动量', str(year)] = float(df_fina_ind_year['dt_netprofit_yoy']) - last_dt_netprofit_yoy
            total_momentum_or_yoy += float(df_fina_ind_year['or_yoy']) - last_or_yoy
            total_momentum_dt_netprofit_yoy += float(df_fina_ind_year['dt_netprofit_yoy']) - last_dt_netprofit_yoy
        last_or_yoy = float(df_fina_ind_year['or_yoy'])
        last_dt_netprofit_yoy = float(df_fina_ind_year['dt_netprofit_yoy'])

    cio_coo_rate = float(-all_cio/all_coo)
    print('cio/coo(<0.7)')
    print(cio_coo_rate)
    df_output.loc['流出占比', '加总或平均'] = float(cio_coo_rate * 100.0)
    #流动比率、速动比率、现金比率
    print('流动比率(1.5-2)、速动比率(0.8-1.2)、现金比率(>0.2)')
    print(all_current_ratio/year_num)
    print(all_quick_ratio/year_num)
    print(all_cash_ratio/year_num)
    df_output.loc['流动比率', '加总或平均'] = all_current_ratio/year_num
    df_output.loc['速动比率', '加总或平均'] = all_quick_ratio/year_num
    df_output.loc['现金流量比率', '加总或平均'] = all_cash_ratio/year_num

    fina_leverage = 1/(1-all_debt_to_assets/year_num/100)
    opt_leverage = sum(lst_opt_leverage)/len(lst_opt_leverage)
    print('资产负债率、财务杠杆、经营杠杆、总杠杆')
    print(all_debt_to_assets/year_num/100)
    print(fina_leverage)
    print(opt_leverage)
    print(fina_leverage*opt_leverage)
    df_output.loc['资产负债率(%)', '加总或平均'] = all_debt_to_assets/year_num
    df_output.loc['财务杠杆', '加总或平均'] = fina_leverage
    df_output.loc['经营杠杆', '加总或平均'] = opt_leverage
    df_output.loc['总杠杆', '加总或平均'] = fina_leverage*opt_leverage

    print('销售毛利率、净利率、毛利率动量')
    mean_momentum_grossprofit_margin = total_momentum_grossprofit_margin / (year_num - 1)
    print(all_grossprofit_margin/year_num)
    print(all_netprofit_margin/year_num)
    print(mean_momentum_grossprofit_margin)
    df_output.loc['毛利率', '加总或平均'] = all_grossprofit_margin/year_num
    df_output.loc['净利率', '加总或平均'] = all_netprofit_margin/year_num
    df_output.loc['毛利率动量', '加总或平均'] = mean_momentum_grossprofit_margin

    print('收益质量：经营净现金流/营业总收入(>0.15)，经营净现金流/净利润(>0.9),营业收入/营业总收入(>0.85)')
    print(all_ocf_div_gr/year_num)
    print(all_ocf_div_ni/year_num)
    print(all_revenue_to_total_revenue/year_num)
    df_output.loc['经营净现金流/营业总收入', '加总或平均'] = 100.0*all_ocf_div_gr/year_num
    df_output.loc['经营净现金流/净利润', '加总或平均'] = 100.0*all_ocf_div_ni/year_num
    df_output.loc['营业收入/营业总收入', '加总或平均'] = 100.0*all_revenue_to_total_revenue/year_num

    print('最后一年的PE，PS')
    df_daily_last = df_daily_basic[df_daily_basic['end_year'] == (start_year+year_num-1)]
    pe = float(df_daily_last['pe_ttm'])
    ps = float(df_daily_last['ps'])
    print(pe)
    print(ps)
    df_output.loc['PE', str(start_year+year_num-1)] = float(df_daily_last['pe_ttm'])
    df_output.loc['PS', str(start_year+year_num-1)] = float(df_daily_last['ps'])

    print('派息率D,加权平均ROE，ROE*(1-D)（>15%）')
    print(all_dv_ratio/year_num)
    print(all_roe_waa/year_num)
    print(str((1-all_dv_ratio/year_num)*(all_roe_waa/year_num))+'%')
    df_output.loc['1-D', '加总或平均'] = 1.0-all_dv_ratio/year_num
    df_output.loc['ROE', '加总或平均'] = all_roe_waa/year_num
    df_output.loc['ROE*(1-D)', '加总或平均'] = (1-all_dv_ratio/year_num)*(all_roe_waa/year_num)

    print('营收增长率(>30%)，营收增长率动量，净利润增长率(>10%)，净利润增长率动量')#TODO：用EPS增长率更好
    mean_or_yoy=all_or_yoy/year_num
    mean_momentum_or_yoy = total_momentum_or_yoy / (year_num - 1)
    mean_dt_netprofit_yoy = all_dt_netprofit_yoy / year_num
    mean_momentum_dt_netprofit_yoy = total_momentum_dt_netprofit_yoy / (year_num - 1)
    print(mean_or_yoy)
    print(mean_momentum_or_yoy)
    print(mean_dt_netprofit_yoy)
    print(mean_momentum_dt_netprofit_yoy)
    df_output.loc['营收增长率(%)', '加总或平均'] = mean_or_yoy
    df_output.loc['营收动量', '加总或平均'] = mean_momentum_or_yoy
    df_output.loc['利润增长率(%)', '加总或平均'] = mean_dt_netprofit_yoy
    df_output.loc['利润动量', '加总或平均'] = mean_momentum_dt_netprofit_yoy

    #可比公司营收增长倍数 = 10*LOG(营收平均增长率/可比公司营收平均增长率)
    #PXG=(pe*ps)^0.3/(营收增长率*45+净利润增长率*20+营收增长率动量*10+净利润增长率动量*10+MIN(营收增长率、动量，净利润增长率、动量)*15+毛利率动量*400+可比公司营收增长倍数)
    print('px,growth,权重综合PXG')
    px = pow(pe * ps, 0.3)
    growth = (mean_or_yoy*0.45+mean_momentum_or_yoy*0.2+mean_dt_netprofit_yoy*0.1+mean_momentum_dt_netprofit_yoy*0.1+
           min(mean_or_yoy,mean_momentum_or_yoy,mean_dt_netprofit_yoy,mean_momentum_dt_netprofit_yoy)*0.15+ mean_momentum_grossprofit_margin*4)
    pxg = px/growth
    print(px)
    print(growth)
    print(pxg)
    df_output.loc['PXG权重综合', '加总或平均'] = pxg*100
    print('现金牛传统行业加权(剔除新兴行业、夕阳行业、周期行业)')
    industry = comany_basic['industry'].values[0]
    print(industry)
    if str(industry) in ['白酒','玻璃','环境保护','文教休闲','家用电器','批发业','农业综合','商贸代理','百货','农药化肥','供气供热','旅游景点','食品','出版业','中成药','保险','林业']:
        #== '白酒' or comany_basic['market'] == '玻璃' or comany_basic['market'] == '环境保护' or comany_basic['market'] == '文教休闲'
        #or comany_basic['market'] == '家用电器'
        tradition_indust_factor = 1.5
    else:
        tradition_indust_factor = 1
    print(tradition_indust_factor)
    df_output.loc['传统行业加成', '加总或平均'] = tradition_indust_factor
    #0.06 / PXG
    PXG_result = 0.06 * tradition_indust_factor/pxg
    print('权重综合:0.06/PXG (>0.15)')
    print(PXG_result)
    df_output.loc['0.06/PXG', '加总或平均'] = PXG_result*100

    #补充参考值、说明
    df_output.loc['流出占比', '参考值'] = '小于70%'
    df_output.loc['流动比率', '参考值'] = '1.5-2'
    df_output.loc['速动比率', '参考值'] = '0.8-1.2'
    df_output.loc['现金流量比率', '参考值'] = '0.2+'
    df_output.loc['ROE*(1-D)', '参考值'] = '大于15%'
    df_output.loc['0.06/PXG', '参考值'] = '大于15%'
    df_output.loc['营收增长率(%)', '参考值'] = '大于20%'
    df_output.loc['营收动量', '参考值'] = '大于0'
    df_output.loc['利润增长率(%)', '参考值'] = '大于20%'
    df_output.loc['利润动量', '参考值'] = '大于0'
    df_output.loc['毛利率', '参考值'] = '大于30%'
    df_output.loc['净利率', '参考值'] = '大于10%'
    df_output.loc['经营净现金流/营业总收入', '参考值'] = '大于15%'
    df_output.loc['经营净现金流/净利润', '参考值'] = '大于90%'
    df_output.loc['营业收入/营业总收入', '参考值'] = '大于85%'


    if write_file:
        df_output.to_excel(out_filename,sheet_name='综合分析', index=True,float_format='%.2f')
        print(df_output)

def price_analysis_by_stock(stock_code,start_year,year_num=5,write_file=True):
    df_comany_basic = pd.read_csv(DATA_PATH + "/stock_basic.csv")
    comany_basic = df_comany_basic[df_comany_basic['ts_code']==stock_code]
    print('公司信息')
    print(comany_basic)
    #此处分析用长周期数据，月线或年线即可，注意用复权后的数据，千万别用未复权数据
    df_monthly = pd.read_csv(DATA_PATH + '/monthly/' + stock_code + "_monthly.csv")

    df_monthly['end_year'] = df_monthly['trade_date'].apply(lambda x:int(x/10000)) #只留年份
    df_monthly = df_monthly.drop_duplicates(subset=['end_year'], keep='first') #多次同期财报除重，以更新最新的财报为准

    columns = ['股票代码','股票名称','初始年份', '初始价格', '结尾年份','结尾价格','总收益率(%)','年化收益率(%,算术平均)']
    index = [stock_code]
    df_output = pd.DataFrame(data='', columns=columns, index=index)
    out_filename = 'output/price_analysis_' + stock_code + '.xls'


    df_output.loc[stock_code, '股票代码'] = stock_code
    df_output.loc[stock_code, '股票名称'] = comany_basic.iloc[0]['name']
    df_output.loc[stock_code, '初始年份'] = start_year
    start_price = float(df_monthly[df_monthly['end_year'] == start_year]['close'])
    df_output.loc[stock_code, '初始价格'] = start_price
    df_output.loc[stock_code, '结尾年份'] = start_year+year_num
    end_price = float(df_monthly[df_monthly['end_year'] == start_year+year_num]['close'])
    df_output.loc[stock_code, '结尾价格'] = end_price
    total_return = (end_price - start_price)/ start_price
    df_output.loc[stock_code, '总收益率(%)'] = total_return*100.0
    yearly_return = total_return*100.0/year_num
    df_output.loc[stock_code, '年化收益率(%,算术平均)'] = yearly_return


    if write_file:
        df_output.to_excel(out_filename,sheet_name='综合分析', index=True,float_format='%.2f')
        print(df_output)


if __name__ == '__main__':
    stock_code = '600519.SH'  # 贵州茅台
    fina_analysis_by_stock(stock_code=stock_code,start_year=2006,year_num=5,write_file=True)

    price_analysis_by_stock(stock_code=stock_code,start_year=2010,year_num=10,write_file=True)
