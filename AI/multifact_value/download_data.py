import os
import time
import pandas as pd
import numpy as np
import tushare as ts
import xgboost as xgb
from sklearn.preprocessing import Binarizer
#from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import *
from tushare_basic import get_daily

DATA_PATH = 'E:/ProgramData/tushare/'

def set_token():
    ts.set_token('35000bb7b3c3efb44ccce92dadf9c6d35f39119d761ac8a367166296')
    return ts.pro_api()#接口，token可在tushare官网里登录个人账号获取


def get_us_basic():
#获取默认美国股票基础信息，单次6000行
    df1 = pro.us_basic(offset=0)
    print('step 1')
    time.sleep(35)
    df2 = pro.us_basic(offset=6001)
    print('step 2')
    time.sleep(35)
    df3 = pro.us_basic(offset=12001)
    print('step 3')
    time.sleep(35)
    df4 = pro.us_basic(offset=18001)
    print('step 4')
    time.sleep(35)
    df5 = pro.us_basic(offset=24001)
    print('step 5')

    data = pd.concat([df1, df2, df3, df4, df5], axis=0)

    data.to_csv(os.path.join(DATA_PATH,'us_stock_basic.csv'),index=False) #将获取到的数据保存为csv文件
    print('获取到美国上市公司：',len(data))


#获取股票列表及其基础数据
def comany_info(pro):
    data=pro.stock_basic(exchage='',list_status='L',
                     fields='ts_code,symbol,name,area,industry,market,list_date,is_hs')
                         #获取TS代码，股票代码，股票名称，所在地域，所属行业，市场类型等信息

    data=data[data['market'].isin(['主板'])]
    #利用.isin()函数过滤数据，只保留科创板的股票，如要添加其他版块则.isin(['科创板'，'xxx'])

    data.to_csv(os.path.join(DATA_PATH,'stock_basic.csv'),index=False) #将获取到的数据保存为csv文件
    #os.path.join(路径，文件名)为保存路径拼接函数。注：事先创建Data文件夹
    print('获取到上市公司：',len(data))

def get_daily_basic(pro,code):
    df = pro.query('daily_basic', ts_code=code, start_date = '19900101',end_date='20230801')
    filename = code+'_daily_basic.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join(DATA_PATH,'daily',filename),index=False) #将获取到的数据保存为csv文件

#此数据已复权
def get_monthly(pro,code):
    #注意此函数直接调ts包即可，不需要token
    df = ts.pro_bar(ts_code=code, freq='M', adj='qfq', start_date='19900101', end_date='20230801')  #用前复权
    filename = code+'_monthly.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join(DATA_PATH,'monthly',filename),index=False) #将获取到的数据保存为csv文件

def get_dividend(pro, code):
    df = pro.dividend(ts_code=code, start_date = '19900101',end_date='20230801')
    filename = code + '_dividend.csv'
    print("saving:%s" % filename)
    df.to_csv(os.path.join(DATA_PATH,'fs', filename), index=False)  # 将获取到的数据保存为csv文件

def get_fina_ind(pro, code):
    df = pro.query('fina_indicator', ts_code=code, start_date = '19900101',end_date='20230801')
    filename = code + '_fina_ind.csv'
    print("saving:%s" % filename)
    df.to_csv(os.path.join(DATA_PATH,'fs', filename), index=False)  # 将获取到的数据保存为csv文件

def get_fs_income(pro,code):
    df = pro.query('income', ts_code=code, start_date='19900101', end_date='20230801')
    filename = code+'_income.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join(DATA_PATH,'fs',filename),index=False) #将获取到的数据保存为csv文件

def get_fs_balancesheet(pro,code):
    df = pro.query('balancesheet', ts_code=code, start_date='19900101', end_date='20230801')
    filename = code+'_balancesheet.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join(DATA_PATH,'fs',filename),index=False) #将获取到的数据保存为csv文件

def get_fs_cashflow(pro, code):
    df = pro.query('cashflow', ts_code=code, start_date='19900101', end_date='20230801')
    filename = code + '_cashflow.csv'
    print("saving:%s" % filename)
    df.to_csv(os.path.join(DATA_PATH,'fs', filename), index=False)  # 将获取到的数据保存为csv文件


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print('Start')
    #获取token，如果调用远程tushare数据而非离线数据，则需要运行此行
    pro=set_token()

    #读取全部公司基本信息
    #print_trade_date(pro)
    #获取美股数据
    #get_us_basic()
    #exit()

    comany_basic = pd.read_csv("F:/ProgramData/tushare/stock_basic.csv")
    #comany_basic = comany_basic[3715:]
    comany_basic=comany_basic[~comany_basic['market'].isin(['主板'])]
    #print(comany_basic['ts_code'])
    ts_code_all = comany_basic['ts_code']
    #ts_code_all = ts_code_all[190:]
    #跳过'002034.SZ'之前的部分
    i=0
    for code in ts_code_all:
        #time.sleep(1)
        if code == '688698.SH':
            print(i)
            break
        i = i+1
    ts_code_all = ts_code_all[i:]
    print(ts_code_all)
    for code in ts_code_all:
        #'''
        time.sleep(1)
        get_fs_income(pro, code)
        time.sleep(1)
        get_fs_balancesheet(pro, code)
        time.sleep(1)
        get_fs_cashflow(pro, code)
        #'''
        #'''
        time.sleep(1)
        get_fina_ind(pro, code)
        time.sleep(1)
        get_monthly(pro, code)
        time.sleep(1)
        get_dividend(pro, code)
        #'''
        time.sleep(10)
        get_daily(pro, code)
        time.sleep(10)
        get_daily_basic(pro, code)
