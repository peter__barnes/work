import os
import time
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.preprocessing import Binarizer
#from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import *
from tushare_basic import *

#注意此数据未复权，不准确
def get_monthly(pro,code,start_date='19920101', end_date='20220201'):
    df = pro.query('monthly', ts_code=code, start_date=start_date, end_date=end_date)
    filename = '[test]'+code+'_monthly.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join('E:/ProgramData/tushare',filename),index=False) #将获取到的数据保存为csv文件
    print(df)
def fina_ind(pro,code,start_date='19920101', end_date='20220201'):
    df = pro.query('fina_indicator', ts_code=code, start_date = start_date,end_date=end_date)#period='20201231')
    time.sleep(3)
    filename = '[test]'+code+'_fina_ind.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join('E:/ProgramData/tushare',filename),index=False) #将获取到的数据保存为csv文件

def daily_basic(pro, code, start_date='20160601', end_date='20170101'):
    df = pro.query('daily_basic', ts_code=code, start_date = start_date,end_date=end_date)#period='20201231')
    time.sleep(3)
    filename = '[test]'+code+'_daily_basic.csv'
    print("saving:%s"%filename)
    df.to_csv(os.path.join('E:/ProgramData/tushare',filename),index=False) #将获取到的数据保存为csv文件

def read_ts_data(code):
    #comany_basic = pd.read_csv("E:/ProgramData/tushare/stock_basic.csv")
    # comany_basic = comany_basic[3715:]
    #comany_basic = comany_basic[comany_basic['market'].isin(['主板'])]
    # print(comany_basic['ts_code'])
    #ts_code_all = comany_basic['ts_code']
    #for code in ts_code_all:
        #time.sleep(20)
    print(code)
    try:
        get_monthly(pro, code, start_date='20161201', end_date='20170101')
        fina_ind(pro, code, start_date='20161201', end_date='20170101')
        daily_basic(pro, code, start_date='20161201', end_date='20170101')
    except Exception as e:
        print('failed:%s,%s'%(code,e))

if __name__ == '__main__':
    print('Start')
    pro = set_token()
    print(pro)
    read_ts_data('002624.SZ') # 完美世界
    read_ts_data('002555.SZ')  # 三七互娱