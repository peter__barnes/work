import pandas as pd
import numpy as np
import torch
import torch.nn.functional as F
from torch import nn,optim
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda, Compose

df = pd.read_csv('heart_failure_clinical_records_dataset.csv')

df_train = df.sample(frac=0.6)
rowlist=[]
for indexs in df_train.index:
	rowlist.append(indexs)
df_test = df.drop(rowlist,axis=0)

train_label = df_train['DEATH_EVENT']
test_label = df_test['DEATH_EVENT']
df_train = df_train.drop('DEATH_EVENT',axis=1)
df_test = df_test.drop('DEATH_EVENT',axis=1)
df_train = df_train.drop('platelets',axis=1)
df_test = df_test.drop('platelets',axis=1)

print(df_train.shape[0])
#print(df_test)
print('-'*50)    

#x_data = torch.Tensor([[1.0], [2.0], [3.0]])
#y_data = torch.Tensor([[2.0], [4.0], [6.0]])

arr_df_train = np.array(df_train)
#print(df_train)
#print(arr_df_train)
x_data = torch.tensor(arr_df_train, dtype=torch.float32) 

arr_train_label = np.array(train_label)
arr_train_label = arr_train_label[:,np.newaxis]
y_data = torch.tensor(arr_train_label, dtype=torch.float32) 
print(x_data)
#print(y_data)
print('='*50)    

class Net(torch.nn.Module):
    def __init__(self,n_input,n_hidden,n_output):
        super(Net,self).__init__()
        self.hidden1 = torch.nn.Linear(n_input,n_hidden)
        self.hidden2 = torch.nn.Linear(n_hidden,n_hidden)
        self.predict = torch.nn.Linear(n_hidden,n_output)

    def forward(self, input):
        out = self.hidden1(input)
        out = F.sigmoid(out)
        out = self.hidden2(out)
        out = F.sigmoid(out)
        out = self.predict(out)
        # out = F.softmax(out)
        return out

# Define model
class NeuralNetwork(torch.nn.Module):
	def __init__(self):
		super(NeuralNetwork, self).__init__() 
		self.flatten = nn.Flatten()
		self.linear_relu_stack = torch.nn.Sequential(
			torch.nn.Linear(11, 10),
			torch.nn.ReLU(),
			torch.nn.Linear(10, 5),
			torch.nn.ReLU(),
			torch.nn.Linear(5, 1)
			)
	def forward(self, x):
		x = self.flatten(x)
		logits = self.linear_relu_stack(x)
		return logits

class Classifier(nn.Module):
    def __init__(self,input_feature,output_size):
        super(Classifier, self).__init__()
        self.linear=nn.Linear(input_feature,output_size)
        # print(input_feature)
        # print(output_size)

    def forward(self,x):
        # print(x.size())
        x=self.linear(x)
        # print(x.size())
        x=torch.sigmoid(x)
        # print(x.size())
        return x


model = Net(11,20,1)
#model = NeuralNetwork()
#model = Classifier(11,1)
    
#criterion = torch.nn.MSELoss(size_average=False)
#criterion = torch.nn.CrossEntropyLoss()
criterion = torch.nn.BCELoss()
#optimizer = torch.optim.SGD(model.parameters(), lr=0.01)
#optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)
#optimizer = torch.optim.Adam(model.parameters(), lr=0.01) 
optimizer = torch.optim.SGD(model.parameters(),lr=0.02)
#optimizer = optim.SGD(model.parameters(), lr=1e-2)
for epoch in range(df_train.shape[0]):
    y_pred = model(x_data)
	#print(y_pred)
	#print('1'*50)    
	#print(y_data)
    y_pred [ y_pred <   0 ]   =   0.1
    y_pred [ y_pred >   1 ]   =   0.9 
    loss = criterion(y_pred, y_data) 
	#print(epoch, loss.item())
    
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

print('*'*50)    
#print('w = ', model.linear.weight.item())
#print('b = ', model.linear.bias.item())
#x_test = torch.Tensor([[4.0]]) 

arr_df_test = np.array(df_test)
x_test = torch.tensor(arr_df_test, dtype=torch.float32) 
print(x_test)
y_test = model(x_test) 
y_test[y_test<0]=0.1
y_test[y_test>1]=0.9
average=sum(y_test)/len(y_test)
y_test[y_test>average]=1
y_test[y_test<=average]=0
#print(y_test)
y_test_arr = y_test.detach().numpy()
y_test_arr = y_test_arr.astype(np.uint8).flatten()
test_label = np.array(test_label)
print('y_pred = ', y_test_arr)
print('y_real= ', test_label)

corr=0
err=0
for i in np.arange(0,y_test_arr.size):
	if y_test_arr[i] == test_label[i]:
		#print("1")
		corr=corr+1
	else:
		#print("0")
		err=err+1
pred_0=0
pred_1=0
for i in np.arange(0,y_test_arr.size):
	if y_test_arr[i] == 1:
		pred_0=pred_0+1
	else:
		pred_1=pred_1+1
print("corr=",corr)
print("err=",err)
print("pred_0=",pred_0)
print("pred_1=",pred_1)

