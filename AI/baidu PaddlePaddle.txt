【TensortRT部署】
去官网下载whl文件（见下面链接），然后手动安装：pip install *.whl
https://www.paddlepaddle.org.cn/inference/user_guides/download_lib.html#windows

但是，并非所有模块都可以轻易调用此功能，paddlepaddleOCR就很难进行TensorRT调用，建议采用Onnx

【开发环境安装】
见官网https://www.paddlepaddle.org.cn/install/quick?docurl=/documentation/docs/zh/install/pip/windows-pip.html

以cuda11.6为例：
python -m pip install paddlepaddle-gpu==2.3.2.post116 -f https://www.paddlepaddle.org.cn/whl/windows/mkl/avx/stable.html

非cuda版本：
python -m pip install paddlepaddle==2.3.2 -f https://www.paddlepaddle.org.cn/whl/windows/mkl/avx/stable.html

文字识别模块：
pip install paddleocr

【OCR】
Onnx调用，代码见工程文件夹orc_code：
	1.部署时，先按照【开发环境安装】安装好gpu版本软件、ocr库，
	2. 运行下列代码，第一次运行时，虽然会因为缺少本地版本的Onnx模型而失败，但是可以自动下载普通版本的模型到en_PP-OCRv3_rec_infer、en_ppocr_mobile_v2.0_cls_infer、en_PP-OCRv3_det_infer。
			python ocr_test.py
	3.三个下载的模型文件夹下都会生成“inference.onnx"文件夹（实际是误生成的，本希望生成在最外层），将“inference.onnx"里内容移到上一层文件夹后，删掉“inference.onnx"
	4.pip install paddle2onnx
	5.分别进入三个模型文件夹，输入以下命令进行模型转换：
		paddle2onnx --model_dir ./ --model_filename inference.pdmodel --params_filename inference.pdiparams --save_file ./inference.onnx --opset_version 10 --input_shape_dict="{'x':[-1,3,-1,-1]}" --enable_onnx_checker True
		此次生成的3个inference.onnx文件就是最终要使用的onnx模型文件
	6.paddleocr库代码每次都会尝试下载，导致重名失败，需要增加一个重名文件判断，如果已经有重名的如'./en_PP-OCRv3_rec_infer/inference.onnx'则跳过下载，参考以下修改：
		6.1.编辑 Anaconda3\envs\yolov5\Lib\site-packages\paddleocr\ppocr\utils\network.py文件   
		6.2.找到maybe_download函数，在os.makedirs(model_storage_directory, exist_ok=True) 这一行前增加两行（如果文件已经存在，直接跳过下载步骤）：
       			if os.path.exists(model_storage_directory):
        				return

paddle2onnx --model_dir ./ --model_filename inference.pdmodel --params_filename inference.pdiparams --save_file ./model.onnx --opset_version 10 --input_shape_dict="{'x':[-1,3,-1,-1]}" --enable_onnx_checker True

【排障】
问题：PaddleOCR提示Error: Can not import avx core while this file exists: xxx\paddle\fluid\core_avx

解决方法：步骤1.找到python安装目录下面的子目录：\Lib\site-packages\paddle\libs

	步骤2.将里面的.dll文件全部拷贝到如下同级子目录：\Lib\site-packages\paddle\fluid\