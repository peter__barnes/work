# This is a sample from kaggle competition
# https://www.kaggle.com/competitions/spaceship-titanic
#待优化点：1.剔除异常值；2.测试集不用重新分箱，而应该用训练集的分箱临界点 3.分类算法或许不该用逻辑回归，因为对非线性特征效果不佳 4.集成学习方法使用的是默认的，未采用融合学习
#    5.年龄分箱方法或许不准确，尤其是成年与否的分界点18岁这类有特殊含义的或许需要用作分界点  6.超参数没有调优，而是随手乱选的几个值
#    7.没做特征筛选，需要剔除掉无效特征，树模型无需专门剔除相关性
from fastai.vision.all import *
from fastai.text.all import *
from fastai.collab import *
from fastai.tabular.all import *
import torch.nn.functional as F
import torch
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import xgboost as xgb
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix, mean_squared_error #混淆矩阵，均方误差
from sklearn.preprocessing import Binarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import accuracy_score

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


data_path = 'E:/data/kaggle_Titanic/'

#分箱：根据数据和分箱节点列表返回箱号
def reset_by_quantile(x, l_bin):
    i = 1
    for i in range(1,len(l_bin)):
        if x < l_bin[i]:
            return i - 1
    return i

def get_by_split(x,pos):
    x_lst = x.split('/')
    if pos == 1:
        return int(x_lst[pos])
    return x_lst[pos]

def auc(m, train, test, y_train, y_test):
    return (metrics.roc_auc_score(y_train, m.predict_proba(train)[:,1]),
                            metrics.roc_auc_score(y_test, m.predict_proba(test)[:,1]))

def data_preprocess(pd_data):
    #缺失值处理
    pd_data = pd_data.copy()
    #年龄用平均值填充缺失值
    age_mean = round(pd_data['Age'].median())
    pd_data['Age'] = pd_data['Age'].fillna(age_mean)
    #用平均值填充缺失值
    RoomService_mean = round(pd_data['RoomService'].median())
    pd_data['RoomService'] = pd_data['RoomService'].fillna(RoomService_mean)
    #用平均值填充缺失值
    FoodCourt_mean = round(pd_data['FoodCourt'].median())
    pd_data['FoodCourt'] = pd_data['FoodCourt'].fillna(FoodCourt_mean)
    #用平均值填充缺失值
    ShoppingMall_mean = round(pd_data['ShoppingMall'].median())
    pd_data['ShoppingMall'] = pd_data['ShoppingMall'].fillna(ShoppingMall_mean)
    #用平均值填充缺失值
    Spa_mean = round(pd_data['Spa'].median())
    pd_data['Spa'] = pd_data['Spa'].fillna(Spa_mean)
    #用平均值填充缺失值
    VRDeck_mean = round(pd_data['VRDeck'].median())
    pd_data['VRDeck'] = pd_data['VRDeck'].fillna(VRDeck_mean)

    #地点不明的填other
    pd_data['HomePlanet'] = pd_data['HomePlanet'].fillna('other')
    pd_data['Destination'] = pd_data['Destination'].fillna('other')
    #默认不是VIP
    pd_data['VIP'] = pd_data['VIP'].fillna(False)
    #CryoSleep不明的填True（未缺失的False多一些）
    pd_data['CryoSleep'] = pd_data['CryoSleep'].fillna(False)
    pd_data['Cabin'] = pd_data['Cabin'].fillna('F/0/S')
    #缺失值抛弃
    #pd_data = pd_data.dropna()
    #特征处理
    pd_data.insert(3, 'Cabin_1', 0)
    pd_data["Cabin_1"] = pd_data.loc[:, 'Cabin'].map(lambda x: get_by_split(x, 0))
    pd_data["Cabin_2"] = pd_data.loc[:, 'Cabin'].map(lambda x: get_by_split(x, 1))
    pd_data["Cabin_3"] = pd_data.loc[:, 'Cabin'].map(lambda x: get_by_split(x, 2))
    #print可打印出Cabin最多的为F/0/S
    #print(pd_data.groupby('Cabin_3').count())

    pd_data['Cabin_1'] = pd_data['Cabin_1'].replace(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'T'], [0, 1, 2, 3, 4, 5, 6, 7])
    pd_data['Cabin_3'] = pd_data['Cabin_3'].replace(['P', 'S'], [0, 1])

    # print(pd_data)
    #sns.barplot(x="Cabin_1", y="Transported", data=pd_data)
    #plt.show()
    #exit()

    # pd_data = pd_data.dropna(subset=['CryoSleep','VIP','Transported'])
    # True/False 变 0 1
    pd_data[['CryoSleep']] = pd_data[['CryoSleep']].astype(int)
    pd_data[['VIP']] = pd_data[['VIP']].astype(int)
    #如果是测试集，则没有这一列数据
    if 'Transported' in pd_data:
        pd_data[['Transported']] = pd_data[['Transported']].astype(int)

    # print(pd_data['Age'].max())
    # 根据age进行分箱前，先判断几个关键的分位值
    age_bin = []
    RoomService_bin = []
    FoodCourt_bin = []
    ShoppingMall_bin = []
    Spa_bin = []
    VRDeck_bin = []
    Cabin_2_bin = []
    # 分5箱
    for i in range(0, 101, 20):
        age_bin.append(np.percentile(pd_data['Age'], i))
        RoomService_bin.append(np.percentile(pd_data['RoomService'], i))
        FoodCourt_bin.append(np.percentile(pd_data['FoodCourt'], i))
        ShoppingMall_bin.append(np.percentile(pd_data['ShoppingMall'], i))
        Spa_bin.append(np.percentile(pd_data['Spa'], i))
        VRDeck_bin.append(np.percentile(pd_data['VRDeck'], i))
        Cabin_2_bin.append(np.percentile(pd_data['Cabin_2'], i))
    # print(age_bin)
    pd_data['Age'] = pd_data['Age'].apply(lambda x: reset_by_quantile(x, age_bin))
    pd_data['RoomService'] = pd_data['RoomService'].apply(lambda x: reset_by_quantile(x, RoomService_bin))
    pd_data['FoodCourt'] = pd_data['FoodCourt'].apply(lambda x: reset_by_quantile(x, FoodCourt_bin))
    pd_data['ShoppingMall'] = pd_data['ShoppingMall'].apply(lambda x: reset_by_quantile(x, ShoppingMall_bin))
    pd_data['Spa'] = pd_data['Spa'].apply(lambda x: reset_by_quantile(x, Spa_bin))
    pd_data['VRDeck'] = pd_data['VRDeck'].apply(lambda x: reset_by_quantile(x, VRDeck_bin))
    pd_data['Cabin_2'] = pd_data['Cabin_2'].apply(lambda x: reset_by_quantile(x, Cabin_2_bin))
    # print(pd_data['Age'])

    # print(pd_data.HomePlanet[(pd_data.HomePlanet != 'Europa' ) & (pd_data.HomePlanet != 'Earth') ])
    pd_data.loc[(pd_data.HomePlanet != 'Europa') & (pd_data.HomePlanet != 'Earth'), 'HomePlanet'] = 'other'
    # 将HomePlanet进行One-Hot编码
    pd_HomePlanet = pd.get_dummies(pd_data[["HomePlanet"]])
    # print(pd_HomePlanet)
    pd_data = pd_data.join(pd_HomePlanet)
    pd_data = pd_data.drop(['HomePlanet'], axis=1)

    pd_data.loc[
        (pd_data.Destination != '55 Cancri e') & (pd_data.Destination != 'TRAPPIST-1e'), 'Destination'] = 'other'
    # 将HomePlanet进行One-Hot编码
    pd_Destination = pd.get_dummies(pd_data[["Destination"]])
    # print(pd_HomePlanet)
    pd_data = pd_data.join(pd_Destination)
    pd_data = pd_data.drop(['Destination'], axis=1)

    pd_data = pd_data.drop(['Cabin'], axis=1)
    pd_data = pd_data.drop(['Name'], axis=1)
    return pd_data

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # pd_adult.to_csv('E:/data/cats/adult.csv')
    pd_train = pd.read_csv(data_path + 'train.csv', skipinitialspace=True)
    pd_test_ori = pd.read_csv(data_path + 'test.csv', skipinitialspace=True)
    #pd_train = pd_train.head(20)

    #print(pd_train)
    pd_train = data_preprocess(pd_train)
    pd_test = data_preprocess(pd_test_ori)

    pd_train = pd_train.drop(['PassengerId'], axis=1)
    pd_test = pd_test.drop(['PassengerId'], axis=1)

    #完成数据清理
    print(pd_train)
    pd_train.to_csv(data_path+'tmp.csv')

    #拆分训练集、验证集
    df_train = pd_train.sample(frac=0.7)
    rowlist = []
    for indexs in df_train.index:
        rowlist.append(indexs)
    df_val = pd_train.drop(rowlist, axis=0)

    #训练和验证时，拆分出分类标签
    train_label = df_train['Transported']
    val_label = df_val['Transported']
    df_train = df_train.drop('Transported', axis=1)
    df_val = df_val.drop('Transported', axis=1)

    #模型训练
    #xgb_train = xgb.DMatrix(df_train, train_label)
    #xgb_val = xgb.DMatrix(df_val)
    #xgb_test = xgb.DMatrix(pd_test)
    print('start training:'+"-" * 30)

    '''
    #原有的，我参照网上入门教学改的代码，评分只有0.77
    # 训练轮数
    num_round = 10

    # 训练过程中实时输出评估结果
    watchlist = [(xgb_train,'train'),(xgb_val,'test')]
    # 定义模型训练参数
    params = {
        "objective": "binary:logistic",
        "booster": "gbtree",
        "max_depth": 5
    }
    # 模型训练
    model = xgb.train(params = params,dtrain=xgb_train,num_boost_round = num_round)

    #模型验证
    preds = model.predict(xgb_val)
    preds_test = model.predict(xgb_test)
    '''
    #深度之眼的参考代码
    # Parameter Tuning
    model = xgb.XGBClassifier()
    param_dist = {"max_depth": [10, 30, 50],
                  "min_child_weight": [1, 3, 6],
                  "n_estimators": [200],
                  "learning_rate": [0.05, 0.1, 0.16], }
    grid_search = GridSearchCV(model, param_grid=param_dist, cv=3,
                               verbose=10, n_jobs=-1)
    #训练模型
    grid_search.fit(df_train, train_label)

    grid_search.best_estimator_

    model = xgb.XGBClassifier(max_depth=3, min_child_weight=1, n_estimators=20, n_jobs=-1, verbose=1, learning_rate=0.16)
    model.fit(df_train, train_label)


    print("=" * 30)
    bi_scaler = Binarizer(threshold=0.5)
    preds = model.predict(df_val)
    preds_test = model.predict(pd_test)
    preds = bi_scaler.fit_transform(preds.reshape(-1, 1))
    preds = preds.flatten()
    preds_test = bi_scaler.fit_transform(preds_test.reshape(-1, 1))
    preds_test = preds_test.flatten()
    # preds = binarizer(preds)


    #print(val_label)
    print("=" * 30)
    print(auc(model, df_train, df_val, train_label, val_label))
    score = accuracy_score(val_label, preds)
    print(score)
    #打印混淆矩阵
    print(confusion_matrix(val_label, preds))


    #确认结果格式并输出最终csv
    preds_test = preds_test.astype(bool)
    #print(preds_test)
    #seri_preds_test = pd.Series(preds_test,index=['Transported'])
    #print('pd_test_ori len=' + str(len(pd_test_ori)))
    #print('preds_test len='+str(len(preds_test)))
    pd_test_ori.insert(loc=1,column='Transported', value=False)
    for i in range(0,len(pd_test_ori)):
        pd_test_ori.loc[i,'Transported'] = preds_test[i]
    #pd_test_ori = pd_test_ori.join(seri_preds_test)
    #print(pd_test_ori)
    pd_test_ori = pd_test_ori.drop(['HomePlanet','CryoSleep','Cabin','Destination','Age','VIP','RoomService','FoodCourt','ShoppingMall','Spa','VRDeck','Name'], axis=1)
    pd_test_ori.to_csv(data_path + 'result.csv',index=False)









