import pytesseract
from PIL import Image
import cv2
import numpy as np
import easyocr
import time
from PIL import ImageEnhance
from paddleocr import PaddleOCR
# 引用 paddle inference 预测库
import paddle.inference
import os

import threading
#from cnocr import CnOcr

def contrast_brightness_demo(image, c, b):            # 定义方法， c @ contrast  对比度 ; b @ brightness 亮度
    h, w, ch = image.shape
    blank = np.zeros([h, w, ch], image.dtype)         # 定义一张空白图像
    dst = cv2.addWeighted(image, c, blank, 1-c, b)     # 设定权重
    #cv.imshow("con-bri-demo", dst)
    return dst

def ocr_detect(lst_img):
    t0 = time.time()
    for i in range(0,len(lst_img)):
        #result = ocr_reader.readtext(img, detail=0)
        result = ocr.ocr(lst_img[i], cls=False)
        print(result)
    t1 = time.time()
    print(t1 - t0)

'''
def cnocr_detect(lst_img):
    t0 = time.time()
    for i in range(0,len(lst_img)):
        #result = ocr_reader.readtext(img, detail=0)
        result = Cnocr.ocr(lst_img[i])
        print(result)
    t1 = time.time()
    print(t1 - t0)
'''
def get_text_from_paddleocr_record(paddleocr_record):
    text =''
    for ele in paddleocr_record:
        #print(ele[1][0])
        text += ele[1][0]
    #print(text)
    return text

if __name__ == '__main__':
    '''
    txt='124,00mhz'
    txt2=txt.replace(',', '.').upper()
    print(txt2)
    exit()
    '''

    img_file = "orc_test.jpg"
    img_file2 = "orc_test2.jpg"

    img = cv2.imread(img_file)
    img2 = cv2.imread(img_file2)
    #img = img[int(img.shape[0]*0.25):int(img.shape[0]*0.75),int(img.shape[1]*0.25):int(img.shape[1]*0.75)]
    #img2 = img2[int(img2.shape[0] * 0.25):int(img2.shape[0] * 0.75), int(img2.shape[1] * 0.25):int(img2.shape[1] * 0.75)]
    #img = Image.open(img_file)

    ocr_reader = easyocr.Reader(['en'], gpu=True)# , detect_network = 'dbnet18')  # need to run only once to load model into memory
    '''
    # 创建 config
    config = paddle.inference.Config("./mobilenet_v1")
    # 启用 GPU 进行预测 - 初始化 GPU 显存 100M, Deivce_ID 为 0
    config.enable_use_gpu(100, 0)
    # 启用 TensorRT 进行预测加速 - Int8

    config.enable_tensorrt_engine(workspace_size=1 << 30,
                                  max_batch_size=1,
                                  min_subgraph_size=1,
                                  precision_mode=paddle.inference.PrecisionType.Int8,
                                  use_static=False, use_calib_mode=True)
    # 设置 TensorRT 的动态 Shape
    config.set_trt_dynamic_shape_info(min_input_shape={"image": [1, 1, 3, 3]},
                                      max_input_shape={"image": [480, 480, 10, 10]},
                                      optim_input_shape={"image": [100, 120, 3, 3]})
    '''

    ocr = PaddleOCR(lang="en",use_onnx=False,use_gpu=True,warmup=True,enable_mkldnn=True,use_angle_cls=False,
                    rec_model_dir='./en_PP-OCRv3_rec_infer/',
                    cls_model_dir='./en_ppocr_mobile_v2.0_cls_infer/',
                    #det_model_dir='./en_PP-OCRv3_det_infer/'
                    det_model_dir='./en_PP-OCRv3_det_slim_infer/')
    #Cnocr = CnOcr()
    #img = contrast_brightness_demo(img, 3, 3)
    #二值化
    '''
    img = cv2.pyrMeanShiftFiltering(img, sp=2, sr=60)
    img = cv2.medianBlur(img, 5)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    '''
    #img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    lst_img= []
    for i in range(0,100):
        lst_img.append(img)

    lst_img2= []
    img_list = os.listdir('images_test2/')
    for file in img_list:
        lst_img2.append(file)
    '''
    #返回二值化阈值ret和二值化后图片binary
    #ret, binary = cv2.threshold(gray, 0, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C)
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    '''
    #cv2.imshow('binary', img)
    #cv2.waitKey(0)

    #text = pytesseract.image_to_string(Image.fromarray(np.asarray(img)), lang="eng")
    # 如果你想试试Tesseract识别中文，只需要将代码中的eng改为chi_sim即可
    #print(text)
    '''
    t1 = threading.Thread(target=ocr_detect, args=(lst_img,))
    t2 = threading.Thread(target=ocr_detect, args=(lst_img2,))
    t1.start()
    t2.start()

    result = Cnocr.ocr(img, cls=True)
    t0 = time.time()
    cnocr_detect(lst_img)
    cnocr_detect(lst_img2)
    t1 = time.time()
    print(t1 - t0)
    '''

    t0 = time.time()
    #text = ocr_reader.readtext(img, detail=0)
    result = ocr.ocr(img, cls=True)
    t1 = time.time()
    text = get_text_from_paddleocr_record(result)
    #print(text)
    #print(t1 - t0)
    t0 = time.time()
    # 设置识别中英文两种语言
    for i in range(0,len(lst_img2)):
        #text = ocr_reader.readtext('images_test/'+lst_img2[i], detail=0)
        #text = ocr.ocr(lst_img2[i], cls=True)
        result = ocr.ocr('images_test2/'+lst_img2[i], cls=True)
        text = get_text_from_paddleocr_record(result)
        print(text)
    t1 = time.time()
    print(t1 - t0)



