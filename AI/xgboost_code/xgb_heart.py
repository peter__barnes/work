# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.preprocessing import Binarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import accuracy_score



df = pd.read_csv('heart_failure_clinical_records_dataset.csv')
#realdata = df.loc[0:20,['age','anaemia','diabetes','high_blood_pressure','sex','smoking','DEATH_EVENT']]
#realdata['age']=round(realdata['age']/20) 
#realdata['age']=realdata['age'].astype(int);
#realdata = df.loc[0:20,['diabetes','sex','smoking','DEATH_EVENT']]

# 定义模型训练参数
params = {
    "objective":"binary:logistic",
    "booster":"gbtree",
    "max_depth":3
}


df_train = df.sample(frac=0.6)
rowlist=[]
for indexs in df_train.index:
	rowlist.append(indexs)
df_test = df.drop(rowlist,axis=0)

train_label = df_train['DEATH_EVENT']
test_label = df_test['DEATH_EVENT']
df_train = df_train.drop('DEATH_EVENT',axis=1)
df_test = df_test.drop('DEATH_EVENT',axis=1)


print(test_label)
print(df_test)

xgb_train = xgb.DMatrix(df_train,train_label)
xgb_test = xgb.DMatrix(df_test)
print("-"*30)
#df_train = df_train.loc[:,['platelets', 'creatinine_phosphokinase', 'high_blood_pressure','diabetes','sex','smoking','DEATH_EVENT']]

# 训练轮数
num_round = 5

# 训练过程中实时输出评估结果
#watchlist = [(xgb_train,'train'),(xgb_test,'test')]

# 模型训练
model = xgb.train(params = params,dtrain=xgb_train,num_boost_round = num_round)

preds = model.predict(xgb_test)
print(preds)

print("="*30)
bi_scaler = Binarizer(threshold=0.5)
preds = bi_scaler.fit_transform(preds.reshape(-1, 1))
preds = preds.flatten()
#preds = binarizer(preds)
print(preds)

print(test_label)

print("="*30)
score =accuracy_score(test_label, preds)
print(score)

