import torch
from torch2trt import torch2trt

import sys
sys.path.insert(0, 'D:/code/framework/yolov5/')
from models.yolo import Model

model = Model("yolov5x-detect-bi.yaml").to("cuda")
ckpt=torch.load('BHJ_best.pt')
model.load_state_dict(ckpt['state_dict'])
#model = torch.load('BHJ_best.pt')
model.eval()

input_names = ['input']
output_names = ['output']

x = torch.randn(1,3,32,32,requires_grad=True)

torch.onnx.export(model, x, 'BHJ_best.onnx', input_names=input_names,
                  output_names=output_names, verbose='True')