'''
import torch
from torch2trt import torch2trt
from torch2trt import TRTModule
import datetime

model = torch.load('BHJ_best.pt').cuda()

trx = torch.ones((1, 3, 608, 608)).cuda()

model_trt = torch2trt(model, [trx])
torch.save(model_trt.state_dict(), 'best_trt.pth')

model_trt = TRTModule()

model_trt.load_state_dict(torch.load('best_trt.pth'))

y_trt = model_trt(trx)


'''
import torch
from torch2trt import torch2trt
from torchvision.models.alexnet import alexnet
import sys
sys.path.insert(0, 'D:/code/framework/yolov5/')
from models.yolo import Model

#obj={key:obj[key].cuda() for key in obj}
model = torch.load('BHJ_best.pt')

trx = torch.ones((1, 3, 608, 608)).cuda()

model_trt = torch2trt(model, [trx])
torch.save(model_trt.state_dict(), 'best_trt.pth')


# create some regular pytorch model...
#torch.load('alexnet_trt.pth')
#model = alexnet(pretrained=True).eval().cuda()
ckpt = Model("yolov5x-detect-bi.yaml").to("cuda")


ckpt.load_state_dict(torch.load("BHJ_best.pt"))
#ckpt = torch.load('BHJ_best.pt').cuda()

# create example data
x = torch.ones((1, 3, 224, 224)).cuda()

# convert to TensorRT feeding sample data as input
model_trt = torch2trt(ckpt, [x])

torch.save(model_trt.state_dict(), 'alexnet_trt.pth')
