#!/usr/bin/env python
#-*-coding:UTF-8-*

datasets = {'banala':{'long':400,'not_long':100,'sweet':350,'not_sweet':150,'yellow':450,'not_yellow':50},
            'orange':{'long':0,'not_long':300,'sweet':150,'not_sweet':150,'yellow':300,'not_yellow':0},
            'other_fruit':{'long':100,'not_long':100,'sweet':150,'not_sweet':50,'yellow':50,'not_yellow':150}
}
 
 
def count_total(data):
    count = {}
    total = 0
    for fruit in data:
        
        count[fruit] = data[fruit]['sweet'] + data[fruit]['not_sweet']
        total += count[fruit]
    return count,total
 
#categories,simpleTotal = count_total(datasets)
#print(categories,simpleTotal)
###########################################################
 
def cal_base_rates(data):
    categories,total = count_total(data)
    cal_base_rates = {}
    for label in categories:
        priori_prob = categories[label]/total
        cal_base_rates[label] = priori_prob
    return cal_base_rates
 
#Prio = cal_base_rates(datasets)
#print(Prio)
############################################################
 
def likelihold_prob(data):

    count,_ = count_total(data)
    likelihold = {}
    for fruit in data:
        attr_prob = {}
        for attr in data[fruit]:
            attr_prob[attr] = data[fruit][attr]/count[fruit]
        likelihold[fruit] = attr_prob
    return likelihold
 
#LikeHold = likelihold_prob(datasets)
#print(LikeHold)
############################################################
 
def evidence_prob(data):

    attrs = list(data['banala'].keys())
    count,total  = count_total(data)
    evidence_prob = {}
 
    for attr in attrs:
        attr_total = 0
        for fruit in data:
            attr_total += data[fruit][attr]
        evidence_prob[attr] = attr_total/total
    return evidence_prob
 
 
class navie_bayes_classifier:
    def __init__(self,data=datasets):
        self._data = datasets
        self._labels = [key for key in self._data.keys()]
        self._priori_prob = cal_base_rates(self._data)
        self._likelihold_prob = likelihold_prob(self._data)
        self._evidence_prob = evidence_prob(self._data)
 
    def get_label(self,length,sweetness,color):
        self._attrs = [length,sweetness,color]
        res = {}
        for label in self._labels:
            prob = self._priori_prob[label]
            for attr in self._attrs:
                prob*=self._likelihold_prob[label][attr]/self._evidence_prob[attr]
            res[label] = prob
        return res


'''
#test_datasets = generate_attires.gen_attrs()
test_datasets=[ ['not_long', 'not_sweet', 'not_yellow'],
            ['not_long', 'sweet', 'not_yellow'],
            ['not_long', 'sweet', 'yellow']
]
classfier = navie_bayes_classifier()
for data in test_datasets:
    print("para",end='\t')
    print(data)
    print("predicttyp", end='\t')
    res=classfier.get_label(*data)
    print(res)
    print('realtype',end='\t')
    #print(sorted(res.items(),key=operator.itemgetter(1),reverse=True)[0][0])
    '''
import pandas as pd
import numpy as np

def cal_dead_alive_num(data_all, col_name, col_val):
    P_col_a = pd.Series({0:1.0, 1:1.0})
    #dead total number
    data_dead = data_all[data_all['DEATH_EVENT'] == 1]
    #alive total number
    data_alive = data_all[data_all['DEATH_EVENT'] == 0]
    
    summary = data_all['DEATH_EVENT'].value_counts()
    P_is_alive = pd.Series({0:0.0, 1:0.0})
    for i,v in summary.items():
        P_is_alive[i] = v
    #Laplace smoothing
    for i in range(0,2):
        if(P_is_alive[i]<1.0e-5):
            P_is_alive[i]=1
    
    summary = data_alive[col_name].value_counts()
    for i,v in summary.items():
        if(i==col_val):
            P_col_a[0] = v / P_is_alive[0]
            
    summary = data_dead[col_name].value_counts()
    for i,v in summary.items():
        if(i==col_val):
            P_col_a[1] = v / P_is_alive[1]
            
    #Laplace smoothing
    for i in range(0,2):
        if(P_col_a[i]<1.0e-5):
            P_col_a[i]=1/P_is_alive[i]
    return P_col_a



df = pd.read_csv('heart_failure_clinical_records_dataset.csv')
#realdata = df.loc[0:20,['age','anaemia','diabetes','high_blood_pressure','sex','smoking','DEATH_EVENT']]
#realdata['age']=round(realdata['age']/20) 
#realdata['age']=realdata['age'].astype(int);
#realdata = df.loc[0:20,['diabetes','sex','smoking','DEATH_EVENT']]

df_train = df.sample(frac=0.6)
rowlist=[]
for indexs in df_train.index:
	rowlist.append(indexs)
df_test=df.drop(rowlist,axis=0)
df_train = df_train.loc[:,['platelets', 'creatinine_phosphokinase', 'high_blood_pressure','diabetes','sex','smoking','DEATH_EVENT']]

def reset_creatinine_phosphokinase(x):
    if x>1000:
        return round(11 + x/1000)
    return round(x/100)
df_train['creatinine_phosphokinase'] = df_train['creatinine_phosphokinase'].apply(lambda x: reset_creatinine_phosphokinase(x))
df_train['creatinine_phosphokinase']=df_train['creatinine_phosphokinase'].astype(int);


quantile = pd.Series(np.arange(4))
quantile[0] =df_train['platelets'].quantile(0.2)
quantile[1] =df_train['platelets'].quantile(0.4)
quantile[2] =df_train['platelets'].quantile(0.6)
quantile[3] =df_train['platelets'].quantile(0.8)
def reset_by_quantile(x, quant):
    if(x<quant[0]):
        return  0
    elif(x<quant[1]):
        return  1
    elif(x<quant[2]):
        return  2
    elif(x<quant[3]):
        return  3
    else:
        return 4

df_train['platelets']=df_train['platelets'].apply(lambda x: reset_by_quantile(x,quantile))
df_train['platelets']=df_train['platelets'].astype(int)
#print(df_train['platelets'])
#print('-'*30)

data_all = df_train;

summary = data_all['DEATH_EVENT'].value_counts()
P_is_alive = pd.Series({0:0.0, 1:0.0})
for i,v in summary.items():
    P_is_alive[i] = v / data_all.shape[0]
#Laplace smoothing
for i in range(0,2):
    if(P_is_alive[i]<1.0e-5):
        P_is_alive[i]=1/data_all.shape[0]


P_diabetes_a = cal_dead_alive_num(data_all, 'diabetes', 1)
P_sex_a = cal_dead_alive_num(data_all, 'sex', 1)
P_smoking_a = cal_dead_alive_num(data_all, 'smoking', 1)
p_high_blood_pressure_a = cal_dead_alive_num(data_all, 'high_blood_pressure', 1)

    
A = (P_is_alive[0] * P_diabetes_a[0] * P_sex_a[0] * P_smoking_a[0]*p_high_blood_pressure_a[0])
B=(P_is_alive[0] * P_diabetes_a[0] * P_sex_a[0] * P_smoking_a[0]*p_high_blood_pressure_a[0])    \
    + (P_is_alive[1] * P_diabetes_a[1] * P_sex_a[1] * P_smoking_a[1]*p_high_blood_pressure_a[1])

print(P_is_alive)
#print(P_diabetes_a)
#print(P_sex_a)
#print(P_smoking_a)

P_alive_ret=A/B
print('-'*30)
print(P_alive_ret)












'''

data_dead = realdata[data_all['DEATH_EVENT'] == 1]
data_alive = realdata[data_all['DEATH_EVENT'] == 0]

summary = data_all['DEATH_EVENT'].value_counts()
P_is_alive = pd.Series({0:1, 1:1})
for i,v in summary.items():
    P_is_alive[i] = v

P_diabetes_a = pd.Series({0:1, 1:1})
summary = data_alive['diabetes'].value_counts()
for i,v in summary.items():
    if(i==1):
        P_diabetes_a[0] = v
summary = data_dead['diabetes'].value_counts()
for i,v in summary.items():
    if(i==1):
        P_diabetes_a[1] = v

P_sex_a = pd.Series({0:1, 1:1})
summary = data_alive['sex'].value_counts()
for i,v in summary.items():
    if(i==0):
        P_sex_a[0] = v
summary = data_dead['sex'].value_counts()
for i,v in summary.items():
    if(i==0):
        P_sex_a[1] = v

P_smoking_a = pd.Series({0:1, 1:1})
summary = data_alive['smoking'].value_counts()
for i,v in summary.items():
    if(i==1):
        P_smoking_a[0] = v
summary = data_dead['smoking'].value_counts()
for i,v in summary.items():
    if(i==1):
        P_smoking_a[1] = v

print(P_is_alive[0])
print(P_diabetes_a[0])
print(P_sex_a[0])
print(P_smoking_a[0])
print(P_is_alive[1])
print(P_diabetes_a[1])
print(P_sex_a[1])
print(P_smoking_a[1])

A = (P_is_alive[0] * P_diabetes_a[0] * P_sex_a[0] * P_smoking_a[0])
B=(P_is_alive[0] * P_diabetes_a[0] * P_sex_a[0] * P_smoking_a[0]) + (P_is_alive[1] * P_diabetes_a[1] * P_sex_a[1] * P_smoking_a[1])

P_alive_ret=A/B
print(P_alive_ret)

'''
