# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

#参考《特征工程入门与实践》一书，5.2 创建基准机器学习流水线：使用sklearn的4种模型（KNN、决策树、随机森林、逻辑回归）进行表格数据分类
#数据集可以用原书的信用卡违约credit_card_default.csv，也可以用heart_failure_clinical_records_dataset.csv，但心脏病预测效果不及xgboost
# 准度不高： 预处理中，写了PCA主成分分析函数，但未使用其输出数据；没有进行归一化 缩放数据
import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
# scikit-learn的PCA主成分分析模块
from sklearn.decomposition import PCA

# 导入网格搜索模块
from sklearn.model_selection import GridSearchCV
def get_best_model_and_accuracy(model, params, X, y):
    grid = GridSearchCV(model, params, error_score=0.)
    grid.fit(X, y) # 拟合模型和参数
    # 经典的性能指标
    print("Best Accuracy: {}".format(grid.best_score_))
    # 得到最佳准确率的最佳参数
    print("Best Parameters: {}".format(grid.best_params_))
    # 拟合的平均时间（秒）
    print("Average Time to Fit (s):{}".format(round(grid.cv_results_['mean_fit_time'].mean(), 3)))
    # 预测的平均时间（秒）
    # 从该指标可以看出模型在真实世界的性能
    print("Average Time to Score (s):{}".format(round(grid.cv_results_['mean_score_time'].mean(), 3)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # 用随机数种子保证随机数永远一致
    np.random.seed(123)

    # sep参数用于修改打印长度，防止打印的内容被省略号替换
    # 显示所有列
    pd.set_option('display.max_columns', None)
    # 显示所有行
    pd.set_option('display.max_rows', None)

    # 导入数据集 http://archive.ics.uci.edu/ml/datasets/default+of+credit+card+clients
    #新版本已经变成xls格式，可以用excel到处为csv，再删掉第一行冗余信息
    #credit_card_default = pd.read_csv('./credit_card_default.csv',index_col=0)
    credit_card_default = pd.read_csv('../heart_failure_clinical_records_dataset.csv', index_col=0)
    #credit_card_default.set_index(0)
    print(credit_card_default.shape)

    #print(credit_card_default)
    # 描述性统计
    # 调用.T方法进行转置，以便更好地观察
    print('describe().T----------------------------------------')
    print(credit_card_default.describe().T)
    print('isnull().sum()----------------------------------------')
    print(credit_card_default.isnull().sum())
    print('corr----------------------------------------')
    print(credit_card_default.corr())
    plot = sns.heatmap(credit_card_default.corr())
    plt.show()

    #X = credit_card_default.drop('default payment next month', axis=1)
    #y = credit_card_default['default payment next month']
    X = credit_card_default.drop('DEATH_EVENT', axis=1)
    y = credit_card_default['DEATH_EVENT']

    # pca主成分分析
    pca = PCA(n_components=5)
    # 在数据上使用PCA
    pca.fit(X)
    x_pca = pca.transform(X)[:5, ]
    print('x_pca----------------------------------------')
    print(x_pca)


    print('y.value_counts----------------------------------------')
    # 取空准确率
    print(y.value_counts(normalize=True))

    # 为网格搜索设置变量
    # 先设置机器学习模型的参数
    # 逻辑回归
    lr_params = {'C': [1e-1, 1e0, 1e1, 1e2], 'penalty': ['l1', 'l2']}
    # KNN
    knn_params = {'n_neighbors': [1, 3, 5, 7]}
    # 决策树
    tree_params = {'max_depth': [None, 1, 3, 5, 7]}
    # 随机森林
    forest_params = {'n_estimators': [10, 50, 100], 'max_depth': [None, 1, 3,
                                                                  5, 7]}

    # 实例化机器学习模型
    lr = LogisticRegression()
    knn = KNeighborsClassifier()
    d_tree = DecisionTreeClassifier()
    forest = RandomForestClassifier()

    #print(get_best_model_and_accuracy(lr, lr_params, X, y))
    # 为流水线设置KNN参数
    knn_pipe_params = {'classifier__{}'.format(k): v for k, v in knn_params.items()}
    # KNN需要标准化的参数
    knn_pipe = Pipeline([('scale', StandardScaler()), ('classifier', knn)])
    # 拟合快，预测慢
    get_best_model_and_accuracy(knn_pipe, knn_pipe_params, X, y)
    print('knn_pipe_params----------------------------------------')
    print(knn_pipe_params) # {'classifier__n_neighbors': [1, 3, 5, 7]}

    get_best_model_and_accuracy(d_tree, tree_params, X, y)
    print('tree_params----------------------------------------')
    print(tree_params)




