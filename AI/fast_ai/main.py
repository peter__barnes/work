# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from fastai.vision.all import *
from fastai.text.all import *
from fastai.collab import *
from fastai.tabular.all import *
import pandas as pd

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.

def is_cat(x): return x[0].isupper()

def run_cat():
    #path = untar_data(URLs.PETS) / 'images'
    path = 'E:/data/cats/' + 'images'

    dls = ImageDataLoaders.from_name_func(
        path, get_image_files(path), valid_pct=0.2, seed=42,
        label_func=is_cat, item_tfms=Resize(224))

    learn = vision_learner(dls, resnet34, metrics=error_rate)
    learn.fine_tune(1)

    learn.save('E:/data/cats/model.pt')
    # learn.load('E:/data/cats/model.pt')

    img = PILImage.create('E:/data/cats/cat001.jpg')
    is_cat, _, probs = learn.predict(img)
    print(f"Is cat001 a cat?: {is_cat}.")
    print(f"Probability it's a cat: {probs[1].item():.6f}")

    img = PILImage.create('E:/data/cats/cat002.jpg')
    is_cat, _, probs = learn.predict(img)
    print(f"Is cat002 a cat?: {is_cat}.")
    print(f"Probability it's a cat: {probs[1].item():.6f}")

    img = PILImage.create('E:/data/cats/dog.jpg')
    is_cat, _, probs = learn.predict(img)
    print(f"Is dog a cat?: {is_cat}.")
    print(f"Probability it's a cat: {probs[1].item():.6f}")

def run_salary():
    path = untar_data(URLs.ADULT_SAMPLE)

    '''
    dls = TabularDataLoaders.from_csv(path / 'adult.csv', path=path, y_names="salary",
                                      cat_names=['workclass', 'education', 'marital-status', 'occupation',
                                                 'relationship', 'race'],
                                      cont_names=['age', 'fnlwgt', 'education-num'],
                                      procs=[Categorify, FillMissing, Normalize])
    '''
    #pd_adult = pd.read_csv(path / 'adult.csv', skipinitialspace=True)
    #pd_adult.to_csv('E:/data/cats/adult.csv')
    pd_adult = pd.read_csv('E:/data/cats/adult.csv', skipinitialspace=True)
    pd_adult_train = pd_adult.iloc[0:-3]
    pd_adult_test = pd_adult.iloc[-3:]
    print(type(pd_adult_test))
    print(len(pd_adult_test))
    dls = TabularDataLoaders.from_df(pd_adult_train, path=path, y_names="salary",at_names=['workclass', 'education', 'marital-status', 'occupation',
                                                 'relationship', 'race'],
                                      cont_names=['age', 'fnlwgt', 'education-num'],
                                      procs=[Categorify, FillMissing, Normalize])

    learn = tabular_learner(dls, metrics=accuracy)
    learn.fit_one_cycle(5)
    #row = pd.Series('salary')
    for i in range(0, len(pd_adult_test)):
        print(type(pd_adult_test.iloc[i]))
        salary, _, probs = learn.predict(pd_adult_test.iloc[i])
        print(salary)
        #print(probs)
    #print(f"salary:{salary}, Probability: {probs[1].item():.6f}")

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #print_hi('PyCharm')
    run_salary()



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
