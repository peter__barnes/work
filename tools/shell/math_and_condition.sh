#/bin/sh
a=1
b=2
c=$a+$b
d=`expr 1 + 2`
e=`expr $a + $b`
echo "$c"
echo $d
echo $e
if [ $d>$a ] #space in this line should't be removed
then  
    echo "d > a"
elif [ $d==$a ]
then
    echo "d = a"
else           #else do not need follow a then
    echo "d < a"
fi

#for  1-10
for i in `seq 1 10`  
do  
	if [ $i == 9 ]
	then
		break
	fi
	echo $i  
done  
