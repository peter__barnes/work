echo "[origin]"
last -n 5

echo ""
echo "[all text]"
last -n 5 | awk '{print $0}' # $0=all line

echo ""
echo "[1st 3rd column]"
last -n 5 | awk '{print $1 "\t" $3}' 
