使用valgrind，之前gcc编译程序时需要加-g参数

以下命令可以检测程序运行中的内存溢出和内存泄漏，其中泄漏只标明量，而溢出会标明出现的位置
valgrind <program_name>                                             
加上具体参数可以获得内存溢出和泄漏的具体位置（./a.out替换程序名）
valgrind --tool=memcheck --leak-check=full ./a.out
如果程序是死循环如QT gtk，退出时想查未释放空间，可以终止程序，然后直接ps aux|grep valgrind查pid，kill pid终止valgrind，这时valgrind退出时会报告剩余信息。

example leak.c：
4    void f(void)
5    {
6        int* x = malloc(10 * sizeof(int));
7        x[10] = 0;  //问题1: 数组下标越界
8    }                  //问题2: 内存没有释放int main(void)
9    int main()
10  {
11    f();
12    return 0;
13  }
gcc -g3 leak.c
valgrind --tool=memcheck --leak-check=full ./a.out
Invalid write of size 4                                  //列出内存溢出错误（问题1,在第7行数组越界）出现位置，并像gdb中bt一样把函数调用的栈也打印出来
==11557==    at 0x4004FA: f (wrong_code.c:7)                
==11557==    by 0x40050A: main (wrong_code.c:11)
40 bytes in 1 blocks are definitely lost in loss record 1 of 1  //列出内存泄漏错误（问题2,在第6行分配空间）出现位置，并像gdb中bt一样把函数调用的栈也打印出来
==11467==    at 0x4A0884D: malloc (vg_replace_malloc.c:263)
==11467==    by 0x4004ED: f (wrong_code.c:6)
==11467==    by 0x40050A: main (wrong_code.c:11)




Useful options
Issuing "valgrind --help" will show an extensive list of options. You need to select

 --leak-check=yes 
to enable valgrind most useful functionality, namely leak checking.
Another very useful option is
 --num-callers=N
that specifies how many callers to show in the stack trace of a leak source or of an access error. The default N=4 appears to be insufficient to locate the source of many leaks. A more reasonable value could be
 --num-callers=8

The option
--gdb-attach=yes
will start the gdb debugger when an access error is detected. This is usually overkill but it allows to debug relatively quickly some of the thougher problems.

Last but not least, programmers who really care about the quality of their codes should add
 --show-reachable=yes


Memcheck is inability to detect all cases of bounds errors in the use of static or stack allocated data.
example (can not detect by valgrind):
    int Stack[5];
    Stack [5] = 0;  /* Error - Stack[0] to  Stack[4] exist,  Stack[5] is out of bounds */
example (can detect by valgrind):
    int i, Stack[5];
    for (i = 0; i <= 5; i++)
        Stack [i] = 0;        /* Within bounds for i=0..4, out of bounds error when i=5 */