大多数USB声卡/DAC在Linux下是即插即用的，如果不能用
1.尝试更换声卡优先级。KDE界面开始菜单，system settings->multimedia->phonon,将外接设备优先级调高，并且在audio hardware step选中外接设备、按Front Left/Right测试
2.如果更换设备以后，系统还自动把人工的设置覆盖了，这时会提升是否需要再把系统设置覆盖掉（revert to ...），按下即可复原人工设置。至此乐之邦 MD10 DAC已经可以工作
3.如果以上2步不奏效，可以在KDE右下方的喇叭按钮上左键->mixer,选择外接卡。
4.如果以上3步不奏效，可以尝试安装所有alsa工具（yum install alsa*）,运行命令lsmod和modprobes加载modules（内核自带的驱动有可能默认有些没启动）
5.如果以上4部不奏效，可以查看芯片型号，将特定驱动安装

PS:不是所有音乐软件都和系统默认一致，例如amarok默认与系统一致，但是专业的音乐播放器 audicious却是默认使用板载声卡。
audicious使用外接设备设置如下：文件(file)->首选项(preferences)->audio,选择output plugin栏的preferences，PCM device和mixer device都设置成外接设备