1.解压gflags免安装版，把gflags所在文件夹（这里边还有很多好东西）设置到环境变量的path中，方便命令行使用。
    （我的电脑-->右键-->属性-->高级-->环境变量-->系统变量-->path）
2.开启检测开关（貌似会更改注册表）
    使用windows工具gflags.exe
    开启 gflags.exe –p /enable yourexecutable.exe /full
    关闭 gflags.exe -p /disable yourexecutable.exe
    注释：上述命令需要在cmd中执行
    如果报错“gflags.exe 不是内部命令”：找到该程序 添加到系统环境变量下，或者用全路径，cmd重启执行命令
3.然后再重启vs ，运行/调试源码，就能找到内存越界的地方（越界时立即产生中断）