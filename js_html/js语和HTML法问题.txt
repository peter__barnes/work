js中取表单：  document.form名
表单中取控件属性： form名.控件名.属性名
表单的某种操作：document.form名.操作名（如：document.form001.submit()）;
event.srcElement.value取表单中触发事件的元素的值


默认的关键字click（）、onclick（）最好都不要用，容易和默认项冲突
在需要中断的地方写入return语句，否则有可能因为继续执行导致alert等方法无法生效。


HTML用\转义