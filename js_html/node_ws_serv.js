var fs= require('fs');
var sleep = require('sleep');
var ws_server = require('ws').Server ; 

wss = new ws_server({ port: 8089});
wss.binaryType = 'arraybuffer';
 
wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
            console.log('received: %s', message);
            });

//    var readable = fs.createReadStream("1.txt");
    var readable = fs.createReadStream("rec_long_hp_dashinit.mp4");
    readable.on('data', (chunk) => {
            //console.log(`Received ${chunk.length} bytes of data.`);
            ws.send(chunk);
            sleep.usleep(30000);
            });
});
