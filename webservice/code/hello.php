<html>
 <head>
  <title>PHP 测试</title>
 </head>
 <body>
 <?php echo '<p>Hello World</p>'; ?> 
    <?php

        header("Content-Type: text/html; charset=utf-8");
        echo '1';
        $client = new SoapClient("http://localhost:2881/helloService?wsdl");
        $client->soap_defencoding = 'utf-8';
        $client->xml_encoding = 'utf-8';  
        echo '2';
        $result = $client->__soapCall("sayHello", array(array('arg0'=>" ok!")));
        $result2 = $client->__soapCall("sayHello", array(array('arg0'=>" ok!")));
        $result3 = $client->__soapCall("printInt", array(array('arg0'=>100)));
        //if the function called do not have any input argument, let array() as the 2nd argument of __soapCall,but can't omit 2nd argument in __soapCall
        //see php manual:
         //public mixed SoapClient::__soapCall ( string $function_name , array $arguments [, array $options [, mixed $input_headers [, array &$output_headers ]]] )
         //example(wrong,illegal):    $client->__soapCall("sayHello");         
        //example(legal):    $client->__soapCall("sayHello",array());         
        echo '3';
        echo "<pre>";
        print_r($result);
        print_r($result2->return);
        print_r($result3->return);
        echo "</pre>";
    ?>

 </body>
</html>

