package com.yjpeng.hello;


import javax.jws.WebService;  
import javax.jws.WebMethod;
import javax.xml.ws.Endpoint;

@WebService
public class HelloService {

	@WebMethod
	public String sayHello(String message) {
		return "Hello ," + message+"\n";
	}
	@WebMethod
	public String printInt(int i){
		return "int:" + Integer.toString(i)+"\n";
	}

	public static void main(String[] args) {
		// create and publish an endPoint
		HelloService hello = new HelloService();
                //replace localhost with real ip,or can't be find in web,can't bind a ip not belong to this pc 
		Endpoint endPoint = Endpoint.publish(
				"http://localhost:2881/helloService", hello);
	}
}