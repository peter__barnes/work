#include "soapH.h"
#include "add.nsmap"

int main()
{
    struct soap soap;
    int master, slave;
    soap_init(&soap);
    master = soap_bind(&soap, "127.0.0.1", 18888, 100);
    if(master < 0)
        soap_print_fault(&soap, stderr);
    else
    {
        sprintf(stderr, "Socket connection successful:master socket = %d ", master);

        while(1)
        {
            slave = soap_accept(&soap);
            if(slave < 0)
            {
                soap_print_fault(&soap, stderr);
                break;
            }
            sprintf(stderr, "accepted connection IP=%d.%d.%d.%d socket=%d",(soap.ip >> 24)&0xFF, (soap.ip >> 16)&0xFF, (soap.ip >> 8)&0xFF, soap.ip&0xFF, slave);
            if(soap_serve(&soap) != SOAP_OK)
                soap_print_fault(&soap, stderr);
            fprintf(stderr, "request served ");
            soap_destroy(&soap);
            soap_end(&soap);
        }
    }
    soap_done(&soap);
}

int ns__add(struct soap *soap, double a, double b, double *result)
{
    *result = a + b;
    return SOAP_OK;
}
