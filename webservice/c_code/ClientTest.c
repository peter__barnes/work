#include "soapH.h"
#include "add.nsmap"

const char server[] = "http://localhost:18888";

int main(int argc, char **argv)
{
    struct soap soap;
    double a, b, result;
    soap_init1(&soap, SOAP_XML_INDENT);
    a = strtod(argv[1], NULL);
    b = strtod(argv[2], NULL);

    soap_call_ns__add(&soap, server, "", a, b, &result);
    if(soap.error)
    {
        soap_print_fault(&soap, stderr);
        exit(1);
    }
    else
        printf("result = %g ", result);
    soap_destroy(&soap);
    soap_end(&soap);
    soap_done(&soap);
    return 0;
}
