安装gsoap：
解压，依次执行./configure,make,make install
中途可能出现报错：error: invalid conversion from 'const X509V3_EXT_METHOD*' to 'X509V3_EXT_METHOD*'
找到对应位置的代码 gsoap/stdsoap2_cpp.cpp  4002：X509V3_EXT_METHOD* meth = X509V3_EXT_get(extension);
将上面那行替换为：X509V3_EXT_METHOD *meth = (X509V3_EXT_METHOD *) X509V3_EXT_get(ext);
继续编译。
将gsoap/stdsoap2.c拷贝到/usr/local/share/gsoap/stdsoap2.c

编译用例源码：
cc -o StandaloneAdd StandaloneAdd.c /usr/local/share/gsoap/stdsoap2.c soapC.c soapServer.c
cc -o ClientTest ClientTest.c /usr/local/share/gsoap/stdsoap2.c soapC.c soapClient.c
这两行编译分别生成服务端和客户端的程序，客户端需要两个参数，服务端返回的结果为两数相加之和，客户端将值打印出来。