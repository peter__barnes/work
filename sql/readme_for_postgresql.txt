postgresql
    install:
        sudo apt install postgresql postgresql-client
    login:
        sudo -u postgres psql

    run sql scripts :   
        sudo -u postgres psql -f initdb_stu.sql

// this sql file currently can't run in mysql
mysql 
    install:
        sudo apt-get install mysql-server
        sudo apt-get install mysql-client libmysqlclient-dev
        ps aux|grep mysqld
    login:
        mysql -u root -p[passwd]        [-h 127.0.0.1]
    run sql scripts :   
        source initdb_stu.sql           (after login)
        or
        mysql -u root -p[passwd]  <initdb_stu.sql

