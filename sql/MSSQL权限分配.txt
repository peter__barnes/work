用户登陆为空白修复:
use yourdatabase
exec sp_change_users_login 'Auto_Fix','youruser'

解决孤立用户问题的步骤
1. 为前一步中的孤立用户运行以下命令：
Use Northwind
go
sp_change_users_login 'update_one', 'test', 'test'
                        
这样,就将服务器登录“test”与 Northwind 数据库用户“test”重新连接起来.sp_change_users_login 存储过程还可以使用“auto_fix”参数对所有孤立用户执行更新,但不推荐这样做,因为 SQL Server 会尝试按名称匹配登录和用户.大多数情况下这都是可行的；但是,如果用户与错误登录关联,该用户可能拥有错误的权限. 
2. 在上一步中运行代码后,用户就可以访问数据库了.然后用户可以使用 sp_password 存储过程更改密码：
Use master
go
sp_password NULL, 'ok', 'test'
                        
此存储过程不能用于 Microsoft Windows NT 安全帐户.通过 Windows NT 网络帐户连接到 SQL Server 服务器的用户是由 Windows NT 授权的；因此,这些用户只能在 Windows NT 中更改密码. 

只有 sysadmin 角色的成员可以更改其他用户的登录密码.


如果导入/导出失败原因是因为不存在用户数据库里的用户,可以先临时创一个同名用户,导入后,将表所属改成***_f后再将临时用户删除掉
-------------
修改表的属主
exec sp_changeobjectowner  'ouruser.table1','dbo'

批量修改表的属主
exec sp_MSForEachTable 'sp_changeobjectowner "?", "dbo"'

收缩数据库
DBCC SHRINKDATABASE('dbname')


批量修改表、触发器、视图、存储过程的属主(需要先在master创建sp_MSforeachObject存储过程)
EXEc sp_MSforeachObject @command1="sp_changeobjectowner '?', 'dbo'",@objectType=1
EXEc sp_MSforeachObject @command1="sp_changeobjectowner '?', 'dbo'",@objectType=2
EXEc sp_MSforeachObject @command1="sp_changeobjectowner '?', 'dbo'",@objectType=3
EXEc sp_MSforeachObject @command1="sp_changeobjectowner '?', 'dbo'",@objectType=4


给所有的存储过程分配用户execute权限
EXEC sp_MSforeachObject @command1= "print '?'", @command2="GRANT EXECUTE ON ? TO test_f", @objectType=4

收回权限:
EXEC sp_MSforeachObject @command1= "print '?'", @command2="Revoke EXECUTE ON ? From test_f", @objectType=4




创建系统存储过程sp_MSforeachObject:
-----------------------
USE MASTER
GO
CREATE proc sp_MSforeachObject
 @objectType int=1,
 @command1 nvarchar(2000), 
 @replacechar nchar(1) = N'?', 
 @command2 nvarchar(2000) = null,
    @command3 nvarchar(2000) = null, 
 @whereand nvarchar(2000) = null,
 @precommand nvarchar(2000) = null, 
 @postcommand nvarchar(2000) = null
as
 /* This proc returns one or more rows for each table (optionally, matching @where), with each table defaulting to its 
own result set */
 /* @precommand and @postcommand may be used to force a single result set via a temp table. */
 /* Preprocessor won't replace within quotes so have to use str(). */
 declare @mscat nvarchar(12)
 select @mscat = ltrim(str(convert(int, 0x0002)))
 if (@precommand is not null)
  exec(@precommand)
 /* Defined  @isobject for save object type */
 Declare @isobject varchar(256)
 select @isobject= case @objectType when 1 then 'IsUserTable'
         when 2 then 'IsView'
         when 3 then 'IsTrigger'
         when 4 then 'IsProcedure' 
         when 5 then 'IsDefault'   
         when 6 then 'IsForeignKey'
         when 7 then 'IsScalarFunction'
         when 8 then 'IsInlineFunction'
         when 9 then 'IsPrimaryKey'
         when 10 then 'IsExtendedProc'    
         when 11 then 'IsReplProc'
         when 12 then 'IsRule'
                  end
 /* Create the select */
 /* Use @isobject variable isstead of IsUserTable string */
EXEC(N'declare hCForEach cursor global for select ''['' + REPLACE(user_name(uid), N'']'', N'']]'') + '']'' + ''.'' + ''['' + 
REPLACE(object_name(id), N'']'', N'']]'') + '']'' from dbo.sysobjects o '
        + N' where OBJECTPROPERTY(o.id, N'''+@isobject+''') = 1 '+N' and o.category & ' + @mscat + N' = 0 '
       + @whereand)
 declare @retval int
 select @retval = @@error
 if (@retval = 0)
  exec @retval = sp_MSforeach_worker @command1, @replacechar, @command2, @command3
 if (@retval = 0 and @postcommand is not null)
  exec(@postcommand)
 return @retval
GO

