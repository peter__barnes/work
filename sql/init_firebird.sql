CONNECT "./itc.fdb";
DROP DATABASE;
CREATE DATABASE "./itc.fdb";
CONNECT "./itc.fdb";

CREATE TABLE ITC_INFO(
	ITCTermCode	char(16)	NOT NULL PRIMARY KEY check(ITCTermCode	SIMILAR TO '[0-9]{16}'),
	NetRoadID	Smallint NOT NULL,	
	RoadID		Smallint NOT NULL,
	StationID		Integer   NOT NULL,	
	LaneID		Smallint NOT NULL,	
	LaneType		Smallint NOT NULL check(LaneType>0 AND LaneType<34),/*0x22 = 34*/	 
	Next_Local_ITCTermTradNo Bigint NOT NULL,
	Session_id  char(14) NOT NULL check(Session_id SIMILAR TO '[0-9]{14}'),
	DES_key char(32) DEFAULT '31313131313131315E5E5E5E5E5E5E5E' NOT NULL 
);

CREATE TABLE EntranceCarList(
	PlateNo		char(12)    NOT NULL PRIMARY KEY,
	PlateColor	Smallint    NOT NULL check( PlateColor>=0 AND PlateColor<10 ),
	CarType		Smallint    NOT NULL check( CarType>=1 AND CarType<=32 ),/*0x20 = 32*/	
	CustomType	char(1)    NOT NULL check(CustomType>='1' AND CustomType<='2' ),
	CardNo		char(20)    NOT NULL check(CardNo SIMILAR TO '[0-9]{16}    '), 
	CardBalance	Bigint NOT NULL,
	addDatetime Timestamp    NOT NULL
);

CREATE TABLE ExitCarList(
	PlateNo			char(12)    NOT NULL  PRIMARY KEY,	
	PlateColor		Smallint    NOT NULL check( PlateColor>=0 AND PlateColor<10 ), 
	CarType			Smallint   NOT NULL check( CarType>=1 AND CarType<=32 ), /*0x20 = 32*/	
	CustomType		char(1)    NOT NULL check(CustomType>='1' AND CustomType<='2' ),	
	CardNo			char(20)    NOT NULL check(CardNo SIMILAR TO '[0-9]{16}    '),	
	CardBalance		Bigint NOT NULL,	
	LaneEnSerialNo	char(16)    NOT NULL,
	EnNetRoadID		Smallint NOT NULL,	
	EnRoadID		Smallint NOT NULL,
	EnStationID		Integer    NOT NULL,	
	EnLaneID		Smallint NOT NULL,	
	EnLaneType		Smallint NOT NULL check(EnLaneType>0 AND EnLaneType<34),/*0x22 = 34*/	 
	EnTime			char(14) NOT NULL check(EnTime			SIMILAR TO '[0-9]{14}'),	
	EnOperatorID	char(12)	NOT NULL check(EnOperatorID	SIMILAR TO '[0-9]{12}'), 
	EnShiftID		char(8)   NOT NULL check(EnShiftID		SIMILAR TO '[0-9]{8}'),
	SquadDate		char(14)    NOT NULL check(SquadDate	SIMILAR TO '[0-9]{14}'),
	EnITCTermCode	char(16)	NOT NULL,	
	ENITCTermTradNo	Bigint NOT NULL ,
	EnVehicleFlag	Smallint NOT NULL check(EnVehicleFlag=0 OR EnVehicleFlag=1),
	OriginalPath	varchar(256)    NOT NULL,	
	addDatetime		Timestamp    NOT NULL	
);

CREATE TABLE BlackCarList(
	PlateNo		char(12)    NOT NULL  PRIMARY KEY,
	PlateColor	smallint   NOT NULL check( PlateColor>=0 AND PlateColor<10 ),
	CarType		smallint   NOT NULL check( CarType>=1 AND CarType<=32 ),/*0x20 = 32*/	
	CustomType	char(1)    NOT NULL check(CustomType>='1' AND CustomType<='2' ),
	addDatetime Timestamp    NOT NULL
);

CREATE TABLE LaneEntranceList(
	EnRecordNo		Smallint ,
	LaneEnSerialNo	char(16)	NOT NULL	,
	PlateNo			char(12)	NOT NULL	,
	PlateColor		smallint	NOT NULL	check( PlateColor>=0 AND PlateColor<10 ),
	CarType			smallint    NOT NULL	check( CarType>=1 AND CarType<=32 ),/*0x20 = 32*/	
	CustomType		char(1)		NOT NULL	check(CustomType>='1' AND CustomType<='2' ),
	CardNo			char(20)	NOT NULL	check(CardNo SIMILAR TO '[0-9]{16}    '),
	CardBalance		Bigint NOT NULL	,
	EnNetRoadID		Smallint NOT NULL	,
	EnRoadID		Smallint NOT NULL	,
	EnStationID		Integer	NOT NULL	,
	EnLaneID		Smallint NOT NULL	,
	EnLaneType		Smallint NOT NULL	check(EnLaneType>0 AND EnLaneType<=34),/*0x22 =34*/
	EnTime			char(14) NOT NULL	check(EnTime			SIMILAR TO '[0-9]{14}'),
	EnOperatorID	char(12) NOT NULL	check(EnOperatorID	SIMILAR TO '[0-9]{12}'),
	EnOpCardNo		char(10) DEFAULT '0000000000' NOT NULL ,
	EnOpCardID		Integer	DEFAULT 0 NOT NULL 	,
	EnShiftID		char(8) NOT NULL	check(EnShiftID		SIMILAR TO '[0-9]{8}'),
	SquadDate		char(14) NOT NULL	check(SquadDate	SIMILAR TO '[0-9]{14}'),
	EnITCTermCode	char(16)	NOT NULL	,
	ENITCTermTradNo	Bigint NOT NULL	,
	EnVehicleFlag	Smallint NOT NULL	check(EnVehicleFlag=0 OR EnVehicleFlag=1),
	ImageSerialNo	varchar(16)	,
	EnITCMac		varchar(8)	DEFAULT '5cdd5cdd' NOT NULL ,
	CONSTRAINT		LaneEntrance_pk PRIMARY KEY (EnITCTermCode,EnTime)
);

/* auto increase LaneEntranceList table ENITCTermTradNo column*/
/*
CREATE GENERATOR GEN_ENITCTermTradNo_ID;
SET GENERATOR GEN_ENITCTermTradNo_ID TO 0;
set term !! ;
CREATE TRIGGER LaneEntranceList_T1 FOR LaneEntranceList
ACTIVE BEFORE INSERT POSITION 0
AS
BEGIN
if (NEW.ENITCTermTradNo	is NULL) then NEW.ENITCTermTradNo = GEN_ID(GEN_ENITCTermTradNo_ID, 1);
END!!
set term ; !!
*/

CREATE TABLE EnPictureList(
	ImageSerialNo	varchar(16)	NOT NULL	PRIMARY KEY,
	SquadDate		Timestamp    NOT NULL	,
	OpTime			Timestamp    NOT NULL	,
	EnImages		BLOB(,0)
);

CREATE TABLE LaneExitList(
	LaneExSerialNo		char(16)	NOT NULL	,
	LaneEnSerialNo		char(16)	NOT NULL	,
	ExRecordNo			Smallint	DEFAULT 0 NOT NULL,
	PlateNo				char(12)	NOT NULL	,
	PlateColor			smallint	NOT NULL	check( PlateColor>=0 AND PlateColor<10 ),
	CarType				smallint	NOT NULL	check( CarType>=1 AND CarType<=32 ),/*0x20 = 32*/	
	CustomType			char(1)		NOT NULL	check(CustomType>='1' AND CustomType<='2' ),
	CardNo				char(20)	NOT NULL	check(CardNo SIMILAR TO '[0-9]{16}    '),
	CardBalance			Bigint NOT NULL	,
	EnNetRoadID			Smallint NOT NULL	,
	EnRoadID			Smallint NOT NULL	,
	EnStationID			Integer	NOT NULL	,
	EnLaneID			Smallint NOT NULL	,
	EnLaneType			Smallint NOT NULL	check(EnLaneType>0 AND EnLaneType<32),/*0x20 = 32*/	
	EnTime				char(14) NOT NULL	check(EnTime			SIMILAR TO '[0-9]{14}'),
	EnOperatorID		char(12) NOT NULL	check(EnOperatorID	SIMILAR TO '[0-9]{12}'),
	EnSquadDate			char(14) NOT NULL	check(EnSquadDate	SIMILAR TO '[0-9]{14}'),
	EnShiftID			char(8) NOT NULL	check(EnShiftID		SIMILAR TO '[0-9]{8}'),
	EnITCTermCode		char(16)	NOT NULL	,
	ENITCTermTradNo		Bigint NOT NULL	,
	EnVehicleFlag		Smallint NOT NULL	check(EnVehicleFlag=0 OR EnVehicleFlag=1),
	ExNetRoadID			Smallint NOT NULL	,
	ExRoadID			Smallint NOT NULL	,
	ExStationID			Integer	NOT NULL	,
	ExLaneID			Smallint NOT NULL	,
	ExLaneType			Smallint NOT NULL	check(ExLaneType>0 AND ExLaneType<32),/*0x20 = 32*/	
	ExTime				char(14) NOT NULL	,
	ExOperatorID		char(12) NOT NULL	check(ExOperatorID	SIMILAR TO '[0-9]{12}'),
	ExOpCardNo			char(10)	DEFAULT '0000000000' NOT NULL 	,
	ExOpCardID			Integer		DEFAULT 0 NOT NULL ,
	ExShiftID			char(8) NOT NULL	check(ExShiftID		SIMILAR TO '[0-9]{8}'),
	ExSquadDate			char(14)   NOT NULL	,
	ExITCTermCode		char(16)	NOT NULL	,
	ExITCTermTradNo		Bigint NOT NULL	,
	ExVehicleFlag		Smallint NOT NULL	,
	ImageSerialNo		varchar(16)	,
	OriginalPath		varchar(256)	NOT NULL	,
	RealPath			varchar(256) DEFAULT '' NOT NULL ,
	TollMoney			Bigint NOT NULL	,
	ExITCMac			char(8)		DEFAULT '5cdd5cdd' NOT NULL ,
	CONSTRAINT			LaneExit_pk PRIMARY KEY (ExITCTermCode,ExTime)
);

CREATE TABLE ExPictureList(
	ImageSerialNo	varchar(16)	NOT NULL	PRIMARY KEY,
	SquadDate		Timestamp    NOT NULL	,
	OpTime			Timestamp    NOT NULL	,
	ExImages		BLOB(,0)
);

insert into ITC_INFO(
	ITCTermCode,NetRoadID,RoadID, 
	StationID,LaneID,LaneType, 
	Next_Local_ITCTermTradNo,Session_id)values( 
	'4412100000000001',17426,16, 
	01,01,01, 
	0,'00000000000000');/*0x10=16 , 0x4412=17426*/
/*
insert into ITC_INFO(
	ITCTermCode,NetRoadID,RoadID, 
	StationID,LaneID,LaneType, 
	Next_Local_ITCTermTradNo,Session_id)values( 
	'4412100000000002',17426,16, 
	16,16,02, 
	0,'00000000000000');
*/

insert into EntranceCarList(PlateNo,PlateColor,CarType,CustomType,CardNo,CardBalance,addDatetime )values('粤A A9A91   ',0,1,'1','1123456789123456    ' , 000000154321, current_timestamp);
insert into EntranceCarList(PlateNo,PlateColor,CarType,CustomType,CardNo,CardBalance,addDatetime )values('粤A A9A92   ',0,1,'1','2123456789123456    ' , 000000254321, current_timestamp);
insert into EntranceCarList(PlateNo,PlateColor,CarType,CustomType,CardNo,CardBalance,addDatetime )values('粤A A9A93   ',0,1,'1','3123456789123456    ' , 000000354321, current_timestamp);
insert into ExitCarList(PlateNo, PlateColor, CarType, CustomType, CardNo, CardBalance,LaneEnSerialNo, EnNetRoadID, EnRoadID, EnStationID, EnLaneID, EnLaneType,EnTime, EnOperatorID, EnShiftID, SquadDate,EnITCTermCode, ENITCTermTradNo, EnVehicleFlag,OriginalPath,addDatetime)values('粤A A9A91   ',0,1,'1','1123456789123456    ' , 000000154321, 'ensdef0123456789', 17426, 16, 01, 01, 01,'20170710234050', '123456123456', '12345678', '20170711010101','4412100000000001', 123456123456, 1 ,'',current_timestamp);

insert into ExitCarList(PlateNo, PlateColor, CarType, CustomType, CardNo, CardBalance,LaneEnSerialNo, EnNetRoadID, EnRoadID, EnStationID, EnLaneID, EnLaneType,EnTime, EnOperatorID, EnShiftID, SquadDate,EnITCTermCode, ENITCTermTradNo, EnVehicleFlag,OriginalPath,addDatetime)values('粤A A9A92   ',0,1,'1','1123456789123456    ' , 000000254321, 'ensdef0123456789', 17426, 16, 01, 01, 01,'20170710234050', '123456123456', '12345678', '20170711010101','4412100000000001', 123456123456, 1 ,'ABCDEFGH',current_timestamp);
