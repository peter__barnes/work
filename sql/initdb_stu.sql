/* sudo -u postgres psql -f initdb_stu.sql */
/*create structure*/
DROP DATABASE IF EXISTS test1 ;

CREATE DATABASE test1;
\c test1;

DROP TABLE IF  EXISTS stud;
DROP TABLE IF  EXISTS class;
DROP TABLE IF  EXISTS subj;

CREATE TABLE class(class_id  SERIAL PRIMARY KEY,grade int,class int,info text DEFAULT '');
CREATE TABLE stud(id  SERIAL PRIMARY KEY,name varchar(15),sex boolean,birthday date CHECK(birthday > '1980-1-1'),class_id int REFERENCES class(class_id));
CREATE TABLE subj(sub_id  SERIAL PRIMARY KEY,name varchar(10),stud_id int REFERENCES stud(id), score int);
CREATE INDEX ind_subj ON subj(score); /*select score from subj ORDER BY score;*/

CREATE OR REPLACE FUNCTION stud_upgrade() 
RETURNS integer
AS $$
	UPDATE class SET grade=grade+1;
	SELECT 0;
$$ LANGUAGE SQL;


/*create init data*/
INSERT INTO class(class_id,grade,class,info) VALUES (1,1,1,'grade 1,class 1');
INSERT INTO class(class_id,grade,class,info) VALUES (2,1,2,'grade 1,class 2');
INSERT INTO class(class_id,grade,class,info) VALUES (3,2,1,'grade 2,class 1');
INSERT INTO class(class_id,grade,class,info) VALUES (4,2,2,'grade 2,class 2');
INSERT INTO class(class_id,grade,class,info) VALUES (5,3,1,'grade 3,class 1');
INSERT INTO class(class_id,grade,class,info) VALUES (6,3,2,'grade 3,class 2');

INSERT INTO stud(name,sex,birthday,class_id) VALUES ('LiXiaoMing',true,'1986-3-9',3);
INSERT INTO stud(name,sex,birthday,class_id) VALUES ('YangXiaohua',false,'1988-9-8',2);
INSERT INTO stud(name,sex,birthday,class_id) VALUES ('Zhangpeng',true,'1987-8-28',4);
INSERT INTO stud(name,sex,birthday,class_id) VALUES ('ZhangChen',true,'1987-10-3',2);
INSERT INTO stud(name,sex,birthday,class_id) VALUES ('LiPing',false,'1988-8-13',3);
INSERT INTO stud(name,sex,birthday,class_id) VALUES ('WangMei',false,'1987-1-20',1);
INSERT INTO stud(name,sex,class_id) VALUES ('YangJi',true,1);
INSERT INTO stud(name,sex,class_id) VALUES ('LiTao',true,4);

INSERT INTO subj(name,stud_id,score) VALUES ('English1',1,68);
INSERT INTO subj(name,stud_id,score) VALUES ('English1',2,78);
INSERT INTO subj(name,stud_id,score) VALUES ('English1',3,89);
INSERT INTO subj(name,stud_id,score) VALUES ('English1',4,92);
INSERT INTO subj(name,stud_id,score) VALUES ('Math1',1,92);
INSERT INTO subj(name,stud_id,score) VALUES ('Math1',2,61);
INSERT INTO subj(name,stud_id,score) VALUES ('Math1',3,82);
INSERT INTO subj(name,stud_id,score) VALUES ('Math1',6,92);
INSERT INTO subj(name,stud_id,score) VALUES ('Math1',7,93);
INSERT INTO subj(name,stud_id,score) VALUES ('Math2',2,58);
INSERT INTO subj(name,stud_id,score) VALUES ('Math2',4,75);
INSERT INTO subj(name,stud_id,score) VALUES ('Chinese1',1,84);
INSERT INTO subj(name,stud_id,score) VALUES ('Chinese1',3,76);
INSERT INTO subj(name,stud_id,score) VALUES ('Chinese2',3,69);
INSERT INTO subj(name,stud_id,score) VALUES ('Physics1',3,82);
INSERT INTO subj(name,stud_id,score) VALUES ('Physics1',6,92);
INSERT INTO subj(name,stud_id,score) VALUES ('Physics1',7,93);
INSERT INTO subj(name,stud_id,score) VALUES ('Physics2',2,58);

BEGIN;
SAVEPOINT save001;
SELECT * from stud_upgrade();
ROLLBACK TO SAVEPOINT save001;
COMMIT;
