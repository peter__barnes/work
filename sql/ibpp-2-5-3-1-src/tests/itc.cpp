
#ifdef _MSC_VER
#pragma warning(disable: 4786 4996)
#ifndef _DEBUG
#pragma warning(disable: 4702)
#endif
#endif

#ifdef IBPP_WINDOWS
#include <windows.h>
#endif

#include "../core/ibpp.h"

#ifdef IBPP_UNIX
#include <unistd.h>
#define DeleteFile(x) unlink(x)
#define Sleep(x) usleep(1000 * x)
#endif

#ifdef HAS_HDRSTOP
#pragma hdrstop
#endif

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <typeinfo>

// Fix to famous MSVC 6 variable scope bug
#if defined(_MSC_VER) && (_MSC_VER < 1300)	// MSVC 6 should be < 1300
#define for if(true)for
#endif

//	The following database name (DbName) will be used during this test.
//	If existing, that DB will be deleted (assuming not used).
//	It will then be dynamically created, and various tests will run
//	while it is being populated and exercised.
//
//	The DB should grow to around 25 MB while being exercised.

#ifdef IBPP_UNIX
	//const char* DbName = "~/test.fdb";
	const char* DbName = "/root/fdb/itc.fdb";
	const char* BkName = "~/test.fbk";
	const std::string ServerName = "localhost";
#else
	const char* DbName = "C:/test.fdb";	// FDB extension (GDB is hacked by Windows Me/XP "System Restore")
	const char* BkName = "C:/test.fbk";
	const std::string ServerName = "localhost";	// Change to "" for local protocol / embedded
#endif

//	The tests use by default the well-known default of SYSDBA/masterkey
//	credentials. Do not forget to update them if required.

const std::string UserName = "SYSDBA";
const std::string Password = "masterkey";

class Test
{
	// Class 'Test' drives all the tests of this module.

	bool _Success;				// if true after all tests, everything succeeded
	int _WriteMode;				// 0 == default, 1 == speed, 2 == safety

	void Test1();
	void Test2(); 
	void Test3();
	void Test4();

public:
	void RunTests();
	int PrintResult();
	Test(int argc, char* argv[]);
	~Test();
};

int Test::PrintResult()
{
	if (_Success) printf(_("\n*** SUCCESS ***\nAll tests were 100%% successfull.\n"));
	else printf(_("\n*** FAILED ***\nSome tests failed, as indicated above.\n"));
	return _Success ? 0 : 1;
}

void Test::RunTests()
{
	int NextTest = 1;

	//IBPP::ClientLibSearchPaths("C:\\integral_90\\firebird\\bin");

	printf(_("\nIBPP Test Suite (Version %d.%d.%d.%d)\n\n"),
		(IBPP::Version & 0xFF000000) >> 24,
		(IBPP::Version & 0x00FF0000) >> 16,
		(IBPP::Version & 0x0000FF00) >> 8,
		(IBPP::Version & 0x000000FF));

	for (;;)
	{
		try
		{
			switch (NextTest++)
			{
				case 1 :	Test1(); break;
				case 2 :	Test2(); break;
				case 3 :	Test3(); break;
				case 4 :	Test4(); break;
				/*
				case 5 :	Test5(); break;
				case 6 :	Test6(); break;
				case 7 :	Test7(); break;
				case 8 :	Test8(); break;
				*/
				default :	return;		// All tests have been run
			}
		}
		// You don't need to catch all these.
		// This is done only as a detailed sample case.
		// Often catching IBPP::Exception& is enough just to report the error string.
		// If you need access to the SQLCode and EngineCode obtained when the error
		// comes from the engine, catch(SQLException&) in addition or use RTTI.
		catch(IBPP::Exception& e)
		{
			_Success = false;
			std::cout << e.what() << "\n";
			IBPP::SQLException* pe = dynamic_cast<IBPP::SQLException*>(&e);
			if (pe == 0) std::cout<< _("Not an engine error\n");
			else std::cout<< "Engine Code : "<< pe->EngineCode()<< "\n";
		}
		catch(std::exception& e)
		{
			_Success = false;
			std::cout << e.what() << "\n";
			printf(_("Test %d --- FAILED\n")
				_("A std::exception '%s' occured !\n\n"),
					NextTest-1, typeid(e).name());
		}
		catch(...)
		{
			_Success = false;
			printf(_("Test %d --- FAILED\n")
				_("A system exception occured !\n\n"), NextTest-1);
		}
	}
}

void Test::Test1()
{
	printf(_("Test 1 --- Checking Date/Time (no DB involved)\n"));

	IBPP::Date dt;
	IBPP::Date dt1;
	IBPP::Date dt2;
	IBPP::Timestamp tm1;
	int asint;
	int year, month, day;
	int hour, min, sec;

	tm1.Now();
	tm1.GetDate(year, month, day);
	tm1.GetTime(hour, min, sec);
	printf(_("           Now : Y=%d M=%d D=%d, %d:%d:%d\n"),
		year, month, day, hour, min, sec);

	dt.SetDate(1965, 4, 5);		// Who's birthday is that ? :)
	tm1 = dt;
	tm1.Add(1);
	tm1.Add(-1);
	dt = tm1;
	asint = dt.GetDate();
	if (asint != 23836)
	{
		_Success = false;
		printf(_("Failed '5 April 1965' date as int test.\n")
			_("Returned %d while 23836 was expected.\n"), asint);
		return;
	}

	dt.SetDate(1900, 1, 1);
	asint = dt.GetDate();
	if (asint != 1)
	{
		_Success = false;
		printf(_("Failed '1 January 1900' date as int test.\n")
			_("Returned %d while 1 was expected.\n"), asint);
		return;
	}

	dt.SetDate(1, 1, 1);	// 1 january 0001
	asint = dt.GetDate();
	if (asint != -693594)
	{
		_Success = false;
		printf(_("Failed '1 January 0001' date as int test.\n")
			_("Returned %d while -693594 was expected.\n"), asint);
		return;
	}
	dt.SetDate(-693594);
	dt.GetDate(year, month, day);
	if (year != 1 || month != 1 || day != 1)
	{
		_Success = false;
		printf(_("Failed -693594 as date test.\n")
			_("Returned Y:%d M:%d D:%d while 1 January 1 was expected.\n"),
			 year, month, day);
		return;
	}

	dt.SetDate(9999, 12, 31);	// 31 december 9999
	asint = dt.GetDate();
	if (asint != 2958464)
	{
		_Success = false;
		printf(_("Failed '31 December 9999' date as int test.\n")
			_("Returned %d while 2958464 was expected.\n"), asint);
		return;
	}

	dt.SetDate(2001, 2, 12);
	asint = dt.GetDate();
	if (asint != 36933)
	{
		_Success = false;
		printf(_("Failed '12 February 2001' date as int test.\n")
			_("Returned %d while 36933 was expected.\n"), asint);
		return;
	}

	dt.SetDate(asint);
	dt2 = dt;
	dt2.Add(15);
	if (dt2.GetDate() != 36948)
	{
		_Success = false;
		printf(_("Failed Date::Add() test.\n")
			_("Returned %d while 36948 was expected.\n"), dt2.GetDate());
		return;
	}

	int diff = dt2.GetDate() - dt.GetDate();
	if (diff != 15)
	{
		_Success = false;
		printf(_("Failed date arithmetic test.\n")
			_("Returned %d while 15 was expected.\n"), diff);
		return;
	}
}

void Test::Test2()
{
	printf(_("Test 2 --- Exercise empty database creation & connection\n"));

	int Major, Minor, PageSize, Pages, Buffers, Sweep;
	bool Sync, Reserve;

	IBPP::Database db1;
	DeleteFile(DbName);
	db1 = IBPP::DatabaseFactory(ServerName, DbName, UserName, Password,
		"", "WIN1252", "PAGE_SIZE 8192 DEFAULT CHARACTER SET WIN1252");
	db1->Create(3);		// 3 is the dialect of the database (could have been 1)

	IBPP::Service svc = IBPP::ServiceFactory(ServerName, UserName, Password);
	svc->Connect();
	svc->SetPageBuffers(DbName, 256);	// Instead of default 2048
	svc->SetSweepInterval(DbName, 5000);	// instead of 20000 by default
	if (_WriteMode == 1) svc->SetSyncWrite(DbName, false);
	else if (_WriteMode == 2) svc->SetSyncWrite(DbName, true);
	svc->SetReadOnly(DbName, false);	// That's the default anyway
	svc->Disconnect();

	db1->Connect();	// A create, does not imply connection

	db1->Info(&Major, &Minor, &PageSize, &Pages, &Buffers, &Sweep, &Sync, &Reserve);
	if (Sync && _WriteMode == 1)
	{
		_Success = false;
		printf(_("The created database has sync writes enabled (safety),\n"
			"while it was expected to be disabled (speed).\n"));
	}

	if (! Sync && _WriteMode == 2)
	{
		_Success = false;
		printf(_("The created database has sync writes disabled (speed),\n"
			"while it was expected to be enabled (safety).\n"));
	}

	if (Sync) printf(_("           Sync Writes is enabled (Safety).\n"
		"           Use 'speed' command-line argument to test the other mode.\n"));
    else printf(_("           Sync Writes is disabled (Speed).\n"
		"           Use 'safety' command-line argument to test the other mode.\n"));
 
	/**/
	printf("           ODS Major %d\n", Major);
	printf("           ODS Minor %d\n", Minor);
	printf("           Page Size %d\n", PageSize);
	printf("           Pages     %d\n", Pages);
	printf("           Buffers   %d\n", Buffers);
	printf("           Sweep     %d\n", Sweep);
	printf("           Reserve   %s\n", Reserve ? _("true") : _("false"));
	/**/

    db1->Disconnect();
}

void Test::Test3()
{
	printf(_("Test 3 --- Exercise basic DDL operations and IBPP::Exceptions\n"));

	IBPP::Database db1;
	db1 = IBPP::DatabaseFactory(ServerName, DbName, UserName, Password);
	db1->Connect();

	// The following transaction configuration values are the defaults and
	// those parameters could have as well be omitted to simplify writing.
	IBPP::Transaction tr1 = IBPP::TransactionFactory(db1,
							IBPP::amWrite, IBPP::ilConcurrency, IBPP::lrWait);
	tr1->Start();

	IBPP::Statement st1 = IBPP::StatementFactory(db1, tr1);

	st1->ExecuteImmediate(	"CREATE TABLE TEST("
							"N2 NUMERIC(9,2), "
							"N6 NUMERIC(15,2), "
							"N5 NUMERIC(9,5), "
							"A1 NUMERIC(9,2) [8], "
							"A2 VARCHAR(30) [0:3, 1:4], "
				 //			"A3 INTEGER [0:1199], "
							"DA TIMESTAMP [0:1], "
							"D DATE, "
							"T TIME, "
							"TS TIMESTAMP, "
							"B BLOB SUB_TYPE 1, "
							"BB BLOB SUB_TYPE 0, "
							"TF CHAR(1), "
							"ID INTEGER, "
							"TX CHAR(30), "
							"VX VARCHAR(30), "
							"TB CHAR(40) CHARACTER SET OCTETS, "
							"VB VARCHAR(40) CHARACTER SET OCTETS)");
	tr1->CommitRetain();
	st1->ExecuteImmediate(	"CREATE VIEW PRODUCT(X, Y) AS "
							"SELECT T.N2, S.N2 FROM TEST T, TEST S"
							);
	tr1->CommitRetain();
	try
	{
		#if defined(IBPP_WINDOWS) && defined(_DEBUG)
			OutputDebugString(_("An exception will now get logged in the debugger: this is expected.\n"));
		#endif
		st1->ExecuteImmediate(	"CREATE SYNTAX ERROR(X, Y) AS "
								"SELECT ERRONEOUS FROM MUSTFAIL M" );
	}
	catch(IBPP::SQLException& e)
	{
		//~ std::cout<< e.what();

		if (e.EngineCode() != 335544569)
		{
			_Success = false;
			printf(_("The error code returned by the engine during a\n"
				"voluntary statement syntax error is unexpected.\n"));
		}
	}

	// Intentionally do not Commit or Rollback the transaction, nor even Disconnect
	// The auto-release mechanisms will have to Rollback and terminate everything cleanly.
}
void Test::Test4()
{
	printf(_("Test 4 --- chentr's own test,insert test\n"));
	IBPP::Database db1;
	db1 = IBPP::DatabaseFactory(ServerName, DbName, UserName, Password);
	db1->Connect();
	// The following transaction configuration values are the defaults and
	// those parameters could have as well be omitted to simplify writing.
	IBPP::Transaction tr1 = IBPP::TransactionFactory(db1,
							IBPP::amWrite, IBPP::ilConcurrency, IBPP::lrWait);
	tr1->Start();

	IBPP::Statement st1 = IBPP::StatementFactory(db1, tr1);
	st1->ExecuteImmediate("CREATE TABLE Test1( str varchar(16)	NOT NULL);") ;
	tr1->CommitRetain();
	st1->ExecuteImmediate("insert into Test1(str) values ('456');") ;
	tr1->CommitRetain();
	db1->Disconnect();
}

Test::Test(int argc, char* argv[])
{
	if (argc == 2 && argv[1] != 0 && strcmp(argv[1], "speed") == 0)
		_WriteMode = 1;
	else if (argc == 2 && argv[1] != 0 && strcmp(argv[1], "safety") == 0)
		_WriteMode = 2;
	else
		_WriteMode = 0;

	_Success = true;
}

Test::~Test()
{
}

int main(int argc, char* argv[])
{
	Test T(argc, argv);

	if (! IBPP::CheckVersion(IBPP::Version))
	{
		printf(_("\nThis program got linked to an incompatible version of the IBPP source code.\n"
			"Can't execute safely.\n"));
		return 2;
	}

	T.RunTests();

	return T.PrintResult();
}

