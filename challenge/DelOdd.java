/*
Problem Definition
Implement a function to delete inplace all the odd value elements in the linked list.

Input format:Space separated integers
Output format:Print only even numbers from the given list (space separated)
Code Restriction:Implement the algorithm in delOddsList() method only.
*/

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

class DelOdd
{
    public static void main(String args[])
    {
        LinkedList<Integer> l = readList();
        delOddsList(l);
        printList(l);
    }

    private static void delOddsList(LinkedList<Integer> l)
    {
        Iterator<Integer> x = l.listIterator();
        while (x.hasNext())
        {
            if(x.next()%2!=0)
            {
                x.remove() ;
            }
        }
        // Implement this function
    }

    private static LinkedList<Integer> readList()
    {
        Scanner s = new Scanner(System.in);
        LinkedList<Integer> l = new LinkedList<Integer>();
        try {
            while(true){
                l.add(s.nextInt());
            }
        } catch (Exception e) {
        }
        s.close();
        return l;
    }

    private static void printList(LinkedList<Integer> l)
    {
        Iterator<Integer> x = l.listIterator();
        while (x.hasNext())
        {
            System.out.print(x.next()+" ");
        }
    }
}
