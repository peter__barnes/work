
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>

int createString(int n, int k)
{
	if(n<=0)
	{
		printf("can't find\n");
		return -1;
	}
	if(k<0)
	{
		printf("can't find\n");
		return -1;
	}
	if(n%3==0)
	{
		if(k>(n*n/3))
		{
			printf("can't find\n");
			return -1;
		}
	}
	else
	{
		if(k>((n*n-1)/3))
		{
			printf("can't find\n");
			return -1;
		}
	}
	printf("find!\n");
	char str[n+1];
	str[n]='\0';
	int max_b;
	int max_c;
	int i=0;
	int j=0;
	char tmp;
	memset(str,'A',n);
	if(0==n%3)
	{
		max_b=max_c=n/3;
	}
	else if(1==n%3)
	{
		max_b=n/3+1;
		max_c=n/3;
	}
	else if(2==n%3)
	{
		max_b= n/3;
		max_c= n/3+1;
	}
	printf("max_b=%d,max_c=%d\n",max_b,max_c);
	for(i=0;i<max_b+max_c;i++)
	{
		str[0]='B';
		j=0;
		if( k == (i*(n-i-1)+j))
		{
			printf("%s,i=%d,j=%d\n",str,i,j);
		}
		//			cnt(str,n);
		for(j=1; str[j]!='B' && j<n;j++)
		{
			str[j]='B';
			str[j-1]='A';
			//cnt();
			if( k == (i*(n-i-1)+j))
			{
				printf("%s,i=%d,j=%d\n",str,i,j);
			}
		}
	}
	for(i=0;i<max_c;i++)
	{
		str[max_c]='C';
		j=0;
		if( k == max_c*(max_b+max_c)+((i)*(max_b+max_c -i-1)+  j-max_c))
		{
			printf("%s,i=%d,j=%d\n",str,i,j);
		}
		//			cnt();
		for(j=max_c+1;str[j]!='C' && j<n;j++)
		{
			tmp = str[j];
			str[j]='C';
			str[j-1]=tmp;
			//	cnt();
			if( k == max_c*(max_b+max_c)+((i)*(max_b+max_c -i-1)+  j-max_c))
			{
				printf("%s,i=%d,j=%d\n",str,i,j);
			}
		}
	}
	printf("\n");
}

int main()
{
//	createString(5,8);
	createString(3,3);
	createString(3,0);
	createString(5,10);
	createString(15,36);
}
/*
Problem Statement

You are given two s: N and K. Lun the dog is interested in strings that satisfy the following conditions:

    The string has exactly N characters, each of which is either 'A', 'B' or 'C'.
    The string s has exactly K pairs (i, j) (0 <= i < j <= N-1) such that s[i] < s[j].

If there exists a string that satisfies the conditions, find and return any such string. Otherwise, return an empty string.
Definition
Class:
ABC
Method:
createString
Parameters:
int, int
Returns:
String
Method signature:
String createString(int N, int K)
(be sure your method is public)
Limits
Time limit (s):
2.000
Memory limit (MB):
256
Constraints
- N will be between 3 and 30, inclusive.
- K will be between 0 and N(N-1)/2, inclusive.
Examples
0)
3
3
Returns: "ABC"
This string has exactly three pairs (i, j) mentioned in the statement: (0, 1), (0, 2) and (1, 2).
1)
3
0
Returns: "CBA"
Please note that there are valid test cases with K = 0.
2)
5
10
Returns: ""
Five characters is too short for this value of K.
3)
15
36
Returns: "CABBACCBAABCBBB"
Please note that this is an example of a solution; other valid solutions will also be accepted.
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. 
*/
