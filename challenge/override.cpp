/*
input: 1
Expected Output: Brass::play
Actual Output: Instrument::play
*/
#include <iostream> 
using namespace std; 

class Instrument { 
    public: 
        void play() const { 
            cout << "Instrument::play" << endl;
        } 
}; 

class Brass : public Instrument { 
    public:
        void play() const { 
            cout << "Brass::play" << endl; 
        } 
    
}; 

class Woodwind : public Instrument { 
    public:
        void play() const { 
            cout << "Woodwind::play" << endl; 
        } 
    
}; 

class Strings: public Instrument {
    public:
        void play() const { 
            cout << "Strings::play" << endl; 
        } 
};

class Percussion: public Instrument {
    public:
        void play() const { 
            cout << "Percussion::play" << endl; 
        } 
};

void play(int family) {
    Instrument *ins = NULL;
    
    switch(family) {
        case 1: 
            ins = new Brass;
            break;
        case 2:
            ins = new Strings;
            break;
        case 3:
            ins = new Woodwind;
            break;
    }
    
    if (ins) {
        
         switch(family) {
        case 1: 
            ((Brass*)ins)->play();
            break;
        case 2:
            ((Strings*)ins)->play();
            break;
        case 3:
            ((Woodwind*)ins)->play();
            break;
        }
    }
}

int main()
{
    play(1);
    return 0;
}
