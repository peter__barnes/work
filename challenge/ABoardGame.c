#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>
// 0 draw , 1 alice win , -1 bob win
int whoWins(int n,char board[2*n][2*n+1])
{
	int i=0;
	int j=0;
	for(i=0; i<n; ++i)
	{
		int cnt=0;
		for(j=n-i-1;j<=n+i;j++)
		{
			if('A'==board[n-i-1][j])
			{
				++cnt;
			}
			else if('B'==board[n-i-1][j])
			{
				--cnt;
			}
		}
		for(j=n-i-1;j<=n+i;j++)
		{
			if('A'==board[n+i][j])
			{
				++cnt;
			}
			else if('B'==board[n+i][j])
			{
				--cnt;
			}
		}
		for(j=n-i;j<n+i;j++)
		{
			if('A'==board[j][n-i-1])
			{
				++cnt;
			}
			else if('B'==board[j][n-i-1])
			{
				--cnt;
			}
		}
		for(j=n-i;j<n+i;j++)
		{
			if('A'==board[j][n+i])
			{
				++cnt;
			}
			else if('B'==board[j][n+i])
			{
				--cnt;
			}
		}
		if(cnt!=0)
		{
			return cnt;
		}
	}
	return 0;
}

int main()
{
	int ret;
	char board[6][6+1]={".....A", "......", "..A...", "...B..", "......", "......"};
	ret = whoWins(3,board);
	printf("%d\n",ret);

	char board2[4][4+1]={"AAAA", "A.BA", "A..A", "AAAA"};
	ret = whoWins(2,board2);
	printf("%d\n",ret);

	char board3[2][2+1]={"..", ".."};
	ret = whoWins(1,board3);
	printf("%d\n",ret);

	char board4[14][14+1]= {"BBB..BAB...B.B", ".AAAAAAAAAAAA.", "AA.AA.AB..A.AB", "..........B.AB", ".A..BBAB.A.BAB", ".AB.B.......A.", ".A..A.AB.A..AB", ".ABAA.BA...BA.", "BAAAB.....ABA.", ".A....B..A..B.", "B...B....B..A.", "BA.B..A.ABA.A.", "BAAAA.AAAAA.A.", "B.B.B.BB.B...."} ;
	ret = whoWins(7,board4);
	printf("%d\n",ret);

	char board5[12][12+1]= {"..A..AAA..AA", "ABABB..AAAAA", "ABBBBBBBBBA.", "AABBBABABBAA", "...BABABABBA", "B.BA..A.BBA.", "AA.A..B.AB.B", "..BA.B.AABAA", "..ABABBBABA.", ".ABB.BBBBBAA", "ABAAA.AA.A.A", "A..AAA.AAA.A"} ;
	ret = whoWins(6,board5);
	printf("%d\n",ret);

	char board6[10][10+1]={"B..ABAABBB", "B.........", "A..A.AA..B", "A.BBBAA..A", "B.AAAAB...", "A..BBBBB.A", "B..ABAABBA", "A......B.B", "B......A.A", "BA.AABBB.A"} ;
	ret = whoWins(5,board6);
	printf("%d\n",ret);
}

/*Problem Statement
Your friends Alice and Bob are playing a board game. They have asked you to help them to determine the winner. The game is played on a square board with 2N rows and 2N columns. The exact rules of the game itself are not important for this problem. Once the game is over, each cell of the board is either empty or contains a single piece that belongs to either Alice or Bob. You are given board, where the j-th character in i-th element (0-based indices) describes the contents of the cell in row i, column j: '.' represents an empty cell, 'A' a cell with Alice's piece and 'B' a cell with Bob's piece.

The entire board is divided into N regions. Region 1 occupies the 4 central cells of the board. Each next region contains all cells that are horizontally, vertically or diagonally adjacent to cells of the immediately previous region and do not belong to any of the previous regions. For example, when N = 4, here is how the regions look:

44444444
43333334
43222234
43211234
43211234
43222234
43333334
44444444

The winner is determined as follows. Consider the lowest numbered region that contains a different number of Alice's and Bob's pieces. The player who has more pieces in this region is the winner. If all regions contain the same number of Alice's and Bob's pieces, the game ends in a draw.

Return "Alice" if Alice wins the given game, "Bob" if Bob wins and "Draw" if the game ends in a draw. Note that return values are case-sensitive.
Definition
Class:
ABoardGame
Method:
whoWins
Parameters:
String[]
Returns:
String
Method signature:
String whoWins(String[] board)
(be sure your method is public)
Limits
Time limit (s):
2.000
Memory limit (MB):
256
Constraints
- board will contain between 2 and 50 elements, inclusive.
- The number of elements in board will be even.
- Each element of board will contain the same number of characters as the number of elements in board.
- Each character in board will be 'A', 'B' or '.'.
Examples
0)
{".....A", "......", "..A...", "...B..", "......", "......"}
Returns: "Alice"
Both Alice and Bob have 1 piece in region 1, so they are tied there. In region 2, they have no pieces at all, so a tie again. Finally, in region 3 Alice has 1 piece, while Bob has none. So Alice is the winner of this game.
1)
{"AAAA", "A.BA", "A..A", "AAAA"}
Returns: "Bob"
Even though Alice has 12 pieces and Bob just one, this one piece is enough for him to win.
2)
{"..", ".."}
Returns: "Draw"
The board can be entirely empty.
3)
{"BBB..BAB...B.B", ".AAAAAAAAAAAA.", "AA.AA.AB..A.AB", "..........B.AB", ".A..BBAB.A.BAB", ".AB.B.......A.", ".A..A.AB.A..AB", ".ABAA.BA...BA.", "BAAAB.....ABA.", ".A....B..A..B.", "B...B....B..A.", "BA.B..A.ABA.A.", "BAAAA.AAAAA.A.", "B.B.B.BB.B...."}
Returns: "Alice"
4)
{"..A..AAA..AA", "ABABB..AAAAA", "ABBBBBBBBBA.", "AABBBABABBAA", "...BABABABBA", "B.BA..A.BBA.", "AA.A..B.AB.B", "..BA.B.AABAA", "..ABABBBABA.", ".ABB.BBBBBAA", "ABAAA.AA.A.A", "A..AAA.AAA.A"}
Returns: "Bob"
5)
{"B..ABAABBB", "B.........", "A..A.AA..B", "A.BBBAA..A", "B.AAAAB...", "A..BBBBB.A", "B..ABAABBA", "A......B.B", "B......A.A", "BA.AABBB.A"}
Returns: "Draw"
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. 
*/
