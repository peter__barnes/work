/*
Problem Definition

Implement the method flip_tree() to modify a binary tree in place to create it's mirror image.
Code Restrictions

Only add code under "// implement this function"
*/

#include <stdlib.h>
#include <stdio.h>
typedef struct binary_node binary_node;
/*A tree node. Holds pointers to left and right sub-trees, and an int value*/
struct binary_node {
  int value;
  struct binary_node *left;
  struct binary_node *right;
};


int print_tree_preorder(binary_node *tree) {
  if (tree == 0) {
    printf("( )");
    return 0;
  }
  printf("( %d ", tree->value);
  print_tree_preorder(tree->left);
  printf(" ");
  print_tree_preorder(tree->right);
  printf(" )");
  return 1;
}

/* 
 * Reads a new tree from stdin. Tree must be in pre-order form, where each node is surrounded by parentheses, 
 * and has form of 
 * ( <int_value> <left child node> <right child node> )
 * a child node may be null, in which case it should appear as 
 * ()
 * Whitespace is ignored.
 */
binary_node *read_tree_preorder() {
   
  char c = 0;
  while (c != '(') {
    scanf("%c", &c);
  }

  int value;
  if (scanf(" %d", &value) != 1) {
    return 0;
  } 

  binary_node *tree = (binary_node*)malloc(sizeof(struct binary_node));   
  tree->value = value;
  tree->left = read_tree_preorder();
  tree->right = read_tree_preorder();

  c = 0;
  while (c != ')') {
    scanf("%c", &c); 
  }
  return tree;
}
/*
void ReverseTree(Node root){
    Node tmp = root.Left;
    root.Left = root.Right;
    root.Right = tmp;
    if(root.Left != null) ReverseTree(root.Left);
    if(root.Right != null) ReverseTree(root.Right);

}
*/
int flip_tree(binary_node *tree) {
    // implement this function
    binary_node *tmp = tree->left;
    tree->left = tree->right;
    tree->right = tmp;
    if((tree->left)!=NULL)
    {
       flip_tree(tree->left);
    }
    if((tree->right)!=NULL)
    {
       flip_tree(tree->right);
    }

  return 0;
}

int main()
{
  binary_node *tree = read_tree_preorder();
  flip_tree(tree);
  print_tree_preorder(tree);
  printf("\n");
  return 0;
}

