#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>
void aca(int ele_num,char jour[ele_num][51])
{
	char name[ele_num][51];
	char cite[ele_num][51];
	double imp[ele_num];
	int paper_num[ele_num];
	int i;
	int j;
	int k;
	char tmp[3];
	int pos;
	memset(cite,0,sizeof(char)*ele_num*51);
	memset(imp,0,sizeof(imp));
	//memset(paper_num,0,sizeof(paper_num));
	for(i=0;i<ele_num;i++)
	{
		paper_num[i]=1;
	}
	for(i=0;i<ele_num;i++)
	{
		for(j=0;'.'!=jour[i][j];j++)
		{
			name[i][j]=jour[i][j];
		}
		name[i][j]='\0';
		for(j=j+1,k=0;'\0'!=jour[i][j];j++,k++)
		{
			cite[i][k]=jour[i][j];
		}
		cite[i][k]='\0';
		printf("%s\n",name[i]);
		printf("%s\n",cite[i]);
	}
	for(i=0;i<ele_num;i++)
	{
		k=0;
		for(j=0;'\0'!=cite[i][j];j++)
		{
			if(' '==cite[i][j] && 0!=k)
			{
				tmp[k]='\0';
				pos=atoi(tmp);
				if(0!=strcmp(name[i],name[pos]))
				{
					imp[pos]++;
				}
				printf("tmp[%d]:%s\n",i,tmp);
				k=0;
			}
			else
			{
				tmp[k]=cite[i][j];
				k++;
			}
		}
		tmp[k]='\0';
		pos=atoi(tmp);
		if(0!=strcmp(name[i],name[pos]))
		{
			imp[pos]++;
		}
		printf("tmp[%d]:%s\n",i,tmp);
		k=0;
	}
	for(i=0;i<ele_num;i++)
	{
		printf("imp[%d]=%f\n",i,imp[i]);
	}
	for(i=0;i<ele_num;i++)
	{
		for(j=i+1;j<ele_num;j++)
		{
			if(0==strcmp(name[i],name[j]))
			{
				imp[i]+=imp[j];
				paper_num[i]++;
				for(k=j;k<ele_num-1;k++)
				{
					strcpy(name[k],name[k+1]);
					imp[k]=imp[k+1];
					paper_num[k]=paper_num[k+1];
				}
				ele_num--;
			}
		}
	}
	for(i=0;i<ele_num;i++)
	{
		imp[i]=imp[i]/paper_num[i];
		printf("imp[%d]=%f\n",i,imp[i]);
	}
	printf("\n\nresult:\n");
	double t_imp;
	char t_name[51];
	for(i=0;i<ele_num;i++)
	{
		for(j=i;j<ele_num;j++)
		{
			if(1e-6<imp[j]-imp[i])
			{
				t_imp=imp[j];
				imp[j]=imp[i];
				imp[i]=t_imp;
				strcpy(t_name,name[j]);
				strcpy(name[j],name[i]);
				strcpy(name[i],t_name);
			}
			if((imp[i]-imp[j])<1e-6 && imp[j]-imp[i]<1e-6)
			{
				if(strcmp(name[i],name[j])>0)
				{
					t_imp=imp[j];
					imp[j]=imp[i];
					imp[i]=t_imp;
					strcpy(t_name,name[j]);
					strcpy(name[j],name[i]);
					strcpy(name[i],t_name);
				}
			}
		}
	}
	for(i=0;i<ele_num;i++)
	{
	//	imp[i]=imp[i]/paper_num[i];
		printf("imp[%d]=%f\n",i,imp[i]);
		printf("name[%d]=%s\n",i,name[i]);
	}

}

int main()
{
/*
	char jour[4][51]={"A.1 3","B.0","C.2","C.1"};
	aca(4,jour);
	*/
/*
	char jour[4][51]={"A.", "B. 0", "C. 1 0 3", "C. 2"};
	aca(4,jour);
	*/
/*
	char jour[9][51]= {"RESPECTED JOURNAL.", "MEDIOCRE JOURNAL. 0", "LOUSY JOURNAL. 0 1", "RESPECTED JOURNAL.", "MEDIOCRE JOURNAL. 3", "LOUSY JOURNAL. 4 3 3 4", "RESPECTED SPECIFIC JOURNAL.", "MEDIOCRE SPECIFIC JOURNAL. 6", "LOUSY SPECIFIC JOURNAL. 6 7"};
	aca(9,jour);
	*/
	/*
	char jour[2][51]={"NO CITATIONS.", "COMPLETELY ORIGINAL."};
	aca(2,jour);
	*/
	char jour[10][51]= {"CONTEMPORARY PHYSICS. 5 4 6 8 7 1 9", "EUROPHYSICS LETTERS. 9", "J PHYS CHEM REF D. 5 4 6 8 7 1 9", "J PHYS SOC JAPAN. 5 4 6 8 7 1 9", "PHYSICAL REVIEW LETTERS. 5 6 8 7 1 9", "PHYSICS LETTERS B. 6 8 7 1 9", "PHYSICS REPORTS. 8 7 1 9", "PHYSICS TODAY. 1 9", "REP PROGRESS PHYSICS. 7 1 9", "REV MODERN PHYSICS."};
	aca(10,jour);
}
