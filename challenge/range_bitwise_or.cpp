#include<stdio.h>
#include<iostream>
using namespace std;
int rangeBitwiseOr(int m, int n) 
{
    int ret=0;
    int i;
    if(m==n)
    {
        ret = m;
    }
    if(m>n)
    {
        for(i=n;i<=m;i++)
        {
            ret = ret | i;
        }
    }
    else
    {
        for(i=m;i<=n;i++)
        {
            ret = ret | i;
        }
    }
    return ret;
}
int main()
{
    int ret = rangeBitwiseOr(0,6);
    cout<<ret<<endl;
    return 0;
}
