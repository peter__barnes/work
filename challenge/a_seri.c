/*
Problem Statement
*** You may only submit a given problem once - no resubmissions will be accepted. ***

An arithmetic series consists of a sequence of terms such that each term minus its immediate predecessor gives the same result. For example, the sequence 3,7,11,15 is the terms of the arithmetic series 3+7+11+15; each term minus its predecessor equals 4. (Of course there is no requirement on the first term since it has no predecessor.)

Given a collection of integers, we want to find the longest arithmetic series that can be formed by choosing a sub-collection (possibly the entire collection). Create a class ASeries that contains a method longest that is given a values and returns the length of the longest arithmetic series that can be formed from values.

Definition
Class: ASeries
Method: longest
Parameters: int[]
Returns: int
Method signature:
int longest(int[] values)
(be sure your method is public)
Limits
Time limit (s): 840.000
Memory limit (MB): 64
Constraints
- values will contain between 2 and 50 elements inclusive.
- Each element of values will be between -1,000,000 and 1,000,000 inclusive.
Examples
0)
{3,8,4,5,6,2,2}
Returns: 5
No arithmetic series using these values is longer than 2,3,4,5,6.

1)
{-1,-5,1,3}
Returns: 3
-1, 1, 3 is an arithmetic series (so is 3,-1,-5).
2)

{-10,-20,-10,-10}
Returns: 3
-10,-10,-10 is an arithmetic series.
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. 
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int sort(int*values,int len)
{
	int i,j;
	int tmp;
	for(i=1;i<len;++i)
	{
		for(j=0;j<i;++j)
		{
			if(values[j]>values[i])
			{
				tmp = values[j];
				values[j] = values[i];
				values[i] = tmp;
			}
		}
	}
}

int find_seri_len(int *values,int len,int first,int second)
{
	int i,j,k;
	int head;
	int step;
	head=values[first];
	step=values[second]-values[first];
	printf("1:%d\t",values[first]);
	printf("2:%d\t",values[second]);
	int cnt=2;
	for(i = second+1;i < len ;i++)
	{
		if(values[i] == (head + cnt * step))
		{
			printf("%d:%d\t",i,values[i]);
			cnt++;
		}
	}
	printf(";cnt=%d\n",cnt);
	return cnt;

}

int find_max_seri(int*values,int len)
{
	int max_seri_len=0;
	int curr_seri_len=0;
	int i,j,k;
	for(i=0;i<len-1;++i)
	{
		for(j=i+1;j<len;++j)
		{
			curr_seri_len = find_seri_len(values,len,i,j);
			if(curr_seri_len> max_seri_len)
			{
				max_seri_len = curr_seri_len ;
			}
		}
	}
	printf("max_seri_len=%d\n",max_seri_len);
	return max_seri_len ;
}

int main()
{
	int i;
	int valuesA[10]={4,2,43,22,11,33,2,5,3,2};
	sort(valuesA,10);
	find_max_seri(valuesA,10);
	int valuesB[7]={3,8,4,5,6,2,2};
	sort(valuesB,7);
	find_max_seri(valuesB,7);
	int valuesC[4]={-1,-5,1,3};
	sort(valuesC,4);
	find_max_seri(valuesC,4);
	int valuesD[4]={-10,-20,-10,-10};
	sort(valuesD,4);
	find_max_seri(valuesD,4);
}


