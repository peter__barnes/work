#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

class Animal {
 public:
  virtual void eat(string food) {
      cout << "Eats " << food << "\n";
  }
};
 

class Mammal : public Animal {
 public:
  virtual void breathe() {}
};
 
class WingedAnimal : public Animal {
 public:
  virtual void flap() {}
};
 

class Bat : public Mammal, public WingedAnimal {
};

int main(void)
{
    Bat b;
    char fruit[256];
    
    while(cin >> fruit) 
    {
//       b.eat(fruit);
        (Animal(Mammal(b))).eat(fruit);
    }
    return 0;
}

/*
Problem Definition :
Modify one line of code at function testBat() to make it compile successfully.

 
Input Format: A single line with specifies fruits a bat can eat.
Example input: banana apple chico

Output would be:

Eats banana

Eats apple

Eats chico

 
Code Restrictions

You have to make modifications only at the classes implementation and/or definition. 
*/
