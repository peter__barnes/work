#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>
long long Atoi(const char *nptr)
{
	return strtoll(nptr, (char **)NULL, 10); 
}

long long value_by_pos(long long one,long long two,long long pos) 
{
	long long i;
	long long value;
	if(0==pos)
	{
		return one;
	}
	else if(1==pos)
	{
		return two;
	}
	for(i=0;i<pos-1;i++)
	{
		value = llabs((one-two));
		one = two;
		two = value;
		//printf("%lld,",value);
	}
//	printf("\n");
	return value;
}

void getElements(char first[19], char second[19], 
	int ele_num,char indices[ele_num][19], char ret[ele_num][19])
{
	long long one=Atoi(first);
	long long two=Atoi(second);
	int i;
	for(i=0;i<ele_num;i++)
	{
		long long value = value_by_pos(one,two,Atoi(indices[i]));
		sprintf(ret[i],"%lld",value);
//		printf("%s,",ret[i]);
	}
}

void print_ele(int ele_num, char data[ele_num][19])
{
	int i;
	for(i=0;i<ele_num;i++)
	{
		printf("%s,",data[i]);
	}
	printf("\n");
}

int main()
{
	char ret[50][19];

	/*
	char first[19]="13";
	char second[19]="31";
	int ele_num=3;
	char indices[50][19]={"3","4","5"};
	getElements(first,second,ele_num,indices,ret);
	print_ele(ele_num, ret);
	*/
	/*
	char first[19]="21";
	char second[19]="12";
	int ele_num=5;
	char indices[50][19]={"0", "1", "2", "3", "4"};
	getElements(first,second,ele_num,indices,ret);
	print_ele(ele_num, ret);
	*/
	/*
	char first[19]="0";
	char second[19]="0";
	int ele_num=1;
	char indices[50][19]={"1000000000000000000"};
	getElements(first,second,ele_num,indices,ret);
	print_ele(ele_num, ret);
	*/
	/*
	char first[19]="823";
	char second[19]="470";
	int ele_num=10;
	char indices[50][19]={"3","1","31","0","8","29","57","75","8","77"};
	getElements(first,second,ele_num,indices,ret);
	print_ele(ele_num, ret);
	*/
	char first[19]="710370";
	char second[19]="177300";
	int ele_num=10;
	char indices[50][19]={"5","95","164721","418","3387","710","0","1197","19507","5848"};
	getElements(first,second,ele_num,indices,ret);
	print_ele(ele_num, ret);

	//Returns: {"178470", "108270", "90", "0", "90", "90", "710370", "90", "0", "0" }
	return 0;
}
/*
Problem Statement

Let's consider an infinite sequence S of non-negative integers defined as follows:

S0 = first;
S1 = second;
Si = |Si-2 - Si-1| for all i >= 2.

You will be given s first and second, representing the 0-th and the 1-st elements of the sequence S, and a indices, each element of which represents a non-negative integer without extra leading zeros. Return a containing as many elements as indices, where the i-th element is equal to the indices[i]-th element of the sequence S (index is 0-based). No element of the return should contain extra leading zeros.
Definition
Class:
AbsSequence
Method:
getElements
Parameters:
String, String, String[]
Returns:
String[]
Method signature:
String[] getElements(String first, String second, String[] indices)
(be sure your method is public)
Limits
Time limit (s):
840.000
Memory limit (MB):
64
Constraints
- first will represent an integer between 0 and 10^18, inclusive, with no extra leading zeros.
- second will represent an integer between 0 and 10^18, inclusive, with no extra leading zeros.
- indices will contain between 1 and 50 elements, inclusive.
- Each element of indices will represent an integer between 0 and 10^18, inclusive, with no extra leading zeros.
Examples
0)
"21"
"12"
{"0", "1", "2", "3", "4"}
Returns: {"21", "12", "9", "3", "6" }
Here S0=21 and S1=12. The next three sequence elements are S2 = |21 - 12| = 9, S3 = |12 - 9| = 3 and S4 = |9 - 3| = 6.
1)
"0"
"0"
{"1000000000000000000"}
Returns: {"0" }
Here we get the sequence consisting of only zeros.
2)
"823"
"470"
{"3","1","31","0","8","29","57","75","8","77"}
Returns: {"117", "470", "2", "823", "115", "87", "49", "25", "115", "23" }
3)
"710370"
"177300"
{"5","95","164721","418","3387","710","0","1197","19507","5848"}
Returns: {"178470", "108270", "90", "0", "90", "90", "710370", "90", "0", "0" }
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. 
*/
