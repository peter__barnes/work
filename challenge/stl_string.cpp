//请用c++ 实现stl中的string类，
//实现构造，拷贝构造，析构，赋值，比较，字符串相加，
//获取长度及子串等功能。
#include<cstring>
#include<iostream>
class mystring
{
	private:
	char *str;
	public:
	mystring();
	mystring(const char *str_in);
	mystring(const mystring &str_in);
	virtual ~mystring();
	mystring& operator=(const mystring &str_in);
	mystring operator+(mystring &str_in);
	const char *c_str() const{return str;}; 
	int getlen();
};

mystring::mystring()
{
		str=NULL;
}

mystring::mystring(const char *str_in)
{
	if(NULL != str_in)
	{
		str=(char *)malloc(strlen(str_in)+1);
		strcpy(str,str_in);	
	}
	else
	{
		str=NULL;
	}
}

mystring::mystring(const mystring &str_in)
{
	str=(char *)malloc(strlen(str_in.str)+1);
	if(NULL != str)
	{
		strcpy(str,str_in.str);	
	}
}

mystring &mystring::operator=(const mystring &str_in)
{
	str=(char *)malloc(strlen(str_in.c_str())+1);
	if(NULL != str)
	{
		strcpy(str,str_in.c_str());	
	}
	return *this;
}

mystring mystring::operator+(mystring &str_in)
{
	int i=strlen(str)+strlen(str_in.c_str())+1;
	char *str_tmp=(char *)malloc(strlen(str)+strlen(str_in.c_str())+1);
	if(NULL != str_tmp)
	{
		strcpy(str_tmp,str);
		strcpy(&str_tmp[strlen(str)],str_in.c_str());	
	}
	mystring str_new(str_tmp) ;
	free(str_tmp);
	return str_new;
}

mystring::~mystring()
{
	free(str);
}

int mystring::getlen()
{
	if(NULL==str)
	{
		return 0;
	}
	return strlen(str);
}

using namespace std;
int main()
{
	mystring *str1=new mystring("123");
	mystring str2("321");
	mystring str3 = str2;
	mystring str4=str3+(*str1)+str2;
	mystring str5=(str4=str3+(*str1)+str2);
	cout<<str1->c_str()<<endl;
	cout<<str2.c_str()<<endl;
	cout<<str3.c_str()<<endl;
	cout<<str4.c_str()<<endl;
	cout<<str5.c_str()<<endl;
}
