/*
Problem definition:
You must implement function print_vector_in_reverse_order(const std::vector& arr) which prints given array to screen in reverse order. Elements of array must be separated by space. You must also print space after last element of array.
Input format
One line with elements of array which will be separated by space.
Output format
One line with elements of array

Code restrictions
You must use iterators to access array element. You must NOT use indices for access elements of array. You must NOT use temporary vector. You must NOT use any STL algorithms.
*/
#include <iostream>
#include <vector>
#include<stdio.h>
using namespace std;
void print_vector_in_reverse_order(vector<int>& arr)
{
    // Please, implement this function
    int size = arr.size();
    int i;
    vector<int>::iterator it = arr.begin();
    for(i=size-1;i>=0;i--)
    {
        printf("%d ",*(it+i));
    }
    printf("\n");
    //printf("%d",size);
}
int main()
{
    int arr[10]={1,2,3,4,5,6,7,8,9,10};
    vector<int> rit(arr,arr+sizeof(arr)/sizeof(arr[0]));
    print_vector_in_reverse_order(rit);
    return 0;
}
