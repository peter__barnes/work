#include <iostream>

using namespace std;

int gcd(int m, int n)
{
    //implement this method
    int c;
    while ( m != 0  ) {
        c = m; m = n%m;  n = c;
    }
    return n;
}
/* Recursive Standard C Function: Greatest Common Divisor */
int gcdr ( int a, int b  )
{
    if ( a==0  ) 
    {
        return b;
    }
    return gcdr ( b%a, a  );
}


int main()
{
    int m, n;
    cin >> m >> n;
    cout <<endl<< gcd(m, n)<<endl;
    return 0;    
}
