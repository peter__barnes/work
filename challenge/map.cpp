#include<iostream>
#include<map>
using namespace std;
template <typename T>
map<int ,int > the_map;  
void read_map(T& m)
{
	cin.ignore(100, '(');
	while (true)
	{
		int n;
		cin >> n;
		if (!cin)
		{
			cin.clear();
			cin.ignore(100, ')');
			return;
		}
		read_map(m[n]);
	}
}

template <typename T>
void print_map(T& m)
{
	if (m.empty())
	{
		cout << "()";
		return;
	}
	cout << "(";
	for (typename T::iterator i = m.begin(); i != m.end(); ++i)
	{
		cout << ' ' << i->first;
		print_map(i->second);
	}
	cout << " )";
}

int main()
{
	read_map(the_map);
	print_map(the_map);
	return 0;
}
