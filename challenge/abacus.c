#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>
void print_abacus(char original[6][13])
{
	int i;
	for(i=0;i<6;i++)
	{
		printf("%s,",original[i]);
	}
	printf("\n");
}

void val_to_abacus(char original[6][13],int val)
{
	int i;
	int j;
	int digit=0;
	int remain=0;
	for(i=0;i<6;i++)
	{
		char tmp[13]="oooooooooooo";
		//printf("%s\n",tmp);
		remain = val/10;
		digit = val - 10*remain;
		val=remain;
		//printf("%d,%d\n",remain,digit);
		tmp[11-digit]='-';
		tmp[10-digit]='-';
		tmp[9-digit]='-';
		//printf("%s\n",tmp);
		strcpy(original[6-1-i],tmp);
	}
}

int abacus_to_val(char original[6][13])
{
	int i;
	int j;
	int val=0;
	for(i=0;i<6;i++)
	{
		for(j=0;j<9;j++)
		{
			if('-'==original[i][j])
			{
				break;
			}
		}
		val=val*10+(9-j);
	}
	return val;
}

void add(char original[6][13], int val)
{
	int ori=abacus_to_val(original);
	int ret=ori+val;
	val_to_abacus(original,ret);
}

int main()
{

	/*
	int val = 5;
	char ori[6][13]={"oooooooo---o","ooooooooo---","ooooooooo---","ooooooooo---","ooooooooo---","oooooooo---o"};
	add(ori,val);
	print_abacus(ori);
	*/
	/*
	int val=5;
	char ori[6][13]= {"ooo---oooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "oo---ooooooo", "---ooooooooo"};
	add(ori,val);
	print_abacus(ori);
	*/
	/*
	int val=21;
	char ori[6][13]= {"ooo---oooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "oo---ooooooo", "---ooooooooo"};
	add(ori,val);
	print_abacus(ori);
	*/
	/*
	int val=100000;
	char ori[6][13]= {"ooooooooo---", "---ooooooooo", "ooooooooo---", "---ooooooooo", "oo---ooooooo", "---ooooooooo"};
	add(ori,val);
	print_abacus(ori);
	*/
	int val=1;
	char ori[6][13]= {"o---oooooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo" };
	add(ori,val);
	print_abacus(ori);

//printf("%d\n",abacus_to_val(ori));
//	val_to_abacus(ori,100001);
	return 0;
}
/*
Problem Statement
An abacus can be used to do arithmetic. The version that we have has 6 horizontal threads, each with nine beads on it. The beads on each thread are always arranged with just one gap, possibly at one of the ends. However many beads are adjacent and at the right end of the thread is the digit value of the thread. The value on the abacus is read by taking the digits in order from top thread to bottom thread and arranging them from left to right (so the top thread is the one that contains the most significant digit).

Create a class Abacus that contains a method add that is given a String[] original and a number val and that returns a String[] showing the abacus after val has been added to the original abacus.

Both in original and in the return, the String[] will contain exactly 6 elements representing the 6 threads in order from top thread to bottom thread. Each element will contain a lowercase 'o' to represent each bead and three consecutive hyphens '-' to indicate the empty part of the thread. Each element will thus contain exactly 12 characters.
Definition
Class:
Abacus
Method:
add
Parameters:
String[], int
Returns:
String[]
Method signature:
String[] add(String[] original, int val)
(be sure your method is public)
Limits
Time limit (s):
840.000
Memory limit (MB):
64
Constraints
- original will contain exactly 6 elements.
- Each element of original will contain exactly 12 characters, 9 lowercase 'o's and 3 consecutive '-'s.
- val will be between 0 and 999,999 inclusive.
- val added to the original abacus will result in a value that can be shown on the abacus.
Examples
0)
{"ooo---oooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "oo---ooooooo", "---ooooooooo"}
5
Returns: {"ooo---oooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "o---oooooooo", "ooooo---oooo" }
When we add 5 to the original, it is necessary to "carry" 1 to the next thread up. This shows the arithmetic 699979 + 5 = 699984
1)
{"ooo---oooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "oo---ooooooo", "---ooooooooo"}
21
Returns: {"oo---ooooooo", "ooooooooo---", "ooooooooo---", "ooooooooo---", "ooooooooo---", "ooooooooo---" }
This shows 699979 + 21 = 700000
2)
{"ooooooooo---", "---ooooooooo", "ooooooooo---", "---ooooooooo", "oo---ooooooo", "---ooooooooo"}
100000
Returns: {"oooooooo---o", "---ooooooooo", "ooooooooo---", "---ooooooooo", "oo---ooooooo", "---ooooooooo" }
3)
{"o---oooooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo", "---ooooooooo" }
1
Returns: {"---ooooooooo", "ooooooooo---", "ooooooooo---", "ooooooooo---", "ooooooooo---", "ooooooooo---" }
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved.
 
*/
