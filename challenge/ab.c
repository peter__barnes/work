/*
Problem Statement

You are given two s: N and K. Lun the dog is interested in strings that satisfy the following conditions:

    The string has exactly N characters, each of which is either 'A' or 'B'.
    The string s has exactly K pairs (i, j) (0 <= i < j <= N-1) such that s[i] = 'A' and s[j] = 'B'.

If there exists a string that satisfies the conditions, find and return any such string. Otherwise, return an empty string.
Definition
Class: AB
Method: createString
Parameters: int, int
Returns: String
Method signature: String createString(int N, int K)
(be sure your method is public)
Limits
Time limit (s): 2.000
Memory limit (MB): 256

Constraints
- N will be between 2 and 50, inclusive.
- K will be between 0 and N(N-1)/2, inclusive.
Examples
0)
3
2
Returns: "ABB"
This string has exactly two pairs (i, j) mentioned in the statement: (0, 1) and (0, 2).
1)
2
0
Returns: "BA"
Please note that there are valid test cases with K = 0.
2)
5
8
Returns: ""
Five characters is too short for this value of K.
3)
10
12
Returns: "BAABBABAAB"
Please note that this is an example of a solution; other valid solutions will also be accepted.
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. 
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void createString(int n, int k,char *buff)
{
	int i;
	int last_mark;
//	int mid_mark;
	int remain;
	buff[n]='\0';
	if(0==n%2 )
	{
		if(k<0 || k > (n*n/4))
		{
			printf("can't find\n");
			buff[0]='\0';
			return ;
		}
		last_mark = n-1;
		remain = k;
		for(; remain > n/2; --last_mark)
		{
			buff[last_mark]='B';
			remain = remain - n/2;
		}
		for(i=0;i<=last_mark;i++)
		{
			if(i==(last_mark -n/2 + remain))
			{
				buff[i]='B';
			}
			else
			{
				buff[i]='A';
			}
		}
		memset(buff,'B',n/2 - (n - 1 - last_mark +1));
	}
	else
	{
		if(k<0 || k > ((n+1)*(n-1)/4))
		{
			printf("can't find\n");
			buff[0]='\0';
			return ;
		}
		last_mark = n-1;
		remain = k;
		for(; remain > (n+1)/2; --last_mark)
		{
			buff[last_mark]='B';
			remain = remain - (n+1)/2;
		}
		for(i=0;i<=last_mark;i++)
		{
			if(i==(last_mark -(n+1)/2 + remain))
			{
				buff[i]='B';
			}
			else
			{
				buff[i]='A';
			}
		}
		memset(buff,'B',(n-1)/2 - (n - 1 - last_mark +1));
	}
}

int main()
{
	int n;
	int k;
	printf("please input N and K\n");
	scanf("%d %d",&n,&k) ;
	if(n<=0)
	{
		printf("can't find\n");
	}
	char buff[n+1];
	createString(n,k,buff);
	printf("resault:%s.\n",buff);
}
