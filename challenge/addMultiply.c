#include <stdio.h>
#include <stdlib.h>
#include<string.h>

void addMultiply(int y)
{
	int x0,x1,x2;
	x0=-1;
	x1=2;
	x2=y+2;
	printf("if y = %d.\tx0=%d,x1=%d,x2=%d\n",y,x0,x1,x2);
}

int main()
{
	addMultiply(6);
	addMultiply(11);
	addMultiply(0);
	addMultiply(500);
	addMultiply(2);
	addMultiply(5);
	return 0;
}
/*
Problem Statement
You are given an y. We are looking for any x that satisfies the following constraints:

    x has exactly three elements
    ( x[0] * x[1] ) + x[2] = y
    Each x[i] must be between -1000 and 1000, inclusive.
    No x[i] can be equal to 0 or 1.

Find and return one such x.

If there are multiple valid solutions, you may return any of them. You may assume that for our constraints on y (specified below) at least one valid x always exists.
Definition
Class:
AddMultiply
Method:
makeExpression
Parameters:
int
Returns:
int[]
Method signature:
int[] makeExpression(int y)
(be sure your method is public)
Limits
Time limit (s):
2.000
Memory limit (MB):
256
Constraints
- y will be between 0 and 500, inclusive.
Examples
0)
6
Returns: {2, 2, 2 }
2*2 + 2 = 6

Note that this is one of many possible solutions. Another solution is:

3*3 + (-3) = 6
1)
11
Returns: {2, 3, 5 }
2)
0
Returns: {7, 10, -70 }
Note that 0 and 1 are not allowed, thus a result like 0 * 0 + 0 would be incorrect.
3)
500
Returns: {-400, -3, -700 }
Some or all of the returned numbers may be negative.
4)
2
Returns: {2, 2, -2 }
5)
5
Returns: {5, 2, -5 }
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved. 
*/
