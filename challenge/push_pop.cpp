#include <iostream>
#include <vector>

using namespace std;

class Queue
{
public:

    Queue()
        : myCont() {};

    int pop()
    {
        // implement this method
        int ret = myCont[0];
        myCont.erase(myCont.begin());
        return ret;
    }
    
    void push(int& value)
    {
        // implement this method
        myCont.push_back(value);
    }
    
    bool empty()
    {
        return myCont.size() == 0;
    }
    
private:
    vector<int> myCont;
};

int main()
{
    Queue queue;
    int val;
    while(cin >> val)
    {
        queue.push(val);
    }
    cout << endl;
    while(!queue.empty())
    {
        cout << queue.pop() <<" ";
    }
    cout << endl;
    return 0;
}

