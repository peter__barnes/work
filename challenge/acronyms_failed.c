#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>
#include <ctype.h>

void parser(char line[2500])
{
	int i;
	int j=0;
	int k=0;
	char seq[2500];
	char first_word[500];
	int first_word_end=0;
	int stop=0;

	bool fst_word_start_upper = false;
	bool lst_word_start_upper = false;
	bool adjecent_start_lower = false;
	int word_cnt=0;
	for(i=0;'\0'!=line[i];i++)
	{
		if(line[i]>='A' && line[i]<='Z')
		{
			sprintf(&seq[j],"%c",line[i]);
			j++;
		}
		if(0 == first_word_end)
		{
			if(' '==line[i] )
			{
				if(k!=0)
				{
					first_word[k]='\0';
					first_word_end=1;
					word_cnt++;
					k=0;
				//	printf("first_word_end:%s\n",first_word);
				}
			}
			else 
			{
				if(0==k)
				{
					if(line[i]>='A' && line[i]<='Z' )
					{
						fst_word_start_upper = true;
					}
				}
				first_word[k]=line[i];
				k++;
			}
		}
		else
		{
			if(' '==line[i] )
			{
				if(k!=0)
				{
					//lst_word_start_upper = false;
					k=0;
					word_cnt++;
					//first_word[k]='\0';
					//first_word_end=1;
				//	printf("first_word_end:%s\n",first_word);
				}
			}
			else 
			{
				if(0==k)
				{
					if(line[i]>='A' && line[i]<='Z' )
					{
						lst_word_start_upper = true;
					}
					else 
					{
						if(word_cnt>1 && false == lst_word_start_upper)
						{
							adjecent_start_lower = true;
						}
						lst_word_start_upper = false;
					}
				}
				//first_word[k]=line[i];
				k++;
			}

		}
		if('.'==line[i])
		{
			stop=1;
		}
	}
	//printf("\nfst_word_start_upper:%d\n",fst_word_start_upper );
	//printf("\nlst_word_start_upper:%d\n",lst_word_start_upper );
	//printf("\nadjecent_start_lower :%d\n",adjecent_start_lower );
	seq[j]='\0';
//	printf("seq:%s\n",seq);
	int seq_len=strlen(seq);

	int first_word_len=strlen(first_word);
	int inc_fst_word=0;
	for(i=0;i<first_word_len;i++)
	{
		if(0!=islower(first_word[i]))
		{
			first_word[i]-=32;
		}
	}
//	printf("first_word:%s\n",seq);
	for(i=0;i<seq_len-first_word_len+1;i++)
	{
		if(0==strncmp(first_word,&seq[i],first_word_len))
		{
			inc_fst_word=1;
		}
	}
	if(1==inc_fst_word || false == lst_word_start_upper || false == fst_word_start_upper 
		|| adjecent_start_lower)
	{
		printf("%s",line);
		//printf("1111111111111111\n");
	}
	else
	{
		printf("%s",seq);
	}

	if(1==stop)
	{
		printf(".");
	}
	//printf("\n");
}

void acronize(int ele_num,char document[ele_num][50])
{
	int i;
	int j;
	int k;
	char doc[2500];
	char line[2500];
	for(i=0,k=0;i<ele_num;i++)
	{
		int len = strlen(document[i]);
		for(j=0;j<len;j++)
		{
			//sprintf(&doc[k],"%c",document[i][j]);
			doc[k]=document[i][j];
			k++;
		}
		sprintf(&doc[k]," ");
		k++;
	}
	doc[k]='\0';
//	printf("[doc]%s\n",doc);
	int doc_len=strlen(doc);
	for(i=0,j=0;i<doc_len;i++)
	{
		if('\0' == doc[i+1] )
		{
			line[j]=doc[i];
			j++;
			line[j]='\0';
			//printf("%s\n",line);
			j=0;
			parser(line);
		}
		else if(' ' == doc[i] && ' ' == doc[i+1])
		{
			line[j]='\0';
			//printf("%s\n",line);
			i++;
			j=0;
			parser(line);
		}
		else
		{
			line[j]=doc[i];
			j++;
		}
	}
	printf("\n");
}

int main()
{
	/*
	char document[2][50]={"TopCoder, Inc. ","TCi coder bb Inc"};
	acronize(2,document);
	*/
	char document[1][50]={"We the people of the United States of America."};
	acronize(1,document);
}

