/*
Problem definition
Insert given integer on end of linked list
Input format :Integer that need to be inserted in linked list.
Output format: Print out list on standard output.
*/
#include <iostream>
#include <stdlib.h>
using namespace std;

class LinkedList;
struct Node
{
    Node()
        : value(), next(NULL) {};
    int value;
    Node* next;
};
class LinkedList
{
private:
    Node* head ;
public:
    LinkedList()
        : head(NULL) {};
    void insertLast(int value)
    {
        // implement this method
        Node* n = head;
//        Node *nnode = (Node *)malloc(sizeof(Node));
        Node *nnode =new Node;
        nnode->value = value;
        if(head == NULL)
        {
            head = nnode;
            return ;
        }
        while(n->next != NULL)
        {
            n = n->next;
        }
        n->next = nnode;
        return ;
    }
    
    friend ostream& operator<<(ostream& oss, const LinkedList& ll);
    
    Node* getHead() const
    {
        return head;
    }
};

ostream& operator<<(ostream& oss, const LinkedList& ll)
{
    Node* n = ll.getHead();
    while(n != '\0')
    {
        cout << n->value << " ";
        n = n->next;
    }
    cout<<endl;
}

int main()
{
    int val;
    LinkedList ll;
    while( cin >> val )
    {
        ll.insertLast(val);
    }
    cout <<endl<<"print list:"<<endl;
    cout << ll;
    return 0;
}
