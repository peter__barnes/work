import java.io.*;

class test {

    public static int count_chars(String s, char c) {
        // implement this function
        int cnt = 0;
        char str[];
        int i;
        str = s.toCharArray();
        s.length();
        for(i=0;i<s.length();i++)
        {
            if(str[i]==c)
            {
                cnt ++;
            }
        }
        if(cnt == 0)
        {
            return -1;
        }
        return cnt;

    }

    public static void main(String args[]) {
        String input = null;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine();
        } catch(Exception e) {
        }
        if (null == input) input = "";
        int result = count_chars(input, 'a');
        System.out.println(result);
    }
}
