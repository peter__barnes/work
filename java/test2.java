import java.util.*;

public class test2 
{
    static class A
    {
        int x = -2;
        void f(int x)
        {
            this.x = x;
        }
        int getX1()
        {
            return x;
        }
    }
    static class B extends A
    {
        float x = -1.0f;
        void f(int x)
        {
            this.x = x;
        }
        int getX2()
        {
            return (int)x*2;
        }
    }
    
    public static void main(String[] args) 
    {
        String str;
        Scanner in = new Scanner(System.in);
        str = in.nextLine();
        int x = Integer.parseInt(str);
        B b = new B();
        b.f(x);
        System.out.println(b.getX1() + " " + b.getX2());
    }
} 





